FROM openjdk:17-alpine
EXPOSE 7788
ADD target/outsource.jar outsource.jar
ENTRYPOINT ["java","-jar","outsource.jar"]
