package com.smart_solution.outsource;

import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.role.Role;
import com.smart_solution.outsource.role.RoleRepository;
import com.smart_solution.outsource.role.RoleType;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategoryRepository;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNorm;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNormRepository;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

@SpringBootApplication
public class OutsourApplication implements CommandLineRunner {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private SanpinAgeNormRepository sanpinAgeNormRepository;

    @Autowired
    private SanpinCategoryRepository sanpinCategoryRepository;


    public static void main(String[] args) {

        TimeZone.setDefault(TimeZone.getTimeZone("GMT+5:00"));
        SpringApplication.run(OutsourApplication.class, args);
        System.out.println(UUID.randomUUID());
    }

    @Override
    public void run(String... args) throws Exception {
//        insertAdmin();
    }

    public void insertAdmin() {

        List<SanpinAgeNorm> list = new ArrayList<>();

        for (SanpinCategory sanpinCategory : sanpinCategoryRepository.findAll()) {
            for (SanpinAgeNorm sanpinAgeNorm : sanpinCategory.getSanpinAgeNorms()) {
                sanpinAgeNorm.setSanpinCategory(sanpinCategory);
                list.add(sanpinAgeNorm);
            }
        }

        sanpinAgeNormRepository.saveAll(list);
    }

    public void insertRole() {

        roleRepository.save(new Role(RoleType.ADMIN.getName()));

        roleRepository.save(new Role(RoleType.CHIEF.getName()));

        roleRepository.save(new Role(RoleType.TECHNOLOGIST.getName()));

        roleRepository.save(new Role(RoleType.STOREKEEPER.getName()));

        roleRepository.save(new Role(RoleType.SUPPLIER.getName()));

        roleRepository.save(new Role(RoleType.KINDERGARDEN_PRINCIPAL.getName()));

        roleRepository.save(new Role(RoleType.NURSE.getName()));

        roleRepository.save(new Role(RoleType.COOK.getName()));

        roleRepository.save(new Role(RoleType.ACCOUNTANT.getName()));
    }

    public void inserftRole() {
        if (roleRepository.existsByName(RoleType.ADMIN.getName())) {
            roleRepository.save(new Role(RoleType.ADMIN.getName()));
        }
        if (roleRepository.existsByName(RoleType.CHIEF.getName()))
            roleRepository.save(new Role(RoleType.CHIEF.getName()));
        if (roleRepository.existsByName(RoleType.TECHNOLOGIST.getName()))
            roleRepository.save(new Role(RoleType.TECHNOLOGIST.getName()));
        if (roleRepository.existsByName(RoleType.STOREKEEPER.getName()))
            roleRepository.save(new Role(RoleType.STOREKEEPER.getName()));
        if (roleRepository.existsByName(RoleType.SUPPLIER.getName()))
            roleRepository.save(new Role(RoleType.SUPPLIER.getName()));
        if (roleRepository.existsByName(RoleType.KINDERGARDEN_PRINCIPAL.getName()))
            roleRepository.save(new Role(RoleType.KINDERGARDEN_PRINCIPAL.getName()));
        if (roleRepository.existsByName(RoleType.NURSE.getName()))
            roleRepository.save(new Role(RoleType.NURSE.getName()));
        if (roleRepository.existsByName(RoleType.COOK.getName()))
            roleRepository.save(new Role(RoleType.COOK.getName()));
        if (roleRepository.existsByName(RoleType.ACCOUNTANT.getName()))
            roleRepository.save(new Role(RoleType.ACCOUNTANT.getName()));
    }
}
