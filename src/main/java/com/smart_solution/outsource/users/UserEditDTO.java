package com.smart_solution.outsource.users;


import lombok.Getter;
import lombok.Setter;

public class UserEditDTO {

    private String username;

    private String oldPassword;

    private String newPassword;


    public UserEditDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
