package com.smart_solution.outsource.users;

import com.smart_solution.outsource.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


public interface UsersRepository extends JpaRepository<Users, Integer> {

    Optional<Users> findByUserName(String login);

    boolean existsByUserName(String name);

    List<Users> findAllByCompany(Company company);
}
