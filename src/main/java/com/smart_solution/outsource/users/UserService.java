package com.smart_solution.outsource.users;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.consumption.Consumption;
import com.smart_solution.outsource.consumption.ConsumptionRepository;
import com.smart_solution.outsource.consumption.number.ConsumptionByNumberOfChildren;
import com.smart_solution.outsource.consumption.number.ConsumptionByNumberOfChildrenRepository;
import com.smart_solution.outsource.dto.BooleanDto;
import com.smart_solution.outsource.inOutProduct.InOutProductRepository;
import com.smart_solution.outsource.ingredient.IngredientRepository;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.role.RoleRepository;
import com.smart_solution.outsource.role.RoleType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public record UserService(
        UsersRepository usersRepository,
        ApplicationUsernamePasswordAuthenticationFilter authenticationFilter,
        PasswordEncoder passwordEncoder,
        RoleRepository roleRepository,
        KindergartenRepository kindergartenRepository,
        IngredientRepository ingredientRepository,
        InOutProductRepository inOutProductRepository,
        AuthenticationManager authenticationManager,
        ConsumptionRepository consumptionRepository,
        ConsumptionByNumberOfChildrenRepository consumptionByNumberOfChildrenRepository
) implements UsersParseDTO {


    public ResponseUser signIn(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        adsadko();
        ResponseUser responseUser = authenticationFilter.successfulAuthentication(request, response);
        if (responseUser.getSuccess()) {
            Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
            responseUser.setSurname(users.getSurname() != null ? users.getSurname() : null);
            responseUser.setName(users.getName() != null ? users.getName() : null);
        }
        return responseUser;
    }

    public void adsadko() {

        List<ConsumptionByNumberOfChildren> list = new ArrayList<>();


        for (Consumption consumption : consumptionRepository.findAll()) {
            for (ConsumptionByNumberOfChildren consumptionByNumberOfChildren : consumption.getNumberOfChildrenList()) {
                consumptionByNumberOfChildren.setConsumptionId(consumption.getId());
                list.add(consumptionByNumberOfChildren);
            }
        }

        consumptionByNumberOfChildrenRepository.saveAll(list);
    }


    public StateMessage add(UsersDTO usersDTO, ResponseUser responseUser) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        boolean res = usersRepository.existsByUserName(usersDTO.getUserName());

        if (!res) {
            Users saveUser = usersRepository.save(new Users(
                    usersDTO.getName(),
                    usersDTO.getFatherName(),
                    usersDTO.getSurname(),
                    usersDTO.getUserName(),
                    passwordEncoder.encode(usersDTO.getPassword()),
                    usersDTO.getPhoneNumber(),
                    roleRepository.findById(usersDTO.getRoleId()).get(),
                    usersDTO.getKindergartenId() == null ? company : null,
                    true
            ));

            if (usersDTO.getKindergartenId() != null) {
                Kindergarten kindergarten = kindergartenRepository.findById(usersDTO.getKindergartenId()).get();
                List<Users> list = kindergarten.getUsers();
                list.add(saveUser);
                kindergarten.setUsers(list);
                Kindergarten save = kindergartenRepository.save(kindergarten);
                saveUser.setKindergartenId(save.getId());
                usersRepository.save(saveUser);
            }

            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
        } else {
            return new StateMessage("Bunday username avval ro`yxatdan o`tgan iltimos boshqa kiriting", false);
        }
    }

    public List<UsersResponseDTO> get(Company company) {

        List<UsersResponseDTO> list = new ArrayList<>();

        for (Users users : usersRepository.findAllByCompany(company)) {

            list.add(parse(users));

        }
        return list;
    }

    @Override
    public UsersResponseDTO parse(Users users) {
        return UsersParseDTO.super.parse(users);
    }

    public StateMessage edit(UsersDTO usersDTO, ResponseUser responseUser, Integer id) {

        Users users = usersRepository.findById(id).get();

        users.setPhoneNumber(usersDTO.getPhoneNumber());
        users.setName(usersDTO.getName());
        users.setFatherName(usersDTO.getFatherName());
        users.setSurname(usersDTO.getSurname());
        users.setRole(roleRepository.findById(usersDTO.getRoleId()).get());

        usersRepository.save(users);

        return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

    }

    public UsersResponse get(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        UsersResponse dto = new UsersResponse();

        dto.setName(users.getName());
        dto.setId(users.getId());
        dto.setFatherName(users.getFatherName());
        dto.setSurname(users.getSurname());
        dto.setPhoneNumber(users.getPhoneNumber());

        if (users.getCompany() != null) {
            dto.setRole(users.getCompany().getName() + "da " + users.getRole().getName().substring(5));
        } else if (users.getKindergartenId() != null) {

            Kindergarten kindergarten = kindergartenRepository.findById(users.getKindergartenId()).get();
            dto.setRole(kindergarten.getName() + "da " + users.getRole().getName().substring(5));

        } else {
            dto.setRole(users.getRole().getName().substring(5));
        }

        dto.setStatus(users.isState() ? "AKTIV" : "AKTIVLASHTIRILMAGAN");

        dto.setUserName(users.getUsername());

        return dto;
    }

    public StateMessage edit(ResponseUser responseUser, UserEditDTO dto) {

        boolean res = false;

        boolean ans = false;

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        try {

            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(users.getUsername(), dto.getOldPassword()));


//            if (dto.getNewPassword() != null) {
//                matches = passwordEncoder.matches(dto.getNewPassword(), users.getPassword());
//            }


            if (!users.getUsername().equals(dto.getUsername())) {
                res = usersRepository.existsByUserName(dto.getUsername());
            }
            String str = "";


            if (!res) {

                users.setUserName(dto.getUsername());

                if (dto.getNewPassword() != null) {
                    users.setPassword(passwordEncoder.encode(dto.getNewPassword()));
                }

                usersRepository.save(users);
                ans = true;
                str = "Muvaffaqiyatli o`zgartirildi";
            }

            if (res) {
                str = "  Bunday username avval ro`yxatdan o`tgan iltimos boshqa kiriting";
            }

            return new StateMessage(str, ans);

        } catch (Exception e) {
            return new StateMessage("Parol noto`g`ri kiritildi. Iltimos tekshirib qaytadan kiriting.", false);
        }
    }

    public StateMessage restriction(Integer id, BooleanDto dto) {

        Users users = usersRepository.findById(id).get();

        users.setState(dto.isResult());
        usersRepository.save(users);


        return new StateMessage(dto.isResult() ? "Muvaffaqiyatli aktivlashtirildi" : "Muvaffaqiyatli bloklandi", true);
    }

    public StateMessage delete(Integer id) {

        Users users = usersRepository.findById(id).get();

        users.setState(false);
        users.setDelete(true);
        users.setCompany(null);
        usersRepository.save(users);

        return new StateMessage("Muvaffaqiyatli o`chirildi", true);
    }


    public void addUsersKin() {
        for (Kindergarten kindergarten : kindergartenRepository.findAll()) {

            String username = getUsername(kindergarten.getId());
            oshpaz(kindergarten, username);
            hamshira(kindergarten, username);
            mudira(kindergarten, username);
        }
    }

    public void oshpaz(Kindergarten kindergarten, String username) {
        Users user1 = usersRepository.save(new Users(
                "XXX",
                "XXX",
                "XXX",
                username + "oshpaz",
                passwordEncoder.encode(username + "oshpaz"),
                "901234567",
                roleRepository.findByName(RoleType.COOK.getName()).get(),
                null,
                true
        ));

        user1.setKindergartenId(kindergarten.getId());
        Users save1 = usersRepository.save(user1);

        List<Users> list1 = kindergarten.getUsers();
        list1.add(save1);
        kindergarten.setUsers(list1);
        Kindergarten save = kindergartenRepository.save(kindergarten);

    }

    public void hamshira(Kindergarten kindergarten, String username) {
        Users user1 = usersRepository.save(new Users(
                "XXX",
                "XXX",
                "XXX",
                username + "hamshira",
                passwordEncoder.encode(username + "hamshira"),
                "901234567",
                roleRepository.findByName(RoleType.NURSE.getName()).get(),
                null,
                true
        ));

        user1.setKindergartenId(kindergarten.getId());
        Users save1 = usersRepository.save(user1);

        List<Users> list1 = kindergarten.getUsers();
        list1.add(save1);
        kindergarten.setUsers(list1);
        Kindergarten save = kindergartenRepository.save(kindergarten);

    }

    public void mudira(Kindergarten kindergarten, String username) {
        Users user1 = usersRepository.save(new Users(
                "XXX",
                "XXX",
                "XXX",
                username + "mudira",
                passwordEncoder.encode(username + "mudira"),
                "901234567",
                roleRepository.findByName(RoleType.KINDERGARDEN_PRINCIPAL.getName()).get(),
                null,
                true
        ));

        user1.setKindergartenId(kindergarten.getId());
        Users save1 = usersRepository.save(user1);

        List<Users> list1 = kindergarten.getUsers();
        list1.add(save1);
        kindergarten.setUsers(list1);
        Kindergarten save = kindergartenRepository.save(kindergarten);

    }

    public String getUsername(Integer id) {

        if (id == 1) {
            return "b1";
        }
        if (id == 2) {
            return "b2";
        }
        if (id == 3) {
            return "b3";
        }
        if (id == 4) {
            return "b4";
        }
        if (id == 5) {
            return "b6";
        }
        if (id == 6) {
            return "b7";
        }
        if (id == 7) {
            return "b8";
        }
        if (id == 8) {
            return "b9";
        }
        if (id == 9) {
            return "b10";
        }
        if (id == 10) {
            return "b11";
        }
        if (id == 11) {
            return "b14";
        }
        if (id == 12) {
            return "b15";
        }
        if (id == 13) {
            return "b16";
        }
        if (id == 14) {
            return "b17";
        }
        if (id == 15) {
            return "b18";
        }
        if (id == 16) {
            return "b19";
        }
        if (id == 17) {
            return "b20";
        }
        if (id == 18) {
            return "b21";
        }
        if (id == 19) {
            return "b22";
        }
        if (id == 20) {
            return "b23";
        }
        if (id == 21) {
            return "n3";
        }
        if (id == 22) {
            return "n4";
        }
        if (id == 23) {
            return "n7";
        }
        if (id == 24) {
            return "n11";
        }
        if (id == 25) {
            return "n12";
        }
        if (id == 26) {
            return "n14";
        }
        if (id == 27) {
            return "n18";
        }
        if (id == 28) {
            return "n22";
        }
        if (id == 29) {
            return "n26";
        }
        return "";
    }

    public List<UsersResponse> getAll(ResponseUser responseUser) {
        return parseRes(usersRepository.findAll());
    }

    public StateMessage resetPassword(Integer id) {

        Users users = usersRepository.findById(id).get();
        String password = users.getUsername() + users.getUsername();
        users.setPassword(passwordEncoder.encode(password));
        usersRepository.save(users);

        return new StateMessage("Parol muvaffaqiyatli o`zgartirildi.", true);
    }
}