package com.smart_solution.outsource.users;

import java.util.ArrayList;
import java.util.List;

public interface UsersParseDTO {

    default UsersResponseDTO parse(Users users) {

        UsersResponseDTO dto = new UsersResponseDTO();

        dto.setId(users.getId());
        dto.setName(users.getName());
        dto.setSurname(users.getSurname());
        dto.setFatherName(users.getFatherName());
        dto.setPhoneNumber(users.getPhoneNumber());
        dto.setRole(users.getRole().getName());
        dto.setState(users.isState());
        return dto;
    }

    default List<UsersResponseDTO> parse(List<Users> users) {
        List<UsersResponseDTO> list = new ArrayList<>();

        for (Users user : users) {

            UsersResponseDTO dto = new UsersResponseDTO();

            dto.setId(user.getId());
            dto.setName(user.getName());
            dto.setSurname(user.getSurname());
            dto.setFatherName(user.getFatherName());
            dto.setPhoneNumber(user.getPhoneNumber());
            dto.setRole(user.getRole().getName());
            dto.setState(user.isState());
            list.add(dto);
        }
        return list;
    }

    default List<UsersResponse> parseRes(List<Users> users) {
        List<UsersResponse> list = new ArrayList<>();

        for (Users user : users) {

            UsersResponse dto = new UsersResponse();

            dto.setId(user.getId());
            dto.setName(user.getName());
            dto.setSurname(user.getSurname());
            dto.setFatherName(user.getFatherName());
            dto.setPhoneNumber(user.getPhoneNumber());
            dto.setRole(user.getRole().getName());
            dto.setUserName(user.getUserName());
            list.add(dto);
        }
        return list;
    }
}
