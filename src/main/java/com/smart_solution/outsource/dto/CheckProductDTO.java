package com.smart_solution.outsource.dto;

import java.math.BigDecimal;

public class CheckProductDTO {

    private String productName;
    private BigDecimal weight;

    public CheckProductDTO(String productName, BigDecimal weight) {
        this.productName = productName;
        this.weight = weight;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
