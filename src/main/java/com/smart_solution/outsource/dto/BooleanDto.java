package com.smart_solution.outsource.dto;


import lombok.Getter;
import lombok.Setter;


public class BooleanDto {
    private boolean result;
    private String comment;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
