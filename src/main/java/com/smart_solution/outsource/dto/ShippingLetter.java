package com.smart_solution.outsource.dto;

import com.smart_solution.outsource.company.CompanyResponseDTO;
import com.smart_solution.outsource.kindergarten.KindergartenResponseDTO;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompanyDTO;

import java.util.List;

public class ShippingLetter {
    private String agreementNumber;
    private List<ShippedProductsCompanyDTO> productList;
    private KindergartenResponseDTO kindergarten;
    private CompanyResponseDTO company;

    public KindergartenResponseDTO getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(KindergartenResponseDTO kindergarten) {
        this.kindergarten = kindergarten;
    }

    public CompanyResponseDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyResponseDTO company) {
        this.company = company;
    }

    public String getAgreementNumber() {
        return agreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    public List<ShippedProductsCompanyDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ShippedProductsCompanyDTO> productList) {
        this.productList = productList;
    }
}
