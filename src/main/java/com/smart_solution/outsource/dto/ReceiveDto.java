package com.smart_solution.outsource.dto;

public class ReceiveDto {

    private Double weight;
    private Double numberPack;
    private String comment;


    
    
    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(Double numberPack) {
        this.numberPack = numberPack;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
