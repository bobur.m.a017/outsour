package com.smart_solution.outsource.dto;

import java.math.BigDecimal;

public class MenuDTO {

    private String day;
    private String mealTime;
    private String mealName;
    private String ageGroup;
    private String productName;
    private String weight;
    private BigDecimal weightDto;
    private String mealWeight;
    private String withoutExit;


    public MenuDTO() {
    }

    public MenuDTO(String day, String mealTime, String mealName, String ageGroup, String productName, BigDecimal weightDto, String mealWeight) {
        this.day = day;
        this.mealTime = mealTime;
        this.mealName = mealName;
        this.ageGroup = ageGroup;
        this.productName = productName;
        this.weightDto = weightDto;
        this.mealWeight = mealWeight;
    }

    public MenuDTO(String day, String mealTime, String mealName, String ageGroup, String productName, String weight, String mealWeight, String withoutExit) {
        this.day = day;
        this.mealTime = mealTime;
        this.mealName = mealName;
        this.ageGroup = ageGroup;
        this.productName = productName;
        this.weight = weight;
        this.mealWeight = mealWeight;
        this.withoutExit = withoutExit;
    }

    public BigDecimal getWeightDto() {
        return weightDto;
    }

    public void setWeightDto(BigDecimal weightDto) {
        this.weightDto = weightDto;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getMealWeight() {
        return mealWeight;
    }

    public void setMealWeight(String mealWeight) {
        this.mealWeight = mealWeight;
    }

    public String getWithoutExit() {
        return withoutExit;
    }

    public void setWithoutExit(String withoutExit) {
        this.withoutExit = withoutExit;
    }
}
