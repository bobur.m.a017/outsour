package com.smart_solution.outsource.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class StringDto {
    private String number;
    private List<Integer> menuKinIdList;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Integer> getMenuKinIdList() {
        return menuKinIdList;
    }

    public void setMenuKinIdList(List<Integer> menuKinIdList) {
        this.menuKinIdList = menuKinIdList;
    }
}
