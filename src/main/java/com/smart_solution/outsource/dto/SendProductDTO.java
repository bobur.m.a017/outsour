package com.smart_solution.outsource.dto;

public class SendProductDTO {

    private Integer productId;
    private Double weight;
    private Double numberPack;

    public SendProductDTO() {
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(Double numberPack) {
        this.numberPack = numberPack;
    }
}
