package com.smart_solution.outsource.dto;

import com.smart_solution.outsource.kindergarten.supplier.SupplierKinProductResponseDTO;

import java.util.List;

public class AgreementOrderDTO {


    private String agreementNumber;
    private Integer agreementId;
    private List<SupplierKinProductResponseDTO> productList;

    public AgreementOrderDTO() {
    }

    public AgreementOrderDTO(String agreementNumber, Integer agreementId) {
        this.agreementNumber = agreementNumber;
        this.agreementId = agreementId;
    }

    public String getAgreementNumber() {
        return agreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    public Integer getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Integer agreementId) {
        this.agreementId = agreementId;
    }

    public List<SupplierKinProductResponseDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<SupplierKinProductResponseDTO> productList) {
        this.productList = productList;
    }
}
