package com.smart_solution.outsource.perDay;


import com.smart_solution.outsource.address.district.District;

import java.util.List;

public class PerDayByDistrict {

    private District district;
    private List<PerDayResponseDTO> perDayList;

    public PerDayByDistrict() {
    }


    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public List<PerDayResponseDTO> getPerDayList() {
        return perDayList;
    }

    public void setPerDayList(List<PerDayResponseDTO> perDayList) {
        this.perDayList = perDayList;
    }
}
