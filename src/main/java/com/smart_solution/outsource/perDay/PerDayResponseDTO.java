package com.smart_solution.outsource.perDay;

import com.smart_solution.outsource.numberOfChildren.NumberOfChildrenResponseDTO;

import java.sql.Timestamp;
import java.util.List;

public class PerDayResponseDTO {
    private Integer id;

    private Timestamp createDate;
    private Timestamp date;
    private Timestamp updateDate;
    private Integer createdBy;
    private Integer updateBy;
    private String status;
    private Integer attachmentId;

    private List<NumberOfChildrenResponseDTO> numberOfChildrenDTOList;

    private Integer kindergartenId;
    private String kindergartenName;

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public PerDayResponseDTO() {
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<NumberOfChildrenResponseDTO> getNumberOfChildrenDTOList() {
        return numberOfChildrenDTOList;
    }

    public void setNumberOfChildrenDTOList(List<NumberOfChildrenResponseDTO> numberOfChildrenDTOList) {
        this.numberOfChildrenDTOList = numberOfChildrenDTOList;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }
}
