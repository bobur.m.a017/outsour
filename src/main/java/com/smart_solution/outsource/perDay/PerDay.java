package com.smart_solution.outsource.perDay;

import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


@Entity
public class PerDay {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreationTimestamp
    private Timestamp createDate;
    private Timestamp dayDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    private Integer createdBy;
    private Integer updateBy;

    private String status;

    private Integer attachmentId;
    private Integer kindergartenId;
    private Boolean state;


    @OneToMany(mappedBy = "perDay", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NumberOfChildren> numberOfChildren;

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getDayDate() {
        return dayDate;
    }

    public void setDayDate(Timestamp dayDate) {
        this.dayDate = dayDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public List<NumberOfChildren> getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(List<NumberOfChildren> numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }
}
