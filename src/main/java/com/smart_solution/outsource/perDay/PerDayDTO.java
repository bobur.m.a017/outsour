package com.smart_solution.outsource.perDay;

import com.smart_solution.outsource.numberOfChildren.NumberOfChildrenDTO;

import java.util.List;

public class PerDayDTO {

    private List<NumberOfChildrenDTO> numberOfChildrenDTOList;
    private Integer kindergartenId;

    public PerDayDTO() {
    }


    public List<NumberOfChildrenDTO> getNumberOfChildrenDTOList() {
        return numberOfChildrenDTOList;
    }

    public void setNumberOfChildrenDTOList(List<NumberOfChildrenDTO> numberOfChildrenDTOList) {
        this.numberOfChildrenDTOList = numberOfChildrenDTOList;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }
}
