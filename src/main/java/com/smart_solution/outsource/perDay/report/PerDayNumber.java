package com.smart_solution.outsource.perDay.report;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;

import java.math.BigDecimal;

public class PerDayNumber {
    private String ageGroupName;
    private Integer ageGroupId;
    private BigDecimal max;
    private BigDecimal min;
    private BigDecimal average;

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public BigDecimal getMax() {
        return max;
    }

    public void setMax(BigDecimal max) {
        this.max = max;
    }

    public BigDecimal getMin() {
        return min;
    }

    public void setMin(BigDecimal min) {
        this.min = min;
    }

    public BigDecimal getAverage() {
        return average;
    }

    public void setAverage(BigDecimal average) {
        this.average = average;
    }
}
