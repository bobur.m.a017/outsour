package com.smart_solution.outsource.perDay.report;

import java.util.List;

public class ReportPerDay {

    private Integer kindergartenId;
    private String kindergartenName;
    private String district;
    private List<PerDayNumber> agerGroup;


    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public List<PerDayNumber> getAgerGroup() {
        return agerGroup;
    }

    public void setAgerGroup(List<PerDayNumber> agerGroup) {
        this.agerGroup = agerGroup;
    }
}
