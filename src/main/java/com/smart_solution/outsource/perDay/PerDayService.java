package com.smart_solution.outsource.perDay;

import com.smart_solution.outsource.attachment.Attachment;
import com.smart_solution.outsource.attachment.AttachmentService;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.consumption.ConsumptionService;
import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.kindergarten.KindergartenType;
import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.numberOfChildren.*;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.order.numberOfKids.NumberOfKidsDTO;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightRepository;
import com.smart_solution.outsource.perDay.report.PerDayNumber;
import com.smart_solution.outsource.perDay.report.ReportPerDay;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.productBalancer.ProductBalancerRepository;
import com.smart_solution.outsource.productMeal.ProductMeal;
import com.smart_solution.outsource.role.RoleType;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;

@Service
public record PerDayService(
        PerDayRepository perDayRepository,
        NumberOfChildrenService numberOfChildrenService,
        KindergartenRepository kindergartenRepository,
        UsersRepository usersRepository,
        AgeGroupRepository ageGroupRepository,
        ConsumptionService consumptionService,
        MenuSaveRepository menuSaveRepository,
        MealRepository mealRepository,
        ProductBalancerRepository productBalancerRepository,
        MenuWeightRepository menuWeightRepository,
        NumberOfChildrenRepository numberOfChildrenRepository,
        AttachmentService attachmentService,
        CompanyRepository companyRepository
) {

    public StateMessage add(PerDayDTO perDayDTO, ResponseUser responseUser, Long date, MultipartFile file) throws IOException {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        if ((users.getRole().getName().equals(RoleType.NURSE.getName()) ||
                (users.getRole().getName().equals(RoleType.KINDERGARDEN_PRINCIPAL.getName())))) {

            Timestamp timestamp = new Timestamp(date);


            List<PerDay> perDayList = perDayRepository.findAllByKindergartenIdAndState(users.getKindergartenId(), true);

            PerDay perDayOld = check(perDayList, timestamp);

            Integer kindergartenId = users.getKindergartenId();


            if (perDayOld == null) {


                MenuSave menuSave = checkMenu(kindergartenId, timestamp);

                if (menuSave != null) {
                    PerDay perDay = new PerDay();
                    perDay.setState(true);
                    perDay.setStatus(Status.INCLUDED.getName());
                    perDay.setCreatedBy(users.getId());
                    perDay.setUpdateBy(users.getId());
                    perDay.setDayDate(timestamp);
                    Attachment add = attachmentService.add(file);
                    perDay.setAttachmentId(add.getId());
//                    perDay.setAttachmentId(1);
                    perDay.setKindergartenId(kindergartenId);

                    List<NumberOfChildren> list = new ArrayList<>();
                    for (NumberOfChildrenDTO numberOfChildrenDTO : perDayDTO.getNumberOfChildrenDTOList()) {
                        list.add(numberOfChildrenService.add(numberOfChildrenDTO, perDay));
                    }
                    perDay.setNumberOfChildren(numberOfChildrenRepository.saveAll(list));

                    PerDay save = perDayRepository.save(perDay);
                    menuSave.setNumberFact(save);
                    menuSaveRepository.save(menuSave);
                    return new StateMessage("Muvaffaqiyatli qo`shildi", true);
                }
                return new StateMessage("Kiritilgan bolalar soni qo`shilmadi. Bu kunga menu biriktirilmagan", false);
            } else {
                return edit(perDayOld.getId(), perDayDTO, responseUser, file);
            }
        }
        return new StateMessage("X A T I L I K !!!  Ruxsat etilmagan yo`l.", true);
    }

    public StateMessage edit(Integer id, PerDayDTO perDayDTO, ResponseUser responseUser, MultipartFile file) throws IOException {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        PerDay perDay = perDayRepository.findById(id).get();


        if (users.getRole().getName().equals(RoleType.NURSE.getName())) {
            if (!perDay.getStatus().equals(Status.INCLUDED.getName())) {
                return new StateMessage("O`zgartirish mumkin emas", false);
            }
        }


        if (users.getRole().getName().equals(RoleType.TECHNOLOGIST.getName())) {
            if (!perDay.getStatus().equals(Status.SUCCESS.getName())) {
                return new StateMessage("O`zgartirish mumkin emas", false);
            }
        }

        perDay.setCreatedBy(users.getId());

        if (users.getRole().equals(RoleType.TECHNOLOGIST.getName())) {

            perDay.setStatus(Status.EDIT.getName());
        } else {
            perDay.setStatus(Status.INCLUDED.getName());
        }
        List<NumberOfChildren> list = new ArrayList<>();

        for (NumberOfChildren numberOfChild : perDay.getNumberOfChildren()) {

            for (NumberOfChildrenDTO numberOfChildrenDTO : perDayDTO.getNumberOfChildrenDTOList()) {
                if (numberOfChild.getAgeGroup().getId().equals(numberOfChildrenDTO.getAgeGroupId())) {

                    list.add(numberOfChildrenService.edit(numberOfChildrenDTO, numberOfChild));
                }

            }
        }
        numberOfChildrenRepository.saveAll(list);
        if (file != null) {

            if (perDay.getAttachmentId() == null){
                Attachment add = attachmentService.add(file);
                perDay.setAttachmentId(add.getId());
            }else {
                Attachment edit = attachmentService.edit(file, perDay.getAttachmentId());
                perDay.setAttachmentId(edit.getId());
            }
        }
        perDayRepository.save(perDay);

        return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

    }

    public StateMessage changeStatus(ResponseUser responseUser, Integer perDayId) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        PerDay perDay = perDayRepository.findById(perDayId).get();
        if (users.getRole().getName().equals(RoleType.KINDERGARDEN_PRINCIPAL.getName())) {

            perDay.setStatus(Status.SUCCESS.getName());

            perDayRepository.save(perDay);

            return new StateMessage("Muvaffaqiyatli tasdiqlandi", true);

        } else if (users.getRole().getName().equals(RoleType.TECHNOLOGIST.getName())) {

            MenuSave menuSave = checkMenu(perDay.getKindergartenId(), perDay.getDayDate());
            StateMessage message = checkProduct(menuSave);

            if (perDay.getStatus().equals(Status.ACCEPTED.getName())) {
                return new StateMessage("Qabul qilingan.", true);
            }

            if (message.isSuccess()) {

                if (menuSave != null) {
//                    if(menuSave.getStatus().equals(Status.SUCCESS.getName())){
                    menuSave.setStatus(Status.ACCEPTED.getName());
                    MenuSave menuSave1 = menuSaveRepository.save(menuSave);

                    perDay.setStatus(Status.ACCEPTED.getName());
                    PerDay save = perDayRepository.save(perDay);
                    consumptionService.add(menuSave1, users.getCompany());

//                    }else {
//                        return new StateMessage("Qabul qilinmadi menyuni mudira tasdiqlamagan", false);
//                    }
                }
                return new StateMessage("Muvaffaqiyatli qabul qilindi", true);
            } else {
                return message;
            }
        }
        return new StateMessage("X A T I L I K !!!  Ruxsat etilmagan yo`l.", true);
    }

    public StateMessage checkProduct(MenuSave menuSave) {

        List<InOutProduct> calculate = calculate(menuSave);
        Kindergarten kindergarten = menuSave.getKindergarten();

        StringBuilder str = new StringBuilder();
        str = new StringBuilder("Ushbu maxsulotlar omborda yetarli emas");
        boolean res = true;


        for (InOutProduct inOutProduct : calculate) {
            BigDecimal weightBalanser = BigDecimal.valueOf(0);
            BigDecimal weightExist = BigDecimal.valueOf(0);
            for (ProductBalancer productBalancer : kindergarten.getConsumable()) {
                if (inOutProduct.getProduct().equals(productBalancer.getProduct())) {
                    weightBalanser = weightBalanser.add(productBalancer.getWeight());
                }
            }
//            for (ProductBalancer productBalancer : kindergarten.getConsumable()) {   /////O`ZGARTIRILDI
//                if (inOutProduct.getProduct().equals(productBalancer.getProduct())) {
//                    weightExist = weightExist.add(productBalancer.getWeight());
//                }
//            }

            BigDecimal weight = weightExist.add(weightBalanser);
            if (weight.compareTo(inOutProduct.getWeight()) < 0) {
                str.append("  ").append(inOutProduct.getProduct().getName());
                res = false;
            }
        }
        return new StateMessage(str.toString(), res);
    }

    public List<InOutProduct> calculate(MenuSave menuSave) {

        List<InOutProduct> list = new ArrayList<>();

        for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
            for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {
                BigDecimal mealWeight = calculateMealWeight(mealAgeStandardSave, menuSave);
                calculateProductWeight(mealAgeStandardSave.getMealId(), mealWeight, list);
            }
        }
        return list;
    }

    public BigDecimal calculateMealWeight(MealAgeStandardSave mealAgeStandardSave, MenuSave menuSave) {

        BigDecimal weight = BigDecimal.valueOf(0);

        for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
            weight = weight.add(calculateNumberKids(ageStandardSave, menuSave));
        }
        return weight;
    }

    public BigDecimal calculateNumberKids(AgeStandardSave ageStandardSave, MenuSave menuSave) {

        PerDay numberFact = menuSave.getNumberFact();
        for (NumberOfChildren numberOfChild : numberFact.getNumberOfChildren()) {
            if (numberOfChild.getAgeGroup().getId().equals(ageStandardSave.getAgeGroupId())) {
                return BigDecimal.valueOf(numberOfChild.getNumberOfKids()).multiply(ageStandardSave.getWeight());
            }
        }
        return BigDecimal.valueOf(0);
    }

    public void calculateProductWeight(Integer mealId, BigDecimal mealWeight, List<InOutProduct> list) {

        Meal meal = mealRepository.findById(mealId).get();

        for (ProductMeal productMeal : meal.getProductMeals()) {
            BigDecimal productMealWeight = productMeal.getWeight().divide(meal.getWeight(), 6, RoundingMode.HALF_UP);
            BigDecimal productWeight = productMealWeight.multiply(mealWeight);
            addMenuWeight(productMeal.getProduct(), productWeight, list);
        }
    }

    public void addMenuWeight(Product product, BigDecimal productWeight, List<InOutProduct> list) {

        boolean res = true;

        if (list != null) {

            for (InOutProduct inOutProduct : list) {
                if (inOutProduct.getProduct().equals(product)) {
                    inOutProduct.setWeight(inOutProduct.getWeight().add(productWeight));
                    res = false;
                }
            }
        }
        if (res) {
            InOutProduct inOutProduct = new InOutProduct();
            inOutProduct.setProduct(product);
            inOutProduct.setWeight(productWeight);

            list.add(inOutProduct);
        }
    }


    public PerDayResponseDTO get(Integer kindergartenId, Timestamp date) {

        List<PerDay> perDayList = perDayRepository.findAllByKindergartenIdAndState(kindergartenId, true);

        PerDay perDay = check(perDayList, date);

        PerDayResponseDTO dto = new PerDayResponseDTO();
        List<NumberOfChildrenResponseDTO> list = new ArrayList<>();

        if (perDay != null) {

            Kindergarten kindergarten = kindergartenRepository.findById(perDay.getKindergartenId()).get();

            dto.setId(perDay.getId());
            dto.setCreatedBy(perDay.getCreatedBy());
            dto.setUpdateBy(perDay.getUpdateBy());
            dto.setStatus(perDay.getStatus());
            dto.setCreateDate(perDay.getCreateDate());
            dto.setUpdateDate(perDay.getUpdateDate());
            dto.setKindergartenId(kindergarten.getId());
            dto.setKindergartenName(kindergarten.getName());
            dto.setAttachmentId(perDay.getAttachmentId());


            for (NumberOfChildren numberOfChild : perDay.getNumberOfChildren()) {

                NumberOfChildrenResponseDTO parseDTO = numberOfChildrenService.parse(numberOfChild);

                list.add(parseDTO);
            }

        } else {

            dto.setStatus(Status.INDEFINITE.getName());

            dto.setKindergartenId(kindergartenId);
            dto.setKindergartenName(kindergartenRepository.findById(kindergartenId).get().getName());

            for (AgeGroup ageGroup : ageGroupRepository.findAll()) {

                NumberOfChildrenResponseDTO parseDTO = numberOfChildrenService.parseNull(ageGroup);

                list.add(parseDTO);
            }
//            dto.setAttachmentId(1);
        }

        list.sort(Comparator.comparing(NumberOfChildrenResponseDTO::getAgeGroupName));
        dto.setNumberOfChildrenDTOList(list);

        return dto;
    }

    public PerDay check(List<PerDay> perDayList, Timestamp timestamp) {

        for (PerDay perDay : perDayList) {

            Date d = new Date(timestamp.getTime());
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            Date dm = new Date(perDay.getDayDate().getTime());
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int yearM = cm.get(Calendar.YEAR);
            int monthM = cm.get(Calendar.MONTH);
            int dayM = cm.get(Calendar.DAY_OF_MONTH);

            if (year == yearM && month == monthM && day == dayM) {
                return perDay;
            }
        }
        return null;
    }

    public ResponseEntity<?> getAll(ResponseUser responseUser, Timestamp date) {

//        List<PerDay> listper = new ArrayList<>();
//        for (MenuSave menuSave : menuSaveRepository.findAll()) {
//            PerDay numberToGuess = menuSave.getNumberToGuess();
//            numberToGuess.setDayDate(menuSave.getDayDate());
//            numberToGuess.setState(false);
//            listper.add(numberToGuess);
//        }
//        perDayRepository.saveAll(listper);


        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        if (users.getKindergartenId() != null) {

            return ResponseEntity.status(200).body(getPerdayKindergarten(kindergartenRepository.findById(users.getKindergartenId()).get(), date));
        }

        List<PerDayByDistrict> list = new ArrayList<>();

        Company company = users.getCompany();

        for (Kindergarten kindergarten : company.getKindergarten()) {

            if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {

                PerDayResponseDTO perDayDTO = get(kindergarten.getId(), date);

                boolean result = false;
                for (PerDayByDistrict perDayByDistrict : list) {

                    if (perDayByDistrict.getDistrict().equals(kindergarten.getAddress().getDistrict())) {
                        List<PerDayResponseDTO> perDayList = new ArrayList<>(perDayByDistrict.getPerDayList());
                        perDayList.add(perDayDTO);
                        perDayByDistrict.setPerDayList(perDayList);
                        result = true;
                    }
                }


                if (list.size() == 0 || !result) {

                    PerDayByDistrict perDayByDistrict = new PerDayByDistrict();
                    perDayByDistrict.setDistrict(kindergarten.getAddress().getDistrict());
                    perDayByDistrict.setPerDayList(List.of(perDayDTO));

                    list.add(perDayByDistrict);
                }
            }

        }

        for (PerDayByDistrict perDayByDistrict : list) {
            List<PerDayResponseDTO> perDayList = perDayByDistrict.getPerDayList();

            perDayList.sort(Comparator.comparing(PerDayResponseDTO::getKindergartenName));
            perDayByDistrict.setPerDayList(perDayList);
        }

        return ResponseEntity.status(200).body(list);
    }

    public ResponseEntity<?> getAllWeb(Timestamp date) {


        Company company = companyRepository.findByName("Governess Business").get();

        List<PerDayByDistrict> list = new ArrayList<>();

        for (Kindergarten kindergarten : company.getKindergarten()) {

            if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {

                PerDayResponseDTO perDayDTO = get(kindergarten.getId(), date);

                boolean result = false;
                for (PerDayByDistrict perDayByDistrict : list) {

                    if (perDayByDistrict.getDistrict().equals(kindergarten.getAddress().getDistrict())) {
                        List<PerDayResponseDTO> perDayList = new ArrayList<>(perDayByDistrict.getPerDayList());
                        perDayList.add(perDayDTO);
                        perDayByDistrict.setPerDayList(perDayList);
                        result = true;
                    }
                }


                if (list.size() == 0 || !result) {

                    PerDayByDistrict perDayByDistrict = new PerDayByDistrict();
                    perDayByDistrict.setDistrict(kindergarten.getAddress().getDistrict());
                    perDayByDistrict.setPerDayList(List.of(perDayDTO));

                    list.add(perDayByDistrict);
                }
            }

        }
        return ResponseEntity.status(200).body(list);
    }

    public PerDayByDistrict getPerdayKindergarten(Kindergarten kindergarten, Timestamp date) {

        PerDayResponseDTO perDayDTO = get(kindergarten.getId(), date);

        PerDayByDistrict perDayByDistrict = new PerDayByDistrict();

        perDayByDistrict.setDistrict(kindergarten.getAddress().getDistrict());

        perDayByDistrict.setPerDayList(List.of(perDayDTO));

        return perDayByDistrict;
    }

    public PerDayResponseDTO parse(PerDay numberFact) {

        PerDayResponseDTO dto = new PerDayResponseDTO();

        Kindergarten kindergarten = kindergartenRepository.findById(numberFact.getKindergartenId()).get();

        dto.setKindergartenId(kindergarten.getId());
        dto.setKindergartenName(kindergarten.getName());
        dto.setStatus(numberFact.getStatus());
        dto.setCreateDate(numberFact.getCreateDate());
        dto.setUpdateDate(numberFact.getUpdateDate());
        dto.setDate(numberFact.getDayDate());
        dto.setAttachmentId(numberFact.getAttachmentId());


        List<NumberOfChildrenResponseDTO> list = new ArrayList<>();
        for (NumberOfChildren numberOfChild : numberFact.getNumberOfChildren()) {
            list.add(numberOfChildrenService.parse(numberOfChild));
        }

        dto.setNumberOfChildrenDTOList(list);
        return dto;
    }

    public MenuSave checkMenu(Integer kindergartenId, Timestamp date) {


        List<MenuSave> list = menuSaveRepository.findAllByKindergarten_Id(kindergartenId);

        for (MenuSave menuSave : list) {
            Date d = new Date(date.getTime());
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            Date dm = new Date(menuSave.getDayDate().getTime());
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int yearM = cm.get(Calendar.YEAR);
            int monthM = cm.get(Calendar.MONTH);
            int dayM = cm.get(Calendar.DAY_OF_MONTH);

            if (year == yearM && month == monthM && day == dayM) {
                return menuSave;
            }
        }

        return null;
    }



    //O`rtacha
    public List<ReportPerDay> averageNumberOfChildren(Long start, Long end, ResponseUser responseUser) {

        List<ReportPerDay> list = new ArrayList<>();
        BigDecimal zero = BigDecimal.valueOf(0);


        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = companyRepository.findById(1).get();


        for (Kindergarten kindergarten : company.getKindergarten()) {

            PerDay max = null;
            PerDay min = null;
            PerDay average = null;

            List<PerDay> allByKindergarten = perDayRepository.findAllByKindergartenId(kindergarten.getId());
            int number = 0;
            for (PerDay perDay : allByKindergarten) {
                if (start <= perDay.getDayDate().getTime() && end >= perDay.getDayDate().getTime()) {
                    if (max == null) {
                        max = perDay;
                        min = perDay;
                        average = perDay;
                    } else {
                        addNumber(average, perDay);

                        if (getNumber(max) < getNumber(perDay)) {
                            max = perDay;
                        }
                        if (getNumber(min) > getNumber(perDay)) {
                            min = perDay;
                        }
                    }
                    number++;
                }
            }
            divide(average, number);


            ReportPerDay report = new ReportPerDay();
            report.setKindergartenId(kindergarten.getId());
            report.setKindergartenName(kindergarten.getName());
            report.setDistrict(kindergarten.getAddress().getDistrict().getName());

            List<PerDayNumber> numberList = new ArrayList<>();

            if (max != null) {
                for (NumberOfChildren numberOfChild : max.getNumberOfChildren()) {
                    PerDayNumber perDayNumber = new PerDayNumber();
                    perDayNumber.setAgeGroupId(numberOfChild.getAgeGroup().getId());
                    perDayNumber.setAgeGroupName(numberOfChild.getAgeGroup().getName());
                    perDayNumber.setMax(BigDecimal.valueOf(numberOfChild.getNumberOfKids()));
                    perDayNumber.setMin(zero);
                    perDayNumber.setAverage(zero);
                    numberList.add(perDayNumber);
                }
            } else {
                for (AgeGroup ageGroup : ageGroupRepository.findAll()) {
                    PerDayNumber perDayNumber = new PerDayNumber();
                    perDayNumber.setAgeGroupId(ageGroup.getId());
                    perDayNumber.setAgeGroupName(ageGroup.getName());
                    perDayNumber.setMax(zero);
                    perDayNumber.setMin(zero);
                    perDayNumber.setAverage(zero);
                    numberList.add(perDayNumber);
                }

            }

            if (min != null) {
                for (NumberOfChildren numberOfChild : min.getNumberOfChildren()) {
                    for (PerDayNumber perDayNumber : numberList) {
                        if (numberOfChild.getAgeGroup().getId().equals(perDayNumber.getAgeGroupId())) {
                            perDayNumber.setMin(BigDecimal.valueOf(numberOfChild.getNumberOfKids()));
                        }
                    }
                }
            }

            if (average != null) {
                for (NumberOfChildren numberOfChild : average.getNumberOfChildren()) {
                    for (PerDayNumber perDayNumber : numberList) {
                        if (numberOfChild.getAgeGroup().getId().equals(perDayNumber.getAgeGroupId())) {
                            perDayNumber.setAverage(BigDecimal.valueOf(numberOfChild.getNumberOfKids()));
                        }
                    }
                }
            }

            report.setAgerGroup(numberList);
            list.add(report);
        }

        for (ReportPerDay report : list) {
            report.getAgerGroup().sort(Comparator.comparing(PerDayNumber::getAgeGroupName));
        }
        list.sort(Comparator.comparing(ReportPerDay::getDistrict));

        return list;
    }

    public int getNumber(PerDay perDay) {

        int number = 0;

        for (NumberOfChildren numberOfChild : perDay.getNumberOfChildren()) {
            number = number + numberOfChild.getNumberOfKids();
        }
        return number;
    }

    public void addNumber(PerDay old, PerDay neww) {

        List<NumberOfChildren> list = new ArrayList<>();

        BigDecimal number = BigDecimal.valueOf(0);
        for (NumberOfChildren numberOfChild : old.getNumberOfChildren()) {

            for (NumberOfChildren ofChild : neww.getNumberOfChildren()) {
                if (numberOfChild.getAgeGroup().getId().equals(ofChild.getAgeGroup().getId())) {
                    numberOfChild.setNumberOfKids(numberOfChild.getNumberOfKids() + ofChild.getNumberOfKids());
                    list.add(numberOfChild);
                }
            }

        }
        old.setNumberOfChildren(list);
    }

    public void divide(PerDay perDay, int number) {
        List<NumberOfChildren> list = new ArrayList<>();

        if (perDay != null) {
            for (NumberOfChildren numberOfChild : perDay.getNumberOfChildren()) {

                numberOfChild.setNumberOfKids(numberOfChild.getNumberOfKids() / number);
                list.add(numberOfChild);

            }
            perDay.setNumberOfChildren(list);
        }
    }
}













