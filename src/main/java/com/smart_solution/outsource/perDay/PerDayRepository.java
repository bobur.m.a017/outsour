package com.smart_solution.outsource.perDay;

import com.smart_solution.outsource.kindergarten.Kindergarten;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface PerDayRepository extends JpaRepository<PerDay, Integer> {

    List<PerDay> findAllByKindergartenIdAndState(Integer id, boolean res);
    List<PerDay> findAllByKindergartenId(Integer id);
}
