package com.smart_solution.outsource.inOutProduct;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InOutProductRepository extends JpaRepository<InOutProduct, UUID> {
}
