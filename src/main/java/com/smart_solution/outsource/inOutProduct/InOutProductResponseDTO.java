package com.smart_solution.outsource.inOutProduct;
import java.math.BigDecimal;

public class InOutProductResponseDTO {
    private BigDecimal weight;
    private BigDecimal numberPack;
    private String productName;
    private BigDecimal pack;

    public InOutProductResponseDTO() {
    }

    public InOutProductResponseDTO(String productName, BigDecimal pack) {
        this.productName = productName;
        this.pack = pack;
    }

    public InOutProductResponseDTO(BigDecimal weight, BigDecimal numberPack, String productName, BigDecimal pack) {
        this.weight = weight;
        this.numberPack = numberPack;
        this.productName = productName;
        this.pack = pack;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }
}
