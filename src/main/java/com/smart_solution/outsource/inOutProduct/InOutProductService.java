package com.smart_solution.outsource.inOutProduct;


import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.dto.ReceiveDto;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.order.OrderMenuRepository;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProductRepository;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productBalancer.ProductBalancerRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service

public record InOutProductService(
        InOutProductRepository inOutProductRepository,
        ProductRepository productRepository,
        ProductBalancerRepository productBalancerRepository,
        UsersRepository usersRepository,
        KindergartenRepository kindergartenRepository,
        CompanyRepository companyRepository,
        OrderMenuRepository orderMenuRepository,
        WeightProductRepository weightProductRepository
) {

//    public InOutProduct addStorage(Storage storage, InOutSupplier inOutSupplier, String senderName, boolean res, String comment, Users users) {
//
//        InOutProduct inOutProduct = new InOutProduct(
//                new Timestamp(System.currentTimeMillis()),
//                inOutSupplier.getPrice(),
//                inOutSupplier.getWeightPack(),
//                Status.NEW.getName(),
//                inOutSupplier.getMeasurementType(),
//                inOutSupplier.getWeightPack(),
//                inOutSupplier.getPack(),
//                inOutSupplier.getNumberPack(),
//                res,
//                inOutSupplier.getComment(),
//                senderName + " " + users.getUsername() + " " + users.getSurname(),
//                users,
//                productRepository.findById(inOutSupplier.getProductId()).get()
//        );
//        inOutProduct.setComment(comment);
//        inOutProduct.setTypeOfPayment(inOutSupplier.getTypeOfPayment());
//        inOutProduct.setPaymentStatus(inOutSupplier.getPaymentStatus());
//
////        inOutProduct.setEnterDate();
////        inOutProduct.setPrice(inOutSupplier.getPrice());
////        inOutProduct.setWeightPack(inOutSupplier.getWeightPack());
////        inOutProduct.setResult(res);
////        inOutProduct.setStatus(Status.NEW.getName());
////        inOutProduct.setMeasurementType(inOutSupplier.getMeasurementType());
////        inOutProduct.setPack(inOutSupplier.getPack());
////        inOutProduct.setNumberPack(inOutSupplier.getNumberPack());
////        inOutProduct.setSenderName(senderName);
////        inOutProduct.setProduct(productRepository.findById(inOutSupplier.getProductId()).get());
//
//        InOutProduct save = inOutProductRepository.save(inOutProduct);
//
//
//        List<InOutProduct> inOutProductList = storage.getInOutProductList() != null ? storage.getInOutProductList() : new ArrayList<>();
//        inOutProductList.add(save);
//        storage.setInOutProductList(inOutProductList);
//        storageRepository.save(storage);
//
//        return save;
//    }
//
//    public StateMessage receive(UUID id, ReceiveDto dto, ResponseUser responseUser) {
//
//        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
//
//        Storage storage = null;
//        Storage storageCompany = null;
//
//        Kindergarten kindergarten = null;
//
//        if (users.getRole().getName().equals(RoleType.STOREKEEPER.getName())) {
//
//            storage = users.getCompany().getStorage();
//
//        } else if (users.getRole().getName().equals(RoleType.COOK.getName())) {
//            kindergarten = kindergartenRepository.findById(users.getKindergartenId()).get();
//
//            storage = kindergarten.getStorage();
//
//        }
//
//        if (storage != null) {
//            Shipping shipping = shippingRepository.findByInOutProduct_Id(id).get();
//
//            InOutSupplier shippedProduct = shipping.getShippedProduct();
//            InOutSupplier requiredProduct = shipping.getRequiredProduct();
//            InOutProduct inOutProduct = shipping.getInOutProduct();
//            InOut inOut = shipping.getInOut();
//
//            if (inOutProduct.getStatus().equals(Status.ACCEPTED.getName()) || inOutProduct.getStatus().equals(Status.PARTIALLY_ACCEPTED.getName())) {
//                return new StateMessage("Maxsulot qabul qilib bo`lingan.", false);
//            }
//
//
//            if (dto.getWeightPack() == 0) {
//
//                inOutProduct.setStatus(Status.THE_PRODUCT_DID_NOT_ARRIVE.getName());
//                inOutProduct.setCheckWeight(BigDecimal.valueOf(0));
//                inOutProductRepository.save(inOutProduct);
//
//                shippedProduct.setStatus(Status.THE_PRODUCT_DID_NOT_ARRIVE.getName());
//                shippedProduct.setComment(dto.getComment());
//                inOutSupplierRepository.save(shippedProduct);
//
//                requiredProduct.setWeightPack(requiredProduct.getWeightPack().add(shippedProduct.getWeightPack()));
//                requiredProduct.setStatus(Status.PARTIALLY_SENT.getName());
//
//                if (requiredProduct.getPack().compareTo(BigDecimal.valueOf(0)) == 0) {
//                    requiredProduct.setNumberPack(BigDecimal.valueOf(0));
//                } else {
//                    requiredProduct.setNumberPack(requiredProduct.getNumberPack().add(shippedProduct.getNumberPack()));
//                }
//
//                inOutSupplierRepository.save(requiredProduct);
//
//                if (users.getRole().getName().equals(RoleType.COOK.getName()) && inOut != null) {
//
//                    inOut.setStatus(Status.ACCEPTED.getName());
//                    inOut.setWeightPack(inOut.getWeightPack().add(inOutProduct.getWeightPack()));
//                    inOut.setNumberPack(inOut.getNumberPack().add(inOutProduct.getNumberPack()));
//                    inOutRepository.save(inOut);
//                }
//
//                if (users.getRole().getName().equals(RoleType.COOK.getName())) {
//
//                    storageCompany = inOutProduct.getUsers().getCompany().getStorage();
////                    addExistingProduct(storageCompany, shippedProduct.getNumberPack(), shippedProduct.getWeightPack(), productRepository.findById(shippedProduct.getProductId()).get(), shippedProduct.getPrice(), shippedProduct.getPack());
//                }
//                return new StateMessage("Maxsulot rad etildi.", true);
//            }
//
//            if (shippedProduct.getWeightPack().compareTo(BigDecimal.valueOf(dto.getWeightPack())) == 0) {
//
//                inOutProduct.setStatus(Status.ACCEPTED.getName());
//                inOutProduct.setCheckWeight(BigDecimal.valueOf(0));
//                InOutProduct save = inOutProductRepository.save(inOutProduct);
//
//                shippedProduct.setStatus(Status.ACCEPTED.getName());
//                shippedProduct.setComment(dto.getComment());
//                inOutSupplierRepository.save(shippedProduct);
//
//
//
//                if (users.getRole().getName().equals(RoleType.COOK.getName()) && kindergarten != null) {
//
//                    BigDecimal weight = calculateWeight(requiredProduct.getOrderNumber(), kindergarten, save.getProduct(), save.getWeightPack());
//                    if (weight.compareTo(BigDecimal.valueOf(0)) == 0){
//                        addExistingProductCook(storage,save,false,save.getNumberPack(),save.getWeightPack());
//                    }else {
//
//                        BigDecimal pack = save.getPack();
//                        if (pack.compareTo(BigDecimal.valueOf(0)) == 0){
//                            pack = BigDecimal.valueOf(1);
//                        }
//
//                        BigDecimal number = weight.divide(pack, 6, RoundingMode.HALF_UP);
//                        addExistingProductCook(storage,save,true,number,weight);
//                        addExistingProductCook(storage,save,false,save.getNumberPack().subtract(number),save.getWeightPack().subtract(weight));
//                    }
//                } else {
//                    addExistingProduct2(storage, save);
//                }
//
//
//                return new StateMessage("Maxsulot muvaffaqiyatli qabul qilindi.", true);
//            } else {
//                BigDecimal number = inOutProduct.getNumberPack().subtract(BigDecimal.valueOf(dto.getNumberPack()));
//                BigDecimal weight = inOutProduct.getWeightPack().subtract(BigDecimal.valueOf(dto.getWeightPack()));
//
//                inOutProduct.setStatus(Status.PARTIALLY_ACCEPTED.getName());
//                inOutProduct.setWeightPack(BigDecimal.valueOf(dto.getWeightPack()));
//                inOutProduct.setCheckWeight(BigDecimal.valueOf(0));
//                inOutProduct.setNumberPack(BigDecimal.valueOf(dto.getNumberPack()));
//                InOutProduct inOutProductSave = inOutProductRepository.save(inOutProduct);
//
//
//                shippedProduct.setWeightPack(BigDecimal.valueOf(dto.getWeightPack()));
//                shippedProduct.setNumberPack(BigDecimal.valueOf(dto.getNumberPack()));
//                shippedProduct.setStatus(Status.PARTIALLY_ACCEPTED.getName());
//                shippedProduct.setComment(dto.getComment());
//                inOutSupplierRepository.save(shippedProduct);
//
//                requiredProduct.setWeightPack(requiredProduct.getWeightPack().add(weight));
//                requiredProduct.setStatus(Status.PARTIALLY_SENT.getName());
//                requiredProduct.setNumberPack(requiredProduct.getNumberPack().add(number));
//                inOutSupplierRepository.save(requiredProduct);
//
//                if (users.getRole().getName().equals(RoleType.COOK.getName()) && inOut != null) {
//
//                    inOut.setStatus(Status.ACCEPTED.getName());
//                    inOut.setWeightPack(inOut.getWeightPack().add(weight));
//                    inOut.setNumberPack(inOut.getNumberPack().add(number));
//                    inOutRepository.save(inOut);
//                }
//
//                if (users.getRole().getName().equals(RoleType.COOK.getName())) {
//                    storageCompany = inOutProduct.getUsers().getCompany().getStorage();
////                    addExistingProduct(storageCompany, number, weight, productRepository.findById(shippedProduct.getProductId()).get(), shippedProduct.getPrice(), shippedProduct.getPack());
//
//
//                    BigDecimal weightwp = calculateWeight(requiredProduct.getOrderNumber(), kindergarten, inOutProductSave.getProduct(), inOutProductSave.getWeightPack());
//                    if (weightwp.compareTo(BigDecimal.valueOf(0)) == 0){
//                        addExistingProductCook(storage,inOutProductSave,false,inOutProductSave.getNumberPack(),inOutProductSave.getWeightPack());
//                    }else {
//                        BigDecimal pack = inOutProductSave.getPack();
//                        if (pack.compareTo(BigDecimal.valueOf(0)) == 0){
//                            pack = BigDecimal.valueOf(1);
//                        }
//                        BigDecimal numberwp = weightwp.divide(pack, 6, RoundingMode.HALF_UP);
//                        addExistingProductCook(storage,inOutProductSave,true,numberwp,weightwp);
//                        addExistingProductCook(storage,inOutProductSave,false,inOutProductSave.getNumberPack().subtract(numberwp),inOutProductSave.getWeightPack().subtract(weightwp));
//                    }
//                } else {
//                    addExistingProduct2(storage, inOutProductSave);
//                }
//                return new StateMessage("Maxsulotning bir qismi qabul qilindi. Qolgani rad etildi", true);
//            }
//        }
//
//        return new StateMessage("XATOLIK", false);
//    }
//
//    public InOutProductResponseDTO parseew(InOutProduct inOutProduct) {
//
//        InOutProductResponseDTO dto = new InOutProductResponseDTO(
//                inOutProduct.getId(),
//                inOutProduct.getEnterDate(),
//                inOutProduct.getPrice(),
//                inOutProduct.getWeight(),
//                inOutProduct.getStatus(),
//                inOutProduct.getMeasurementType(),
//                inOutProduct.getWeightPack(),
//                inOutProduct.getPack(),
//                inOutProduct.getNumberPack(),
//                inOutProduct.getSenderName(),
//                inOutProduct.getProduct().getId(),
//                inOutProduct.getProduct().getName(),
//                inOutProduct.getComment()
//        );
////
////        dto.setId(inOutProduct.getId());
////        dto.setProductId(inOutProduct.getProduct().getId());
////        dto.setProductName(inOutProduct.getProduct().getName());
////        dto.setPrice(inOutProduct.getPrice());
////        dto.setEnterDate(inOutProduct.getEnterDate());
////        dto.setStatus(inOutProduct.getStatus());
////        dto.setMeasurementType(inOutProduct.getMeasurementType());
////        dto.setWeightPack(inOutProduct.getWeightPack());
////        dto.setPack(inOutProduct.getPack());
////        dto.setNumberPack(inOutProduct.getNumberPack());
////        dto.setSenderName(inOutProduct.getSenderName());
//
//        if (inOutProduct.getUsers() != null) {
//            dto.setUsersId(inOutProduct.getUsers().getId());
//            dto.setUsersName(inOutProduct.getUsers().getName() + " " + inOutProduct.getUsers().getFatherName() + " " + inOutProduct.getUsers().getSurname());
//        }
//
//        dto.setTypeOfPayment(inOutProduct.getTypeOfPayment());
//        dto.setPaymentStatus(inOutProduct.getPaymentStatus());
//
//        return dto;
//    }
//
//    public List<InOutProductResponseDTO> get() {
//        List<InOutProductResponseDTO> list = new ArrayList<>();
//
//        for (Product product : productRepository.findAll()) {
//            InOutProductResponseDTO dto = new InOutProductResponseDTO();
//            dto.setProductId(product.getId());
//            dto.setProductName(product.getName());
//            dto.setWeight(BigDecimal.valueOf(0.0));
//            dto.setWeightPack(BigDecimal.valueOf(0));
//            dto.setMeasurementType(product.getMeasurementType());
//
//            list.add(dto);
//        }
//        return list;
//    }
//
//    public void addDto(InOutProduct inOutProduct, List<InOutProductResponseDTO> list) {
//        for (InOutProductResponseDTO inOut : list) {
//            if (Objects.equals(inOut.getProductId(), inOutProduct.getProduct().getId())) {
//                inOut.setId(inOutProduct.getId());
//                inOut.setPrice(inOutProduct.getPrice());
//                inOut.setEnterDate(inOutProduct.getEnterDate());
//                inOut.setStatus(inOutProduct.getStatus());
//                inOut.setMeasurementType(inOutProduct.getMeasurementType());
//                inOut.setPack(inOutProduct.getPack());
//                inOut.setWeightPack(inOutProduct.getWeightPack());
//                inOut.setNumberPack(inOutProduct.getNumberPack());
//
//                inOut.setSenderName(inOutProduct.getSenderName());
//
//                if (inOutProduct.getUsers() != null) {
//                    inOut.setUsersId(inOutProduct.getUsers().getId());
//                    inOut.setUsersName(inOutProduct.getUsers().getName() + " " + inOutProduct.getUsers().getFatherName() + " " + inOutProduct.getUsers().getSurname());
//                }
//                inOut.setWeight(inOutProduct.getWeight());
//            }
//        }
//    }
//
//    public void addExistingProduct2(Storage storage, InOutProduct inOutProduct) {
//
//        List<ProductBalancer> list;
//        boolean ans = inOutProduct.getResult();
//
//        if (ans) {
//            list = storage.getProductBalancerList() != null ? storage.getProductBalancerList() : new ArrayList<>();
//        } else {
//            list = storage.getExistingProduct() != null ? storage.getExistingProduct() : new ArrayList<>();
//        }
//
//        boolean res = false;
//
//
//        for (ProductBalancer productBalancer : list) {
//
//            if (productBalancer.getProduct().equals(inOutProduct.getProduct())) {
//
//                InOut inOut = new InOut(
//                        new Timestamp(System.currentTimeMillis()),
//                        inOutProduct.getPrice(),
//                        inOutProduct.getWeightPack(),
//                        inOutProduct.getPack(),
//                        inOutProduct.getNumberPack(),
//                        Status.ACCEPTED.getName(),
//                        inOutProduct.getMeasurementType()
//                );
//                InOut save = inOutRepository.save(inOut);
//
//                List<InOut> inOutList = productBalancer.getInOutList();
//                inOutList.add(save);
//                productBalancer.setInOutList(inOutList);
//                productBalancerRepository.save(productBalancer);
//                res = true;
//            }
//        }
//
//        if (!res) {
//
//            ProductBalancer productBalancer = new ProductBalancer();
//
//            productBalancer.setProduct(inOutProduct.getProduct());
//            ProductBalancer productBalancerSave = productBalancerRepository.save(productBalancer);
//
//            InOut inOut = new InOut(
//                    new Timestamp(System.currentTimeMillis()),
//                    inOutProduct.getPrice(),
//                    inOutProduct.getWeightPack(),
//                    inOutProduct.getPack(),
//                    inOutProduct.getNumberPack(),
//                    Status.ACCEPTED.getName(),
//                    inOutProduct.getMeasurementType()
//            );
//            InOut save = inOutRepository.save(inOut);
//
//            List<InOut> inOutList = productBalancerSave.getInOutList() != null ? productBalancerSave.getInOutList() : new ArrayList<>();
//            inOutList.add(save);
//            productBalancerSave.setInOutList(inOutList);
//
//            ProductBalancer save1 = productBalancerRepository.save(productBalancerSave);
//            list.add(save1);
//
//            if (ans) {
//                storage.setProductBalancerList(list);
//            } else {
//                storage.setExistingProduct(list);
//            }
//            storageRepository.save(storage);
//        }
//    }
//
//    public void addExistingProductCook(Storage storage, InOutProduct inOutProduct, boolean ans, BigDecimal number, BigDecimal weight) {
//
//        List<ProductBalancer> list;
//
//        if (ans) {
//            list = storage.getProductBalancerList() != null ? storage.getProductBalancerList() : new ArrayList<>();
//        } else {
//            list = storage.getExistingProduct() != null ? storage.getExistingProduct() : new ArrayList<>();
//        }
//
//        boolean res = false;
//
//
//        for (ProductBalancer productBalancer : list) {
//
//            if (productBalancer.getProduct().equals(inOutProduct.getProduct())) {
//
//                InOut inOut = new InOut(
//                        new Timestamp(System.currentTimeMillis()),
//                        inOutProduct.getPrice(),
//                        weight,
//                        inOutProduct.getPack(),
//                        number,
//                        Status.ACCEPTED.getName(),
//                        inOutProduct.getMeasurementType()
//                );
//
//                InOut save = inOutRepository.save(inOut);
//
//                List<InOut> inOutList = productBalancer.getInOutList();
//                inOutList.add(save);
//                productBalancer.setInOutList(inOutList);
//                productBalancerRepository.save(productBalancer);
//                res = true;
//            }
//        }
//
//        if (!res) {
//
//            ProductBalancer productBalancer = new ProductBalancer();
//
//            productBalancer.setProduct(inOutProduct.getProduct());
//            ProductBalancer productBalancerSave = productBalancerRepository.save(productBalancer);
//
//            InOut inOut = new InOut(
//                    new Timestamp(System.currentTimeMillis()),
//                    inOutProduct.getPrice(),
//                    weight,
//                    inOutProduct.getPack(),
//                    number,
//                    Status.ACCEPTED.getName(),
//                    inOutProduct.getMeasurementType()
//            );
//            InOut save = inOutRepository.save(inOut);
//
//            List<InOut> inOutList = productBalancerSave.getInOutList() != null ? productBalancerSave.getInOutList() : new ArrayList<>();
//            inOutList.add(save);
//            productBalancerSave.setInOutList(inOutList);
//
//            ProductBalancer save1 = productBalancerRepository.save(productBalancerSave);
//            list.add(save1);
//
//            if (ans) {
//                storage.setProductBalancerList(list);
//            } else {
//                storage.setExistingProduct(list);
//            }
//            storageRepository.save(storage);
//        }
//    }
//
//    public BigDecimal calculateWeight(String orderName, Kindergarten kindergarten, Product product, BigDecimal weight) {
//        OrderMenu orderMenu = orderMenuRepository.findByOrderNumber(orderName).get();
//        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {
//            if (orderKindergarten.getKindergarten().equals(kindergarten)) {
//                for (WeightProduct weightProduct : orderKindergarten.getWeightProducts()) {
//                    if (weightProduct.getProduct().equals(product)) {
//
//                        if (weightProduct.getWeight().compareTo(weight) > 0) {
//
//                            weightProduct.setWeight(weightProduct.getWeight().subtract(weight));
//                            weightProductRepository.save(weightProduct);
//                            return weight;
//                        } else {
//                            BigDecimal weight1 = weightProduct.getWeight();
//
//                            weightProduct.setWeight(BigDecimal.valueOf(0));
//                            weightProductRepository.save(weightProduct);
//                            return weight1;
//                        }
//                    }
//                }
//            }
//        }
//        return BigDecimal.valueOf(0);
//    }
//
//    public void addExistingProduct(Storage storage, BigDecimal numberPack, BigDecimal weightPack, Product product, BigDecimal price, BigDecimal pack) {
//
//        List<ProductBalancer> list;
//
//
//        list = storage.getProductBalancerList() != null ? storage.getProductBalancerList() : new ArrayList<>();
//
//
//        boolean res = false;
//
//
//        for (ProductBalancer productBalancer : list) {
//
//            if (productBalancer.getProduct().equals(product)) {
//
//                InOut inOut = new InOut(
//                        new Timestamp(System.currentTimeMillis()),
//                        price,
//                        weightPack,
//                        pack,
//                        numberPack,
//                        Status.ACCEPTED.getName(),
//                        product.getMeasurementType()
//                );
//
//                InOut save = inOutRepository.save(inOut);
//
//                List<InOut> inOutList = productBalancer.getInOutList();
//                inOutList.add(save);
//                productBalancer.setInOutList(inOutList);
//                productBalancerRepository.save(productBalancer);
//                res = true;
//            }
//        }
//
//        if (!res) {
//
//            ProductBalancer productBalancer = new ProductBalancer();
//
//            productBalancer.setProduct(product);
//            ProductBalancer productBalancerSave = productBalancerRepository.save(productBalancer);
//
//            InOut inOut = new InOut(
//                    new Timestamp(System.currentTimeMillis()),
//                    price,
//                    weightPack,
//                    pack,
//                    numberPack,
//                    Status.ACCEPTED.getName(),
//                    product.getMeasurementType()
//            );
//
//            InOut save = inOutRepository.save(inOut);
//
//            List<InOut> inOutList = productBalancerSave.getInOutList() != null ? productBalancerSave.getInOutList() : new ArrayList<>();
//            inOutList.add(save);
//            productBalancerSave.setInOutList(inOutList);
//
//            ProductBalancer save1 = productBalancerRepository.save(productBalancerSave);
//            list.add(save1);
//
//
//            storage.setProductBalancerList(list);
//
//            storageRepository.save(storage);
//
//        }
//    }
//
//

//YANGI TAXRIR

    public InOutProductResponseDTO parse(InOutProduct inOutProduct){
        return new InOutProductResponseDTO(
                inOutProduct.getWeight(),
                inOutProduct.getNumberPack(),
                inOutProduct.getProduct().getName(),
                inOutProduct.getProduct().getPack()
        );
    }

    public StateMessage receiveKin(UUID id, ReceiveDto dto, ResponseUser responseUser) {
        return null;
    }

    public StateMessage receiveCom(UUID id, ReceiveDto dto, ResponseUser responseUser) {
        return null;
    }
}
