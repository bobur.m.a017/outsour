package com.smart_solution.outsource.inOutProduct;


import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.product.Product;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class InOutProduct {

    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal numberPack;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;


    public InOutProduct(BigDecimal weight, Product product) {
        this.weight = weight;
        this.product = product;
    }

    public InOutProduct(BigDecimal weight, BigDecimal numberPack, Product product) {
        this.weight = weight;
        this.numberPack = numberPack;
        this.product = product;
    }

    public InOutProduct() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
