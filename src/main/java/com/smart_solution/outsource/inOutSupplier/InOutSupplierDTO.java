package com.smart_solution.outsource.inOutSupplier;

import com.sun.istack.NotNull;

public class   InOutSupplierDTO {

    @NotNull
    private Double price;
    @NotNull
    private Double numberPack;
    @NotNull
    private Double weight;
    private String comment;
    @NotNull
    private boolean paymentStatus;
    @NotNull
    private boolean typeOfPayment;


    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public boolean isTypeOfPayment() {
        return typeOfPayment;
    }

    public void setTypeOfPayment(boolean typeOfPayment) {
        this.typeOfPayment = typeOfPayment;
    }

    public InOutSupplierDTO() {
    }


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(Double numberPack) {
        this.numberPack = numberPack;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
