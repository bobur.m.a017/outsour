package com.smart_solution.outsource.order.numberOfKids.numberOfKidsDTO;

public class NumberOfKidsDTO {
    private String ageGroupName;
    private Long number;

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }
}
