package com.smart_solution.outsource.order.numberOfKids.numberOfKidsDTO;

import java.util.List;

public class NumberOfKidsKinDTO {
    private List<NumberOfKidsDateDTO> numberOfKidsDateDTOList;
    private String kindergartenName;


    public List<NumberOfKidsDateDTO> getNumberOfKidsDateDTOList() {
        return numberOfKidsDateDTOList;
    }

    public void setNumberOfKidsDateDTOList(List<NumberOfKidsDateDTO> numberOfKidsDateDTOList) {
        this.numberOfKidsDateDTOList = numberOfKidsDateDTOList;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }
}
