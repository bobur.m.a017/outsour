package com.smart_solution.outsource.order.numberOfKids;

import lombok.Getter;
import lombok.Setter;

public class NumberOfKidsResponseDTO {

    private Integer id;
    private Integer ageGroupId;
    private String ageGroupName;
    private Integer number;


    public NumberOfKidsResponseDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
