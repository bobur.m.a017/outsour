package com.smart_solution.outsource.order.numberOfKids;

import lombok.Getter;
import lombok.Setter;

public class NumberOfKidsDTO {

    private Integer id;
    private Integer ageGroupId;
    private Integer number;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public NumberOfKidsDTO() {
    }
}
