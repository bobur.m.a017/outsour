package com.smart_solution.outsource.order.numberOfKids;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NumberOfKidsRepository extends JpaRepository<NumberOfKids, Integer> {
}
