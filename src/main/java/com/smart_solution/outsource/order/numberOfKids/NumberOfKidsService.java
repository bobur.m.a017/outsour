package com.smart_solution.outsource.order.numberOfKids;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import org.springframework.stereotype.Service;

@Service
public record NumberOfKidsService(
        NumberOfKidsRepository numberOfKidsRepository,
        AgeGroupRepository ageGroupRepository
) {
    public NumberOfKids add(NumberOfKidsDTO dto) {

        NumberOfKids numberOfKids = new NumberOfKids();

        numberOfKids.setNumber(dto.getNumber());

        AgeGroup ageGroup = ageGroupRepository.findById(dto.getAgeGroupId()).get();

        numberOfKids.setAgeGroup(ageGroup);
        numberOfKids.setAgeGroupName(ageGroup.getName());

        return numberOfKids;
    }

    public NumberOfKidsResponseDTO parse(NumberOfKids numberOfKids) {

        NumberOfKidsResponseDTO dto = new NumberOfKidsResponseDTO();
        dto.setId(numberOfKids.getId());
        dto.setNumber(numberOfKids.getNumber());
        dto.setAgeGroupId(numberOfKids.getAgeGroup().getId());
        dto.setAgeGroupName(numberOfKids.getAgeGroup().getName());

        return dto;
    }
}
