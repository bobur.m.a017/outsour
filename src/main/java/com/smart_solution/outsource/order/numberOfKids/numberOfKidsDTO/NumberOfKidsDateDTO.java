package com.smart_solution.outsource.order.numberOfKids.numberOfKidsDTO;

import java.sql.Timestamp;
import java.util.List;

public class NumberOfKidsDateDTO {
    private List<NumberOfKidsDTO> numberOfKidsDTOList;
    private Timestamp date;

    public List<NumberOfKidsDTO> getNumberOfKidsDTOList() {
        return numberOfKidsDTOList;
    }

    public void setNumberOfKidsDTOList(List<NumberOfKidsDTO> numberOfKidsDTOList) {
        this.numberOfKidsDTOList = numberOfKidsDTOList;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
