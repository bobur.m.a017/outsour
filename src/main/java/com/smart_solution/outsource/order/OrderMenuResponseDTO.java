package com.smart_solution.outsource.order;

import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.inOutProduct.InOutProductResponseDTO;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergartenResponseDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.List;

public class OrderMenuResponseDTO {

    private Integer id;
    private String number;
    private String status;

    private Timestamp createDate;
    private Timestamp updateDate;


    // Xar bir zayafka yaratilayotgan MTTlar;
    private List<OrderKindergartenResponseDTO> orderKindergartenList;


    //Ushbu zayafkaga umumiy qancha maxsulot kerakligi.
    // (Bunda orderKindergartenList ning requiredProductlar yig`indisi xisoblanbadi)
    private List<InOutProductResponseDTO> detectedProduct;


    //Korxona omborida mavjud maxsulotlar;
    private List<InOutProductResponseDTO> existingProduct;


    //Ushbu zayafka to`liq yopilishi uchun kerakli maxsulot.
    // (Bunda umumiy maxsulotdan omborda mavjud maxsulot ayriladi).
    private List<InOutProductResponseDTO> requiredProduct;


    public OrderMenuResponseDTO() {
    }

    public OrderMenuResponseDTO(Integer id, String number, String status, Timestamp createDate, Timestamp updateDate) {
        this.id = id;
        this.number = number;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<OrderKindergartenResponseDTO> getOrderKindergartenList() {
        return orderKindergartenList;
    }

    public void setOrderKindergartenList(List<OrderKindergartenResponseDTO> orderKindergartenList) {
        this.orderKindergartenList = orderKindergartenList;
    }

    public List<InOutProductResponseDTO> getDetectedProduct() {
        return detectedProduct;
    }

    public void setDetectedProduct(List<InOutProductResponseDTO> detectedProduct) {
        this.detectedProduct = detectedProduct;
    }

    public List<InOutProductResponseDTO> getExistingProduct() {
        return existingProduct;
    }

    public void setExistingProduct(List<InOutProductResponseDTO> existingProduct) {
        this.existingProduct = existingProduct;
    }

    public List<InOutProductResponseDTO> getRequiredProduct() {
        return requiredProduct;
    }

    public void setRequiredProduct(List<InOutProductResponseDTO> requiredProduct) {
        this.requiredProduct = requiredProduct;
    }
}
