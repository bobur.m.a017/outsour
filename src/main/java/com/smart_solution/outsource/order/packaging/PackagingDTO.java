package com.smart_solution.outsource.order.packaging;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

public class PackagingDTO {
    private Integer productId;
    private Double pack;

    public PackagingDTO() {
    }


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getPack() {
        return pack;
    }

    public void setPack(Double pack) {
        this.pack = pack;
    }
}
