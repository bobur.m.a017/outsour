package com.smart_solution.outsource.order;

import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.agreement.AgreementRepository;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergartenRepository;
import com.smart_solution.outsource.agreement.price.AgreementPrice;
import com.smart_solution.outsource.agreement.product.AgreementProduct;
import com.smart_solution.outsource.agreement.product.AgreementProductRepository;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.dto.AgreementOrderDTO;
import com.smart_solution.outsource.dto.SendProductDTO;
import com.smart_solution.outsource.dto.StringDto;
import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.inOutProduct.InOutProductRepository;
import com.smart_solution.outsource.inOutProduct.InOutProductResponseDTO;
import com.smart_solution.outsource.inOutProduct.InOutProductService;
import com.smart_solution.outsource.kindergarten.*;
import com.smart_solution.outsource.kindergarten.supplier.SupplierKinProductDTO;
import com.smart_solution.outsource.kindergarten.supplier.SupplierKinProductResponseDTO;
import com.smart_solution.outsource.kindergarten.supplier.SupplierKindergartenDTO;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProduct;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProductRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveService;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import com.smart_solution.outsource.order.agreement.AgreementOrder;
import com.smart_solution.outsource.order.agreement.AgreementOrderRepository;
import com.smart_solution.outsource.order.dto.OrderProductDTO;
import com.smart_solution.outsource.perDay.PerDay;
import com.smart_solution.outsource.perDay.PerDayRepository;
import com.smart_solution.outsource.order.dto.MenuSaveIdDate;
import com.smart_solution.outsource.order.numberOfKids.numberOfKidsDTO.NumberOfKidsDTO;
import com.smart_solution.outsource.order.numberOfKids.numberOfKidsDTO.NumberOfKidsDateDTO;
import com.smart_solution.outsource.order.numberOfKids.numberOfKidsDTO.NumberOfKidsKinDTO;
import com.smart_solution.outsource.order.orderKindergarten.*;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightDTO;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightRepository;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProductDTO;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProductRepository;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.productBalancer.ProductBalancerRepository;
import com.smart_solution.outsource.productSuppiler.ProductSupplier;
import com.smart_solution.outsource.productSuppiler.ProductSupplierRepository;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.storage.StorageService;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompanyRepository;
import com.smart_solution.outsource.supplier.SupplierRepository;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;

@Service
public record
OrderMenuServiceNew(
        OrderMenuRepository orderMenuRepository,
        UsersRepository usersRepository,
        CompanyRepository companyRepository,
        OrderKindergartenService orderKindergartenService,
        MenuSaveRepository menuSaveRepository,
        InOutProductRepository inOutProductRepository,
        InOutProductService inOutProductService,
        SupplierRepository supplierRepository,
        ProductRepository productRepository,
        ProductBalancerRepository productBalancerRepository,
        OrderKindergartenRepository orderKindergartenRepository,
        MenuWeightRepository menuWeightRepository,
        KindergartenRepository kindergartenRepository,
        AgeGroupRepository ageGroupRepository,
        MenuSaveService menuSaveService,
        ProductSupplierRepository productSupplierRepository,
        ProductsToBeShippedRepository productsToBeShippedRepository,
        ProductsToBeShippedCompanyRepository productsToBeShippedCompanyRepository,
        StorageService storageService,
        PerDayRepository perDayRepository,
        WeightProductRepository weightProductRepository,
        AgreementKindergartenRepository agreementKindergartenRepository,
        AgreementRepository agreementRepository,
        AgreementOrderRepository agreementOrderRepository,
        AgreementProductRepository agreementProductRepository,
        CheckProductRepository checkProductRepository
) {


    public StateMessage add(ResponseUser responseUser, String number) {

        boolean res = orderMenuRepository.existsByOrderNumber(number);

        if (!res) {
            Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

            OrderMenu save = orderMenuRepository.save(new OrderMenu(number, company, Status.CREATED.getName()));

            List<OrderMenu> orderList = company.getOrderList();
            orderList.add(save);
            company.setOrderList(orderList);
            companyRepository.save(company);

            return new StateMessage("Muvaffaqiyatli qo`shildi", true);

        } else {
            return new StateMessage("Ushbu raqam bilan avval zayafka yaratilgan", false);
        }
    }

    public StateMessage addKin(OrderKindergartenDTO dto, Integer id) {
        OrderMenu orderMenu = orderMenuRepository.findById(id).get();

        if (orderMenu.getStatus().equals(Status.CREATED.getName())) {
            Kindergarten kindergarten = kindergartenRepository.findById(dto.getKindergartenId()).get();

            OrderKindergarten orderKindergarten = orderKindergartenService.addOrderKindergarten(dto, orderMenu, kindergarten);

            List<OrderKindergarten> orderKindergartenList = orderMenu.getOrderKindergartenList();
            orderKindergartenList.add(orderKindergarten);
            orderMenu.setOrderKindergartenList(orderKindergartenList);
            OrderMenu orderSave = orderMenuRepository.save(orderMenu);
            calculate(orderSave);

            return new StateMessage(kindergarten.getName() + " muvaffaqiyatli zayafkaga qo`shildi", true);
        }
        return new StateMessage("O`zgartirish amalga oshmadi. Zayafka yopilgan", false);
    }

    public void calculate(OrderMenu orderMenu) {
        List<InOutProduct> detectedProduct = calculateDetectedProduct(orderMenu);
        orderMenu.setDetectedProduct(detectedProduct);

        List<InOutProduct> existingProduct = calculateExistingProduct(orderMenu, orderMenu.getCompany());
        orderMenu.setExistingProduct(existingProduct);

        List<InOutProduct> requiredProduct = calculateRequiredProduct(orderMenu);
        orderMenu.setRequiredProduct(requiredProduct);

        orderMenuRepository.save(orderMenu);
    }

    public StateMessage edit(StringDto editDto, Integer id) {

        OrderMenu orderMenu = orderMenuRepository.findById(id).get();

        if (!orderMenu.getStatus().equals(Status.CREATED.getName())) {
            return new StateMessage("Zayafkani o`zgartirib bo`lmaydi.", false);
        }

        if (editDto.getMenuKinIdList().size() > 0) {
            for (Integer orderKinId : editDto.getMenuKinIdList()) {
                deleteMenuKin(orderKinId, orderMenu);
                calculate(orderMenu);
            }
        }

        if (!orderMenu.getOrderNumber().equals(editDto.getNumber())) {
            boolean res = orderMenuRepository.existsByOrderNumber(editDto.getNumber());

            if (!res) {

                orderMenu.setOrderNumber(editDto.getNumber());
                orderMenuRepository.save(orderMenu);

                return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);
            } else {
                return new StateMessage("Ushbu raqam bilan avval zayafka yaratilgan", false);
            }
        }

        return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);
    }

    public void deleteMenuKin(Integer id, OrderMenu orderMenu) {

        OrderKindergarten orderKindergarten = orderKindergartenRepository.findById(id).get();

        if (orderKindergarten.getKindergarten().getType().equals(KindergartenType.TAMINOT.getName())) {

            Kindergarten kindergarten = orderKindergarten.getKindergarten();

            for (InOutProduct inOutProduct : orderKindergarten.getDetectedProduct()) {

                List<AgreementOrder> list = agreementOrderRepository.findAllByOrderKindergartenAndProduct(orderKindergarten, inOutProduct.getProduct());

                for (AgreementOrder agreementOrder : list) {
                    Optional<AgreementProduct> optional = agreementProductRepository.findByAgreementKindergarten_AgreementAndAgreementKindergarten_KindergartenAndProduct(agreementOrder.getAgreement(), kindergarten, inOutProduct.getProduct());

                    if (optional.isPresent()) {
                        AgreementProduct agreementProduct = optional.get();
                        agreementProduct.setSuccessWeight(agreementProduct.getSuccessWeight().subtract(agreementOrder.getWeight()));
                        agreementProduct.setResidue(agreementProduct.getWeight().subtract(agreementProduct.getSuccessWeight()));
                        agreementProductRepository.save(agreementProduct);
                    }
                    orderKindergartenService.calculateAgreement(agreementOrder.getAgreement());
                    checkAgreementWeight(agreementOrder.getAgreement());
                }
            }
        }

        List<OrderKindergarten> orderKindergartenList = orderMenu.getOrderKindergartenList();

        orderKindergartenList.remove(orderKindergarten);
        orderKindergarten.setOrderMenu(null);
        orderKindergartenRepository.save(orderKindergarten);
        orderMenu.setOrderKindergartenList(orderKindergartenList);
        OrderMenu save = orderMenuRepository.save(orderMenu);


        List<InOutProduct> deleteIOP = new ArrayList<>();


        if (save.getOrderKindergartenList().size() == 0) {

            deleteIOP.addAll(save.getRequiredProduct());
            deleteIOP.addAll(save.getDetectedProduct());
            deleteIOP.addAll(save.getRequiredProduct());
            save.setDetectedProduct(null);
            save.setRequiredProduct(null);
            save.setExistingProduct(null);
            orderMenuRepository.save(save);
        }
        List<WeightProduct> deleteWP = new ArrayList<>();
        List<MenuWeight> deleteMW = new ArrayList<>();
        List<PerDay> deletePD = new ArrayList<>();
        List<MenuSave> menuSaveList = new ArrayList<>();

        deleteIOP.addAll(orderKindergarten.getDetectedProduct());
        deleteIOP.addAll(orderKindergarten.getExistingProduct());
        deleteIOP.addAll(orderKindergarten.getRequiredProduct());
        deleteIOP.addAll(orderKindergarten.getRequiredProductPack());
        for (MenuWeight menuWeight : orderKindergarten.getMenuWeightList()) {
            if (menuWeight.getWeightProducts() != null)
                deleteWP.addAll(menuWeight.getWeightProducts());
            menuWeight.setWeightProducts(null);
            deleteMW.add(menuWeight);
            MenuSave menuSave = menuWeight.getMenuSave();
            PerDay numberToGuess = menuSave.getNumberToGuess();
            deletePD.add(numberToGuess);
            menuSave.setStatus(Status.CREATED.getName());
            menuSave.setNumberToGuess(null);
            menuSave.setState(false);
            menuSaveList.add(menuSave);
        }
        orderKindergarten.setDetectedProduct(null);
        orderKindergarten.setExistingProduct(null);
        orderKindergarten.setRequiredProductPack(null);
        orderKindergarten.setRequiredProduct(null);
        orderKindergarten.setMenuWeightList(null);
        orderKindergarten.setKindergarten(null);

        List<MenuWeight> menuWeights = menuWeightRepository.saveAll(deleteMW);
        menuSaveRepository.saveAll(menuSaveList);
        List<PerDay> perDays = perDayRepository.saveAll(deletePD);
        OrderKindergarten orderKindergartens = orderKindergartenRepository.save(orderKindergarten);
        inOutProductRepository.deleteAll(deleteIOP);
        weightProductRepository.deleteAll(deleteWP);
        menuWeightRepository.deleteAll(menuWeights);
        perDayRepository.deleteAll(perDays);

        List<AgreementOrder> allByOrderKindergarten = agreementOrderRepository.findAllByOrderKindergarten(orderKindergartens);
        agreementOrderRepository.deleteAll(allByOrderKindergarten);

        orderKindergartenRepository.delete(orderKindergartens);
    }

    public StateMessage delete(Integer id, ResponseUser responseUser) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();
        OrderMenu orderMenu = orderMenuRepository.findById(id).get();

        if (!orderMenu.getStatus().equals(Status.CREATED.getName())) {
            return new StateMessage("Zayafkani o`zgartirib bo`lmaydi.", false);
        }


        List<OrderMenu> orderList = company.getOrderList();
        orderList.remove(orderMenu);
        company.setOrderList(orderList);
        companyRepository.save(company);

        List<InOutProduct> deleteIOP = new ArrayList<>();
        List<WeightProduct> deleteWP = new ArrayList<>();
        List<MenuWeight> deleteMW = new ArrayList<>();
        List<OrderKindergarten> deleteOK = new ArrayList<>();
        List<PerDay> deletePD = new ArrayList<>();
        List<MenuSave> menuSaveList = new ArrayList<>();
        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {
            deleteIOP.addAll(orderKindergarten.getDetectedProduct());
            deleteIOP.addAll(orderKindergarten.getExistingProduct());
            deleteIOP.addAll(orderKindergarten.getRequiredProduct());
            deleteIOP.addAll(orderKindergarten.getRequiredProductPack());
            for (MenuWeight menuWeight : orderKindergarten.getMenuWeightList()) {
                if (menuWeight.getWeightProducts() != null)
                    deleteWP.addAll(menuWeight.getWeightProducts());
                menuWeight.setWeightProducts(null);
                deleteMW.add(menuWeight);
                MenuSave menuSave = menuWeight.getMenuSave();
                PerDay numberToGuess = menuSave.getNumberToGuess();
                deletePD.add(numberToGuess);
                menuSave.setStatus(Status.CREATED.getName());
                menuSave.setNumberToGuess(null);
                menuSave.setState(false);
                menuSaveList.add(menuSave);
            }
            orderKindergarten.setDetectedProduct(null);
            orderKindergarten.setExistingProduct(null);
            orderKindergarten.setRequiredProductPack(null);
            orderKindergarten.setRequiredProduct(null);
            orderKindergarten.setMenuWeightList(null);
            orderKindergarten.setKindergarten(null);
            deleteOK.add(orderKindergarten);
        }
        deleteIOP.addAll(orderMenu.getDetectedProduct());
        deleteIOP.addAll(orderMenu.getRequiredProduct());
        deleteIOP.addAll(orderMenu.getExistingProduct());
        orderMenu.setDetectedProduct(null);
        orderMenu.setExistingProduct(null);
        orderMenu.setRequiredProduct(null);

        List<MenuWeight> menuWeights = menuWeightRepository.saveAll(deleteMW);
        menuSaveRepository.saveAll(menuSaveList);
        List<PerDay> perDays = perDayRepository.saveAll(deletePD);
        List<OrderKindergarten> orderKindergartens = orderKindergartenRepository.saveAll(deleteOK);
        OrderMenu save = orderMenuRepository.save(orderMenu);

        inOutProductRepository.deleteAll(deleteIOP);
        weightProductRepository.deleteAll(deleteWP);
        menuWeightRepository.deleteAll(menuWeights);
        perDayRepository.deleteAll(perDays);
        orderKindergartenRepository.deleteAll(orderKindergartens);

        orderMenuRepository.delete(save);
        return new StateMessage("Muvaffaqiyatli o`chirildi", true);
    }

    public List<MenuSaveIdDate> getDateKin(Integer id) {

        List<MenuSaveIdDate> list = new ArrayList<>();
        List<MenuSave> list2 = menuSaveRepository.findAllByKindergarten_IdAndState(id, false);

        for (MenuSave menuSave : list2) {

            list.add(new MenuSaveIdDate(menuSave.getId(), menuSave.getDayDate()));

        }

        list.sort(Comparator.comparing(MenuSaveIdDate::getDate));
        return list;
    }

    public List<OrderMenuDTO> getAll(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        List<OrderMenuDTO> list = new ArrayList<>();
        List<OrderMenu> orderList;

        if (users.getCompany() == null) {
            orderList = orderMenuRepository.findAll();
        } else {
            orderList = users.getCompany().getOrderList();
        }

        for (OrderMenu orderMenu : orderList) {

            list.add(new OrderMenuDTO(orderMenu.getId(), orderMenu.getOrderNumber(), orderMenu.getStatus(), orderMenu.getCreateDate()));

        }
        list.sort(Comparator.comparing(OrderMenuDTO::getCreateDate));
        return list;
    }

    public OrderMenuResponseDTO get(Integer id) {

        OrderMenu orderMenu = orderMenuRepository.findById(id).get();

        OrderMenuResponseDTO dto = new OrderMenuResponseDTO(
                orderMenu.getId(),
                orderMenu.getOrderNumber(),
                orderMenu.getStatus(),
                orderMenu.getCreateDate(),
                orderMenu.getUpdateDate()
        );

        List<InOutProductResponseDTO> detectedProduct = getAllProductNameListIOP();
        for (InOutProduct inOutProduct : orderMenu.getDetectedProduct()) {
            for (InOutProductResponseDTO a : detectedProduct) {
                if (a.getProductName().equals(inOutProduct.getProduct().getName())) {
                    InOutProductResponseDTO parse = inOutProductService.parse(inOutProduct);
                    a.setWeight(parse.getWeight());
                    a.setNumberPack(parse.getNumberPack());
                }
            }
        }
        dto.setDetectedProduct(detectedProduct);

        List<InOutProductResponseDTO> existingProduct = getAllProductNameListIOP();
        for (InOutProduct inOutProduct : orderMenu.getExistingProduct()) {
            for (InOutProductResponseDTO a : existingProduct) {
                if (a.getProductName().equals(inOutProduct.getProduct().getName())) {
                    InOutProductResponseDTO parse = inOutProductService.parse(inOutProduct);
                    a.setWeight(parse.getWeight());
                    a.setNumberPack(parse.getNumberPack());
                }
            }
        }
        dto.setExistingProduct(existingProduct);


        List<InOutProductResponseDTO> requiredProduct = getAllProductNameListIOP();
        for (InOutProduct inOutProduct : orderMenu.getRequiredProduct()) {

            for (InOutProductResponseDTO a : requiredProduct) {
                if (a.getProductName().equals(inOutProduct.getProduct().getName())) {
                    InOutProductResponseDTO parse = inOutProductService.parse(inOutProduct);
                    a.setWeight(parse.getWeight());
                    a.setNumberPack(parse.getNumberPack());
                }
            }
        }
        dto.setRequiredProduct(requiredProduct);


        List<OrderKindergartenResponseDTO> orderKindergartenList = new ArrayList<>();
        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {
            orderKindergartenList.add(orderKindergartenService.parse(orderKindergarten));
        }
        dto.setOrderKindergartenList(orderKindergartenList);

//        return dto;
        return sortProduct(dto);
    }

    public List<InOutProductResponseDTO> getAllProductNameListIOP() {
        List<InOutProductResponseDTO> list = new ArrayList<>();

        for (Product product : productRepository.findAll()) {
            list.add(new InOutProductResponseDTO(BigDecimal.valueOf(0), BigDecimal.valueOf(0), product.getName(), product.getPack()));
        }
        return list;
    }

    public OrderMenuResponseDTO sortProduct(OrderMenuResponseDTO dto) {

        List<Product> deleteProduct = new ArrayList<>();
        for (Product product : productRepository.findAll()) {
            boolean res = true;

            for (InOutProductResponseDTO inOutProductResponseDTO : dto.getDetectedProduct()) {
                if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                    if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                        res = false;
                    }
                }
            }
            for (InOutProductResponseDTO inOutProductResponseDTO : dto.getRequiredProduct()) {
                if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                    if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                        res = false;
                    }
                }
            }
            for (InOutProductResponseDTO inOutProductResponseDTO : dto.getExistingProduct()) {
                if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                    if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                        res = false;
                    }
                }
            }
            for (OrderKindergartenResponseDTO orderKin : dto.getOrderKindergartenList()) {
                for (InOutProductResponseDTO inOutProductResponseDTO : orderKin.getDetectedProduct()) {
                    if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                        if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                            res = false;
                        }
                    }
                }
                for (InOutProductResponseDTO inOutProductResponseDTO : orderKin.getExistingProduct()) {
                    if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                        if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                            res = false;
                        }
                    }
                }
                for (InOutProductResponseDTO inOutProductResponseDTO : orderKin.getRequiredProduct()) {
                    if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                        if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                            res = false;
                        }
                    }
                }
                for (InOutProductResponseDTO inOutProductResponseDTO : orderKin.getRequiredProductPack()) {
                    if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                        if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                            res = false;
                        }
                    }
                }
                for (MenuWeightDTO menuWeightDTO : orderKin.getMenuWeightList()) {
                    for (WeightProductDTO inOutProductResponseDTO : menuWeightDTO.getWeightProducts()) {
                        if (inOutProductResponseDTO.getProductName().equals(product.getName())) {
                            if (inOutProductResponseDTO.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                                res = false;
                            }
                        }
                    }
                }
            }
            if (res) {
                deleteProduct.add(product);
            }
        }


        List<InOutProductResponseDTO> omd = new ArrayList<>();
        for (InOutProductResponseDTO inOutProductResponseDTO : dto.getDetectedProduct()) {
            if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                omd.add(inOutProductResponseDTO);
            }
        }
        dto.setDetectedProduct(omd);

        List<InOutProductResponseDTO> ome = new ArrayList<>();
        for (InOutProductResponseDTO inOutProductResponseDTO : dto.getExistingProduct()) {
            if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                ome.add(inOutProductResponseDTO);
            }
        }
        dto.setExistingProduct(ome);

        List<InOutProductResponseDTO> omr = new ArrayList<>();
        for (InOutProductResponseDTO inOutProductResponseDTO : dto.getRequiredProduct()) {
            if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                omr.add(inOutProductResponseDTO);
            }
        }
        dto.setRequiredProduct(omr);

        for (OrderKindergartenResponseDTO orderKindergartenResponseDTO : dto.getOrderKindergartenList()) {

            List<InOutProductResponseDTO> okd = new ArrayList<>();
            for (InOutProductResponseDTO inOutProductResponseDTO : orderKindergartenResponseDTO.getDetectedProduct()) {
                if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                    okd.add(inOutProductResponseDTO);
                }
            }
            orderKindergartenResponseDTO.setDetectedProduct(okd);

            List<InOutProductResponseDTO> oke = new ArrayList<>();
            for (InOutProductResponseDTO inOutProductResponseDTO : orderKindergartenResponseDTO.getExistingProduct()) {
                if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                    oke.add(inOutProductResponseDTO);
                }
            }
            orderKindergartenResponseDTO.setExistingProduct(oke);

            List<InOutProductResponseDTO> okr = new ArrayList<>();
            for (InOutProductResponseDTO inOutProductResponseDTO : orderKindergartenResponseDTO.getRequiredProduct()) {
                if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                    okr.add(inOutProductResponseDTO);
                }
            }
            orderKindergartenResponseDTO.setRequiredProduct(okr);

            List<InOutProductResponseDTO> okrp = new ArrayList<>();
            for (InOutProductResponseDTO inOutProductResponseDTO : orderKindergartenResponseDTO.getRequiredProductPack()) {
                if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                    okrp.add(inOutProductResponseDTO);
                }
            }
            orderKindergartenResponseDTO.setRequiredProductPack(okrp);


            for (MenuWeightDTO menuWeightDTO : orderKindergartenResponseDTO.getMenuWeightList()) {
                List<WeightProductDTO> wpd = new ArrayList<>();
                for (WeightProductDTO inOutProductResponseDTO : menuWeightDTO.getWeightProducts()) {
                    if (sortCheck(deleteProduct, inOutProductResponseDTO.getProductName())) {
                        wpd.add(inOutProductResponseDTO);
                    }
                }
                menuWeightDTO.setWeightProducts(wpd);
            }
        }


        dto.getDetectedProduct().sort(Comparator.comparing(InOutProductResponseDTO::getProductName));
        dto.getExistingProduct().sort(Comparator.comparing(InOutProductResponseDTO::getProductName));
        dto.getRequiredProduct().sort(Comparator.comparing(InOutProductResponseDTO::getProductName));

        for (OrderKindergartenResponseDTO orderKindergartenResponseDTO : dto.getOrderKindergartenList()) {
            for (MenuWeightDTO menuWeightDTO : orderKindergartenResponseDTO.getMenuWeightList()) {
                menuWeightDTO.getWeightProducts().sort(Comparator.comparing(WeightProductDTO::getProductName));
            }
            orderKindergartenResponseDTO.getDetectedProduct().sort(Comparator.comparing(InOutProductResponseDTO::getProductName));
            orderKindergartenResponseDTO.getExistingProduct().sort(Comparator.comparing(InOutProductResponseDTO::getProductName));
            orderKindergartenResponseDTO.getRequiredProduct().sort(Comparator.comparing(InOutProductResponseDTO::getProductName));
            orderKindergartenResponseDTO.getRequiredProductPack().sort(Comparator.comparing(InOutProductResponseDTO::getProductName));
        }


        return dto;
    }

    public boolean sortCheck(List<Product> list, String name) {
        for (Product product : list) {
            if (product.getName().equals(name)) {
                return false;
            }
        }
        return true;
    }


    public StateMessage send(Integer id, ResponseUser responseUser, List<Integer> productList) {

        OrderMenu orderMenu = orderMenuRepository.findById(id).get();
        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        sortingOfSelectedProducts(orderMenu, productList);
        addWeightToMenuSave(orderMenu);

        StateMessage stateMessage = checkSupplier(company, orderMenu);

        if (orderMenu.getStatus().equals(Status.CREATED.getName())) {
            if (!stateMessage.isSuccess()) {
                return new StateMessage("XATOLIK.    " + stateMessage.getText(), false);
            } else {
                orderMenu.setStatus(Status.SEND.getName());
                orderMenuRepository.save(orderMenu);
                sendSupplier(company, orderMenu);
                sendStorage(orderMenu, company);
                storageService.addShouldBeSentProduct(orderMenu, company);
                return new StateMessage("Zayafka muvofiyaqatli taminotchiga va omborchiga yuborildi.", true);
            }
        }
        return new StateMessage("Zayafka yuborib bo`lingan.", false);
    } //YUBORISH


    public void sortingOfSelectedProducts(OrderMenu orderMenu, List<Integer> list) {

        List<InOutProduct> delete = new ArrayList<>();
        List<WeightProduct> deletewp = new ArrayList<>();
        List<MenuWeight> menuWeightDelete = new ArrayList<>();
        List<OrderKindergarten> orderKindergartenList = new ArrayList<>();
        List<OrderKindergarten> okDelete = new ArrayList<>();
        List<InOutProduct> omd = new ArrayList<>();
        List<InOutProduct> omr = new ArrayList<>();
        List<InOutProduct> ome = new ArrayList<>();

        for (InOutProduct inOutProduct : orderMenu.getDetectedProduct()) {
            boolean res = true;
            for (Integer integer : list) {
                if (integer.equals(inOutProduct.getProduct().getId())) {
                    res = false;
                    omd.add(inOutProduct);
                }
            }
            if (res) {
                delete.add(inOutProduct);
            }
        }

        for (InOutProduct inOutProduct : orderMenu.getRequiredProduct()) {
            boolean res = true;
            for (Integer integer : list) {
                if (integer.equals(inOutProduct.getProduct().getId())) {
                    res = false;
                    omr.add(inOutProduct);
                }
            }
            if (res) {
                delete.add(inOutProduct);
            }
        }

        for (InOutProduct inOutProduct : orderMenu.getExistingProduct()) {
            boolean res = true;
            for (Integer integer : list) {
                if (integer.equals(inOutProduct.getProduct().getId())) {
                    res = false;
                    ome.add(inOutProduct);
                }
            }
            if (res) {
                delete.add(inOutProduct);
            }
        }

        orderMenu.setDetectedProduct(omd);
        orderMenu.setExistingProduct(ome);
        orderMenu.setRequiredProduct(omr);

        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {
            List<InOutProduct> okd = new ArrayList<>();
            List<InOutProduct> okr = new ArrayList<>();
            List<InOutProduct> oke = new ArrayList<>();
            List<InOutProduct> okp = new ArrayList<>();

            for (InOutProduct inOutProduct : orderKindergarten.getDetectedProduct()) {
                boolean res = true;
                for (Integer integer : list) {
                    if (integer.equals(inOutProduct.getProduct().getId())) {
                        res = false;
                        okd.add(inOutProduct);
                    }
                }
                if (res) {
                    delete.add(inOutProduct);
                }
            }

            for (InOutProduct inOutProduct : orderKindergarten.getRequiredProduct()) {
                boolean res = true;
                for (Integer integer : list) {
                    if (integer.equals(inOutProduct.getProduct().getId())) {
                        res = false;
                        okr.add(inOutProduct);
                    }
                }
                if (res) {
                    delete.add(inOutProduct);
                }
            }

            for (InOutProduct inOutProduct : orderKindergarten.getExistingProduct()) {
                boolean res = true;
                for (Integer integer : list) {
                    if (integer.equals(inOutProduct.getProduct().getId())) {
                        res = false;
                        oke.add(inOutProduct);
                    }
                }
                if (res) {
                    delete.add(inOutProduct);
                }
            }

            for (InOutProduct inOutProduct : orderKindergarten.getRequiredProductPack()) {
                boolean res = true;
                for (Integer integer : list) {
                    if (integer.equals(inOutProduct.getProduct().getId())) {
                        res = false;
                        okp.add(inOutProduct);
                    }
                }
                if (res) {
                    delete.add(inOutProduct);
                }
            }

            orderKindergarten.setRequiredProduct(okr);
            orderKindergarten.setExistingProduct(oke);
            orderKindergarten.setDetectedProduct(okd);
            orderKindergarten.setRequiredProductPack(okp);

            List<MenuWeight> menuWeightList = new ArrayList<>();

            for (MenuWeight menuWeight : orderKindergarten.getMenuWeightList()) {
                List<WeightProduct> wp = new ArrayList<>();

                for (WeightProduct weightProduct : menuWeight.getWeightProducts()) {
                    boolean res = true;
                    for (Integer integer : list) {
                        if (integer.equals(weightProduct.getProduct().getId())) {
                            res = false;
                            wp.add(weightProduct);
                        }
                    }
                    if (res) {
                        deletewp.add(weightProduct);
                    }
                }
                if (wp.size() == 0) {
                    menuWeight.setWeightProducts(null);
                    menuWeightDelete.add(menuWeight);
                } else {
                    menuWeight.setWeightProducts(weightProductRepository.saveAll(wp));
                    menuWeightList.add(menuWeight);
                }
            }
            if (menuWeightList.size() == 0) {
                orderKindergarten.setOrderMenu(null);
                okDelete.add(orderKindergarten);
            } else {
                orderKindergarten.setMenuWeightList(menuWeightRepository.saveAll(menuWeightList));
                orderKindergartenList.add(orderKindergarten);
            }
        }

        inOutProductRepository.deleteAll(delete);
        weightProductRepository.deleteAll(deletewp);
        menuWeightRepository.deleteAll(menuWeightDelete);
        orderMenu.setOrderKindergartenList(orderKindergartenRepository.saveAll(orderKindergartenList));

        orderMenuRepository.save(orderMenu);
    }

    public void addWeightToMenuSave(OrderMenu orderMenu) {
        List<MenuSave> list = new ArrayList<>();
        List<CheckProduct> checkProductList = new ArrayList<>();

        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {
            for (MenuWeight menuWeight : orderKindergarten.getMenuWeightList()) {

                MenuSave menuSave = menuWeight.getMenuSave();
                List<WeightProduct> weightProducts = menuWeight.getWeightProducts();
                for (WeightProduct weightProduct : weightProducts) {
//                    Product product = weightProduct.getProduct();
                    Integer productId = weightProduct.getMaxsulotId();
                    CheckProduct checkProduct = checkProductRepository.findByMenuSaveIdAndProductId(menuSave.getId(), productId);
                    checkProduct.setWeight(weightProduct.getWeight());
                    checkProduct.setCondition(Status.MAXSULOT_YUBORILDI.getName());
                    checkProductList.add(checkProduct);
                }
                list.add(menuSave);
            }
        }

        checkProductRepository.saveAll(checkProductList);

        for (MenuSave menuSave : list) {
            List<CheckProduct> ch1 = checkProductRepository.findAllByMenuSaveId(menuSave.getId());
            List<CheckProduct> ch2 = checkProductRepository.findAllByMenuSaveIdAndCondition(menuSave.getId(), Status.MAXSULOT_YUBORILDI.getName());
            if (ch1.size() == ch2.size()) {
                menuSave.setStatus(Status.MAXSULOT_YUBORILDI.getName());
            }
        }

        menuSaveRepository.saveAll(list);
    }

    public void sendSupplier(Company company, OrderMenu orderMenu) {

        List<ProductsToBeShipped> productsToBeShippedList = new ArrayList<>();

        for (InOutProduct inOutProduct : orderMenu.getRequiredProduct()) {
            if (inOutProduct.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                ProductSupplier productSupplier = productSupplierRepository.findByCompanyAndProduct(company, inOutProduct.getProduct()).get();
                productsToBeShippedList.add(new ProductsToBeShipped(
                        productSupplier.getSupplier(),
                        inOutProduct.getProduct(),
                        company,
                        orderMenu,
                        inOutProduct.getWeight(),
                        inOutProduct.getNumberPack(),
                        new Timestamp(System.currentTimeMillis()),
                        Status.NEW.getName(),
                        orderMenu.getOrderNumber()
                ));
            }
        }
        productsToBeShippedRepository.saveAll(productsToBeShippedList);
    }

    public StateMessage checkSupplier(Company company, OrderMenu orderMenu) {
        boolean ans = true;
        int count = 0;

        StringBuilder str = new StringBuilder();

        for (InOutProduct inOutProduct : orderMenu.getRequiredProduct()) {
            Optional<ProductSupplier> optional = productSupplierRepository.findByCompanyAndProduct(company, inOutProduct.getProduct());
            if (optional.isEmpty()) {
                count++;
                str.append("  ").append(count).append("-").append(inOutProduct.getProduct().getName());
                ans = false;
            }
        }

        if (!ans) {
            str.append("   ushbu maxsulotlarni yetkazib beruvchi ta`minotchi mavjud emas");
        }

        return new StateMessage(str.toString(), ans);
    }

    public void sendStorage(OrderMenu orderMenu, Company company) {

        List<ProductsToBeShippedCompany> list = new ArrayList<>();
        List<OrderKindergarten> orderKindergartenList = new ArrayList<>(orderMenu.getOrderKindergartenList());

        for (OrderKindergarten orderKindergarten : orderKindergartenList) {

            if (orderKindergarten.getKindergarten().getType().equals(KindergartenType.TAMINOT.getName())) {
                sendStorageTaminot(orderKindergarten, company, orderMenu);
            } else {
                for (InOutProduct inOutProduct : orderKindergarten.getRequiredProductPack()) {
                    if (inOutProduct.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                        list.add(new ProductsToBeShippedCompany(
                                inOutProduct.getProduct(),
                                company,
                                orderKindergarten.getKindergarten(),
                                orderMenu,
                                inOutProduct.getWeight(),
                                inOutProduct.getNumberPack(),
                                new Timestamp(System.currentTimeMillis()),
                                Status.NEW.getName(),
                                null

                        ));
                    }
                }
            }
        }
        productsToBeShippedCompanyRepository.saveAll(list);
    }

    public void sendStorageTaminot(OrderKindergarten orderKindergarten, Company company, OrderMenu orderMenu) {

        List<ProductsToBeShippedCompany> list = new ArrayList<>();

        for (InOutProduct inOutProduct : orderKindergarten.getRequiredProductPack()) {

            if (inOutProduct.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {

                List<AgreementOrder> product = agreementOrderRepository.findAllByOrderKindergartenAndProduct(orderKindergarten, inOutProduct.getProduct());

                for (AgreementOrder agreementOrder : product) {
                    list.add(new ProductsToBeShippedCompany(
                            inOutProduct.getProduct(),
                            company,
                            orderKindergarten.getKindergarten(),
                            orderMenu,
                            inOutProduct.getWeight(),
                            inOutProduct.getNumberPack(),
                            new Timestamp(System.currentTimeMillis()),
                            Status.NEW.getName(),
                            agreementOrder.getAgreement().getNumber()
                    ));
                }

            }
        }
        productsToBeShippedCompanyRepository.saveAll(list);
    }

    public List<InOutProduct> calculateDetectedProduct(OrderMenu orderMenu) {

        List<InOutProduct> list = orderMenu.getDetectedProduct() != null ? orderMenu.getDetectedProduct() : new ArrayList<>();

        BigDecimal zero = BigDecimal.valueOf(0);

        for (InOutProduct inOutProduct : list) {
            inOutProduct.setWeight(zero);
            inOutProduct.setNumberPack(zero);
        }

        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {
            for (InOutProduct inOutProduct : orderKindergarten.getRequiredProductPack()) {
                calculateDetectedProductAddList(inOutProduct, list, orderMenu);
            }
        }
        return list;
    }

    public void calculateDetectedProductAddList(InOutProduct inOutProduct, List<InOutProduct> list, OrderMenu orderMenu) {

        boolean res = true;
        for (InOutProduct outProduct : list) {
            if (outProduct.getProduct().equals(inOutProduct.getProduct())) {
                outProduct.setWeight(outProduct.getWeight().add(inOutProduct.getWeight()));
                outProduct.setNumberPack(outProduct.getNumberPack().add(inOutProduct.getNumberPack()));
                res = false;
            }
        }
        if (res) {
            list.add(new InOutProduct(
                    inOutProduct.getWeight(),
                    inOutProduct.getNumberPack(),
                    inOutProduct.getProduct()
            ));
        }
    }

    public List<InOutProduct> calculateExistingProduct(OrderMenu orderMenu, Company company) {

        List<InOutProduct> list = orderMenu.getExistingProduct() != null ? orderMenu.getExistingProduct() : new ArrayList<>();

        BigDecimal zero = BigDecimal.valueOf(0);

        for (InOutProduct inOutProduct : list) {
            inOutProduct.setWeight(zero);
            inOutProduct.setNumberPack(zero);
        }
        for (InOutProduct inOutProduct : orderMenu.getDetectedProduct()) {
            for (ProductBalancer productBalancer : company.getReserve()) {
                if ((inOutProduct.getProduct().equals(productBalancer.getProduct())) && productBalancer.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                    BigDecimal weight = inOutProduct.getWeight().compareTo(productBalancer.getWeight()) > 0 ? productBalancer.getWeight() : inOutProduct.getWeight();
                    calculateExistingProductAddList(inOutProduct, list, orderMenu, weight);
                }
            }
        }
        return list;
    }

    public void calculateExistingProductAddList(InOutProduct inOutProduct, List<InOutProduct> list, OrderMenu orderMenu, BigDecimal weight) {

        boolean res = true;
        for (InOutProduct outProduct : list) {
            if (outProduct.getProduct().equals(inOutProduct.getProduct())) {
                outProduct.setWeight(outProduct.getWeight().add(weight));
                res = false;
            }
        }
        if (res) {
            list.add(new InOutProduct(
                    weight,
                    inOutProduct.getProduct()
            ));
        }
    }

    public List<InOutProduct> calculateRequiredProduct(OrderMenu orderMenu) {

        List<InOutProduct> list = orderMenu.getRequiredProduct() != null ? orderMenu.getRequiredProduct() : new ArrayList<>();

        BigDecimal zero = BigDecimal.valueOf(0);

        for (InOutProduct inOutProduct : list) {
            inOutProduct.setWeight(zero);
            inOutProduct.setNumberPack(zero);
        }
        for (InOutProduct detected : orderMenu.getDetectedProduct()) {
            boolean res = true;
            for (InOutProduct existing : orderMenu.getExistingProduct()) {
                if ((existing.getProduct().equals(detected.getProduct())) && existing.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                    BigDecimal weight = detected.getWeight().subtract(existing.getWeight());

                    BigDecimal pack = detected.getProduct().getPack();


                    BigDecimal numberPack;
                    BigDecimal multiply;
                    if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {

                        numberPack = weight;
                        multiply = weight;

                    } else {
                        numberPack = weight.divide(pack, 0, RoundingMode.UP);
                        multiply = weight.multiply(detected.getProduct().getPack());
                    }

                    calculateRequiredProductAddList(multiply, numberPack, detected.getProduct(), list, orderMenu);
                    res = false;
                    break;
                }
            }
            if (res) {
                calculateRequiredProductAddList(detected.getWeight(), detected.getNumberPack(), detected.getProduct(), list, orderMenu);
            }
        }
        return list;
    }

    public void calculateRequiredProductAddList(BigDecimal weight, BigDecimal numberPack, Product product, List<InOutProduct> list, OrderMenu orderMenu) {

        boolean res = true;
        for (InOutProduct outProduct : list) {
            if (outProduct.getProduct().equals(product)) {
                outProduct.setWeight(outProduct.getWeight().add(weight));
                outProduct.setNumberPack(outProduct.getNumberPack().add(numberPack));
                res = false;
            }
        }
        if (res) {
            list.add(new InOutProduct(
                    weight,
                    numberPack,
                    product
            ));
        }
    }

    public StateMessage sendProduct(ResponseUser responseUser, List<SendProductDTO> list, String name) {

        boolean b = orderMenuRepository.existsByOrderNumber(name);
        boolean a = productsToBeShippedRepository.existsAllByName(name);

        if (a || b) {
            return new StateMessage("Maxsulot yuborilmadi. Bunday nom bilan zayafka mavjud", false);
        }

        StringBuilder str = new StringBuilder();

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();
        for (SendProductDTO sendProductDTO : list) {
            Product product = productRepository.findById(sendProductDTO.getProductId()).get();

            BigDecimal weight = BigDecimal.valueOf(sendProductDTO.getWeight());
            BigDecimal numberPack = BigDecimal.valueOf(sendProductDTO.getNumberPack());


            if (product.getPack().compareTo(BigDecimal.valueOf(0)) > 0){

                numberPack = numberPack.divide(BigDecimal.valueOf(1),0,RoundingMode.UP);
                weight = numberPack.multiply(product.getPack());
            }


            if (sendProductDTO.getWeight() == null || sendProductDTO.getNumberPack() == null || sendProductDTO.getWeight() == 0) {
                str.append(product.getName()).append(", ");
            } else {
                Optional<ProductSupplier> optional = productSupplierRepository.findByCompanyAndProduct(company, product);

                if (optional.isPresent()) {
                    ProductSupplier productSupplier = optional.get();
                    productsToBeShippedRepository.save(new ProductsToBeShipped(
                            productSupplier.getSupplier(),
                            product,
                            company,
                            null,
                            BigDecimal.valueOf(sendProductDTO.getWeight()),
                            BigDecimal.valueOf(sendProductDTO.getNumberPack()),
                            new Timestamp(System.currentTimeMillis()),
                            Status.NEW.getName(),
                            name
                    ));
                } else {
                    str.append(product.getName()).append(", ");
                }
            }
        }

        if (str.length() > 0) {
            return new StateMessage(str + " Ushcu maxsulotlar yuborilmadi", false);
        }
        return new StateMessage("Muvaffaqiyatli yuborildi", true);
    }

    public List<NumberOfKidsKinDTO> getNumberOfKids(Integer id) {
        OrderMenu orderMenu = orderMenuRepository.findById(id).get();

        List<NumberOfKidsKinDTO> list = new ArrayList<>();

        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {

            NumberOfKidsKinDTO kinDTO = new NumberOfKidsKinDTO();
            kinDTO.setKindergartenName(orderKindergarten.getKindergarten().getName() + "  " + orderKindergarten.getKindergarten().getAddress().getDistrict().getName());

            List<NumberOfKidsDateDTO> dateList = new ArrayList<>();

            for (MenuWeight menuWeight : orderKindergarten.getMenuWeightList()) {

                NumberOfKidsDateDTO dateDTO = new NumberOfKidsDateDTO();
                dateDTO.setDate(menuWeight.getMenuSave().getDayDate());

                List<NumberOfKidsDTO> numberList = new ArrayList<>();

                MenuSave menuSave = menuWeight.getMenuSave();
                PerDay numberToGuess = menuSave.getNumberToGuess();

                for (NumberOfChildren numberOfChild : numberToGuess.getNumberOfChildren()) {
                    NumberOfKidsDTO number = new NumberOfKidsDTO();
                    number.setNumber(numberOfChild.getNumberOfKids().longValue());
                    number.setAgeGroupName(numberOfChild.getAgeGroup().getName());
                    numberList.add(number);
                }

                dateDTO.setNumberOfKidsDTOList(numberList);
                dateList.add(dateDTO);
            }
            kinDTO.setNumberOfKidsDateDTOList(dateList);
            list.add(kinDTO);
        }
        return list;
    }

    public List<SupplierKindergartenDTO> getSupplierKindergarten(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        List<SupplierKindergartenDTO> list = new ArrayList<>();

        Company company = users.getCompany();
        for (Kindergarten kindergarten : company.getKindergarten()) {
            if (kindergarten.getType().equals(KindergartenType.TAMINOT.getName())) {

                List<AgreementKindergarten> allByKindergarten = agreementKindergartenRepository.findAllByKindergarten(kindergarten);

                List<AgreementOrderDTO> productList = getProduct(allByKindergarten);

                SupplierKindergartenDTO supplierKindergartenDTO = new SupplierKindergartenDTO(kindergarten.getName(), kindergarten.getAddress().getDistrict().getName(), kindergarten.getId(), productList);

                if (productList.size() > 0)
                    list.add(supplierKindergartenDTO);
            }
        }


        return list;
    }

    public List<AgreementOrderDTO> getProduct(List<AgreementKindergarten> list) {

        List<AgreementOrderDTO> agreementList = new ArrayList<>();

        for (AgreementKindergarten agreementKindergarten : list) {
            if (agreementKindergarten.getAgreement().getStatus().equals(Status.SUCCESS.getName())) {
                for (AgreementProduct agreementProduct : agreementKindergarten.getAgreementProductList()) {

                    boolean res = true;

                    for (AgreementOrderDTO agreementOrderDTO : agreementList) {
                        if (agreementOrderDTO.getAgreementId().equals(agreementKindergarten.getAgreement().getId())) {
                            List<SupplierKinProductResponseDTO> productList = agreementOrderDTO.getProductList();
                            BigDecimal pack = agreementProduct.getProduct().getMeasurementType().equals("dona") ? (agreementProduct.getProduct().getPack().compareTo(BigDecimal.valueOf(0)) == 0 ? BigDecimal.valueOf(1) : agreementProduct.getProduct().getPack()) : (agreementProduct.getProduct().getRounding().compareTo(BigDecimal.valueOf(0)) == 0 ? BigDecimal.valueOf(1) : agreementProduct.getProduct().getRounding());
                            productList.add(new SupplierKinProductResponseDTO(
                                    agreementProduct.getProduct().getId(), agreementProduct.getProduct().getName(), agreementProduct.getResidue(), pack));

                            agreementOrderDTO.setProductList(productList);
                            res = false;
                            break;
                        }
                    }

                    if (res) {
                        AgreementOrderDTO agreementOrderDTO = new AgreementOrderDTO(agreementKindergarten.getAgreement().getNumber(), agreementKindergarten.getAgreement().getId());

                        List<SupplierKinProductResponseDTO> productList = new ArrayList<>();
                        BigDecimal pack = agreementProduct.getProduct().getMeasurementType().equals("dona") ? (agreementProduct.getProduct().getPack().compareTo(BigDecimal.valueOf(0)) == 0 ? BigDecimal.valueOf(1) : agreementProduct.getProduct().getPack()) : (agreementProduct.getProduct().getRounding().compareTo(BigDecimal.valueOf(0)) == 0 ? BigDecimal.valueOf(1) : agreementProduct.getProduct().getRounding());
                        productList.add(new SupplierKinProductResponseDTO(
                                agreementProduct.getProduct().getId(), agreementProduct.getProduct().getName(), agreementProduct.getResidue(), pack));
                        agreementOrderDTO.setProductList(productList);
                        agreementList.add(agreementOrderDTO);
                    }

                }
            }
        }
        return agreementList;
    }

    public StateMessage addSupplierKin(Integer kindergartenId, List<SupplierKinProductDTO> list, Integer id, Integer agreementId) {

        OrderMenu orderMenu = orderMenuRepository.findById(id).get();
        Agreement agreement = agreementRepository.findById(agreementId).get();

        List<AgreementOrder> agreementOrderList = new ArrayList<>();

        Kindergarten kindergarten = kindergartenRepository.findById(kindergartenId).get();

        if (kindergarten.getType().equals(KindergartenType.TAMINOT.getName())) {

            if (orderMenu.getStatus().equals(Status.CREATED.getName())) {

                OrderKindergarten orderKindergarten = orderKindergartenService.addOrderKindergartenSupplier(list, kindergarten, orderMenu, agreementId);

                for (SupplierKinProductDTO dto : list) {
                    Product product = productRepository.findById(dto.getProductId()).get();
                    AgreementOrder agreementOrder = new AgreementOrder();
                    agreementOrder.setAgreement(agreement);
                    agreementOrder.setOrderMenu(orderMenu);
                    agreementOrder.setOrderKindergarten(orderKindergarten);
                    agreementOrder.setProduct(product);
                    agreementOrder.setWeight(BigDecimal.valueOf(dto.getInputWeight()));

                    agreementOrderList.add(agreementOrder);
                }

                List<OrderKindergarten> orderKindergartenList = orderMenu.getOrderKindergartenList();
                orderKindergartenList.add(orderKindergarten);
                orderMenu.setOrderKindergartenList(orderKindergartenList);
                OrderMenu orderSave = orderMenuRepository.save(orderMenu);
                calculate(orderSave);

                agreementOrderRepository.saveAll(agreementOrderList);
                orderKindergartenService.calculateAgreement(agreement);
                checkAgreementWeight(agreement);

                return new StateMessage(kindergarten.getName() + " muvaffaqiyatli zayafkaga qo`shildi", true);
            }
            return new StateMessage("O`zgartirish amalga oshmadi. Zayafka yopilgan", false);
        }
        return new StateMessage("Amal bajarilmadi MTT toifasi boshqa.", false);
    }

    public void checkAgreementWeight(Agreement agreement) {
        boolean res = true;
        for (AgreementPrice agreementPrice : agreement.getAgreementPriceList()) {
            if (agreementPrice.getResidue().compareTo(BigDecimal.valueOf(0)) > 0) {
                res = false;
                break;
            }
        }
        if (res) {
            agreement.setStatus(Status.TOLIQ_YETKAZIB_BERILDI.getName());
        } else {
            agreement.setStatus(Status.SUCCESS.getName());
        }
        agreementRepository.save(agreement);
    }

    public StateMessage editSendProduct(SendProductDTO sendProductDTO, UUID id) {

        ProductsToBeShipped productsToBeShipped = productsToBeShippedRepository.findById(id).get();

        if (productsToBeShipped.getStatus().equals(Status.NEW.getName())) {

            productsToBeShipped.setWeight(BigDecimal.valueOf(sendProductDTO.getWeight()));
            productsToBeShipped.setNumberPack(BigDecimal.valueOf(sendProductDTO.getNumberPack()));

            productsToBeShippedRepository.save(productsToBeShipped);
            return new StateMessage("Muvaffaqiyatli o`zgartirildi.", true);
        }
        return new StateMessage("O`zgartirish amalga oshmadi.", false);
    }


    public List<OrderProductDTO> getProductByOrder(Integer orderId) {


        List<OrderProductDTO> list = new ArrayList<>();

        OrderMenu orderMenu = orderMenuRepository.findById(orderId).get();
        for (InOutProduct inOutProduct : orderMenu.getRequiredProduct()) {
            list.add(new OrderProductDTO(inOutProduct.getProduct().getId(), inOutProduct.getProduct().getName(), inOutProduct.getNumberPack()));
        }

        return list;

    }

    public List<Integer> getProductByOrderID(Integer orderId) {


        List<Integer> list = new ArrayList<>();

        OrderMenu orderMenu = orderMenuRepository.findById(orderId).get();
        for (InOutProduct inOutProduct : orderMenu.getRequiredProduct()) {
            list.add(inOutProduct.getProduct().getId());
        }

        return list;

    }
}















