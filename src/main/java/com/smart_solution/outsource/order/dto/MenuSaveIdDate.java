package com.smart_solution.outsource.order.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

public class MenuSaveIdDate {

    private Integer menuId;
    private Timestamp date;

    public MenuSaveIdDate(Integer menuId, Timestamp date) {
        this.menuId = menuId;
        this.date = date;
    }

    public MenuSaveIdDate() {
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
