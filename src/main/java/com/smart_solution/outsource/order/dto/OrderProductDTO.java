package com.smart_solution.outsource.order.dto;

import java.math.BigDecimal;

public class OrderProductDTO {

    private Integer productId;
    private String name;
    private BigDecimal weight;

    public OrderProductDTO(Integer productId, String name, BigDecimal weight) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
    }

    public OrderProductDTO() {
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
