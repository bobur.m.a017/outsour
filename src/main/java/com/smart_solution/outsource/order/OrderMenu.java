package com.smart_solution.outsource.order;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class OrderMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @Column(unique = true)
    private String orderNumber;

    private Boolean state = false;

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;



    @OneToMany(mappedBy = "orderMenu" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShipped> productsToBeShippedList;

    @OneToMany(mappedBy = "orderMenu" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProducts> shippedProductsList;

    @OneToMany(mappedBy = "orderMenu" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShippedCompany> productsToBeShippedCompanyList;

    @OneToMany(mappedBy = "orderMenu" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProductsCompany> shippedProductsCompanyList;




    // Zayafka xolatini ko`rsatib turadi. Misol: To`liq yakunlandi, Qisman yakunlandi, Yakunlanmadi.
    private String status;


    // Xar bir zayafka yaratilayotgan MTTlar;
    @OneToMany(mappedBy = "orderMenu",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<OrderKindergarten> orderKindergartenList;


    //Ushbu zayafkaga umumiy qancha maxsulot kerakligi.
    // (Bunda orderKindergartenList ning requiredProductlar yig`indisi xisoblanbadi)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> detectedProduct;


    //Korxona omborida mavjud maxsulotlar;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> existingProduct;


    //Ushbu zayafka to`liq yopilishi uchun kerakli maxsulot.
    // (Bunda umumiy maxsulotdan omborda mavjud maxsulot ayriladi).
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> requiredProduct;



    public OrderMenu() {
    }

    public OrderMenu(String orderNumber, Company company, String status) {
        this.orderNumber = orderNumber;
        this.company = company;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public List<ProductsToBeShipped> getProductsToBeShippedList() {
        return productsToBeShippedList;
    }

    public void setProductsToBeShippedList(List<ProductsToBeShipped> productsToBeShippedList) {
        this.productsToBeShippedList = productsToBeShippedList;
    }

    public List<ShippedProducts> getShippedProductsList() {
        return shippedProductsList;
    }

    public void setShippedProductsList(List<ShippedProducts> shippedProductsList) {
        this.shippedProductsList = shippedProductsList;
    }

    public List<ProductsToBeShippedCompany> getProductsToBeShippedCompanyList() {
        return productsToBeShippedCompanyList;
    }

    public void setProductsToBeShippedCompanyList(List<ProductsToBeShippedCompany> productsToBeShippedCompanyList) {
        this.productsToBeShippedCompanyList = productsToBeShippedCompanyList;
    }

    public List<ShippedProductsCompany> getShippedProductsCompanyList() {
        return shippedProductsCompanyList;
    }

    public void setShippedProductsCompanyList(List<ShippedProductsCompany> shippedProductsCompanyList) {
        this.shippedProductsCompanyList = shippedProductsCompanyList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderKindergarten> getOrderKindergartenList() {
        return orderKindergartenList;
    }

    public void setOrderKindergartenList(List<OrderKindergarten> orderKindergartenList) {
        this.orderKindergartenList = orderKindergartenList;
    }

    public List<InOutProduct> getDetectedProduct() {
        return detectedProduct;
    }

    public void setDetectedProduct(List<InOutProduct> detectedProduct) {
        this.detectedProduct = detectedProduct;
    }

    public List<InOutProduct> getExistingProduct() {
        return existingProduct;
    }

    public void setExistingProduct(List<InOutProduct> existingProduct) {
        this.existingProduct = existingProduct;
    }

    public List<InOutProduct> getRequiredProduct() {
        return requiredProduct;
    }

    public void setRequiredProduct(List<InOutProduct> requiredProduct) {
        this.requiredProduct = requiredProduct;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
