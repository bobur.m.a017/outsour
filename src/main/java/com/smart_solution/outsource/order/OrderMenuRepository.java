package com.smart_solution.outsource.order;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderMenuRepository extends JpaRepository<OrderMenu,Integer>{

    boolean existsByOrderNumber(String str);

    Optional<OrderMenu> findByOrderNumber(String str);
}
