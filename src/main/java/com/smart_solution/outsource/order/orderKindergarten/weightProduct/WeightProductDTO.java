package com.smart_solution.outsource.order.orderKindergarten.weightProduct;


import java.math.BigDecimal;
import java.util.UUID;

public class WeightProductDTO {

    private UUID id;
    private BigDecimal weight;
    private String productName;

    public WeightProductDTO(UUID id, BigDecimal weight, String productName) {
        this.id = id;
        this.weight = weight;
        this.productName = productName;
    }

    public WeightProductDTO(BigDecimal weight, String productName) {
        this.weight = weight;
        this.productName = productName;
    }

    public WeightProductDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
