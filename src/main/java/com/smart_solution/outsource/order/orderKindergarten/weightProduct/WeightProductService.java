package com.smart_solution.outsource.order.orderKindergarten.weightProduct;

import org.springframework.stereotype.Service;

@Service
public record WeightProductService (){

    public WeightProductDTO parse(WeightProduct weightProduct){
        return new WeightProductDTO(
                weightProduct.getId(),
                weightProduct.getWeight(),
                weightProduct.getProduct().getName()
        );
    }
}
