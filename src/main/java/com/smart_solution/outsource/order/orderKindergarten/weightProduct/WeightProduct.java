package com.smart_solution.outsource.order.orderKindergarten.weightProduct;


import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.product.Product;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class WeightProduct {


    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;


    @Column(precision = 19, scale = 6)
    private BigDecimal checkWeight;

    private Integer maxsulotId;

    @ManyToOne
    private Product product;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private MenuWeight menuWeight;

    public WeightProduct(BigDecimal weight, Product product, MenuWeight menuWeight,Integer maxsulotId) {
        this.weight = weight;
        this.product = product;
        this.menuWeight = menuWeight;
        this.maxsulotId = maxsulotId;
    }

    public Integer getMaxsulotId() {
        return maxsulotId;
    }

    public void setMaxsulotId(Integer maxsulotId) {
        this.maxsulotId = maxsulotId;
    }

    public WeightProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getCheckWeight() {
        return checkWeight;
    }

    public void setCheckWeight(BigDecimal checkWeight) {
        this.checkWeight = checkWeight;
    }

    public WeightProduct() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public MenuWeight getMenuWeight() {
        return menuWeight;
    }

    public void setMenuWeight(MenuWeight menuWeight) {
        this.menuWeight = menuWeight;
    }
}
