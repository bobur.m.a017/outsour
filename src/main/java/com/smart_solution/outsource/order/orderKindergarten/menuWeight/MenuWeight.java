package com.smart_solution.outsource.order.orderKindergarten.menuWeight;

import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class MenuWeight {

    @Id
    private UUID id = UUID.randomUUID();

    @OneToMany(mappedBy = "menuWeight", cascade = CascadeType.ALL)
    private List<WeightProduct> weightProducts;

    @OneToOne
    private MenuSave menuSave;

    private Integer orderKindergartenId;

    public Integer getOrderKindergartenId() {
        return orderKindergartenId;
    }

    public void setOrderKindergartenId(Integer orderKindergartenId) {
        this.orderKindergartenId = orderKindergartenId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<WeightProduct> getWeightProducts() {
        return weightProducts;
    }

    public void setWeightProducts(List<WeightProduct> weightProducts) {
        this.weightProducts = weightProducts;
    }

    public MenuSave getMenuSave() {
        return menuSave;
    }

    public void setMenuSave(MenuSave menuSave) {
        this.menuSave = menuSave;
    }

}
