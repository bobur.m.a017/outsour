package com.smart_solution.outsource.order.orderKindergarten.menuWeight;

import com.smart_solution.outsource.inOutProduct.InOutProductResponseDTO;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightDTO;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProductDTO;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProductService;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public record MenuWeightService(
        WeightProductService weightProductService,
        ProductRepository productRepository
) {
    public MenuWeightDTO parse(MenuWeight menuWeight, MenuSave menuSave) {

        MenuWeightDTO dto = new MenuWeightDTO(
                menuWeight.getId().toString(),
                menuSave.getDayDate()
        );

        List<WeightProductDTO> weightProductList = getAllProductNameListWP();
        for (WeightProduct weightProduct : menuWeight.getWeightProducts()) {

            for (WeightProductDTO a : weightProductList) {
                if (a.getProductName().equals(weightProduct.getProduct().getName())){
                    WeightProductDTO parse = weightProductService.parse(weightProduct);
                    a.setWeight(parse.getWeight());
                }
            }
        }
        dto.setWeightProducts(weightProductList);
        return dto;
    }

    public List<WeightProductDTO> getAllProductNameListWP() {
        List<WeightProductDTO> list = new ArrayList<>();

        for (Product product : productRepository.findAll()) {
            list.add(new WeightProductDTO(BigDecimal.valueOf(0),product.getName()));
        }
        return list;
    }

}
