package com.smart_solution.outsource.order.orderKindergarten.weightProduct;


import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface WeightProductRepository extends JpaRepository<WeightProduct,UUID> {
}
