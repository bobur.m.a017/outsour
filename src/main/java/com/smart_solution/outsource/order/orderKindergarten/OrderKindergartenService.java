package com.smart_solution.outsource.order.orderKindergarten;

import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.agreement.AgreementRepository;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergartenRepository;
import com.smart_solution.outsource.agreement.price.AgreementPrice;
import com.smart_solution.outsource.agreement.price.AgreementPriceRepository;
import com.smart_solution.outsource.agreement.product.AgreementProduct;
import com.smart_solution.outsource.agreement.product.AgreementProductRepository;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.inOutProduct.InOutProductRepository;
import com.smart_solution.outsource.inOutProduct.InOutProductResponseDTO;
import com.smart_solution.outsource.inOutProduct.InOutProductService;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.kindergarten.supplier.SupplierKinProductDTO;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProduct;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProductRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveService;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import com.smart_solution.outsource.perDay.PerDay;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.order.OrderMenuRepository;
import com.smart_solution.outsource.order.numberOfKids.NumberOfKidsDTO;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightDTO;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightRepository;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightService;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.perDay.PerDayRepository;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.productBalancer.ProductBalancerRepository;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.supplier.SupplierRepository;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public record OrderKindergartenService(
        OrderMenuRepository orderMenuRepository,
        UsersRepository usersRepository,
        CompanyRepository companyRepository,
        MenuSaveRepository menuSaveRepository,
        InOutProductRepository inOutProductRepository,
        InOutProductService inOutProductService,
        SupplierRepository supplierRepository,
        ProductRepository productRepository,
        ProductBalancerRepository productBalancerRepository,
        OrderKindergartenRepository orderKindergartenRepository,
        MenuWeightRepository menuWeightRepository,
        KindergartenRepository kindergartenRepository,
        AgeGroupRepository ageGroupRepository,
        MenuSaveService menuSaveService,
        MenuWeightService menuWeightService,
        PerDayRepository perDayRepository,
        AgreementKindergartenRepository agreementKindergartenRepository,
        AgreementProductRepository agreementProductRepository,
        AgreementRepository agreementRepository,
        AgreementPriceRepository agreementPriceRepository,
        CheckProductRepository checkProductRepository
) {

    public StateMessage edit(OrderKindergartenDTO dto, Integer id) {
        List<InOutProduct> deleteList = new ArrayList<>();

        OrderKindergarten orderKindergarten = orderKindergartenRepository.findById(id).get();

        Kindergarten kindergarten = kindergartenRepository.findById(dto.getKindergartenId()).get();
        PerDay perDay = addNumberOfKids(dto.getNumberList(), kindergarten.getId());
        List<MenuSave> menuSaveList = menuAddNumberOfKids(dto.getMenuIdList(), perDay);

        List<MenuWeight> menuWeightList = addMenuWeight(menuSaveList, orderKindergarten.getId());
        List<MenuWeight> menuWeightListOld = orderKindergarten.getMenuWeightList();
        menuWeightList.addAll(menuWeightListOld);
        orderKindergarten.setMenuWeightList(menuWeightList);

        deleteList.addAll(orderKindergarten.getDetectedProduct());
        deleteList.addAll(orderKindergarten.getExistingProduct());
        deleteList.addAll(orderKindergarten.getRequiredProduct());
        deleteList.addAll(orderKindergarten.getRequiredProductPack());

        orderKindergarten.setDetectedProduct(calculateDetectedProduct(orderKindergarten));
        orderKindergarten.setExistingProduct(calculateExistingProduct(orderKindergarten));
        orderKindergarten.setRequiredProduct(calculateRequiredProduct(orderKindergarten));
        orderKindergarten.setRequiredProductPack(calculateRequiredProductPack(orderKindergarten));
        orderKindergartenRepository.save(orderKindergarten);

        inOutProductRepository.deleteAll(deleteList);

        return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);
    }

    public OrderKindergarten addOrderKindergarten(OrderKindergartenDTO dto, OrderMenu orderMenu, Kindergarten kindergarten) {

        OrderKindergarten orderKindergarten = new OrderKindergarten();

        PerDay perDay = addNumberOfKids(dto.getNumberList(), dto.getKindergartenId());
        List<MenuSave> menuSaveList = menuAddNumberOfKids(dto.getMenuIdList(), perDay);

        List<MenuWeight> menuWeightList = addMenuWeight(menuSaveList, orderKindergarten.getId());

        orderKindergarten.setOrderMenu(orderMenu);
        orderKindergarten.setKindergarten(kindergarten);
        orderKindergarten.setMenuWeightList(menuWeightList);
        orderKindergarten.setDetectedProduct(calculateDetectedProduct(orderKindergarten));
        orderKindergarten.setExistingProduct(calculateExistingProduct(orderKindergarten));
        orderKindergarten.setRequiredProduct(calculateRequiredProduct(orderKindergarten));
        orderKindergarten.setRequiredProductPack(calculateRequiredProductPack(orderKindergarten));

        return orderKindergartenRepository.save(orderKindergarten);
    }

    public PerDay addNumberOfKids(List<NumberOfKidsDTO> dtoList, Integer kindergartenId) {

        PerDay perDay = new PerDay();

        List<NumberOfChildren> list = new ArrayList<>();

        for (NumberOfKidsDTO numberOfKidsDTO : dtoList) {
            NumberOfChildren numberOfChildren = new NumberOfChildren();
            AgeGroup ageGroup = ageGroupRepository.findById(numberOfKidsDTO.getAgeGroupId()).get();
            numberOfChildren.setAgeGroup(ageGroup);
            numberOfChildren.setNumberOfKids(numberOfKidsDTO.getNumber());
            numberOfChildren.setPerDay(perDay);
            list.add(numberOfChildren);
        }
        perDay.setNumberOfChildren(list);
        perDay.setKindergartenId(kindergartenId);
        perDay.setState(false);
        return perDay;
    }

    public List<MenuSave> menuAddNumberOfKids(List<Integer> menuIdList, PerDay perDay) {

        List<MenuSave> menuSaveList = new ArrayList<>();

        for (Integer menuId : menuIdList) {
            MenuSave menuSave = menuSaveRepository.findById(menuId).get();

            menuSave.setNumberToGuess(perDay);
            perDay.setDayDate(menuSave.getDayDate());
            menuSave.setState(true);
            menuSave.setStatus(Status.ADD_ORDER.getName());
            menuSaveList.add(menuSave);
        }
        List<PerDay> list = new ArrayList<>();
        List<MenuSave> saveList = menuSaveRepository.saveAll(menuSaveList);
        for (MenuSave menuSave : saveList) {
            PerDay numberToGuess = menuSave.getNumberToGuess();
            numberToGuess.setDayDate(menuSave.getDayDate());
            list.add(numberToGuess);
        }
        perDayRepository.saveAll(list);

        return saveList;
    }

    public List<MenuWeight> addMenuWeight(List<MenuSave> list, Integer okId) {

        List<MenuWeight> menuWeightList = new ArrayList<>();

        for (MenuSave menuSave : list) {
            MenuWeight menuWeight = new MenuWeight();
            menuWeight.setMenuSave(menuSave);
            menuWeight.setOrderKindergartenId(okId);
            List<WeightProduct> weightProductList = menuSaveService.calculateMenuProduct(menuSave, menuWeight);

            menuWeight.setWeightProducts(weightProductList);
            menuWeightList.add(menuWeight);
        }
        return menuWeightRepository.saveAll(menuWeightList);
    }

    public List<InOutProduct> calculateDetectedProduct(OrderKindergarten orderKindergarten) {
        List<InOutProduct> list = new ArrayList<>();

        for (MenuWeight menuWeight : orderKindergarten.getMenuWeightList()) {
            for (WeightProduct weightProduct : menuWeight.getWeightProducts()) {
                boolean res = true;

                for (InOutProduct inOutProduct : list) {
                    if (inOutProduct.getProduct().equals(weightProduct.getProduct())) {
                        inOutProduct.setWeight(inOutProduct.getWeight().add(weightProduct.getWeight()));
                        res = false;
                        break;
                    }
                }
                if (res) {
                    list.add(new InOutProduct(
                            weightProduct.getWeight(),
                            weightProduct.getProduct()
                    ));
                }
            }
        }

        return list;
    }

    public List<InOutProduct> calculateExistingProduct(OrderKindergarten orderKindergarten) {

        List<InOutProduct> list = new ArrayList<>();

        Kindergarten kindergarten = orderKindergarten.getKindergarten();

        for (InOutProduct inOutProduct : orderKindergarten.getDetectedProduct()) {

            boolean res = true;

            for (ProductBalancer productBalancer : kindergarten.getConsumable()) {   /////O`ZGARTIRILDI
                if ((inOutProduct.getProduct().equals(productBalancer.getProduct())) && productBalancer.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {

                    BigDecimal weight = productBalancer.getWeight();
                    List<MenuSave> menuList = menuSaveRepository.findAllByKindergartenAndStatus(orderKindergarten.getKindergarten(), Status.MAXSULOT_YUBORILDI.getName());

                    for (MenuSave menuSave : menuList) {
                        CheckProduct checkProduct = checkProductRepository.findByKindergartenIdAndMenuSaveIdAndProductIdAndCondition(orderKindergarten.getKindergarten().getId(), menuSave.getId(), productBalancer.getProduct().getId(),Status.MAXSULOT_YUBORILDI.getName());
                        weight = weight.subtract(checkProduct.getWeight());

                    }

                    if (weight.compareTo(BigDecimal.valueOf(0)) > 0) {
                        weight = inOutProduct.getWeight().compareTo(weight) > 0 ? weight : inOutProduct.getWeight();
                    } else {
                        weight = BigDecimal.valueOf(0);
                    }


                    list.add(new InOutProduct(
                            weight, inOutProduct.getProduct()
                    ));
                    res = false;
                    break;
                }
            }
            if (res) {
                list.add(new InOutProduct(
                        BigDecimal.valueOf(0), inOutProduct.getProduct()
                ));
            }
        }
        return list;
    }

    public List<InOutProduct> calculateRequiredProduct(OrderKindergarten orderKindergarten) {

        List<InOutProduct> list = new ArrayList<>();

        for (InOutProduct detected : orderKindergarten.getDetectedProduct()) {

            boolean res = true;
            if (orderKindergarten.getExistingProduct() != null) {
                for (InOutProduct existing : orderKindergarten.getExistingProduct()) {
                    if ((existing.getProduct().equals(detected.getProduct())) && existing.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                        BigDecimal weight = detected.getWeight().subtract(existing.getWeight());

                        list.add(new InOutProduct(
                                weight, detected.getProduct()
                        ));
                        res = false;
                        break;
                    }
                }
            }


            if (res) {
                list.add(new InOutProduct(
                        detected.getWeight(), detected.getProduct()
                ));
            }
        }
        return list;
    }

    public List<InOutProduct> calculateRequiredProductPack(OrderKindergarten orderKindergarten) {

        List<InOutProduct> list = new ArrayList<>();

        for (InOutProduct required : orderKindergarten.getRequiredProduct()) {

            Product product = required.getProduct();
            BigDecimal pack = product.getPack();

            BigDecimal numberPack;
            BigDecimal weightPack;

            if (pack.compareTo(BigDecimal.valueOf(0)) != 0) {
                numberPack = required.getWeight().divide(pack, 0, RoundingMode.UP);
                weightPack = numberPack.multiply(pack);
            } else {
                BigDecimal rounding = required.getWeight().divide(product.getRounding(), 0, RoundingMode.UP);
                numberPack = rounding.multiply(product.getRounding());
                weightPack = rounding.multiply(product.getRounding());
            }


            list.add(new InOutProduct(
                    weightPack, numberPack, required.getProduct()
            ));
        }

        return list;
    }

    public OrderKindergartenResponseDTO parse(OrderKindergarten orderKindergarten) {

        OrderKindergartenResponseDTO dto = new OrderKindergartenResponseDTO(
                orderKindergarten.getId(),
                orderKindergarten.getKindergarten().getName() + "  " + orderKindergarten.getKindergarten().getAddress().getDistrict().getName()
        );

        List<InOutProductResponseDTO> detectedProduct = getAllProductNameListIOP();
        for (InOutProduct inOutProduct : orderKindergarten.getDetectedProduct()) {
            for (InOutProductResponseDTO a : detectedProduct) {
                if (a.getProductName().equals(inOutProduct.getProduct().getName())) {
                    InOutProductResponseDTO parse = inOutProductService.parse(inOutProduct);
                    a.setWeight(parse.getWeight());
                    a.setNumberPack(parse.getNumberPack());
                }
            }
        }
        dto.setDetectedProduct(detectedProduct);

        List<InOutProductResponseDTO> existingProduct = getAllProductNameListIOP();
        for (InOutProduct inOutProduct : orderKindergarten.getExistingProduct()) {
            for (InOutProductResponseDTO a : existingProduct) {
                if (a.getProductName().equals(inOutProduct.getProduct().getName())) {
                    InOutProductResponseDTO parse = inOutProductService.parse(inOutProduct);
                    a.setWeight(parse.getWeight());
                    a.setNumberPack(parse.getNumberPack());
                }
            }
        }
        dto.setExistingProduct(existingProduct);

        List<InOutProductResponseDTO> requiredProduct = getAllProductNameListIOP();
        for (InOutProduct inOutProduct : orderKindergarten.getRequiredProduct()) {
            for (InOutProductResponseDTO a : requiredProduct) {
                if (a.getProductName().equals(inOutProduct.getProduct().getName())) {
                    InOutProductResponseDTO parse = inOutProductService.parse(inOutProduct);
                    a.setWeight(parse.getWeight());
                    a.setNumberPack(parse.getNumberPack());
                }
            }
        }
        dto.setRequiredProduct(requiredProduct);

        List<InOutProductResponseDTO> requiredProductPack = getAllProductNameListIOP();
        for (InOutProduct inOutProduct : orderKindergarten.getRequiredProductPack()) {
            for (InOutProductResponseDTO a : requiredProductPack) {
                if (a.getProductName().equals(inOutProduct.getProduct().getName())) {
                    InOutProductResponseDTO parse = inOutProductService.parse(inOutProduct);
                    a.setWeight(parse.getWeight());
                    a.setNumberPack(parse.getNumberPack());
                }
            }
        }
        dto.setRequiredProductPack(requiredProductPack);

        List<MenuWeightDTO> menuWeight = new ArrayList<>();

        if (orderKindergarten.getMenuWeightList() == null) {
            dto.setMenuWeightList(new ArrayList<>());
        } else {
            for (MenuWeight weight : orderKindergarten.getMenuWeightList()) {
                MenuSave menuSave = weight.getMenuSave();
                menuWeight.add(menuWeightService.parse(weight, menuSave));
            }
            dto.setMenuWeightList(menuWeight);
        }


        return dto;
    }

    public List<InOutProductResponseDTO> getAllProductNameListIOP() {
        List<InOutProductResponseDTO> list = new ArrayList<>();

        for (Product product : productRepository.findAll()) {
            list.add(new InOutProductResponseDTO(BigDecimal.valueOf(0), BigDecimal.valueOf(0), product.getName(), product.getPack()));
        }
        return list;
    }

    public OrderKindergarten addOrderKindergartenSupplier(List<SupplierKinProductDTO> list, Kindergarten kindergarten, OrderMenu orderMenu, Integer agreementId) {


        Optional<OrderKindergarten> optional = orderKindergartenRepository.findByKindergartenAndOrderMenu(kindergarten, orderMenu);

        if (optional.isEmpty()) {


            OrderKindergarten orderKindergarten = new OrderKindergarten();
            orderKindergarten.setOrderMenu(orderMenu);
            orderKindergarten.setKindergarten(kindergarten);

            List<InOutProduct> inOutProductList = new ArrayList<>();
            for (SupplierKinProductDTO dto : list) {
                if (dto.getWeight() >= dto.getInputWeight()) {
                    Product product = productRepository.findById(dto.getProductId()).get();

                    if (product.getMeasurementType().equals("dona")) {

                        BigDecimal numberPack;
                        BigDecimal weight;

                        numberPack = BigDecimal.valueOf(dto.getInputWeight());
                        weight = BigDecimal.valueOf(dto.getInputWeight()).multiply(product.getRounding());

                        subtractAgreement(kindergarten, product, weight, agreementId);
                        inOutProductList.add(new InOutProduct(weight, numberPack, product));
                    } else {
                        BigDecimal numberPack;
                        BigDecimal weight;

                        if (product.getPack().compareTo(BigDecimal.valueOf(0)) == 0) {
                            numberPack = BigDecimal.valueOf(dto.getInputWeight());
                            weight = BigDecimal.valueOf(dto.getInputWeight());
                        } else {
                            numberPack = BigDecimal.valueOf(dto.getInputWeight()).divide(product.getPack(), 0, RoundingMode.UP);
                            weight = numberPack.multiply(product.getPack());
                        }
                        subtractAgreement(kindergarten, product, weight, agreementId);
                        inOutProductList.add(new InOutProduct(weight, numberPack, product));
                    }
                }
            }

            orderKindergarten.setDetectedProduct(inOutProductList);
            orderKindergarten.setRequiredProduct(calculateRequiredProduct(orderKindergarten));
            orderKindergarten.setRequiredProductPack(calculateRequiredProductPack(orderKindergarten));

            calculateAgreement(agreementRepository.findById(agreementId).get());

            return orderKindergartenRepository.save(orderKindergarten);
        } else {

            OrderKindergarten orderKindergarten = optional.get();
            List<InOutProduct> inOutProductList = orderKindergarten.getDetectedProduct();
            for (SupplierKinProductDTO dto : list) {
                if (dto.getWeight() >= dto.getInputWeight()) {
                    Product product = productRepository.findById(dto.getProductId()).get();

                    BigDecimal numberPack;
                    BigDecimal weight;

                    if (product.getPack().compareTo(BigDecimal.valueOf(0)) == 0) {
                        numberPack = BigDecimal.valueOf(dto.getInputWeight());
                        weight = BigDecimal.valueOf(dto.getInputWeight());
                    } else {
                        numberPack = BigDecimal.valueOf(dto.getInputWeight()).divide(product.getPack(), 0, RoundingMode.UP);
                        weight = numberPack.multiply(product.getPack());
                    }

                    boolean res = true;
                    for (InOutProduct inOutProduct : inOutProductList) {
                        if (inOutProduct.getProduct().getId().equals(dto.getProductId())) {
                            inOutProduct.setWeight(inOutProduct.getWeight().add(weight));
                            inOutProduct.setNumberPack(inOutProduct.getNumberPack().add(numberPack));
                            res = false;
                            subtractAgreement(kindergarten, product, weight, agreementId);
                            break;
                        }
                    }

                    if (res) {
                        subtractAgreement(kindergarten, product, weight, agreementId);
                        inOutProductList.add(new InOutProduct(weight, numberPack, product));
                    }
                }
            }

            orderKindergarten.setDetectedProduct(inOutProductList);
            orderKindergarten.setRequiredProduct(calculateRequiredProduct(orderKindergarten));
            orderKindergarten.setRequiredProductPack(calculateRequiredProductPack(orderKindergarten));


            calculateAgreement(agreementRepository.findById(agreementId).get());

            return orderKindergartenRepository.save(orderKindergarten);
        }
    }

    public void subtractAgreement(Kindergarten kindergarten, Product product, BigDecimal weight, Integer agreementId) {

        if (product.getMeasurementType().equals("dona")) {
            weight = weight.divide(product.getRounding(), 6, RoundingMode.HALF_UP);
        }

        Agreement agreement = agreementRepository.findById(agreementId).get();

        Optional<AgreementProduct> optional = agreementProductRepository.findByAgreementKindergarten_KindergartenAndProductAndAgreementKindergarten_Agreement(kindergarten, product, agreement);

        AgreementProduct agreementProduct = optional.get();

        if (weight.compareTo(BigDecimal.valueOf(0)) > 0) {
            if (agreementProduct.getResidue().compareTo(weight) >= 0) {
                agreementProduct.setSuccessWeight(agreementProduct.getSuccessWeight().add(weight));
                agreementProduct.setResidue(agreementProduct.getWeight().subtract(agreementProduct.getSuccessWeight()));
            }
        }
        agreementProductRepository.save(agreementProduct);
    }

    public void calculateAgreement(Agreement agreement) {

        List<AgreementPrice> list = new ArrayList<>();

        for (AgreementPrice agreementPrice : agreement.getAgreementPriceList()) {
            BigDecimal weight = BigDecimal.valueOf(0);
            BigDecimal successWeight = BigDecimal.valueOf(0);
            BigDecimal residue = BigDecimal.valueOf(0);

            for (AgreementKindergarten agreementKindergarten : agreement.getAgreementKindergartenList()) {
                for (AgreementProduct agreementProduct : agreementKindergarten.getAgreementProductList()) {
                    if (agreementProduct.getProduct().equals(agreementPrice.getProduct())) {
                        weight = weight.add(agreementProduct.getWeight());
                        successWeight = successWeight.add(agreementProduct.getSuccessWeight());
                        residue = residue.add(agreementProduct.getResidue());
                    }
                }
            }
            agreementPrice.setWeight(weight);
            agreementPrice.setSuccessWeight(successWeight);
            agreementPrice.setResidue(residue);

            list.add(agreementPrice);
        }
        agreementPriceRepository.saveAll(list);
    }
}