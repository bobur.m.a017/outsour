package com.smart_solution.outsource.order.orderKindergarten;

import com.smart_solution.outsource.inOutProduct.InOutProductResponseDTO;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightDTO;

import java.util.List;

public class OrderKindergartenResponseDTO {

    private Integer id;

    private List<MenuWeightDTO> menuWeightList;

    //Ushbu zayafka qaysi MTTga tegishli ekanligi saqlanadi.
    private String kindergartenName;


    //Ushbu zayafkaga umumiy qancha maxsulot kerakligi.
    // (Bunda orderKindergartenList ning requiredProductlar yig`indisi xisoblanbadi)
    private List<InOutProductResponseDTO> detectedProduct;


    //Korxona omborida mavjud maxsulotlar;
    private List<InOutProductResponseDTO> existingProduct;


    //Ushbu zayafka to`liq yopilishi uchun kerakli maxsulot.
    // (Bunda umumiy maxsulotdan omborda mavjud maxsulot ayriladi).
    private List<InOutProductResponseDTO> requiredProduct;


    //Ushbu zayafka to`liq yopilishi uchun kerakli maxsulot.
    // (Bunda umumiy maxsulotdan omborda mavjud maxsulot ayriladi).
    private List<InOutProductResponseDTO> requiredProductPack;

    public OrderKindergartenResponseDTO(Integer id, String kindergartenName) {
        this.id = id;
        this.kindergartenName = kindergartenName;
    }

    public OrderKindergartenResponseDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<MenuWeightDTO> getMenuWeightList() {
        return menuWeightList;
    }

    public void setMenuWeightList(List<MenuWeightDTO> menuWeightList) {
        this.menuWeightList = menuWeightList;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public List<InOutProductResponseDTO> getDetectedProduct() {
        return detectedProduct;
    }

    public void setDetectedProduct(List<InOutProductResponseDTO> detectedProduct) {
        this.detectedProduct = detectedProduct;
    }

    public List<InOutProductResponseDTO> getExistingProduct() {
        return existingProduct;
    }

    public void setExistingProduct(List<InOutProductResponseDTO> existingProduct) {
        this.existingProduct = existingProduct;
    }

    public List<InOutProductResponseDTO> getRequiredProduct() {
        return requiredProduct;
    }

    public void setRequiredProduct(List<InOutProductResponseDTO> requiredProduct) {
        this.requiredProduct = requiredProduct;
    }

    public List<InOutProductResponseDTO> getRequiredProductPack() {
        return requiredProductPack;
    }

    public void setRequiredProductPack(List<InOutProductResponseDTO> requiredProductPack) {
        this.requiredProductPack = requiredProductPack;
    }
}
