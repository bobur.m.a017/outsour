package com.smart_solution.outsource.order.orderKindergarten;


import com.smart_solution.outsource.order.numberOfKids.NumberOfKidsDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class OrderKindergartenDTO {

    private List<Integer> menuIdList;
    private List<NumberOfKidsDTO> numberList;
    private Integer kindergartenId;


    public OrderKindergartenDTO() {
    }

    public List<Integer> getMenuIdList() {
        return menuIdList;
    }

    public void setMenuIdList(List<Integer> menuIdList) {
        this.menuIdList = menuIdList;
    }

    public List<NumberOfKidsDTO> getNumberList() {
        return numberList;
    }

    public void setNumberList(List<NumberOfKidsDTO> numberList) {
        this.numberList = numberList;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }
}
