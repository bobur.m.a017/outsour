package com.smart_solution.outsource.order.orderKindergarten;

import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.order.OrderMenu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderKindergartenRepository extends JpaRepository<OrderKindergarten,Integer> {
    Optional<OrderKindergarten> findByKindergartenAndOrderMenu(Kindergarten kindergarten, OrderMenu orderMenu);
}
