package com.smart_solution.outsource.order.orderKindergarten.menuWeight;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProductDTO;

import java.sql.Timestamp;
import java.util.List;

public class MenuWeightDTO {

    private String id;

    private List<WeightProductDTO> weightProducts;

    private String menuSaveName;

    private Timestamp menuDate;

    public MenuWeightDTO(String id, Timestamp menuDate) {
        this.id = id;
        this.menuDate = menuDate;
    }

    public MenuWeightDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<WeightProductDTO> getWeightProducts() {
        return weightProducts;
    }

    public void setWeightProducts(List<WeightProductDTO> weightProducts) {
        this.weightProducts = weightProducts;
    }

    public String getMenuSaveName() {
        return menuSaveName;
    }

    public void setMenuSaveName(String menuSaveName) {
        this.menuSaveName = menuSaveName;
    }

    public Timestamp getMenuDate() {
        return menuDate;
    }

    public void setMenuDate(Timestamp menuDate) {
        this.menuDate = menuDate;
    }
}
