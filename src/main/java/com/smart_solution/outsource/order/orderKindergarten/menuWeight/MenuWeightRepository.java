package com.smart_solution.outsource.order.orderKindergarten.menuWeight;

import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface MenuWeightRepository extends JpaRepository<MenuWeight, UUID > {
    Optional<MenuWeight> findByMenuSave_Id(Integer id);
}
