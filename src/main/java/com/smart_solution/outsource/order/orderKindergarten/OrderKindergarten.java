package com.smart_solution.outsource.order.orderKindergarten;

import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
public class OrderKindergarten {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private OrderMenu orderMenu;


    @OneToMany(cascade = CascadeType.ALL)
    private List<MenuWeight> menuWeightList;


    //Ushbu zayafkaga umumiy qancha maxsulot kerakligi.
    // (Bunda orderKindergartenList ning requiredProductlar yig`indisi xisoblanbadi)
    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> detectedProduct;


    //Korxona omborida mavjud maxsulotlar;
    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> existingProduct;


    //Ushbu zayafka to`liq yopilishi uchun kerakli maxsulot.
    // (Bunda umumiy maxsulotdan omborda mavjud maxsulot ayriladi).
    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> requiredProduct;


    //Ushbu zayafka to`liq yopilishi uchun kerakli maxsulot.
    // (Bunda umumiy maxsulotdan omborda mavjud maxsulot ayriladi).
    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> requiredProductPack;

    //Ushbu zayafka qaysi MTTga tegishli ekanligi saqlanadi.
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Kindergarten kindergarten;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OrderMenu getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(OrderMenu orderMenu) {
        this.orderMenu = orderMenu;
    }

    public List<MenuWeight> getMenuWeightList() {
        return menuWeightList;
    }

    public void setMenuWeightList(List<MenuWeight> menuWeightList) {
        this.menuWeightList = menuWeightList;
    }

    public List<InOutProduct> getDetectedProduct() {
        return detectedProduct;
    }

    public void setDetectedProduct(List<InOutProduct> detectedProduct) {
        this.detectedProduct = detectedProduct;
    }

    public List<InOutProduct> getExistingProduct() {
        return existingProduct;
    }

    public void setExistingProduct(List<InOutProduct> existingProduct) {
        this.existingProduct = existingProduct;
    }

    public List<InOutProduct> getRequiredProduct() {
        return requiredProduct;
    }

    public void setRequiredProduct(List<InOutProduct> requiredProduct) {
        this.requiredProduct = requiredProduct;
    }

    public List<InOutProduct> getRequiredProductPack() {
        return requiredProductPack;
    }

    public void setRequiredProductPack(List<InOutProduct> requiredProductPack) {
        this.requiredProductPack = requiredProductPack;
    }

    public Kindergarten getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(Kindergarten kindergarten) {
        this.kindergarten = kindergarten;
    }
}
