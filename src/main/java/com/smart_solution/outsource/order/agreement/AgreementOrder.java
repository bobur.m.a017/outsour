package com.smart_solution.outsource.order.agreement;

import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.product.Product;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AgreementOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private BigDecimal weight;

    @ManyToOne
    private OrderMenu orderMenu;

    @ManyToOne
    private Agreement agreement;

    @ManyToOne
    private Product product;

    @ManyToOne
    private OrderKindergarten orderKindergarten;

    public AgreementOrder() {
    }

    public AgreementOrder(Integer id, BigDecimal weight, OrderMenu orderMenu, Agreement agreement) {
        this.id = id;
        this.weight = weight;
        this.orderMenu = orderMenu;
        this.agreement = agreement;
    }

    public OrderKindergarten getOrderKindergarten() {
        return orderKindergarten;
    }

    public void setOrderKindergarten(OrderKindergarten orderKindergarten) {
        this.orderKindergarten = orderKindergarten;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public OrderMenu getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(OrderMenu orderMenu) {
        this.orderMenu = orderMenu;
    }

    public Agreement getAgreement() {
        return agreement;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }
}
