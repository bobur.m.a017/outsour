package com.smart_solution.outsource.order.agreement;

import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AgreementOrderRepository extends JpaRepository<AgreementOrder, Integer> {

    List<AgreementOrder> findAllByOrderKindergartenAndProduct(OrderKindergarten orderKindergarten, Product product);
    List<AgreementOrder> findAllByOrderKindergarten(OrderKindergarten orderKindergarten);
}
