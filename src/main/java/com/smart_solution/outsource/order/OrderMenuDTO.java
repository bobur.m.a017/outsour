package com.smart_solution.outsource.order;

import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

public class OrderMenuDTO {

    private Integer id;
    private String number;
    private String status;
    private Timestamp createDate;

    public OrderMenuDTO(Integer id, String number, String status, Timestamp createDate) {
        this.id = id;
        this.number = number;
        this.status = status;
        this.createDate = createDate;
    }

    public OrderMenuDTO(Integer id, String number) {
        this.id = id;
        this.number = number;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public OrderMenuDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
