package com.smart_solution.outsource.address;

import com.smart_solution.outsource.address.district.DistrictRepository;
import com.smart_solution.outsource.address.region.RegionRepository;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    private final AddressRepository addressRepository;
    private final DistrictRepository districtRepository;
    private final RegionRepository regionRepository;

    public AddressService(AddressRepository addressRepository, DistrictRepository districtRepository, RegionRepository regionRepository) {
        this.addressRepository = addressRepository;
        this.districtRepository = districtRepository;
        this.regionRepository = regionRepository;
    }


    public Address add(Integer districtId, String street) {
        return addressRepository.save(new Address(street, regionRepository.findByDistrict_Id(districtId).get(), districtRepository.findById(districtId).get()));
    }

    public Address edit(Address address, Integer districtId, String street) {
        address.setStreet(street);
        address.setDistrict(districtRepository.findById(districtId).get());
        address.setRegion(regionRepository.findByDistrict_Id(districtId).get());
        return addressRepository.save(address);
    }
}
