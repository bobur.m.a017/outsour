package com.smart_solution.outsource.address.district;


import com.smart_solution.outsource.address.region.Region;
import com.smart_solution.outsource.address.region.RegionRepository;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DistrictService {
    private final DistrictRepository districtRepository;
    private final RegionRepository regionRepository;

    public DistrictService(DistrictRepository districtRepository, RegionRepository regionRepository) {
        this.districtRepository = districtRepository;
        this.regionRepository = regionRepository;
    }

    public StateMessage add(DistrictDTO districtDTO) {

        boolean res = districtRepository.existsByName(districtDTO.getName());

        if (!res) {
            District district = new District();
            district.setName(districtDTO.getName());
            District save = districtRepository.save(district);

            Region region = regionRepository.findById(districtDTO.getRegion_id()).get();
            List<District> district1 = region.getDistrict();
            district1.add(save);
            region.setDistrict(district1);
            regionRepository.save(region);
            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
        } else {
            return new StateMessage("Bunday nomli tuman avval qo`shilgan yoki noto`g`ri kiritildi", false);
        }
    }

    public StateMessage edit(DistrictDTO districtDTO, Integer id) {

        District district = districtRepository.findById(id).get();
        district.setName(districtDTO.getName());
        districtRepository.save(district);

        return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);
    }

}
