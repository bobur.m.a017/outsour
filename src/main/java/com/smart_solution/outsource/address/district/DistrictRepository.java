package com.smart_solution.outsource.address.district;

import com.smart_solution.outsource.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DistrictRepository extends JpaRepository<District, Integer> {
    boolean existsByName(String name);
}
