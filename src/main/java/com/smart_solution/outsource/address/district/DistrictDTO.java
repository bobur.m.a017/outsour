package com.smart_solution.outsource.address.district;


import lombok.Getter;
import lombok.Setter;

public class DistrictDTO {
    private Integer region_id;
    private String name;

    public Integer getRegion_id() {
        return region_id;
    }

    public void setRegion_id(Integer region_id) {
        this.region_id = region_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
