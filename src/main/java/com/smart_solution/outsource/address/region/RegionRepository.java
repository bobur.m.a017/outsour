package com.smart_solution.outsource.address.region;

import com.smart_solution.outsource.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface RegionRepository extends JpaRepository<Region, Integer> {
    Optional<Region> findByDistrict_Id(Integer id);
}
