package com.smart_solution.outsource.address.region;

import com.smart_solution.outsource.address.district.District;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;


//    @OneToMany(fetch = FetchType.EAGER)
    @OneToMany
    private List<District> district;

    public Region(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Region() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<District> getDistrict() {
        return district;
    }

    public void setDistrict(List<District> district) {
        this.district = district;
    }
}
