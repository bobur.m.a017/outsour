package com.smart_solution.outsource.address;

import com.smart_solution.outsource.address.region.Region;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepository extends JpaRepository<Address, Integer> {
}
