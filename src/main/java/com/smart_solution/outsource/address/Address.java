package com.smart_solution.outsource.address;

import com.smart_solution.outsource.address.district.District;
import com.smart_solution.outsource.address.region.Region;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String street;


    @ManyToOne
    private Region region;

    @ManyToOne
    private District district;


    public Address(String street, Region region, District district) {
        this.street = street;
        this.region = region;
        this.district = district;
    }

    public Address() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }
}
