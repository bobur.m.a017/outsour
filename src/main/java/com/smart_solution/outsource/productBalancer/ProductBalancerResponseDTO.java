package com.smart_solution.outsource.productBalancer;

import java.math.BigDecimal;


public class ProductBalancerResponseDTO {

    private String productName;
    private Integer productId;
    private BigDecimal weight;
    private BigDecimal numberPack;
    private BigDecimal pack;


    public ProductBalancerResponseDTO(String productName, Integer productId, BigDecimal weight, BigDecimal numberPack, BigDecimal pack) {
        this.productName = productName;
        this.productId = productId;
        this.weight = weight;
        this.numberPack = numberPack;
        this.pack = pack;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
