package com.smart_solution.outsource.productBalancer;

import com.smart_solution.outsource.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductBalancerRepository extends JpaRepository<ProductBalancer, Integer> {
}
