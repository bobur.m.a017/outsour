package com.smart_solution.outsource.productBalancer;

import org.springframework.stereotype.Service;

@Service
public record ProductBalancerService (){


    public ProductBalancerResponseDTO parse(ProductBalancer productBalancer){

        return new ProductBalancerResponseDTO(productBalancer.getProduct().getName(),
                productBalancer.getProduct().getId(),
                productBalancer.getWeight(),
                productBalancer.getNumberPack(),
                productBalancer.getProduct().getPack());
    }
}
