package com.smart_solution.outsource.productBalancer;

import com.smart_solution.outsource.product.Product;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class ProductBalancer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    private Product product;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal numberPack;


    public ProductBalancer(Product product, BigDecimal weight) {
        this.product = product;
        this.weight = weight;

    }

    public ProductBalancer(Product product, BigDecimal weight, BigDecimal numberPack) {
        this.product = product;
        this.weight = weight;
        this.numberPack = numberPack;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public ProductBalancer() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
