package com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


public class AgeGroupSanpinNormResponseDTO {

    private Integer id;

    private String ageGroupName;
    private BigDecimal percentage;
    private BigDecimal DoneWeight;
    private Integer daily;
    private BigDecimal planWeight;

    public AgeGroupSanpinNormResponseDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getDoneWeight() {
        return DoneWeight;
    }

    public void setDoneWeight(BigDecimal doneWeight) {
        DoneWeight = doneWeight;
    }

    public Integer getDaily() {
        return daily;
    }

    public void setDaily(Integer daily) {
        this.daily = daily;
    }

    public BigDecimal getPlanWeight() {
        return planWeight;
    }

    public void setPlanWeight(BigDecimal planWeight) {
        this.planWeight = planWeight;
    }
}
