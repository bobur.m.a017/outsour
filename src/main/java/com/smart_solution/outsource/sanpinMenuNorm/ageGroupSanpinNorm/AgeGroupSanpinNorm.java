package com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AgeGroupSanpinNorm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private AgeGroup ageGroup;
    private Integer daily;


    @Column(precision = 19,scale = 6)
    private BigDecimal doneWeight;
    @Column(precision = 19,scale = 6)
    private BigDecimal planWeight;
    @Column(precision = 19, scale = 6)
    private BigDecimal doneProtein;
    @Column(precision = 19, scale = 6)
    private BigDecimal planProtein;
    @Column(precision = 19, scale = 6)
    private BigDecimal doneKcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal planKcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal doneOil;
    @Column(precision = 19, scale = 6)
    private BigDecimal planOil;
    @Column(precision = 19, scale = 6)
    private BigDecimal doneCarbohydrates;
    @Column(precision = 19, scale = 6)
    private BigDecimal planCarbohydrates;

    public AgeGroupSanpinNorm() {
    }


    public BigDecimal getDoneProtein() {
        return doneProtein;
    }

    public void setDoneProtein(BigDecimal doneProtein) {
        this.doneProtein = doneProtein;
    }

    public BigDecimal getPlanProtein() {
        return planProtein;
    }

    public void setPlanProtein(BigDecimal planProtein) {
        this.planProtein = planProtein;
    }

    public BigDecimal getDoneOil() {
        return doneOil;
    }

    public void setDoneOil(BigDecimal doneOil) {
        this.doneOil = doneOil;
    }

    public BigDecimal getPlanOil() {
        return planOil;
    }

    public void setPlanOil(BigDecimal planOil) {
        this.planOil = planOil;
    }

    public BigDecimal getDoneCarbohydrates() {
        return doneCarbohydrates;
    }

    public void setDoneCarbohydrates(BigDecimal doneCarbohydrates) {
        this.doneCarbohydrates = doneCarbohydrates;
    }

    public BigDecimal getPlanCarbohydrates() {
        return planCarbohydrates;
    }

    public void setPlanCarbohydrates(BigDecimal planCarbohydrates) {
        this.planCarbohydrates = planCarbohydrates;
    }

    public BigDecimal getPlanKcal() {
        return planKcal;
    }

    public void setPlanKcal(BigDecimal planKcal) {
        this.planKcal = planKcal;
    }

    public BigDecimal getDoneKcal() {
        return doneKcal;
    }

    public void setDoneKcal(BigDecimal doneKcal) {
        this.doneKcal = doneKcal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }

    public BigDecimal getDoneWeight() {
        return doneWeight;
    }

    public void setDoneWeight(BigDecimal doneWeight) {
        this.doneWeight = doneWeight;
    }

    public Integer getDaily() {
        return daily;
    }

    public void setDaily(Integer daily) {
        this.daily = daily;
    }

    public BigDecimal getPlanWeight() {
        return planWeight;
    }

    public void setPlanWeight(BigDecimal planWeight) {
        this.planWeight = planWeight;
    }
}
