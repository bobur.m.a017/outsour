package com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AgeGroupSanpinNormRepository extends JpaRepository<AgeGroupSanpinNorm, Integer> {
}
