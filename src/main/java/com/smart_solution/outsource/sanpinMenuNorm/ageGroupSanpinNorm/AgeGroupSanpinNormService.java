package com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNorm;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNorm;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public record AgeGroupSanpinNormService(
        AgeGroupSanpinNormRepository ageGroupSanpinNormRepository,
        AgeGroupRepository ageGroupRepository
) {


    public List<AgeGroupSanpinNorm> add(Integer daily, SanpinCategory sanpinCategory) {

        List<AgeGroupSanpinNorm> list = new ArrayList<>();

        for (AgeGroup ageGroup : ageGroupRepository.findAll()) {

            if (ageGroup.getId() == 1 || ageGroup.getId() == 2) {

                AgeGroupSanpinNorm ageGroupSanpinNorm = new AgeGroupSanpinNorm();
                ageGroupSanpinNorm.setAgeGroup(ageGroup);
                ageGroupSanpinNorm.setDaily(daily);
                ageGroupSanpinNorm.setDoneWeight(BigDecimal.valueOf(0));
                ageGroupSanpinNorm.setDoneCarbohydrates(BigDecimal.valueOf(0));
                ageGroupSanpinNorm.setDoneKcal(BigDecimal.valueOf(0));
                ageGroupSanpinNorm.setDoneOil(BigDecimal.valueOf(0));
                ageGroupSanpinNorm.setDoneProtein(BigDecimal.valueOf(0));


                for (SanpinAgeNorm sanpinAgeNorm : sanpinCategory.getSanpinAgeNorms()) {
                    if (sanpinAgeNorm.getAgeGroup().equals(ageGroup)) {
                        ageGroupSanpinNorm.setPlanWeight(BigDecimal.valueOf(daily).multiply(sanpinAgeNorm.getWeight()));
                        ageGroupSanpinNorm.setPlanCarbohydrates(BigDecimal.valueOf(daily).multiply(sanpinAgeNorm.getCarbohydrates()));
                        ageGroupSanpinNorm.setPlanKcal(BigDecimal.valueOf(daily).multiply(sanpinAgeNorm.getKcal()));
                        ageGroupSanpinNorm.setPlanOil(BigDecimal.valueOf(daily).multiply(sanpinAgeNorm.getOil()));
                        ageGroupSanpinNorm.setPlanProtein(BigDecimal.valueOf(daily).multiply(sanpinAgeNorm.getProtein()));
                    }
                }

                list.add(ageGroupSanpinNormRepository.save(ageGroupSanpinNorm));
            }
        }
        return list;
    }

    public boolean check(SanpinMenuNorm sanpinMenuNorm, AgeGroup ageGroup) {

        for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {
            if (ageGroupSanpinNorm.getAgeGroup().equals(ageGroup)) {
                return true;
            }
        }
        return false;
    }


    public AgeGroupSanpinNormResponseDTO parse(AgeGroupSanpinNorm ageGroupSanpinNorm) {

        AgeGroupSanpinNormResponseDTO dto = new AgeGroupSanpinNormResponseDTO();
        dto.setId(ageGroupSanpinNorm.getId());
        dto.setAgeGroupName(ageGroupSanpinNorm.getAgeGroup().getName());

        dto.setDaily(ageGroupSanpinNorm.getDaily() != null ? ageGroupSanpinNorm.getDaily() : 0);
        dto.setDoneWeight(ageGroupSanpinNorm.getDoneWeight() != null ? (ageGroupSanpinNorm.getDoneWeight().multiply(BigDecimal.valueOf(1000))).divide(BigDecimal.valueOf(1),0,RoundingMode.HALF_UP) : BigDecimal.valueOf(0));
        dto.setPlanWeight(ageGroupSanpinNorm.getPlanWeight() != null ? (ageGroupSanpinNorm.getPlanWeight().multiply(BigDecimal.valueOf(1000))).divide(BigDecimal.valueOf(1),0,RoundingMode.HALF_UP) : BigDecimal.valueOf(0));
        if (dto.getPlanWeight().compareTo(BigDecimal.valueOf(0)) != 0 && dto.getDoneWeight().compareTo(BigDecimal.valueOf(0)) != 0) {
            dto.setPercentage(dto.getDoneWeight().divide(dto.getPlanWeight(), 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)));
        } else {
            dto.setPercentage(BigDecimal.valueOf(0));
        }

        return dto;
    }

//    public BigDecimal shortening(Float number) {
//        DecimalFormat df = new DecimalFormat("##.##");
//        df.setRoundingMode(RoundingMode.DOWN);
//
//        String format = df.format(number);
//
//        String replace = format.replace(',', '.');
//        float v = Float.parseFloat(replace);
//        return v;
//    }
}
