package com.smart_solution.outsource.sanpinMenuNorm;

import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormResponseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class SanpinMenuNormResponseDTO {


    private Integer id;
    private String sanpinCategoryName;

    List<AgeGroupSanpinNormResponseDTO> dtoList;

    public SanpinMenuNormResponseDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSanpinCategoryName() {
        return sanpinCategoryName;
    }

    public void setSanpinCategoryName(String sanpinCategoryName) {
        this.sanpinCategoryName = sanpinCategoryName;
    }

    public List<AgeGroupSanpinNormResponseDTO> getDtoList() {
        return dtoList;
    }

    public void setDtoList(List<AgeGroupSanpinNormResponseDTO> dtoList) {
        this.dtoList = dtoList;
    }
}
