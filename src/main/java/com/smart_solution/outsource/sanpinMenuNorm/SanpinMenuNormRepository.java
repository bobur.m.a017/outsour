package com.smart_solution.outsource.sanpinMenuNorm;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SanpinMenuNormRepository extends JpaRepository<SanpinMenuNorm, Integer> {

    List<SanpinMenuNorm> findAllBySanpinCategory_Id(Integer id);
}
