package com.smart_solution.outsource.sanpinMenuNorm;


import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
public class SanpinMenuNorm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private SanpinCategory sanpinCategory;

    @OneToMany
    List<AgeGroupSanpinNorm> ageGroupSanpinNormList;


    public SanpinMenuNorm() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SanpinCategory getSanpinCategory() {
        return sanpinCategory;
    }

    public void setSanpinCategory(SanpinCategory sanpinCategory) {
        this.sanpinCategory = sanpinCategory;
    }

    public List<AgeGroupSanpinNorm> getAgeGroupSanpinNormList() {
        return ageGroupSanpinNormList;
    }

    public void setAgeGroupSanpinNormList(List<AgeGroupSanpinNorm> ageGroupSanpinNormList) {
        this.ageGroupSanpinNormList = ageGroupSanpinNormList;
    }
}
