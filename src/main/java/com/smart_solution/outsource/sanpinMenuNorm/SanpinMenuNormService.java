package com.smart_solution.outsource.sanpinMenuNorm;

import com.smart_solution.outsource.menu.ageStandard.AgeStandard;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.multiMenu.MultiMenu;
import com.smart_solution.outsource.productMeal.ProductMeal;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormRepository;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormResponseDTO;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormService;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategoryRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Service
public record SanpinMenuNormService(
        SanpinMenuNormRepository sanpinMenuNormRepository,
        SanpinCategoryRepository sanpinCategoryRepository,
        AgeGroupSanpinNormService ageGroupSanpinNormService,
        AgeGroupSanpinNormRepository ageGroupSanpinNormRepository
) {


    public List<SanpinMenuNorm> add(Integer daily) {

        List<SanpinMenuNorm> list = new ArrayList<>();

        for (SanpinCategory sanpinCategory : sanpinCategoryRepository.findAll()) {
            SanpinMenuNorm sanpinMenuNorm = new SanpinMenuNorm();
            sanpinMenuNorm.setSanpinCategory(sanpinCategory);
            sanpinMenuNorm.setAgeGroupSanpinNormList(ageGroupSanpinNormService.add(daily, sanpinCategory));
            list.add(sanpinMenuNormRepository.save(sanpinMenuNorm));

        }
        return list;
    }

    public SanpinMenuNormResponseDTO parse(SanpinMenuNorm sanpinMenuNorm) {

        SanpinMenuNormResponseDTO dto = new SanpinMenuNormResponseDTO();
        dto.setId(sanpinMenuNorm.getId());
        dto.setSanpinCategoryName(sanpinMenuNorm.getSanpinCategory().getName());
        List<AgeGroupSanpinNormResponseDTO> list = new ArrayList<>();

        for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {

            list.add(ageGroupSanpinNormService.parse(ageGroupSanpinNorm));
        }
        dto.setDtoList(list);
        return dto;
    }

    public void calculate(MultiMenu multiMenu) {

        for (SanpinMenuNorm sanpinMenuNorm : multiMenu.getSanpinMenuNormList()) {
            for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {
                BigDecimal weight = calculateWeight(ageGroupSanpinNorm, multiMenu, sanpinMenuNorm.getSanpinCategory());
                ageGroupSanpinNorm.setDoneWeight(weight);
                ageGroupSanpinNormRepository.save(ageGroupSanpinNorm);
            }
        }
    }

    public BigDecimal calculateWeight(AgeGroupSanpinNorm ageGroupSanpinNorm, MultiMenu multiMenu, SanpinCategory sanpinCategory) {

        BigDecimal weight = BigDecimal.valueOf(0);

        for (MealAgeStandard mealAgeStandard : multiMenu.getMealAgeStandardList()) {
            for (AgeStandard ageStandard : mealAgeStandard.getAgeStandards()) {
                if (ageStandard.getAgeGroup().equals(ageGroupSanpinNorm.getAgeGroup())) {
                    for (ProductMeal productMeal : mealAgeStandard.getMeal().getProductMeals()) {
                        if (productMeal.getProduct().getSanpinCategory().equals(sanpinCategory)) {


                            if (productMeal.getWeight().compareTo(BigDecimal.valueOf(0)) != 0 && mealAgeStandard.getMeal().getWeight().compareTo(BigDecimal.valueOf(0)) != 0) {
                                BigDecimal divide1 = productMeal.getWeight().divide(mealAgeStandard.getMeal().getWeight(), 6, RoundingMode.HALF_UP);

                                BigDecimal multiply = divide1.multiply(ageStandard.getWeight());
                                 weight = weight.add(multiply);

                            }
                        }
                    }
                }
            }
        }
        return weight;
    }
}
