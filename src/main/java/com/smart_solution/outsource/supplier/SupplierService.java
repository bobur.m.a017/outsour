package com.smart_solution.outsource.supplier;

import com.smart_solution.outsource.address.AddressService;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.inOutProduct.InOutProductRepository;
import com.smart_solution.outsource.inOutProduct.InOutProductService;
import com.smart_solution.outsource.inOutSupplier.InOutSupplierDTO;
import com.smart_solution.outsource.info.InfoService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.productSuppiler.ProductSupplier;
import com.smart_solution.outsource.productSuppiler.ProductSupplierRepository;
import com.smart_solution.outsource.role.RoleRepository;
import com.smart_solution.outsource.role.RoleType;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedDTO;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedRepository;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedService;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsDTO;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsRepository;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsService;
import com.smart_solution.outsource.supplier.shipping.Shipping;
import com.smart_solution.outsource.supplier.shipping.ShippingRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

@Service
public record SupplierService(
        SupplierRepository supplierRepository,
        InfoService infoService,
        AddressService addressService,
        ProductSupplierRepository productSupplierRepository,
        UsersRepository userRepository,
        RoleRepository roleRepository,
        PasswordEncoder passwordEncoder,
        InOutProductRepository inOutProductRepository,
        CompanyRepository companyRepository,
        InOutProductService inOutProductService,
        ProductsToBeShippedRepository productsToBeShippedRepository,
        ProductsToBeShippedService productsToBeShippedService,
        ShippedProductsRepository shippedProductsRepository,
        ShippingRepository shippingRepository,
        ShippedProductsService shippedProductsService
) implements SupplierParseDTO {


    public StateMessage add(SupplierDTO supplierDTO) {

        boolean res = supplierRepository.existsByName(supplierDTO.getCompanyName());

        boolean res1 = userRepository.existsByUserName(supplierDTO.getUserName());

        if (res) {
            return new StateMessage("Bunday nomli ta`minotchi avval ro`yxatdan o`tgan iltimos tekshirib qaytadan kiriting kiriting", false);
        }
        if (res1) {
            return new StateMessage("Ushbu username bilan avval foydalanuvchi ro`yxatdan o`tgan. Iltimos boshqa username tanlang", false);
        }


        Supplier supplier = new Supplier();
        supplier.setStatus(Status.SUCCESS.getName());
        supplier.setName(supplierDTO.getCompanyName());

        supplier.setInfo(infoService.add(supplierDTO.getAccountNumber(), supplierDTO.getMfo(), supplierDTO.getInn(), supplierDTO.getPhoneNumber()));

        supplier.setAddress(addressService.add(supplierDTO.getDistrictId(), supplierDTO.getStreet()));

        Supplier save = supplierRepository.save(supplier);

        Users users = new Users();
        users.setName(supplierDTO.getLeaderName());
        users.setFatherName(supplierDTO.getLeaderFatherName());
        users.setSurname(supplierDTO.getLeaderSurname());
        users.setRole(roleRepository.findByName(RoleType.SUPPLIER.getName()).get());
        users.setPhoneNumber(supplierDTO.getLeaderPhoneNumber());
        users.setPassword(passwordEncoder.encode(supplierDTO.getPassword()));
        users.setUserName(supplierDTO.getUserName());
        users.setState(true);
        users.setSupplier(save);
        Users saveUser = userRepository.save(users);

        if (save.getUsers() == null) {
            List<Users> list = new ArrayList<>();
            list.add(saveUser);
            save.setUsers(list);
        } else {
            List<Users> usersList = save.getUsers();
            usersList.add(saveUser);
            save.setUsers(usersList);
        }
        supplierRepository.save(save);

        return new StateMessage("Arizangiz muvaffaqiyatli yuborildi", true);

    }

    public StateMessage edit(SupplierDTO supplierDTO, ResponseUser responseUser, Integer supplierId) {

        Supplier supplier = supplierRepository.findById(supplierId).get();

        boolean res2 = true;

        if (!supplier.getName().equals(supplierDTO.getCompanyName())) {
            res2 = supplierRepository.existsByName(supplierDTO.getCompanyName());
        }
        boolean res = supplierRepository.existsByUsers_Id(responseUser.getId());

        if (res && res2) {
            supplier.setName(supplierDTO.getCompanyName());

            infoService.edit(supplier.getInfo(), supplierDTO.getAccountNumber(), supplierDTO.getMfo(), supplierDTO.getInn(), supplierDTO.getPhoneNumber());

            addressService.edit(supplier.getAddress(), supplierDTO.getDistrictId(), supplierDTO.getStreet());

            supplierRepository.save(supplier);
            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

        } else {
            return new StateMessage("Bunday nomli ta`minotchi avval ro`yxatdan o`tgan iltimos tekshirib qaytadan kiriting", false);
        }
    }

    public List<SupplierResponseDTO> get(Company company) {

        List<SupplierResponseDTO> list = new ArrayList<>();
        boolean res = true;

        for (ProductSupplier productSupplier : company.getProductSupplier()) {

            if (productSupplier.getSupplier() != null) {

                for (SupplierResponseDTO supplierResponseDTO : list) {
                    if (productSupplier.getSupplier().getName().equals(supplierResponseDTO.getName())) {
                        res = false;
                    }
                }
                if (res) {
                    list.add(parse(productSupplier.getSupplier()));
                }
            }
            res = true;
        }
        return list;
    }

    public List<Supplier> getSupplier(Company company) {

        List<Supplier> list = new ArrayList<>();
        boolean res = true;

        for (ProductSupplier productSupplier : company.getProductSupplier()) {

            if (productSupplier.getSupplier() != null) {

                for (Supplier supplier : list) {
                    if (productSupplier.getSupplier().equals(supplier)) {
                        res = false;
                    }
                }
                if (res) {
                    list.add(productSupplier.getSupplier());
                }
            }
            res = true;
        }
        return list;
    }

    public List<SupplierResponseDTO> get() {

        List<SupplierResponseDTO> list = new ArrayList<>();

        for (Supplier supplier : supplierRepository.findAll()) {

            if (supplier.getStatus().equals(Status.SUCCESS.getName())) {
                list.add(parse(supplier));
            }
        }
        return list;
    }

    public StateMessage changeStatus(Integer id, boolean ans) {

        Supplier supplier = supplierRepository.findById(id).get();

        if (ans) {
            supplier.setStatus(Status.SUCCESS.getName());

            for (Users user : supplier.getUsers()) {
                user.setState(true);
                userRepository.save(user);
            }
            supplierRepository.save(supplier);
            return new StateMessage("Muvaffaqiyatli tastiqlandi", true);
        } else {

            Users users = supplier.getUsers().get(0);
            supplier.setUsers(null);
            Supplier save = supplierRepository.save(supplier);
            userRepository.delete(users);

            supplierRepository.delete(save);

            return new StateMessage("Muvaffaqiyatli o`chirildi", true);
        }
    }


    public StateMessage addShippedProduct(ResponseUser responseUser, InOutSupplierDTO dto, UUID requiredId) {
        ProductsToBeShipped productsToBeShipped = productsToBeShippedRepository.findById(requiredId).get();
        BigDecimal zero = BigDecimal.valueOf(0);

        String paymentStatus = dto.isPaymentStatus() ? Status.PAID.getName() : Status.UN_PAID.getName();

        String typeOfPayment = "";
        if (dto.isPaymentStatus()){
            typeOfPayment = dto.isTypeOfPayment() ? Status.CASH.getName() : Status.CASHLESS.getName();
        }



        if (productsToBeShipped.getStatus().equals(Status.FULLY_SENT.getName())) {
            return new StateMessage("XATOLIK. Maxsulot yuborib bo`lingan", false);
        }
        if (dto.getWeight() == 0) {
            return new StateMessage("XATOLIK. 0 qiymatni yuborib bo`lmaydi", false);
        }
        if ((productsToBeShipped.getProduct().getPack().compareTo(zero) > 0) && dto.getNumberPack() % 1 > 0) {
            return new StateMessage(productsToBeShipped.getProduct().getName() + "ni yarimta yuborib bo`lmaydi", false);
        }
        if (productsToBeShipped.getWeight().compareTo(BigDecimal.valueOf(dto.getWeight())) < 0) {
            return new StateMessage("XATOLIK. Talab qilingan miqdordan ko`p kiritildi", false);
        }


        Users users = userRepository.findByUserName(responseUser.getUsername()).get();
        Supplier supplier = users.getSupplier();

        if (productsToBeShipped.getWeight().compareTo(BigDecimal.valueOf(dto.getWeight())) == 0) {
            productsToBeShipped.setStatus(Status.FULLY_SENT.getName());
        } else {
            productsToBeShipped.setStatus(Status.QISMAN_YUBORILDI.getName());
        }

        if (productsToBeShipped.getSuccessNumberPack() == null || productsToBeShipped.getSuccessWeight() == null) {
            productsToBeShipped.setSuccessNumberPack(zero);
            productsToBeShipped.setSuccessWeight(zero);
        }

        productsToBeShipped.setWeight(productsToBeShipped.getWeight().subtract(BigDecimal.valueOf(dto.getWeight())));
        productsToBeShipped.setNumberPack(productsToBeShipped.getNumberPack().subtract(BigDecimal.valueOf(dto.getNumberPack())));
        productsToBeShipped.setSuccessWeight(productsToBeShipped.getSuccessWeight().add(BigDecimal.valueOf(dto.getWeight())));
        productsToBeShipped.setSuccessNumberPack(productsToBeShipped.getSuccessNumberPack().add(BigDecimal.valueOf(dto.getNumberPack())));

        ShippedProducts shippedProducts = shippedProductsRepository.save(new ShippedProducts(
                productsToBeShipped.getProduct(),
                productsToBeShipped.getCompany(),
                productsToBeShipped.getOrderMenu(),
                BigDecimal.valueOf(dto.getWeight()),
                BigDecimal.valueOf(dto.getNumberPack()),
                BigDecimal.valueOf(dto.getPrice()),
                new Timestamp(System.currentTimeMillis()),
                users,
                Status.NEW.getName(),
                supplier,
                paymentStatus,
                typeOfPayment,
                dto.getComment(),
                productsToBeShipped.getName()
        ));
        ProductsToBeShipped save = productsToBeShippedRepository.save(productsToBeShipped);
        shippingRepository.save(new Shipping(save, shippedProducts));
        return new StateMessage("Muvaffaqiyatli yuborildi.", true);
    }


    public List<ProductsToBeShippedDTO> getRequiredProduct(ResponseUser responseUser, Long start, Long end) {

        Users users = userRepository.findByUserName(responseUser.getUsername()).get();
        List<ProductsToBeShippedDTO> list = new ArrayList<>();
        Supplier supplier = users.getSupplier();

        if (start != null && end != null) {
            List<ProductsToBeShipped> allBySupplier = productsToBeShippedRepository.findAllBySupplier(supplier);
            for (ProductsToBeShipped productsToBeShipped : allBySupplier) {
                if (start < productsToBeShipped.getRequestDate().getTime() && end > productsToBeShipped.getRequestDate().getTime()) {
                    list.add(productsToBeShippedService.parse(productsToBeShipped));
                }
            }

        } else {

            List<ProductsToBeShipped> shippedList = productsToBeShippedRepository.findAllBySupplierAndStatus(supplier, Status.NEW.getName());
            shippedList.addAll(productsToBeShippedRepository.findAllBySupplierAndStatus(supplier, Status.QISMAN_YUBORILDI.getName()));

            for (ProductsToBeShipped productsToBeShipped : shippedList) {
                list.add(productsToBeShippedService.parse(productsToBeShipped));
            }
        }

        list.sort(Comparator.comparing(ProductsToBeShippedDTO::getProductName));
        return list;
    }

    public List<ShippedProductsDTO> getShippedProduct(ResponseUser responseUser, Long start, Long end) {

        if (start == null || end == null){
            Timestamp time = new Timestamp(System.currentTimeMillis() );

            start = new Date(time.getYear(),time.getMonth(),time.getDate()).getTime();
            end = new Date(time.getYear(),time.getMonth(),(time.getDate()+1)).getTime();
        }

        Users users = userRepository.findByUserName(responseUser.getUsername()).get();

        Supplier supplier = users.getSupplier();

        List<ShippedProductsDTO> list = new ArrayList<>();

        List<ShippedProducts> all = shippedProductsRepository.findAllBySupplier(supplier);
        for (ShippedProducts shippedProducts : all) {
            if (start < shippedProducts.getTimeOfShipment().getTime() && end > shippedProducts.getTimeOfShipment().getTime()) {
                list.add(shippedProductsService.parse(shippedProducts));
            }
        }

        list.sort(Comparator.comparing(ShippedProductsDTO::getProductName));
        return list;
    }

    public StateMessage editShippedProduct(Double price,Boolean paymentStatus, Boolean typeOfPayment, UUID id) {

        ShippedProducts shippedProducts = shippedProductsRepository.findById(id).get();

        if (price != null){
            shippedProducts.setPrice(BigDecimal.valueOf(price));
        }
        if (paymentStatus != null) {

            String status = paymentStatus ? Status.PAID.getName() : Status.UN_PAID.getName();

            String type = "";
            if (paymentStatus){
                type = typeOfPayment ? Status.CASH.getName() : Status.CASHLESS.getName();
            }

            shippedProducts.setPaymentStatus(status);
            shippedProducts.setTypeOfPayment(type);
        }
        shippedProductsRepository.save(shippedProducts);
        return new StateMessage("O`zgartirish muvaffaqiyatli amalga oshirildi", true);
    }

    public SupplierResponseDTO getOne(ResponseUser responseUser) {

        Users users = userRepository.findByUserName(responseUser.getUsername()).get();

        Supplier supplier = users.getSupplier();

        return parse(supplier);
    }

}
