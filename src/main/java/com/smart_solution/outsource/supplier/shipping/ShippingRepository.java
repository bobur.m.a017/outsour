package com.smart_solution.outsource.supplier.shipping;

import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface ShippingRepository extends JpaRepository<Shipping, Integer> {

    Optional<Shipping> findByShippedProducts(ShippedProducts shippedProducts);

}
