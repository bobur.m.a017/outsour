package com.smart_solution.outsource.supplier.shipping;

import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.UUID;

@Entity
public class Shipping {

    @Id
    private UUID id = UUID.randomUUID();

    @OneToOne
    private ProductsToBeShipped productsToBeShipped;

    @OneToOne
    private ShippedProducts shippedProducts;

    public Shipping() {
    }

    public Shipping(ProductsToBeShipped productsToBeShipped, ShippedProducts shippedProducts) {
        this.productsToBeShipped = productsToBeShipped;
        this.shippedProducts = shippedProducts;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ProductsToBeShipped getProductsToBeShipped() {
        return productsToBeShipped;
    }

    public void setProductsToBeShipped(ProductsToBeShipped productsToBeShipped) {
        this.productsToBeShipped = productsToBeShipped;
    }

    public ShippedProducts getShippedProducts() {
        return shippedProducts;
    }

    public void setShippedProducts(ShippedProducts shippedProducts) {
        this.shippedProducts = shippedProducts;
    }
}
