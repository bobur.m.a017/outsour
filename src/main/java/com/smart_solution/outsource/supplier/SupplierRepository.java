package com.smart_solution.outsource.supplier;

import com.smart_solution.outsource.users.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface SupplierRepository extends JpaRepository<Supplier, Integer> {

    boolean existsByName(String name);


    boolean existsByUsers_Id(Integer users_id);


}
