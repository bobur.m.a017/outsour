package com.smart_solution.outsource.supplier;


import lombok.Getter;
import lombok.Setter;

public class SupplierDTO {

    private String companyName;
    private String accountNumber;
    private String mfo;
    private String inn;
    private String phoneNumber;
    private Integer districtId;
    private String street;

    private String leaderName;
    private String leaderFatherName;
    private String leaderSurname;
    private String userName;
    private String password;
    private String leaderPhoneNumber;

    public SupplierDTO() {
    }


    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMfo() {
        return mfo;
    }

    public void setMfo(String mfo) {
        this.mfo = mfo;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    public String getLeaderFatherName() {
        return leaderFatherName;
    }

    public void setLeaderFatherName(String leaderFatherName) {
        this.leaderFatherName = leaderFatherName;
    }

    public String getLeaderSurname() {
        return leaderSurname;
    }

    public void setLeaderSurname(String leaderSurname) {
        this.leaderSurname = leaderSurname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLeaderPhoneNumber() {
        return leaderPhoneNumber;
    }

    public void setLeaderPhoneNumber(String leaderPhoneNumber) {
        this.leaderPhoneNumber = leaderPhoneNumber;
    }
}
