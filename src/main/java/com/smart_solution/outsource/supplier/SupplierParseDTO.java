package com.smart_solution.outsource.supplier;

public interface SupplierParseDTO {

    default public SupplierResponseDTO parse(Supplier supplier) {

        SupplierResponseDTO dto = new SupplierResponseDTO();

        dto.setId(supplier.getId());
        dto.setName(supplier.getName());
        dto.setPhoneNumber(supplier.getInfo().getPhoneNumber());
        dto.setRegion(supplier.getAddress().getRegion().getName());
        dto.setDistrict(supplier.getAddress().getDistrict().getName());
        dto.setStreet(supplier.getAddress().getStreet());
        dto.setAccountNumber(supplier.getInfo().getAccountNumber());
        dto.setInn(supplier.getInfo().getInn());
        dto.setMfo(supplier.getInfo().getMfo());
        return dto;
    }
}
