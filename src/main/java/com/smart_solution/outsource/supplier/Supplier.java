package com.smart_solution.outsource.supplier;


import com.smart_solution.outsource.address.Address;
import com.smart_solution.outsource.info.Info;
import com.smart_solution.outsource.payment.Payment;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import com.smart_solution.outsource.users.Users;

import javax.persistence.*;
import java.util.List;

@Entity
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String status;

    @OneToOne
    private Address address;

    @OneToOne
    private Info info;

    @OneToMany
    private List<Users> users;

    // Korxonaga yuboriladigan maxsulotlar
    @OneToMany(mappedBy = "supplier" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShipped> productsToBeShippedList;


    // Korxonaga yuborilgan maxsulotlar
    @OneToMany(mappedBy = "supplier" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProducts> shippedProductsList;

    // Korxonaga yuborilgan maxsulotlar
    @OneToMany(mappedBy = "supplier" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Payment> paymentList;


    public List<Payment> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<Payment> paymentList) {
        this.paymentList = paymentList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }


    public List<ProductsToBeShipped> getProductsToBeShippedList() {
        return productsToBeShippedList;
    }

    public void setProductsToBeShippedList(List<ProductsToBeShipped> productsToBeShippedList) {
        this.productsToBeShippedList = productsToBeShippedList;
    }

    public List<ShippedProducts> getShippedProductsList() {
        return shippedProductsList;
    }

    public void setShippedProductsList(List<ShippedProducts> shippedProductsList) {
        this.shippedProductsList = shippedProductsList;
    }
}
