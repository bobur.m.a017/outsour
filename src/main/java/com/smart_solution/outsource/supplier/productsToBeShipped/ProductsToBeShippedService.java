package com.smart_solution.outsource.supplier.productsToBeShipped;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public record ProductsToBeShippedService() {
    public ProductsToBeShippedDTO parse(ProductsToBeShipped a){
        return new ProductsToBeShippedDTO(
                a.getId(),
                a.getSupplier().getName(),
                a.getProduct().getName(),
                a.getProduct().getId(),
                a.getCompany().getId(),
                a.getCompany().getName(),
                a.getOrderMenu() != null ? a.getOrderMenu().getId() : null,
                a.getOrderMenu() != null ? a.getOrderMenu().getOrderNumber() : (a.getName() != null ? a.getName() : "ZAXIRA UCHUN"),
                a.getWeight(),
                a.getSuccessWeight()!= null ? a.getSuccessWeight() : BigDecimal.valueOf(0),
                a.getNumberPack(),
                a.getSuccessNumberPack()!= null ? a.getSuccessNumberPack() : BigDecimal.valueOf(0),
                a.getRequestDate(),
                a.getStatus(),
                a.getProduct().getPack(),
                a.getProduct().getMeasurementType()
        );
    }
}
