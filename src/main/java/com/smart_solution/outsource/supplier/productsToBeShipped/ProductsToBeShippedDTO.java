package com.smart_solution.outsource.supplier.productsToBeShipped;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.supplier.Supplier;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

public class ProductsToBeShippedDTO {

    private UUID id = UUID.randomUUID();

    private String supplierName;
    private String productName;
    private Integer productId;


    private Integer companyId;
    private String companyName;

    private Integer orderId;
    private String orderNumber;

    private BigDecimal weight;
    private BigDecimal successWeight;
    private BigDecimal numberPack;
    private BigDecimal successNumberPack;

    private Timestamp requestDate;

    private String status;
    private BigDecimal pack;
    private String measurementType;


    public ProductsToBeShippedDTO(UUID id, String supplierName, String productName, Integer productId, Integer companyId, String companyName, Integer orderId, String orderNumber, BigDecimal weight, BigDecimal successWeight, BigDecimal numberPack, BigDecimal successNumberPack, Timestamp requestDate, String status, BigDecimal pack, String measurementType) {
        this.id = id;
        this.supplierName = supplierName;
        this.productName = productName;
        this.productId = productId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.orderId = orderId;
        this.orderNumber = orderNumber;
        this.weight = weight;
        this.successWeight = successWeight;
        this.numberPack = numberPack;
        this.successNumberPack = successNumberPack;
        this.requestDate = requestDate;
        this.status = status;
        this.pack = pack;
        this.measurementType = measurementType;
    }

    public ProductsToBeShippedDTO() {
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public BigDecimal getSuccessNumberPack() {
        return successNumberPack;
    }

    public void setSuccessNumberPack(BigDecimal successNumberPack) {
        this.successNumberPack = successNumberPack;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
