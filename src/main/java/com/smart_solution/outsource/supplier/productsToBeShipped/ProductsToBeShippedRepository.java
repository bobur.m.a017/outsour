package com.smart_solution.outsource.supplier.productsToBeShipped;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ProductsToBeShippedRepository extends JpaRepository<ProductsToBeShipped, UUID> {

    List<ProductsToBeShipped> findAllBySupplierAndStatus(Supplier supplier, String status);

    List<ProductsToBeShipped> findAllBySupplier(Supplier supplier);

    List<ProductsToBeShipped> findAllByCompanyAndOrderMenu(Company company, OrderMenu orderMenu);

    List<ProductsToBeShipped> findAllByCompanyAndStatusAndOrderMenu(Company company, String status, OrderMenu orderMenu);

    boolean existsAllByName(String name);
}
