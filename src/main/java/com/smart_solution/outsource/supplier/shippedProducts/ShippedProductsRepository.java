package com.smart_solution.outsource.supplier.shippedProducts;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ShippedProductsRepository extends JpaRepository<ShippedProducts, UUID> {

    List<ShippedProducts> findAllBySupplierAndStatus(Supplier supplier, String status);
    List<ShippedProducts> findAllByCompanyAndSupplier(Company company, Supplier supplier);
    List<ShippedProducts> findAllBySupplier(Supplier supplier);
    List<ShippedProducts> findAllByCompany(Company company);
}
