package com.smart_solution.outsource.supplier.shippedProducts;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.users.Users;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

public class ShippedProductsDTO {


    private UUID id;

    private String productName;
    private Integer productId;
    private String companyName;
    private Integer companyId;
    private String orderNumber;
    private Integer orderId;
    private BigDecimal sendWeight;
    private BigDecimal successWeight;
    private BigDecimal sendNumberPack;
    private BigDecimal successNumberPack;
    private BigDecimal price;
    private Timestamp timeOfShipment;
    private Timestamp timeTaken;
    private String theSender;
    private String receiver;
    private String status;
    private String supplier;
    private String paymentStatus;
    private String typeOfPayment;

    private BigDecimal pack;
    private String measurementType;
    private String comment;


    public ShippedProductsDTO(UUID id, String productName, Integer productId, String companyName, Integer companyId, String orderNumber, Integer orderId, BigDecimal sendWeight, BigDecimal successWeight, BigDecimal sendNumberPack, BigDecimal successNumberPack, BigDecimal price, Timestamp timeOfShipment, Timestamp timeTaken, String theSender, String receiver, String status, String supplier, String paymentStatus, String typeOfPayment, BigDecimal pack, String measurementType, String comment) {
        this.id = id;
        this.productName = productName;
        this.productId = productId;
        this.companyName = companyName;
        this.companyId = companyId;
        this.orderNumber = orderNumber;
        this.orderId = orderId;
        this.sendWeight = sendWeight;
        this.successWeight = successWeight;
        this.sendNumberPack = sendNumberPack;
        this.successNumberPack = successNumberPack;
        this.price = price;
        this.timeOfShipment = timeOfShipment;
        this.timeTaken = timeTaken;
        this.theSender = theSender;
        this.receiver = receiver;
        this.status = status;
        this.supplier = supplier;
        this.paymentStatus = paymentStatus;
        this.typeOfPayment = typeOfPayment;
        this.pack = pack;
        this.measurementType = measurementType;
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ShippedProductsDTO() {
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getSendWeight() {
        return sendWeight;
    }

    public void setSendWeight(BigDecimal sendWeight) {
        this.sendWeight = sendWeight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getSendNumberPack() {
        return sendNumberPack;
    }

    public void setSendNumberPack(BigDecimal sendNumberPack) {
        this.sendNumberPack = sendNumberPack;
    }

    public BigDecimal getSuccessNumberPack() {
        return successNumberPack;
    }

    public void setSuccessNumberPack(BigDecimal successNumberPack) {
        this.successNumberPack = successNumberPack;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Timestamp getTimeOfShipment() {
        return timeOfShipment;
    }

    public void setTimeOfShipment(Timestamp timeOfShipment) {
        this.timeOfShipment = timeOfShipment;
    }

    public Timestamp getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Timestamp timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getTheSender() {
        return theSender;
    }

    public void setTheSender(String theSender) {
        this.theSender = theSender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTypeOfPayment() {
        return typeOfPayment;
    }

    public void setTypeOfPayment(String typeOfPayment) {
        this.typeOfPayment = typeOfPayment;
    }
}
