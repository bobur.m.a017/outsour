package com.smart_solution.outsource.supplier.shippedProducts;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.users.Users;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
public class ShippedProducts {

    @Id
    private UUID id = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private OrderMenu orderMenu;

    private String name;

    @Column(precision = 19, scale = 6)
    private BigDecimal sendWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal successWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal sendNumberPack;

    @Column(precision = 19, scale = 6)
    private BigDecimal successNumberPack;

    @Column(precision = 19, scale = 6)
    private BigDecimal price;

    private Timestamp timeOfShipment;

    private Timestamp timeTaken;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Users theSender;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Users receiver;

    private String status;
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Supplier supplier;

    private String paymentStatus;
    private String typeOfPayment;

    public ShippedProducts() {
    }

    public ShippedProducts(Product product, Company company, OrderMenu orderMenu, BigDecimal sendWeight, BigDecimal sendNumberPack, BigDecimal price, Timestamp timeOfShipment, Users theSender, String status, Supplier supplier, String paymentStatus, String typeOfPayment,  String comment, String name) {
        this.product = product;
        this.company = company;
        this.orderMenu = orderMenu;
        this.sendWeight = sendWeight;
        this.sendNumberPack = sendNumberPack;
        this.price = price;
        this.timeOfShipment = timeOfShipment;
        this.theSender = theSender;
        this.status = status;
        this.supplier = supplier;
        this.paymentStatus = paymentStatus;
        this.typeOfPayment = typeOfPayment;
        this.comment = comment;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public OrderMenu getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(OrderMenu orderMenu) {
        this.orderMenu = orderMenu;
    }

    public BigDecimal getSendWeight() {
        return sendWeight;
    }

    public void setSendWeight(BigDecimal sendWeight) {
        this.sendWeight = sendWeight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getSendNumberPack() {
        return sendNumberPack;
    }

    public void setSendNumberPack(BigDecimal sendNumberPack) {
        this.sendNumberPack = sendNumberPack;
    }

    public BigDecimal getSuccessNumberPack() {
        return successNumberPack;
    }

    public void setSuccessNumberPack(BigDecimal successNumberPack) {
        this.successNumberPack = successNumberPack;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Timestamp getTimeOfShipment() {
        return timeOfShipment;
    }

    public void setTimeOfShipment(Timestamp timeOfShipment) {
        this.timeOfShipment = timeOfShipment;
    }

    public Timestamp getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Timestamp timeTaken) {
        this.timeTaken = timeTaken;
    }

    public Users getTheSender() {
        return theSender;
    }

    public void setTheSender(Users theSender) {
        this.theSender = theSender;
    }

    public Users getReceiver() {
        return receiver;
    }

    public void setReceiver(Users receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTypeOfPayment() {
        return typeOfPayment;
    }

    public void setTypeOfPayment(String typeOfPayment) {
        this.typeOfPayment = typeOfPayment;
    }
}
