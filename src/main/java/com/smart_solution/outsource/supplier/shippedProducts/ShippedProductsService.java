package com.smart_solution.outsource.supplier.shippedProducts;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public record ShippedProductsService() {

    public ShippedProductsDTO parse(ShippedProducts a){
        return new ShippedProductsDTO(
                a.getId(),
                a.getProduct().getName(),
                a.getProduct().getId(),
                a.getCompany().getName(),
                a.getCompany().getId(),
                a.getOrderMenu() != null ? a.getOrderMenu().getOrderNumber() :(a.getName() != null ? a.getName() : "ZAXIRA UCHUN"),
                a.getOrderMenu() != null ? a.getOrderMenu().getId() : null,
                a.getSendWeight(),
                a.getSuccessWeight()!= null ? a.getSuccessWeight() : BigDecimal.valueOf(0),
                a.getSendNumberPack(),
                a.getSuccessNumberPack()!= null ? a.getSuccessNumberPack() : BigDecimal.valueOf(0),
                a.getPrice(),
                a.getTimeOfShipment(),
                a.getTimeTaken(),
                a.getTheSender().getName()+" "+a.getTheSender().getSurname(),
                a.getReceiver() != null ? a.getReceiver().getName()+" "+a.getReceiver().getSurname() : "",
                a.getStatus(),
                a.getSupplier().getName(),
                a.getPaymentStatus(),
                a.getTypeOfPayment(),
                a.getProduct().getPack(),
                a.getProduct().getMeasurementType(),
                a.getComment()
        );
    }
}
