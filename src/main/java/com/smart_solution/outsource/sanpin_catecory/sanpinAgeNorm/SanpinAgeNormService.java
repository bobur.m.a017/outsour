package com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm;


import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategoryRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class SanpinAgeNormService {


    private final SanpinAgeNormRepository sanpinAgeNormRepository;
    private final AgeGroupRepository ageGroupRepository;
    private final SanpinCategoryRepository sanpinCategoryRepository;

    public SanpinAgeNormService(SanpinAgeNormRepository sanpinAgeNormRepository, AgeGroupRepository ageGroupRepository, SanpinCategoryRepository sanpinCategoryRepository) {
        this.sanpinAgeNormRepository = sanpinAgeNormRepository;
        this.ageGroupRepository = ageGroupRepository;
        this.sanpinCategoryRepository = sanpinCategoryRepository;
    }

    public SanpinAgeNorm add(SanpinAgeNormDTO sanpinAgeNormDTO, SanpinCategory sanpinCategory) {

        SanpinAgeNorm sanpinAgeNorm = new SanpinAgeNorm();
        sanpinAgeNorm.setAgeGroup(ageGroupRepository.findById(sanpinAgeNormDTO.getAgeGroupId()).get());
        sanpinAgeNorm.setWeight(BigDecimal.valueOf(sanpinAgeNormDTO.getWeight()).divide(BigDecimal.valueOf(1000),4, RoundingMode.HALF_UP));
        sanpinAgeNorm.setCarbohydrates(BigDecimal.valueOf(sanpinAgeNormDTO.getCarbohydrates()).divide(BigDecimal.valueOf(1000),4, RoundingMode.HALF_UP));
        sanpinAgeNorm.setKcal(BigDecimal.valueOf(sanpinAgeNormDTO.getKcal()).divide(BigDecimal.valueOf(1000),4, RoundingMode.HALF_UP));
        sanpinAgeNorm.setOil(BigDecimal.valueOf(sanpinAgeNormDTO.getOil()).divide(BigDecimal.valueOf(1000),4, RoundingMode.HALF_UP));
        sanpinAgeNorm.setProtein(BigDecimal.valueOf(sanpinAgeNormDTO.getProtein()).divide(BigDecimal.valueOf(1000),4, RoundingMode.HALF_UP));
        sanpinAgeNorm.setSanpinCategory(sanpinCategory);
        SanpinAgeNorm save = sanpinAgeNormRepository.save(sanpinAgeNorm);

        return save;
    }

    public void edit(SanpinAgeNormDTO sanpinAgeNormDTO, Integer id) {

        if (sanpinAgeNormDTO.getId() == null) {
            SanpinCategory sanpinCategory = sanpinCategoryRepository.findById(id).get();
            SanpinAgeNorm add = add(sanpinAgeNormDTO,sanpinCategory);

            List<SanpinAgeNorm> sanpinAgeNorms = sanpinCategory.getSanpinAgeNorms();
            sanpinAgeNorms.add(add);
            sanpinCategory.setSanpinAgeNorms(sanpinAgeNorms);
            sanpinCategoryRepository.save(sanpinCategory);
        } else {
            SanpinAgeNorm sanpinAgeNorm = sanpinAgeNormRepository.findById(sanpinAgeNormDTO.getId()).get();
            sanpinAgeNorm.setAgeGroup(ageGroupRepository.findById(sanpinAgeNormDTO.getAgeGroupId()).get());
            sanpinAgeNorm.setWeight(BigDecimal.valueOf(sanpinAgeNormDTO.getWeight() / 1000));
            sanpinAgeNorm.setCarbohydrates(BigDecimal.valueOf(sanpinAgeNormDTO.getCarbohydrates() / 1000));
            sanpinAgeNorm.setKcal(BigDecimal.valueOf(sanpinAgeNormDTO.getKcal() / 1000));
            sanpinAgeNorm.setOil(BigDecimal.valueOf(sanpinAgeNormDTO.getOil()));
            sanpinAgeNorm.setProtein(BigDecimal.valueOf(sanpinAgeNormDTO.getProtein() / 1000));
            SanpinAgeNorm save = sanpinAgeNormRepository.save(sanpinAgeNorm);
            sanpinAgeNormRepository.save(sanpinAgeNorm);
        }
    }

    public SanpinAgeNormDTO parse(SanpinAgeNorm sanpinAgeNorm) {

        SanpinAgeNormDTO dto = new SanpinAgeNormDTO();

        dto.setId(sanpinAgeNorm.getId());
        dto.setWeight(Float.valueOf(sanpinAgeNorm.getWeight().multiply(BigDecimal.valueOf(1000)).toString()));
        dto.setCarbohydrates(Float.valueOf(sanpinAgeNorm.getCarbohydrates().multiply(BigDecimal.valueOf(1000)).toString()));
        dto.setKcal(Float.valueOf(sanpinAgeNorm.getKcal().multiply(BigDecimal.valueOf(1000)).toString()));
        dto.setOil(Float.valueOf(sanpinAgeNorm.getOil().multiply(BigDecimal.valueOf(1000)).toString()));
        dto.setProtein(Float.valueOf(sanpinAgeNorm.getProtein().multiply(BigDecimal.valueOf(1000)).toString()));

        dto.setAgeGroupId(sanpinAgeNorm.getAgeGroup().getId());
        dto.setAgeGroupName(sanpinAgeNorm.getAgeGroup().getName());
        return dto;
    }
}
