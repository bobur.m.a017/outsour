package com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface SanpinAgeNormRepository extends JpaRepository<SanpinAgeNorm, Integer> {

    Optional<SanpinAgeNorm> findByAgeGroupAndSanpinCategory(AgeGroup ageGroup, SanpinCategory sanpinCategory);
}
