package com.smart_solution.outsource.sanpin_catecory;

import org.springframework.data.jpa.repository.JpaRepository;


public interface SanpinCategoryRepository extends JpaRepository<SanpinCategory, Integer> {
    boolean existsByName(String name);
}
