package com.smart_solution.outsource.sanpin_catecory;


import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNormDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class SanpinDTO {

    private Integer id;
    private String name;

    private List<SanpinAgeNormDTO> sanpinAgeNormDTOList;

    public SanpinDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public SanpinDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SanpinAgeNormDTO> getSanpinAgeNormDTOList() {
        return sanpinAgeNormDTOList;
    }

    public void setSanpinAgeNormDTOList(List<SanpinAgeNormDTO> sanpinAgeNormDTOList) {
        this.sanpinAgeNormDTOList = sanpinAgeNormDTOList;
    }
}
