package com.smart_solution.outsource.sanpin_catecory;

import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.multiMenu.MultiMenu;
import com.smart_solution.outsource.multiMenu.MultiMenuRepository;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.product_category.ProductCategory;
import com.smart_solution.outsource.product_category.TestAGSDTO;
import com.smart_solution.outsource.product_category.TestPCDTO;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNorm;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormRepository;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormResponseDTO;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNorm;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNormDTO;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNormRepository;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNormService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public record SanpinService(
        SanpinCategoryRepository sanpinCategoryRepository,
        SanpinAgeNormService sanpinAgeNormService,
        ProductRepository productRepository,
        SanpinMenuNormRepository sanpinMenuNormRepository,
        MultiMenuRepository multiMenuRepository
) {

    public StateMessage add(SanpinDTO sanpinDTO) {

        boolean res = sanpinCategoryRepository.existsByName(sanpinDTO.getName());

        if (!res) {

            SanpinCategory sanpinCategory = new SanpinCategory();
            sanpinCategory.setName(sanpinDTO.getName());

            List<SanpinAgeNorm> list = new ArrayList<>();

            for (SanpinAgeNormDTO sanpinAgeNormDTO : sanpinDTO.getSanpinAgeNormDTOList()) {

                list.add(sanpinAgeNormService.add(sanpinAgeNormDTO, sanpinCategory));

            }
            sanpinCategory.setSanpinAgeNorms(list);

            sanpinCategoryRepository.save(sanpinCategory);

            return new StateMessage("Muvaffaqiyatli qo`shildi", true);

        }
        return new StateMessage("Bunday nomli sanpin categoriya mavjud iltimos tekshirib qaytadan kiriting", false);
    }

    public StateMessage edit(SanpinDTO sanpinDTO, Integer id) {

        SanpinCategory sanpinCategory = sanpinCategoryRepository.findById(id).get();

        boolean res = false;

        if (!sanpinCategory.getName().equals(sanpinDTO.getName())) {
            res = sanpinCategoryRepository.existsByName(sanpinDTO.getName());
        }

        if (!res) {

            sanpinCategory.setName(sanpinDTO.getName());
            for (SanpinAgeNormDTO sanpinAgeNormDTO : sanpinDTO.getSanpinAgeNormDTOList()) {
                sanpinAgeNormService.edit(sanpinAgeNormDTO, id);
            }
            sanpinCategoryRepository.save(sanpinCategory);

            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

        } else {
            return new StateMessage("Bunday nomli sanpin categoriya mavjud iltimos tekshirib qaytadan kiriting", false);
        }
    }

    public List<SanpinDTO> get() {

        List<SanpinDTO> list = new ArrayList<>();

        for (SanpinCategory sanpinCategory : sanpinCategoryRepository.findAll()) {

            List<SanpinAgeNormDTO> ageNormDTOList = new ArrayList<>();

            for (SanpinAgeNorm sanpinAgeNorm : sanpinCategory.getSanpinAgeNorms()) {
                ageNormDTOList.add(sanpinAgeNormService.parse(sanpinAgeNorm));
            }
            SanpinDTO sanpinDTO = new SanpinDTO();
            sanpinDTO.setId(sanpinCategory.getId());
            sanpinDTO.setName(sanpinCategory.getName());
            sanpinDTO.setSanpinAgeNormDTOList(ageNormDTOList);

            list.add(sanpinDTO);
        }
        return list;
    }

    public StateMessage delete(Integer id) {
        boolean res = productRepository.existsBySanpinCategory_Id(id);

        if (!res) {
            for (SanpinMenuNorm sanpinMenuNorm : sanpinMenuNormRepository.findAllBySanpinCategory_Id(id)) {
                for (MultiMenu multiMenu : multiMenuRepository.findAll()) {
                    multiMenu.getSanpinMenuNormList().remove(sanpinMenuNorm);
                }
                sanpinMenuNormRepository.delete(sanpinMenuNorm);
            }
            sanpinCategoryRepository.deleteById(id);
        }

        return res ? new StateMessage("O`chirilmadi ushbu kategoriyaga maxsulot biriktirilgan", false) : new StateMessage("O`chirildi, qayta tiklab bo`lmaydi", true);
    }

    public List<TestPCDTO> getTest() {

        List<TestPCDTO> list = new ArrayList<>();
        List<SanpinCategory> all = sanpinCategoryRepository.findAll();

        all.sort(Comparator.comparing(SanpinCategory::getId));

        for (SanpinCategory sanpinCategory :all ) {
            TestPCDTO dto = new TestPCDTO();

            dto.setName(sanpinCategory.getName());
            List<TestAGSDTO> dtoList = new ArrayList<>();

            for (SanpinAgeNorm sanpinAgeNorm : sanpinCategory.getSanpinAgeNorms()) {
                TestAGSDTO agsdto = new TestAGSDTO();
                agsdto.setWeight(sanpinAgeNorm.getWeight());
                agsdto.setAgeGroupId(sanpinAgeNorm.getAgeGroup().getId());
                agsdto.setCarbohydrates(sanpinAgeNorm.getCarbohydrates());
                agsdto.setKcal(sanpinAgeNorm.getKcal());
                agsdto.setOil(sanpinAgeNorm.getOil());
                agsdto.setProtein(sanpinAgeNorm.getProtein());
                agsdto.setAgeGroupName(sanpinAgeNorm.getAgeGroup().getName());
                dtoList.add(agsdto);
            }
            dto.setSanpinAgeNormDTOList(dtoList);
            list.add(dto);
        }
        return list;
    }
}
