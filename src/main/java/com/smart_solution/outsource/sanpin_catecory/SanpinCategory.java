package com.smart_solution.outsource.sanpin_catecory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNorm;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Entity
public class SanpinCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @JsonIgnore
    @OneToMany
    private List<SanpinAgeNorm> sanpinAgeNorms;





    public SanpinCategory(String name) {
        this.name = name;
    }

    public SanpinCategory(String name, Float norma3_4, Float norma4_7) {
        this.name = name;
    }

    public SanpinCategory() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SanpinAgeNorm> getSanpinAgeNorms() {
        return sanpinAgeNorms;
    }

    public void setSanpinAgeNorms(List<SanpinAgeNorm> sanpinAgeNorms) {
        this.sanpinAgeNorms = sanpinAgeNorms;
    }
}
