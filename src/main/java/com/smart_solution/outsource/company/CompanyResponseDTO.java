package com.smart_solution.outsource.company;


import com.smart_solution.outsource.kindergarten.KindergartenResponseDTO;
import com.smart_solution.outsource.supplier.SupplierResponseDTO;
import com.smart_solution.outsource.users.UsersResponseDTO;

import java.util.List;

public class CompanyResponseDTO {

    private Integer id;
    private String companyName;
    private String accountNumber;
    private String mfo;
    private String inn;
    private String phoneNumber;
    private String region;
    private String district;
    private String street;

    private List<UsersResponseDTO> usersList;
    private List<KindergartenResponseDTO> kindergartenList;
    private List<SupplierResponseDTO> supplierList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMfo() {
        return mfo;
    }

    public void setMfo(String mfo) {
        this.mfo = mfo;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public List<UsersResponseDTO> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<UsersResponseDTO> usersList) {
        this.usersList = usersList;
    }

    public List<KindergartenResponseDTO> getKindergartenList() {
        return kindergartenList;
    }

    public void setKindergartenList(List<KindergartenResponseDTO> kindergartenList) {
        this.kindergartenList = kindergartenList;
    }

    public List<SupplierResponseDTO> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<SupplierResponseDTO> supplierList) {
        this.supplierList = supplierList;
    }
}
