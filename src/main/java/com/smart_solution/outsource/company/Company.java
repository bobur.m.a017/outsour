package com.smart_solution.outsource.company;

import com.smart_solution.outsource.address.Address;
import com.smart_solution.outsource.info.Info;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.payment.Payment;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.productSuppiler.ProductSupplier;
import com.smart_solution.outsource.storage.garbage.GarbageProduct;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String name;

    @ManyToOne
    private Address address;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    private String status;

    @OneToOne
    private Info info;

    @OneToMany
    private List<Kindergarten> kindergarten;


    @OneToMany
    private List<ProductSupplier> productSupplier;

    @OneToMany(mappedBy = "company" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<OrderMenu> orderList;


    @OneToMany(mappedBy = "company" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Payment> paymentList;





    //Ushbu korxonaga ta`minotchilar tomonidan yuboriladigan maxsulotlar
    @OneToMany(mappedBy = "company" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShipped> productsToBeShippedList;


    //Ushbu korxonaga ta`minotchilar tomonidan yuborilgan maxsulotlar
    @OneToMany(mappedBy = "company" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProducts> shippedProductsList;

    //Ushbu korxonadan mttlarga yuboriladigan maxsulotlar
    @OneToMany(mappedBy = "company" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShippedCompany> productsToBeShippedCompanyList;

    //Ushbu korxonadan mttlarga yuborilgan maxsulotlar
    @OneToMany(mappedBy = "company" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProductsCompany> shippedProductsCompanyList;


    // Aynigan maxsulotlar saqlanadi.
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    private List<GarbageProduct> qualityDeteriorates;


    // Omborda mavjud maxsulotlar saqlanadi.
    @OneToMany(cascade = CascadeType.ALL)
    private List<ProductBalancer> shouldBeSent;

    // Bu yerda zayafkadan tashqari olib kelingan yoki ortib qolgan maxsulotlar saqlanadi.
    @OneToMany(cascade = CascadeType.ALL)
    private List<ProductBalancer> reserve;


    public List<Payment> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<Payment> paymentList) {
        this.paymentList = paymentList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Kindergarten> getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(List<Kindergarten> kindergarten) {
        this.kindergarten = kindergarten;
    }

    public List<ProductSupplier> getProductSupplier() {
        return productSupplier;
    }

    public void setProductSupplier(List<ProductSupplier> productSupplier) {
        this.productSupplier = productSupplier;
    }

    public List<OrderMenu> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderMenu> orderList) {
        this.orderList = orderList;
    }

    public List<ProductsToBeShipped> getProductsToBeShippedList() {
        return productsToBeShippedList;
    }

    public void setProductsToBeShippedList(List<ProductsToBeShipped> productsToBeShippedList) {
        this.productsToBeShippedList = productsToBeShippedList;
    }

    public List<ShippedProducts> getShippedProductsList() {
        return shippedProductsList;
    }

    public void setShippedProductsList(List<ShippedProducts> shippedProductsList) {
        this.shippedProductsList = shippedProductsList;
    }

    public List<ProductsToBeShippedCompany> getProductsToBeShippedCompanyList() {
        return productsToBeShippedCompanyList;
    }

    public void setProductsToBeShippedCompanyList(List<ProductsToBeShippedCompany> productsToBeShippedCompanyList) {
        this.productsToBeShippedCompanyList = productsToBeShippedCompanyList;
    }

    public List<ShippedProductsCompany> getShippedProductsCompanyList() {
        return shippedProductsCompanyList;
    }

    public void setShippedProductsCompanyList(List<ShippedProductsCompany> shippedProductsCompanyList) {
        this.shippedProductsCompanyList = shippedProductsCompanyList;
    }

    public List<GarbageProduct> getQualityDeteriorates() {
        return qualityDeteriorates;
    }

    public void setQualityDeteriorates(List<GarbageProduct> qualityDeteriorates) {
        this.qualityDeteriorates = qualityDeteriorates;
    }

    public List<ProductBalancer> getShouldBeSent() {
        return shouldBeSent;
    }

    public void setShouldBeSent(List<ProductBalancer> shouldBeSent) {
        this.shouldBeSent = shouldBeSent;
    }

    public List<ProductBalancer> getReserve() {
        return reserve;
    }

    public void setReserve(List<ProductBalancer> reserve) {
        this.reserve = reserve;
    }
}
