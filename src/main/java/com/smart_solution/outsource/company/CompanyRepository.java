package com.smart_solution.outsource.company;

import com.smart_solution.outsource.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface CompanyRepository extends JpaRepository<Company, Integer> {
    boolean existsByName(String name);
    Optional<Company> findByName(String name);
}
