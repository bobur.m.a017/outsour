package com.smart_solution.outsource.company;

import com.smart_solution.outsource.address.Address;
import com.smart_solution.outsource.address.AddressService;
import com.smart_solution.outsource.info.Info;
import com.smart_solution.outsource.info.InfoService;
import com.smart_solution.outsource.kindergarten.KindergartenService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.role.RoleRepository;
import com.smart_solution.outsource.role.RoleType;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.supplier.SupplierService;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.UserService;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyService implements CompanyParseDTO {

    @Autowired
    private  CompanyRepository companyRepository;
    @Autowired
    private  UsersRepository usersRepository;
    @Autowired
    private  PasswordEncoder passwordEncoder;
    @Autowired
    private  AddressService addressService;
    @Autowired
    private  InfoService infoService;
    @Autowired
    private  UserService userService;
    @Autowired
    private  SupplierService supplierService;
    @Autowired
    private  KindergartenService kindergartenService;
    @Autowired
    private  RoleRepository roleRepository;

    public CompanyService() {
    }

    public StateMessage add(CompanyDTO companyDTO) {
        boolean res = companyRepository.existsByName(companyDTO.getCompanyName());

        if (!res) {
            Company company = new Company();
            company.setName(companyDTO.getCompanyName());

            Info info = infoService.add(companyDTO.getAccountNumber(), companyDTO.getMfo(), companyDTO.getInn(), companyDTO.getPhoneNumber());
            company.setInfo(info);

            Address address = addressService.add(companyDTO.getDistrictId(), companyDTO.getStreet());
            company.setAddress(address);

            Company saveCompany = companyRepository.save(company);

            usersRepository.save(new Users(
                    companyDTO.getLeaderName(),
                    companyDTO.getLeaderFatherName(),
                    companyDTO.getLeaderSurname(),
                    companyDTO.getUserName(),
                    passwordEncoder.encode(companyDTO.getPassword()),
                    companyDTO.getLeaderPhoneNumber(),
                    roleRepository.findByName(RoleType.CHIEF.getName()).get(),
                    saveCompany,
                    true
            ));

            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
        } else {
            return new StateMessage("Bunday nomli kompaniya avval qo`shilgan", false);
        }
    }

    public StateMessage edit(CompanyDTO companyDTO, Integer id, ResponseUser responseUser) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        boolean res = true;

        if (!companyDTO.getCompanyName().equals(company.getName())) {
            res = !companyRepository.existsByName(companyDTO.getCompanyName());
        }

        if (res) {
            company.setName(companyDTO.getCompanyName());

            infoService.edit(company.getInfo(), companyDTO.getAccountNumber(), companyDTO.getMfo(), companyDTO.getInn(), companyDTO.getPhoneNumber());

            addressService.edit(company.getAddress(), companyDTO.getDistrictId(), companyDTO.getStreet());

            companyRepository.save(company);
            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

        } else {
            return new StateMessage("Bunday nomli korxona avval ro`yxatdan o`tgan iltimos tekshirib qaytadan kiriting", false);
        }
    }

    public List<CompanyResponseDTO> get(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        List<Company> companyList;

        if (users.getRole().getName().equals(RoleType.CHIEF.getName())) {
            companyList = List.of(users.getCompany());
        } else {
            companyList = companyRepository.findAll();
        }


        List<CompanyResponseDTO> list = new ArrayList<>();

        for (Company company : companyList) {

            CompanyResponseDTO companyResponseDTO = parse(company);
            companyResponseDTO.setUsersList(userService.get(company));
            companyResponseDTO.setSupplierList(supplierService.get(company));
            companyResponseDTO.setKindergartenList(kindergartenService.get(company));

            list.add(companyResponseDTO);
        }
        return list;
    }

    public CompanyResponseDTO getOne(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();


        return parse(users.getCompany());
    }

    public StateMessage register(CompanyDTO companyDTO) {
        boolean res = companyRepository.existsByName(companyDTO.getCompanyName());
        boolean res1 = usersRepository.existsByUserName(companyDTO.getUserName());

        if (res) {
            return new StateMessage("Bunday nomli korxona avval ro`yxatdan o`tgan iltimos tekshirib qaytadan kiriting kiriting", false);
        }
        if (res1) {
            return new StateMessage("Ushbu username bilan avval foydalanuvchi ro`yxatdan o`tgan. Iltimos boshqa username tanlang", false);
        }

        Company company = new Company();
        company.setName(companyDTO.getCompanyName());

        Info info = infoService.add(companyDTO.getAccountNumber(), companyDTO.getMfo(), companyDTO.getInn(), companyDTO.getPhoneNumber());
        company.setInfo(info);

        Address address = addressService.add(companyDTO.getDistrictId(), companyDTO.getStreet());
        company.setAddress(address);
        company.setStatus(Status.APPEALED_FOR_REGISTRATION.getName());

        Company saveCompany = companyRepository.save(company);

        usersRepository.save(new Users(
                companyDTO.getLeaderName(),
                companyDTO.getLeaderFatherName(),
                companyDTO.getLeaderSurname(),
                companyDTO.getUserName(),
                passwordEncoder.encode(companyDTO.getPassword()),
                companyDTO.getLeaderPhoneNumber(),
                roleRepository.findByName(RoleType.CHIEF.getName()).get(),
                saveCompany,
                false
        ));

        return new StateMessage("Arizangiz muvaffaqiyatli yuborildi", true);
    }

    public StateMessage changeStatus(Integer id, boolean ans) {

        Company company = companyRepository.findById(id).get();
        List<Users> usersList = usersRepository.findAllByCompany(company);

        if (ans) {
            company.setStatus(Status.SUCCESS.getName());

            for (Users users : usersList) {
                users.setState(true);
                usersRepository.save(users);
            }
            companyRepository.save(company);
            return new StateMessage("Muvaffaqiyatli tastiqlandi", true);
        } else {
            usersRepository.deleteAll(usersList);
            companyRepository.delete(company);
            return new StateMessage("Muvaffaqiyatli o`chirildi", true);
        }
    }

    public List<CompanyResponseDTO> getAll(Long start, Long end) {

        List<Company> companyList = companyRepository.findAll();
        List<CompanyResponseDTO> list = new ArrayList<>();

        for (Company company : companyList) {

            if (start != null && end != null) {
                if (start < company.getCreateDate().getTime() && end > company.getCreateDate().getTime()) {
                    CompanyResponseDTO companyResponseDTO = parse(company);
                    list.add(companyResponseDTO);
                }
            } else {
                if (company.getStatus().equals(Status.APPEALED_FOR_REGISTRATION.getName())) {
                    CompanyResponseDTO companyResponseDTO = parse(company);
                    list.add(companyResponseDTO);
                }
            }
        }
        return list;
    }
}