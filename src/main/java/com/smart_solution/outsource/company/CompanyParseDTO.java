package com.smart_solution.outsource.company;

public interface CompanyParseDTO {

    default public CompanyResponseDTO parse(Company company) {

        CompanyResponseDTO dto = new CompanyResponseDTO();
        dto.setId(company.getId());
        dto.setCompanyName(company.getName());
        dto.setAccountNumber(company.getInfo().getAccountNumber());
        dto.setMfo(company.getInfo().getMfo());
        dto.setInn(company.getInfo().getInn());
        dto.setPhoneNumber(company.getInfo().getPhoneNumber());
        dto.setRegion(company.getAddress().getRegion().getName());
        dto.setDistrict(company.getAddress().getDistrict().getName());
        dto.setStreet(company.getAddress().getStreet());

        return dto;
    }
}
