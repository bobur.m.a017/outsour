package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.meal.MealDTO;
import com.smart_solution.outsource.meal.MealService;
import com.smart_solution.outsource.menu.mealTime.MealTimeDTO;
import com.smart_solution.outsource.menu.mealTime.MealTimeService;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/out/api/mealTime")
public class MealTimeController {

    private final MealTimeService mealTimeService;

    public MealTimeController(MealTimeService mealTimeService) {
        this.mealTimeService = mealTimeService;
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody MealTimeDTO mealTimeDTO) {
        try{
        StateMessage stateMessage = mealTimeService.add(mealTimeDTO);
        return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody MealTimeDTO mealTimeDTO) {
        try {
            StateMessage stateMessage = mealTimeService.edit(mealTimeDTO, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping
    public ResponseEntity<?> get() {
        try {
            List<MealTimeDTO> list = mealTimeService.get();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage stateMessage = mealTimeService.delete(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
