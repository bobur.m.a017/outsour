package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.productSuppiler.ProductSupplierDTO;
import com.smart_solution.outsource.productSuppiler.ProductSupplierResponseDTO;
import com.smart_solution.outsource.productSuppiler.ProductSupplierService;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/out/api/productSupplier")
public class ProductSupplierController {

    private final ProductSupplierService productSupplierService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;


    public ProductSupplierController(ProductSupplierService productSupplierService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.productSupplierService = productSupplierService;
        this.authenticationFilter = authenticationFilter;
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody ProductSupplierDTO productSupplierDTO, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = productSupplierService.add(productSupplierDTO, responseUser);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());

        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @GetMapping
    public ResponseEntity<?> get(HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ProductSupplierResponseDTO> list = productSupplierService.get(responseUser);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());

        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {

        try {
            StateMessage stateMessage =  productSupplierService.delete(id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
