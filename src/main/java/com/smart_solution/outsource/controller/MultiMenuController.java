package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.calculate.CalculateProduct;
import com.smart_solution.outsource.calculate.CalculateService;
import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.dto.MenuDTO;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveResponseDTO;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveService;
import com.smart_solution.outsource.menu.menu.MenuRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.multiMenu.menu.KindergartenByAddressResponseDTO;
import com.smart_solution.outsource.multiMenu.menu.MenuAddKindergartenDTO;
import com.smart_solution.outsource.multiMenu.MultiMenuDTO;
import com.smart_solution.outsource.multiMenu.MultiMenuResponseDTO;
import com.smart_solution.outsource.multiMenu.MultiMenuService;
import com.smart_solution.outsource.multiMenu.menu.MenuSaveEditDTO;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/out/api/multiMenu")
public class MultiMenuController {

    private final MultiMenuService multiMenuService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final MenuSaveService menuSaveService;
    private final MenuRepository menuRepository;
    private final CalculateService calculateService;

    public MultiMenuController(MultiMenuService multiMenuService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, MenuSaveService menuSaveService, MenuRepository menuRepository, CalculateService calculateService) {
        this.multiMenuService = multiMenuService;
        this.authenticationFilter = authenticationFilter;
        this.menuSaveService = menuSaveService;
        this.menuRepository = menuRepository;
        this.calculateService = calculateService;
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody MultiMenuDTO multiMenuDTO) {
        try {
        StateMessage stateMessage = multiMenuService.add(multiMenuDTO);
        return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
    @GetMapping
    public ResponseEntity<?> get() {
        try {
            List<MultiMenuResponseDTO> list = multiMenuService.get();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
    @PostMapping("/addMenu")
    public ResponseEntity<?> addMenu(@RequestBody MenuAddKindergartenDTO dto) {
        try {
            StateMessage stateMessage = multiMenuService.addMenu(dto);

            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN','HAMSHIRA')")
    @GetMapping("/getMenu")
    public ResponseEntity<?> getMenu(@Param(value = "date") Long date, HttpServletRequest request) {

        ResponseUser responseUser = authenticationFilter.parseToken(request);

        try {
        List<KindergartenByAddressResponseDTO> list = multiMenuService.getMenu(responseUser, date);
        return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

//    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
//    @PatchMapping("/edit")
//    public ResponseEntity<?> edit(@RequestBody MenuSaveEditDTO menuSaveEditDTO) {
//
//        try {
//            menuSaveService.add(menuRepository.findById(menuSaveEditDTO.getMenuId()).get(), menuSaveEditDTO.getDate(), menuSaveEditDTO.getKindergartenId(), menuSaveEditDTO.getMultiMenuId());
//            return ResponseEntity.status(200).body(new StateMessage("O`zgartirildi", true));
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
//    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
    @PatchMapping("/deleteMenuSave/{id}")
    public ResponseEntity<?> deleteMenuSave(@PathVariable Integer id) {

        try {
            StateMessage stateMessage = menuSaveService.delete(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {

        try {
            StateMessage stateMessage = multiMenuService.delete(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('HAMSHIRA','OSHPAZ','BOG`CHA MUDIRASI')")
    @GetMapping("/getMenuKin")
    public ResponseEntity<?> getMenuKin(@Param(value = "date") Long date, HttpServletRequest request) {

        ResponseUser responseUser = authenticationFilter.parseToken(request);

        try {
            MenuSaveResponseDTO menuKin = multiMenuService.getMenuKin(responseUser, date);

            if (menuKin != null) {
                return ResponseEntity.status(200).body(menuKin);
            } else {
                return ResponseEntity.status(200).body(new StateMessage("Bu kunga hali menyu biriktirilmagan.", false));
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
    @GetMapping("/getMultiMenu/{id}")
    public ResponseEntity<?> getMultiMenu(@PathVariable Integer id, HttpServletRequest request) {

        ResponseUser responseUser = authenticationFilter.parseToken(request);

        try {
        List<MenuDTO> list = multiMenuService.getMenu(id);
        return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/getTest")
    public ResponseEntity<?> getTest(@Param("menuId") Integer menuId, @Param("ageGroupId") Integer ageGroupId, @Param("number") Integer number) {


        try {
        List<CalculateProduct> list = calculateService.getwqwedrw(menuId, ageGroupId, number);
        return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
