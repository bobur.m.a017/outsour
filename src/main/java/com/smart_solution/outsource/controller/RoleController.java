package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.role.RoleDTO;
import com.smart_solution.outsource.role.RoleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/out/api/role")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @GetMapping("/{name}")
    public ResponseEntity<?> get(@PathVariable String name) {
        try {
            List<RoleDTO> list = roleService.get(name);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
