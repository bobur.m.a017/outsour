package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.dto.SendProductDTO;
import com.smart_solution.outsource.dto.StringDto;
import com.smart_solution.outsource.kindergarten.supplier.SupplierKinProductDTO;
import com.smart_solution.outsource.kindergarten.supplier.SupplierKindergartenDTO;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.order.OrderMenuDTO;
import com.smart_solution.outsource.order.OrderMenuResponseDTO;
import com.smart_solution.outsource.order.OrderMenuServiceNew;
import com.smart_solution.outsource.order.dto.MenuSaveIdDate;
import com.smart_solution.outsource.order.dto.OrderProductDTO;
import com.smart_solution.outsource.order.numberOfKids.numberOfKidsDTO.NumberOfKidsKinDTO;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergartenDTO;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/out/api/order")
public class OrderMenuController {

    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final OrderMenuServiceNew orderMenuServiceNew;

    public OrderMenuController(ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, OrderMenuServiceNew orderMenuServiceNew) {
        this.authenticationFilter = authenticationFilter;
        this.orderMenuServiceNew = orderMenuServiceNew;
    }

    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody StringDto number, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = orderMenuServiceNew.add(responseUser, number.getNumber());
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @PatchMapping("{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody StringDto editDto) {

        try {
            StateMessage stateMessage = orderMenuServiceNew.edit(editDto, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @PostMapping("/addKin/{orderId}")
    public ResponseEntity<?> addKin(@PathVariable Integer orderId, @RequestBody OrderKindergartenDTO orderKindergartenDTO, HttpServletRequest request) {

        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            Timestamp t1 = new Timestamp(System.currentTimeMillis());

            StateMessage stateMessage = orderMenuServiceNew.addKin(orderKindergartenDTO, orderId);
            Timestamp t2 = new Timestamp(System.currentTimeMillis());
            System.out.println(t1 + "\n" + t2);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getDate/{kindergartenId}")
    public ResponseEntity<?> getDate(@PathVariable Integer kindergartenId, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<MenuSaveIdDate> list = orderMenuServiceNew.getDateKin(kindergartenId);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<OrderMenuDTO> list = orderMenuServiceNew.getAll(responseUser);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @GetMapping("{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {

        try {
            OrderMenuResponseDTO dto = orderMenuServiceNew.get(id);
            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @PatchMapping("/send/{id}")
    public ResponseEntity<?> send(@PathVariable Integer id, @RequestBody List<Integer> list, HttpServletRequest request) {

//        try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);
        StateMessage message = orderMenuServiceNew.send(id, responseUser, list);
        return ResponseEntity.status(200).body(message);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = orderMenuServiceNew.delete(id, responseUser);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @PostMapping("/sendProduct")
    public ResponseEntity<?> sendProduct(@Param("name") String name, @RequestBody List<SendProductDTO> list, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage;
            if (name == null) {
                stateMessage = new StateMessage("Zayafka raqamini kiritilmagan.", false);
            }else {

                stateMessage = orderMenuServiceNew.sendProduct(responseUser, list, name);
            }

            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @PutMapping("/editSendProduct/{id}")
    public ResponseEntity<?> editSendProduct(@PathVariable UUID id, @RequestBody SendProductDTO dto, HttpServletRequest request) {

        try {
//            return ResponseEntity.status(200).body(new StateMessage("XATOLIK", false));
            StateMessage stateMessage = orderMenuServiceNew.editSendProduct(dto, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getNumberOfKids/{id}")
    public ResponseEntity<?> getNumberOfKids(@PathVariable Integer id) {

        try {
            List<NumberOfKidsKinDTO> list = orderMenuServiceNew.getNumberOfKids(id);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getProduct/{id}")
    public ResponseEntity<?> getProduct(@PathVariable Integer id) {
        try {
            List<OrderProductDTO> list = orderMenuServiceNew.getProductByOrder(id);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getSupplierKindergarten")
    public ResponseEntity<?> getSupplierKindergarten(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<SupplierKindergartenDTO> list = orderMenuServiceNew.getSupplierKindergarten(responseUser);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BUXGALTER')")
    @PostMapping("/addSupplierKin/{id}")
    public ResponseEntity<?> addSupplierKin(@PathVariable Integer id, @Param("kindergartenId") Integer kindergartenId, @Param("agreementId") Integer agreementId, @RequestBody List<SupplierKinProductDTO> list, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);

            StateMessage message = orderMenuServiceNew.addSupplierKin(kindergartenId, list, id, agreementId);
            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
