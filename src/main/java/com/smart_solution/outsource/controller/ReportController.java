package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.act.SupplierReportService;
import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.consumption.ConsumptionService;
import com.smart_solution.outsource.dto.ReportDTO;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.report.Report;
import com.smart_solution.outsource.report.ReportService;
import com.smart_solution.outsource.report.dto.MenuDTO;
import com.smart_solution.outsource.report.dto.ReportPriceDTO;
import com.smart_solution.outsource.users.ResponseUser;
import io.swagger.models.auth.In;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/out/api/report")
public class ReportController {


    private final ConsumptionService consumptionService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final ReportService reportService;
    private final SupplierReportService supplierReportService;


    public ReportController(ConsumptionService consumptionService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, ReportService reportService, SupplierReportService supplierReportService) {
        this.consumptionService = consumptionService;
        this.authenticationFilter = authenticationFilter;
        this.reportService = reportService;
        this.supplierReportService = supplierReportService;
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TEXNOLOG','BUXGALTER')")
    @PatchMapping
    public ResponseEntity<?> get(@RequestBody ReportDTO dto, HttpServletRequest request) {
//        try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);
        Report report = reportService.get(dto.getStart(), dto.getEnd(), dto.getList(), responseUser);
        if (report == null) {
            return ResponseEntity.status(500).body(new StateMessage("Ma`lumot mavjud emas", false));
        }
        return ResponseEntity.status(200).body(report);

//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TEXNOLOG','BUXGALTER','HAMSHIRA','OSHPAZ','BOG`CHA MUDIRASI')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getMenu(@Param("start") Long start, @Param("end") Long end, @PathVariable Integer id, HttpServletRequest request) {
        try {

        ResponseUser responseUser = authenticationFilter.parseToken(request);

        List<MenuDTO> list = reportService.getMenu(responseUser, id, start, end);
        return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','BUXGALTER')")
    @GetMapping("/act/{supplierId}")
    public ResponseEntity<?> getSupplierReport(@PathVariable Integer supplierId, @Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
//        try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);
        if (supplierId == null || start == null || end == null) {
            return ResponseEntity.status(200).body("Noto`g`ri so`rov. Barcha qatorlar toldirilishi shart");
        }
        return ResponseEntity.status(200).body(supplierReportService.getReport(start, end, supplierId, responseUser));
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }
//
//    @PreAuthorize("hasAnyRole('BUXGALTER')")
//    @GetMapping("/getTest")
//    public ResponseEntity<?> getTest() {
////        try {
//        return ResponseEntity.status(200).body(new StateMessage("Ruxsat etilmagan yo`l",false));
////        return ResponseEntity.status(200).body(consumptionService.getTest());
////        } catch (Exception e) {
////            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
////        }
//    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getPriceReport/{kindergartenId}")
    public ResponseEntity<?> getPriceReport(@PathVariable Integer kindergartenId, @Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
        try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);

       List<ReportPriceDTO> list = reportService.getPriceReport(responseUser,kindergartenId,start,end);


        return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}










