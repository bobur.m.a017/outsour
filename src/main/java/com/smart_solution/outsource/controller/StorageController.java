package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.dto.*;
import com.smart_solution.outsource.inOut.InOutDTO;
import com.smart_solution.outsource.inOutProduct.InOutProductService;
import com.smart_solution.outsource.kindergarten.KindergartenRequiredNumberDTO;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.productBalancer.ProductBalancerResponseDTO;
import com.smart_solution.outsource.storage.StorageService;
import com.smart_solution.outsource.storage.garbage.GarbageProductDTO;
import com.smart_solution.outsource.storage.garbage.GarbageProductResponseDTO;
import com.smart_solution.outsource.storage.garbage.GarbageProductService;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompanyDTO;
import com.smart_solution.outsource.storage.report.StorageReportDTO;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompanyDTO;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedDTO;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsDTO;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/out/api/storage")
public class StorageController {

    private final StorageService storageService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final GarbageProductService garbageProductService;
    private final InOutProductService inOutProductService;

    public StorageController(StorageService storageService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, GarbageProductService garbageProductService, InOutProductService inOutProductService) {
        this.storageService = storageService;
        this.authenticationFilter = authenticationFilter;
        this.garbageProductService = garbageProductService;
        this.inOutProductService = inOutProductService;
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','OMBORCHI','BUXGALTER','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getExistingProduct")
    public ResponseEntity<?> getExistingProduct(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ProductBalancerResponseDTO> list = storageService.getExistingProductCom(responseUser);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','OMBORCHI','BUXGALTER','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getProductBalancer")
    public ResponseEntity<?> getProductBalancer(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ProductBalancerResponseDTO> list = storageService.getShouldBeSentCom(responseUser);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PostMapping("/garbageAdd")
    public ResponseEntity<?> addGarbage(@RequestBody GarbageProductDTO dto, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = garbageProductService.add(responseUser, dto);
            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','OSHPAZ')")
    @DeleteMapping("/garbageDelete/{id}")
    public ResponseEntity<?> deleteGarbage(@PathVariable Integer id) {

        try {

            StateMessage message = garbageProductService.delete(id);
            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','TEXNOLOG','RAHBAR')")
    @GetMapping("/garbageGet")
    public ResponseEntity<?> getGarbage(@Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<GarbageProductResponseDTO> list = garbageProductService.get(responseUser, start, end);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PutMapping("/garbageChange/{id}")
    public ResponseEntity<?> garbageChangeStatus(@PathVariable Integer id, @RequestBody BooleanDto dto, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = garbageProductService.changeStatus(id, dto, responseUser);
            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @GetMapping("/getInOut")
    public ResponseEntity<?> getInOut(@Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ShippedProductsDTO> list = storageService.getInOut(responseUser, start, end);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PutMapping("/receive/{id}")
    public ResponseEntity<?> receive(@PathVariable UUID id, @RequestBody ReceiveDto dto, HttpServletRequest request) {

        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);
//            StateMessage message = new StateMessage("dsfc", false);

            StateMessage message = storageService.receiveCom(id, dto, responseUser);
            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PostMapping("/addShippedProduct/{id}")
    public ResponseEntity<?> addShippedProduct(@PathVariable UUID id, @RequestBody InOutDTO dto, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message;
            if (dto.getWeight() <= 0) {
                message = new StateMessage("Kiritilgan son 0 dan katta bo`lishi kerak", false);
            } else {
//                message = new StateMessage("Kiritilgan son 0 dan katta bo`lishi kerak", false);
                message = storageService.addShippedProductCom(responseUser, dto, id);
            }

            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @GetMapping("/getRequiredProduct/{id}")
    public ResponseEntity<?> getRequiredProduct(@PathVariable Integer id, @Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<ProductsToBeShippedCompanyDTO> list = storageService.getRequiredProductCom(id, responseUser, start, end);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @GetMapping("/getShippedProduct/{id}")
    public ResponseEntity<?> getShippedProduct(@PathVariable Integer id, @Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<ShippedProductsCompanyDTO> list = storageService.getShippedProductCom(id, responseUser, start, end);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @GetMapping("/getShippingLetter/{id}")
    public ResponseEntity<?> getShippingLetter(@PathVariable Integer id, @Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<ShippingLetter> list = storageService.getShippingLetter(id, responseUser, start, end);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @GetMapping("/checkProduct/{id}")
    public ResponseEntity<?> checkProduct(@PathVariable Integer id, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<CheckProductDTO> list = storageService.checkProduct(id, responseUser);

            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @GetMapping("/addShippedProductAllKin/{id}")
    public ResponseEntity<?> addShippedProductAllKin(@PathVariable Integer id, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            StateMessage message = storageService.addShippedProductAllKin(id, responseUser);

            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PostMapping("/sendingSpecifiedProducts")
    public ResponseEntity<?> sendingSpecifiedProducts(@RequestBody List<UUID> list, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            StateMessage message = storageService.sendingSpecifiedProducts(list, responseUser);

            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','TEXNOLOG','RAHBAR','BUXGALTER')")
    @GetMapping("/getKindergarten")
    public ResponseEntity<?> getKindergarten(HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<KindergartenRequiredNumberDTO> list = storageService.gerKinderNumber(responseUser);
            list.sort(Comparator.comparing(KindergartenRequiredNumberDTO::getName));

            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PostMapping("/sendProductKindergarten/{kindergartenId}")
    public ResponseEntity<?> sendProductKindergarten(HttpServletRequest request, @PathVariable Integer kindergartenId, @RequestBody List<SendProductDTO> list) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            StateMessage stateMessage = storageService.sendProductKindergarten(responseUser, kindergartenId, list);

            return ResponseEntity.status(200).body(stateMessage);
//            return null;
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','TEXNOLOG','RAHBAR','BUXGALTER')")
    @GetMapping("/getStorageKindergarten/{id}")
    public ResponseEntity<?> getStorageKindergarten(@PathVariable Integer id) {

        try {
            List<ProductBalancerResponseDTO> list = storageService.getConsumableKindergarten(id);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','TEXNOLOG','RAHBAR','BUXGALTER')")
    @GetMapping("/getNotExistingProduct/{id}")
    public ResponseEntity<?> getNotExistingProduct(@PathVariable Integer id) {

        try {
            List<ProductBalancerResponseDTO> list = storageService.getNotExistingProduct(id);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','TEXNOLOG','RAHBAR','BUXGALTER')")
    @GetMapping("/getNotExistingProductAll")
    public ResponseEntity<?> getNotExistingProductAll(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ProductBalancerResponseDTO> list = storageService.getNotExistingProductAll(responseUser);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','TEXNOLOG','RAHBAR','BUXGALTER')")
    @GetMapping("/getStorageReport")
    public ResponseEntity<?> getStorageReport(@Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {

//        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<StorageReportDTO> list = storageService.getStorageReport(start, end, responseUser);
            return ResponseEntity.status(200).body(list);

//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @GetMapping("/getRequiredProduct")
    public ResponseEntity<?> getRequiredProduct(@Param("start") Long start, @Param("end") Long end,HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<ProductsToBeShippedDTO> list = storageService.getRequiredProduct(responseUser, start, end);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
