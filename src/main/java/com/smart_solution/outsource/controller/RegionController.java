package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.address.region.Region;
import com.smart_solution.outsource.address.region.RegionRepository;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/out/api/region")
public class RegionController {

    private final RegionRepository regionRepository;

    public RegionController(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TEXNOLOG','OMBORCHI','TA`MINOTCHI','BOG`CHA MUDIRASI','HAMSHIRA','OSHPAZ','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> get() {

        try {
            return ResponseEntity.status(200).body(regionRepository.findAll());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public StateMessage add(@RequestBody List<Region> regions) {

        for (Region region : regions) {
            regionRepository.save(new Region(region.getId(), region.getName()));
        }
        return null;
    }
}
