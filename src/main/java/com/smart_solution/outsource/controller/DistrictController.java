package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.address.district.DistrictDTO;
import com.smart_solution.outsource.address.district.DistrictService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/out/api/district")
public class DistrictController {

    private final DistrictService districtService;

    public DistrictController(DistrictService districtService) {
        this.districtService = districtService;
    }


//    @PostMapping
//    public ResponseEntity<?> add(DistrictDTO districtDTO) {
//        try {
//            StateMessage stateMessage = districtService.add(districtDTO);
//            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
//    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody List<DistrictDTO> districtDTO) {
        for (DistrictDTO dto : districtDTO) {
            districtService.add(dto);
        }
        return null;
    }

}
