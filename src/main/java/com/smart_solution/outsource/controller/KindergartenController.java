package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.agreement.price.AgreementPriceDTO;
import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.kindergarten.ByDistrict;
import com.smart_solution.outsource.kindergarten.KindergartenDTO;
import com.smart_solution.outsource.kindergarten.KindergartenResponseDTO;
import com.smart_solution.outsource.kindergarten.KindergartenService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/out/api/kindergarten")
public class KindergartenController {

    private final KindergartenService kindergartenService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;

    public KindergartenController(KindergartenService kindergartenService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.kindergartenService = kindergartenService;
        this.authenticationFilter = authenticationFilter;
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody KindergartenDTO kindergartenDTO, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = kindergartenService.add(kindergartenDTO, responseUser);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','BOG`CHA MUDIRASI')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody KindergartenDTO kindergartenDTO, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = kindergartenService.edit(kindergartenDTO, responseUser, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','BOG`CHA MUDIRASI','TEXNOLOG','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> get(HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<KindergartenResponseDTO> list = kindergartenService.get(responseUser);

            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BOG`CHA MUDIRASI','OSHPAZ','HAMSHIRA')")
    @GetMapping("/getOne")
    public ResponseEntity<?> getOne(HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            KindergartenResponseDTO dto = kindergartenService.getOne(responseUser);

            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getByAddress")
    public ResponseEntity<?> getByAddress(HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ByDistrict> list = kindergartenService.getByAddress(responseUser);

            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TEXNOLOG','BUXGALTER','OMBORCHI')")
    @GetMapping("/getByAddressAll")
    public ResponseEntity<?> getByAddressAll(HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ByDistrict> list = kindergartenService.getByAddressAll(responseUser);

            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getByAddressSupplier")
    public ResponseEntity<?> getByAddressSupplier(HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ByDistrict> list = kindergartenService.getByAddressSupplier(responseUser);

            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id, HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = kindergartenService.delete(id, responseUser);

            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @GetMapping("/getType")
    public ResponseEntity<?> addPrice(@Param("type") Integer type) {
        try {
            List<KindergartenResponseDTO> list = kindergartenService.getType(type);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BOG`CHA MUDIRASI','OSHPAZ','HAMSHIRA','BUXGALTER','RAHBAR','TEXNOLOG')")
    @GetMapping("/getOne/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            KindergartenResponseDTO dto = kindergartenService.getOne(id);

            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
