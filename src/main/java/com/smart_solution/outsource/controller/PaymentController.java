package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.payment.PaymentDTO;
import com.smart_solution.outsource.payment.PaymentResponseDTO;
import com.smart_solution.outsource.payment.PaymentService;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("out/api/payment")
public class PaymentController {

    private final PaymentService paymentService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;


    public PaymentController(PaymentService paymentService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.paymentService = paymentService;
        this.authenticationFilter = authenticationFilter;
    }


    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody PaymentDTO dto, HttpServletRequest request) {
        try {
            if (dto.getTimeOfPayment() == null){
                dto.setTimeOfPayment(new Timestamp(System.currentTimeMillis()));
            }
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = paymentService.add(dto,responseUser);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody PaymentDTO dto, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = paymentService.edit(dto,id, responseUser);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage stateMessage = paymentService.delete(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Integer id) {
        try {
            PaymentResponseDTO dto = paymentService.get(id);
            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("supplierId") Integer supplierId, @Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<PaymentResponseDTO> list = paymentService.getAll(supplierId,start,end,responseUser);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @GetMapping("/getAverageNumberOfChildren")
    public ResponseEntity<?> getAverageNumberOfChildren(@Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<PaymentResponseDTO> list = paymentService.getAverageNumberOfChildren(start,end,responseUser);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
