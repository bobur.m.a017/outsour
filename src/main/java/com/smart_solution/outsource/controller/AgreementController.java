package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.agreement.AgreementDTO;
import com.smart_solution.outsource.agreement.AgreementResponseDTO;
import com.smart_solution.outsource.agreement.AgreementService;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergartenDTO;
import com.smart_solution.outsource.agreement.price.AgreementPriceDTO;
import com.smart_solution.outsource.agreement.product.AgreementProductDTO;
import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.kindergarten.KindergartenResponseDTO;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.users.ResponseUser;
import io.swagger.models.auth.In;
import org.apache.coyote.AbstractProcessorLight;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/out/api/agreegent")
public class AgreementController {

    private final AgreementService agreementService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;


    public AgreementController(AgreementService agreementService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.agreementService = agreementService;
        this.authenticationFilter = authenticationFilter;
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody AgreementDTO agreementDTO, HttpServletRequest request) {
        try {
            if (agreementDTO.getDate() == null)
                return ResponseEntity.status(200).body(new StateMessage("Sana kiritish majburiy", false));

            if (agreementDTO.getNumber() == null)
                return ResponseEntity.status(200).body(new StateMessage("Shartnoma raqamini kiritish majburiy", false));

            if (agreementDTO.getDistrictId() == null)
                return ResponseEntity.status(200).body(new StateMessage("Tuman tanlash majburiy", false));

            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = agreementService.add(agreementDTO, responseUser);
            return ResponseEntity.status(200).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PostMapping("/addKin/{id}")
    public ResponseEntity<?> addKin(@PathVariable Integer id, @RequestBody List<Integer> list, HttpServletRequest request) {
        try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);
        StateMessage message = agreementService.addKin(responseUser, id, list);
        return ResponseEntity.status(200).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PostMapping("/addProduct/{id}")
    public ResponseEntity<?> addProduct(@PathVariable Integer id, @RequestBody List<AgreementProductDTO> list, HttpServletRequest request) {
        try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);
        StateMessage message = agreementService.addProduct(responseUser, id, list);
        return ResponseEntity.status(200).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody AgreementDTO agreementDTO, HttpServletRequest request) {
        try {

            if (agreementDTO.getDate() == null)
                return ResponseEntity.status(400).body(new StateMessage("Sana kiritish majburiy", false));

            if (agreementDTO.getNumber() == null)
                return ResponseEntity.status(400).body(new StateMessage("Shartnoma raqamini kiritish majburiy", false));

            if (agreementDTO.getDistrictId() == null)
                return ResponseEntity.status(400).body(new StateMessage("Tuman tanlash majburiy", false));

            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = agreementService.edit(id, agreementDTO, responseUser);
            return ResponseEntity.status(200).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PatchMapping("/editKin/{id}")
    public ResponseEntity<?> editKin(@PathVariable Integer id, @RequestBody AgreementKindergartenDTO dto, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
//            StateMessage message = agreementService.editKin(responseUser, id, dto);
            return ResponseEntity.status(200).body(null);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = agreementService.delete(id);
            return ResponseEntity.status(200).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @DeleteMapping("/deleteKin/{id}")
    public ResponseEntity<?> deleteKin(@PathVariable Integer id,@RequestBody List<Integer> list) {
        try {
            StateMessage message = agreementService.deleteKin(list, id);
            return ResponseEntity.status(200).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER','RAHBAR')")
    @GetMapping
    public ResponseEntity<?> get(HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<AgreementResponseDTO> list = agreementService.getAll(responseUser);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER','RAHBAR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            return agreementService.getOne(id);


        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @GetMapping("/getOneByProduct/{id}")
    public ResponseEntity<?> getOneByProduct(@PathVariable Integer id) {
        try {
            return agreementService.getOneByProduct(id);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PostMapping("/addPrice")
    public ResponseEntity<?> addPrice(@RequestBody List<AgreementPriceDTO> list) {
        try {
            StateMessage message = agreementService.addPrice(list);
            return ResponseEntity.status(200).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PutMapping("/confirm/{id}")
    public ResponseEntity<?> confirm(@PathVariable Integer id, @Param("confirm") boolean confirm, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = agreementService.confirm(id, responseUser, confirm);
            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PutMapping("/send/{id}")
    public ResponseEntity<?> send(@PathVariable Integer id, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = agreementService.send(id);
            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    //TESTDAN O`TDI
    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @GetMapping("/getKindergartenByAgreement/{id}")
    public ResponseEntity<?> getKindergartenByAgreement(@PathVariable Integer id) {
        try {
            List<KindergartenResponseDTO> list = agreementService.getKindergartenByAgreement(id);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}

