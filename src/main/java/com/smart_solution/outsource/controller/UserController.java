package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.dto.BooleanDto;
import com.smart_solution.outsource.dto.StringDto;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.users.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/out/api/user")
public class UserController {

    private final UserService userService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;

    public UserController(UserService userService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.userService = userService;
        this.authenticationFilter = authenticationFilter;
    }



    @PostMapping("/signIn")
    public ResponseEntity<?> signIn(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        try {
//            userService.addIngre();

            ResponseUser responseUser = userService.signIn(request, response);
            return ResponseEntity.status(responseUser.getSuccess() ? 200 : 500).body(responseUser);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody UsersDTO usersDTO, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = userService.add(usersDTO, responseUser);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody UsersDTO usersDTO, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = userService.edit(usersDTO, responseUser, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TA`MINOTCHI','HAMSHIRA','OMBORCHI','BUXGALTER','OSHPAZ','BOG`CHA MUDIRASI','TEXNOLOG')")
    @GetMapping
    public ResponseEntity<?> get(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            UsersResponse dto = userService.get(responseUser);
            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TA`MINOTCHI','HAMSHIRA','OMBORCHI','BUXGALTER','OSHPAZ','BOG`CHA MUDIRASI','TEXNOLOG')")
    @PatchMapping
    public ResponseEntity<?> edit(@RequestBody UserEditDTO dto, HttpServletRequest request) {



        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = userService.edit(responseUser, dto);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 401).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PutMapping("/restriction/{id}")
    public ResponseEntity<?> restriction(@PathVariable Integer id, @RequestBody BooleanDto dto) {

        try {
            StateMessage stateMessage = userService.restriction(id,dto);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> detete(@PathVariable Integer id) {

        try {
            StateMessage stateMessage = userService.delete(id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAll(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<UsersResponse> list = userService.getAll(responseUser);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/resetPassword/{id}")
    public ResponseEntity<?> resetPassword(@PathVariable Integer id) {

        try {
            StateMessage stateMessage = userService.resetPassword(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
