package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.price.PriceDTO;
import com.smart_solution.outsource.price.PriceResponseDTO;
import com.smart_solution.outsource.price.PriceService;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/out/api/price")
public class PriceController {

    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final PriceService priceService;

    public PriceController(ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, PriceService priceService) {
        this.authenticationFilter = authenticationFilter;
        this.priceService = priceService;
    }

    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody PriceDTO dto, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = priceService.add(dto);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }

    @PreAuthorize("hasAnyRole('ADMIN','BUXGALTER','TEXNOLOG','RAHBAR')")
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Integer id, @Param("year") Integer year, @Param("month") Integer month, HttpServletRequest request) {

        try {
            if (year ==null || month == null){
                return ResponseEntity.status(200).body(new StateMessage("Yil va oyni tanlash majburiy", false));
            }
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<PriceResponseDTO> list = priceService.get(id,year,month);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
