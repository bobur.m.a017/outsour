package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveResponseDTO;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveService;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandardDTO;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandardService;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/out/api/menu")
public class MenuController {


    private final MealAgeStandardService mealAgeStandardService;
    private final MenuSaveService menuSaveService;

    public MenuController(MealAgeStandardService mealAgeStandardService, MenuSaveService menuSaveService) {

        this.mealAgeStandardService = mealAgeStandardService;
        this.menuSaveService = menuSaveService;
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
    @PatchMapping("{mealTimeStandardId}")
    public ResponseEntity<?> edit(@PathVariable Integer mealTimeStandardId, @RequestBody MealAgeStandardDTO mealAgeStandardDTO) {

        try {
            StateMessage stateMessage = mealAgeStandardService.add(mealAgeStandardDTO, mealTimeStandardId);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN')")
    @DeleteMapping("{mealAgeStandardId}")
    public ResponseEntity<?> delete(@PathVariable Integer mealAgeStandardId) {

        try {
            StateMessage stateMessage = mealAgeStandardService.delete(mealAgeStandardId);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN', 'HAMSHIRA', 'OSHPAZ', 'BOG`CHA MUDIRASI')")
    @GetMapping("/getSaveMenu/{id}")
    public ResponseEntity<?> getSaveMenu(@PathVariable Integer id) {

        try {
            MenuSaveResponseDTO dto = menuSaveService.get(id);
            return ResponseEntity.status(dto == null ? 400 : 200).body(dto == null ? new StateMessage("Ma`lumot mavjud emas", false) : dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN', 'BOG`CHA MUDIRASI')")
    @PutMapping("/confirmationMenu/{id}")
    public ResponseEntity<?> confirmationMenu(@PathVariable Integer id) {

        try {
            StateMessage confirmation = menuSaveService.confirmation(id);
            return ResponseEntity.status(confirmation.isSuccess() ? 200 : 500).body(confirmation);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
