package com.smart_solution.outsource.controller;


import com.google.gson.Gson;
import com.smart_solution.outsource.attachment.AttachmentRepository;
import com.smart_solution.outsource.attachment.AttachmentService;
import com.smart_solution.outsource.meal.MealDTO;
import com.smart_solution.outsource.meal.MealResponseDTO;
import com.smart_solution.outsource.meal.MealService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.product.ProductRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/out/api/meal")
public class MealController {

    private final MealService mealService;
    private final ProductRepository productRepository;
    private final AttachmentRepository attachmentRepository;
    private final AttachmentService attachmentService;

    public MealController(MealService mealService, ProductRepository productRepository, AttachmentRepository attachmentRepository, AttachmentService attachmentService) {
        this.mealService = mealService;
        this.productRepository = productRepository;
        this.attachmentRepository = attachmentRepository;
        this.attachmentService = attachmentService;
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestParam String jsonString,
                                 MultipartHttpServletRequest request) throws IOException {
        try {

        Gson gson = new Gson();
        MealDTO mealDTO = gson.fromJson(jsonString, MealDTO.class);

        MultipartFile files = request.getFile("files");
        StateMessage stateMessage = mealService.add(mealDTO, files);
        return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/addTest")
    public ResponseEntity<?> add(@RequestBody List<MealDTO> list) throws IOException {
        for (MealDTO mealDTO : list) {

        try {
            StateMessage stateMessage = mealService.add(mealDTO, null);
//            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
        }
        return ResponseEntity.status(200).body("XAMMASI A`LO");
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(
            @PathVariable Integer id,
            @RequestParam String jsonString,
            MultipartHttpServletRequest request) {
        try {
            Gson gson = new Gson();
            MealDTO mealDTO = gson.fromJson(jsonString, MealDTO.class);

            MultipartFile files = request.getFile("files");

            StateMessage stateMessage = mealService.edit(mealDTO, id, files);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG')")
    @GetMapping
    public ResponseEntity<?> get() {
        try {
        List<MealResponseDTO> list = mealService.get();

        return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','HAMSHIRA','BOG`CHA MUDIRASI','OSHPAZ','BUXGALTER','RAHBAR')")
    @GetMapping("/getAttachment/{id}")
    public ResponseEntity<?> getAttachment(@PathVariable Integer id) {
        try {

//        if (id == null){
//            id = 1;
//        }


        return ResponseEntity.status(200).body(attachmentService.getAttachment(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage stateMessage = mealService.delete(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @GetMapping("/getMeal")
    public ResponseEntity<?> getMeal(@Param("menuId") Integer menuId, @Param("mealAgeStandardId") Integer mealAgeStandardId) {
        try {
        MealResponseDTO dto = mealService.getMeal(menuId, mealAgeStandardId);

        return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAll() {
        try {
        List<MealDTO> list = mealService.getAll();

        return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
