package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.dto.ReceiveDto;
import com.smart_solution.outsource.inOutProduct.InOutProductService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.productBalancer.ProductBalancerResponseDTO;
import com.smart_solution.outsource.storage.StorageService;
import com.smart_solution.outsource.storage.garbage.GarbageProductDTO;
import com.smart_solution.outsource.storage.garbage.GarbageProductResponseDTO;
import com.smart_solution.outsource.storage.garbage.GarbageProductService;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompanyDTO;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/out/api/cook")
public class CookController {

    private final StorageService storageService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final GarbageProductService garbageProductService;

    public CookController(StorageService storageService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, GarbageProductService garbageProductService) {
        this.storageService = storageService;
        this.authenticationFilter = authenticationFilter;
        this.garbageProductService = garbageProductService;
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @GetMapping("/getExistingProduct")
    public ResponseEntity<?> getExistingProduct(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ProductBalancerResponseDTO> list = storageService.getExistingProductKin(responseUser);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @GetMapping("/getProductBalancer")
    public ResponseEntity<?> getProductBalancer(HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<ProductBalancerResponseDTO> list = storageService.getConsumableKin(responseUser);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @GetMapping("/getInOut")
    public ResponseEntity<?> getInOut(@Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            if (end != null)
                end = end + 86000000;
            List<ShippedProductsCompanyDTO> list = storageService.getInOutProductKin(responseUser, start, end);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @PutMapping("/receive/{id}")
    public ResponseEntity<?> receive(@PathVariable UUID id, @RequestBody ReceiveDto dto, HttpServletRequest request) {

        try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);
        StateMessage message;
        if ((dto.getNumberPack() == null) || (dto.getWeight() == null)) {
            message = new StateMessage("Null berib yuborish mumkin emas", false);
        } else {
//            message = new StateMessage("Null berib yuborish mumkin emas", false);
            message = storageService.receiveKin(id, dto, responseUser);
        }
//            StateMessage message = new StateMessage("", false);
        return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @PostMapping("/garbageAdd")
    public ResponseEntity<?> addGarbage(@RequestBody GarbageProductDTO dto, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage message = garbageProductService.add(responseUser, dto);
            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @DeleteMapping("/garbageDelete/{id}")
    public ResponseEntity<?> deleteGarbage(@PathVariable Integer id) {

        try {

            StateMessage message = garbageProductService.delete(id);
            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OSHPAZ')")
    @GetMapping("/garbageGet")
    public ResponseEntity<?> getGarbage(@Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            List<GarbageProductResponseDTO> list = garbageProductService.get(responseUser, start, end);
            return ResponseEntity.status(200).body(list);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
