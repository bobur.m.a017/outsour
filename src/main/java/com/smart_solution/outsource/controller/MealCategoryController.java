package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.meal.mealCategory.MealCategoryDTO;
import com.smart_solution.outsource.meal.mealCategory.MealCategoryService;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/out/api/mealCategory")
public class MealCategoryController {

    private final MealCategoryService mealCategoryService;

    public MealCategoryController(MealCategoryService mealCategoryService) {
        this.mealCategoryService = mealCategoryService;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody MealCategoryDTO mealCategoryDTO) {


        try {

            StateMessage stateMessage = mealCategoryService.add(mealCategoryDTO);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody MealCategoryDTO mealCategoryDTO) {


        try {

            StateMessage stateMessage = mealCategoryService.edit(mealCategoryDTO, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping
    public ResponseEntity<?> get() {

        try {

            List<MealCategoryDTO> list = mealCategoryService.get();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {

        try {
            StateMessage stateMessage = mealCategoryService.delete(id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500 ).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }
}
