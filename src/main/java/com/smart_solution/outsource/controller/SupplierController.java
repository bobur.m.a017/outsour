package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.dto.BooleanDto;
import com.smart_solution.outsource.inOutSupplier.InOutSupplierDTO;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.supplier.SupplierDTO;
import com.smart_solution.outsource.supplier.SupplierResponseDTO;
import com.smart_solution.outsource.supplier.SupplierService;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedDTO;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsDTO;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/out/api/supplier")
public class SupplierController {

    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final SupplierService supplierService;

    public SupplierController(ApplicationUsernamePasswordAuthenticationFilter authenticationFilter,
            SupplierService supplierService) {
        this.authenticationFilter = authenticationFilter;
        this.supplierService = supplierService;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody SupplierDTO supplierDTO) {

        try {
            StateMessage stateMessage = supplierService.add(supplierDTO);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TA`MINOTCHI')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody SupplierDTO supplierDTO,
            HttpServletRequest request) {

        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = supplierService.edit(supplierDTO, responseUser, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/changeStatus/{id}")
    public ResponseEntity<?> change(@PathVariable Integer id, @RequestBody BooleanDto ans) {

        try {

            StateMessage stateMessage = supplierService.changeStatus(id, ans.isResult());
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> get() {
        try {
            List<SupplierResponseDTO> list = supplierService.get();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TA`MINOTCHI')")
    @GetMapping("/getRequiredProduct")
    public ResponseEntity<?> getRequiredProduct(@Param("start") Long start, @Param("end") Long end,HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            List<ProductsToBeShippedDTO> list = supplierService.getRequiredProduct(responseUser, start, end);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TA`MINOTCHI')")
    @GetMapping("/getShippedProduct")
    public ResponseEntity<?> getShippedProduct(@Param("start") Long start, @Param("end") Long end,
            HttpServletRequest request) {
         try {

        ResponseUser responseUser = authenticationFilter.parseToken(request);

        List<ShippedProductsDTO> list = supplierService.getShippedProduct(responseUser, start, end);
        return ResponseEntity.status(200).body(list);
         } catch (Exception e) {
         return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
         }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TA`MINOTCHI')")
    @PostMapping("/addShippedProduct/{requiredId}")
    public ResponseEntity<?> addShippedProduct(@PathVariable UUID requiredId, @RequestBody InOutSupplierDTO dto,
            HttpServletRequest request) {
         try {
        ResponseUser responseUser = authenticationFilter.parseToken(request);
        StateMessage message;
        if (dto.getWeight() <= 0) {
            message = new StateMessage("Kiritilgan son 0 dan katta bo`lishi kerak", false);
        } else {
            if ((dto.getNumberPack() == null) || (dto.getWeight() == null) || (dto.getPrice() == null)) {
                message = new StateMessage("Null berib yuborish mumkin emas", false);
            } else {

                message = supplierService.addShippedProduct(responseUser, dto, requiredId);
            }
        }
        return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);
         } catch (Exception e) {
         return
         ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
         }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TA`MINOTCHI')")
    @PostMapping("/editShippedProduct/{id}")
    public ResponseEntity<?> editShippedProduct(@PathVariable UUID id, @Param("price") Double price,@Param("paymentStatus") Boolean paymentStatus, @Param("typeOfPayment") Boolean typeOfPayment, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);

            if (id == null || (price == null && paymentStatus == null && typeOfPayment == null)){
                return ResponseEntity.status(500).body(new StateMessage("null qiymat qabul qilmaydi",false));
            }

            StateMessage message = supplierService.editShippedProduct(price,paymentStatus,typeOfPayment, id);
            return ResponseEntity.status(message.isSuccess() ? 200 : 500).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TA`MINOTCHI')")
    @GetMapping("/getOne")
    public ResponseEntity<?> getOne(HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            SupplierResponseDTO dto = supplierService.getOne(responseUser);
            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
