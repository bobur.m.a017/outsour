package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.product.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/out/api/product")
public class ProductController {


    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody ProductDTO productDTO) {

//        for (ProductDTO productDTO : list) {
            try {
                StateMessage stateMessage = productService.add(productDTO);
                return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
            }
//        }
//        return ResponseEntity.status(200).body("XAMMASI AJOYIB");
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody ProductDTO productDTO, HttpServletRequest request) {
        try {

            productDTO.setCarbohydrates(productDTO.getCarbohydrates() / 1000);
            productDTO.setKcal(productDTO.getKcal() / 1000);
            productDTO.setOil(productDTO.getOil() / 1000);
            productDTO.setProtein(productDTO.getProtein() / 1000);
            productDTO.setPack(productDTO.getPack() / 1000);
            productDTO.setRounding(productDTO.getRounding() / 1000);

            StateMessage stateMessage = productService.edit(productDTO, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'RAXBAR','TEXNOLOG','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> get() {
        try {
            List<ProductResponseDTO> list = productService.get();

            list.sort(Comparator.comparing(ProductResponseDTO::getName));
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage stateMessage = productService.delete(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAll() {
        try {
            List<TestDTO> list = productService.getTest();
            list.sort(Comparator.comparing(TestDTO::getName));
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN', 'RAXBAR','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getSpecial")
    public ResponseEntity<?> getSpecial() {
        try {
            List<ProductResponseDTO> list = productService.getSpecial();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


//    @PreAuthorize("hasAnyRole('ADMIN')")
//    @GetMapping("/getAllProduct")
//    public ResponseEntity<?> getAllProduct() {
//        try {
//            List<ProductDTOOO> list = productService.getProduct();
//            return ResponseEntity.status(200).body(list);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
//    }


}
