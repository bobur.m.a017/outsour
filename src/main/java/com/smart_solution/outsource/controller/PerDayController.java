package com.smart_solution.outsource.controller;


import com.google.gson.Gson;
import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildrenDTO;
import com.smart_solution.outsource.perDay.PerDayDTO;
import com.smart_solution.outsource.perDay.PerDayService;
import com.smart_solution.outsource.perDay.report.ReportPerDay;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/out/api/perDay")
public class PerDayController {

    private final PerDayService perDayService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;


    public PerDayController(PerDayService perDayService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.perDayService = perDayService;
        this.authenticationFilter = authenticationFilter;
    }


    @PreAuthorize("hasAnyRole('HAMSHIRA','ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestParam String jsonString, @Param("date") Long date, HttpServletRequest request, MultipartHttpServletRequest requestFile) throws IOException {
        try {

        Gson gson = new Gson();
        PerDayDTO perDayDTO = gson.fromJson(jsonString, PerDayDTO.class);

        MultipartFile files = requestFile.getFile("files");

        ResponseUser responseUser = authenticationFilter.parseToken(request);

        StateMessage stateMessage;

        if (perDayDTO.getKindergartenId() == null) {
            stateMessage = new StateMessage("Barcha maydonlar to`ldirilishi shart", false);
        }else {

            boolean res = false;

            for (NumberOfChildrenDTO numberOfChildrenDTO : perDayDTO.getNumberOfChildrenDTOList()) {
                if (numberOfChildrenDTO.getNumber() == null) {
                    res = true;
                    break;
                }
                if (numberOfChildrenDTO.getAgeGroupId() == null) {
                    res = true;
                    break;
                }
            }
            if (res) {
                stateMessage = new StateMessage("Barcha maydonlar to`ldirilishi shart", false);
            } else {
                stateMessage = perDayService.add(perDayDTO, responseUser, date, files);
            }
        }

        return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('HAMSHIRA','ADMIN','TEXNOLOG')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestParam String jsonString, HttpServletRequest request, MultipartHttpServletRequest requestFile) throws IOException {
        try {

            Gson gson = new Gson();
            PerDayDTO perDayDTO = gson.fromJson(jsonString, PerDayDTO.class);

            MultipartFile files = requestFile.getFile("files");

            StateMessage stateMessage;
            ResponseUser responseUser = authenticationFilter.parseToken(request);




            String str = "";

            if (files == null){
                str = "Rasm kelmadi ";
            }else if (files.isEmpty()){
                str = "Rasm kelmadi ";
            }

            if (id == null) {
                stateMessage = new StateMessage("Barcha maydonlar to`ldirilishi shart. Bog`cha id kelmayapti", false);
            }else {

                boolean res = false;


                for (NumberOfChildrenDTO numberOfChildrenDTO : perDayDTO.getNumberOfChildrenDTOList()) {
                    if (numberOfChildrenDTO.getNumber() == null) {
                        res = true;
                        str = str+ " bola soni kelmayapti ";
                        break;
                    }
                    if (numberOfChildrenDTO.getAgeGroupId() == null) {
                        res = true;
                        str = str+ " ageGroupId kelmayapti ";
                        break;
                    }
                }
                if (res) {
                    stateMessage = new StateMessage("Barcha maydonlar to`ldirilishi shart "+str, false);
                } else {
                    stateMessage = perDayService.edit(id, perDayDTO, responseUser, files);
                }
            }
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('BOG`CHA MUDIRASI','TEXNOLOG','ADMIN')")
    @PutMapping("/change/{id}")
    public ResponseEntity<?> change(@PathVariable Integer id, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = perDayService.changeStatus(responseUser, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('BOG`CHA MUDIRASI','TEXNOLOG','ADMIN','HAMSHIRA','OSHPAZ','RAHBAR')")
    @GetMapping
    public ResponseEntity<?> get(@Param("date") Long date, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);

            return perDayService.getAll(responseUser, new Timestamp(date == null ? System.currentTimeMillis() : date));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @GetMapping("/get")
    public ResponseEntity<?> gett(@Param("date") Long date, HttpServletRequest request) {
        try {
//        ResponseUser responseUser = authenticationFilter.parseToken(request);

            return perDayService.getAllWeb(new Timestamp(date == null ? System.currentTimeMillis() : date));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEXNOLOG','ADMIN','BUXGALTER','RAHBAR')")
    @GetMapping("/averageNumberOfChildren")
    public ResponseEntity<?> averageNumberOfChildren(@Param("start") Long start, @Param("end") Long end, HttpServletRequest request) {
//        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);

            if (start == null || end == null) {
                return ResponseEntity.status(200).body("");
            }

            List<ReportPerDay> list = perDayService.averageNumberOfChildren(start, end, responseUser);
            return ResponseEntity.status(200).body(list);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }
}
