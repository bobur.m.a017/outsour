package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.company.CompanyDTO;
import com.smart_solution.outsource.company.CompanyService;
import com.smart_solution.outsource.config.ApplicationUsernamePasswordAuthenticationFilter;
import com.smart_solution.outsource.dto.BooleanDto;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.users.ResponseUser;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/out/api/company")
public class CompanyController {

    private final CompanyService companyService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;

    public CompanyController(CompanyService companyService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.companyService = companyService;
        this.authenticationFilter = authenticationFilter;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody CompanyDTO companyDTO) {
        try {

            StateMessage stateMessage = companyService.add(companyDTO);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody CompanyDTO companyDTO, HttpServletRequest request) {
        try {
            ResponseUser responseUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = companyService.edit(companyDTO, id, responseUser);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','TA`MINOTCHI','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> get(HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            return ResponseEntity.status(200).body(companyService.get(responseUser));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','RAHBAR','BUXGALTER','TEXNOLOG','OMBORCHI')")
    @GetMapping("/get")
    public ResponseEntity<?> getOne(HttpServletRequest request) {
        try {

            ResponseUser responseUser = authenticationFilter.parseToken(request);

            return ResponseEntity.status(200).body(companyService.getOne(responseUser));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody CompanyDTO companyDTO) {
        try {

            StateMessage stateMessage = companyService.register(companyDTO);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAll(@Param("start") Long start, @Param("end") Long end) {
        try {
            return ResponseEntity.status(200).body(companyService.getAll(start, end));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/changeStatus/{id}")
    public ResponseEntity<?> change(@PathVariable Integer id, @RequestBody BooleanDto ans) {

        try {

            StateMessage stateMessage = companyService.changeStatus(id,ans.isResult());
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
