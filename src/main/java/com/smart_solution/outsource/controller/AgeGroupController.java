package com.smart_solution.outsource.controller;

import com.smart_solution.outsource.menu.ageGroup.AgeGroupDTO;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupService;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("out/api/ageGroup")
public class AgeGroupController {
    private final AgeGroupService ageGroupService;

    public AgeGroupController(AgeGroupService ageGroupService) {
        this.ageGroupService = ageGroupService;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody AgeGroupDTO ageGroupDTO) {
        try {
            return ResponseEntity.status(500).body("Yosh toifasi qo`shib bo`lmaydi. Ishlab chiquvchiga murojaat qiling");
//            StateMessage stateMessage = ageGroupService.add(ageGroupDTO);
//            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            return ResponseEntity.status(500).body("Yosh toifasi o`chirib bo`lmaydi. Ishlab chiquvchiga murojaat qiling");

//            StateMessage stateMessage = ageGroupService.delete(id);
//            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody AgeGroupDTO ageGroupDTO) {
        try {

            return ResponseEntity.status(500).body("Yosh toifasi o`zgartirib bo`lmaydi. Ishlab chiquvchiga murojaat qiling");


//            StateMessage stateMessage = ageGroupService.edit(ageGroupDTO, id);
//            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','HAMSHIRA','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> get() {
        try {
            List<AgeGroupDTO> list = ageGroupService.getAll();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
