package com.smart_solution.outsource.controller;


import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.product_category.TestPCDTO;
import com.smart_solution.outsource.sanpin_catecory.SanpinDTO;
import com.smart_solution.outsource.sanpin_catecory.SanpinService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/out/api/sanpinCategory")
public class SanpinController {


    private final SanpinService sanpinService;

    public SanpinController(SanpinService sanpinService) {
        this.sanpinService = sanpinService;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody List<SanpinDTO> list) {
        for (SanpinDTO sanpinDTO : list) {
            try {
                StateMessage stateMessage = sanpinService.add(sanpinDTO);
//                return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
            }
        }
        return ResponseEntity.status(200).body("XAMMASI AJOYIB");
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody SanpinDTO sanpinDTO) {

        try {
            StateMessage stateMessage = sanpinService.edit(sanpinDTO, id);
            return ResponseEntity.status(stateMessage.isSuccess() ? 200 : 500).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping()
    public ResponseEntity<?> get() {
        try {
            List<SanpinDTO> list = sanpinService.get();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
           StateMessage stateMessage = sanpinService.delete(id);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAll() {

        try {
            List<TestPCDTO> list = sanpinService.getTest();
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}