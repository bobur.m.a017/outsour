package com.smart_solution.outsource.numberOfChildren;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

public class NumberOfChildrenDTO {

    private Integer id;
    private Integer number;
    private Integer ageGroupId;


    public NumberOfChildrenDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }
}
