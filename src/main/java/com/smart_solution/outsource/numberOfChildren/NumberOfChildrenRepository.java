package com.smart_solution.outsource.numberOfChildren;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NumberOfChildrenRepository extends JpaRepository<NumberOfChildren, Integer> {
}
