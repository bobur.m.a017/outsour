package com.smart_solution.outsource.numberOfChildren;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.perDay.PerDay;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public record NumberOfChildrenService(
        NumberOfChildrenRepository numberOfChildrenRepository,
        AgeGroupRepository ageGroupRepository) {


    public NumberOfChildren add(NumberOfChildrenDTO numberOfChildrenDTO, PerDay perDay) {

        NumberOfChildren numberOfChildren = new NumberOfChildren();
        numberOfChildren.setNumberOfKids(numberOfChildrenDTO.getNumber());
        numberOfChildren.setAgeGroup(ageGroupRepository.findById(numberOfChildrenDTO.getAgeGroupId()).get());
        numberOfChildren.setPerDay(perDay);
        return numberOfChildren;
    }

    public NumberOfChildren edit(NumberOfChildrenDTO numberOfChildrenDTO, NumberOfChildren numberOfChildren ) {

        numberOfChildren.setNumberOfKids(numberOfChildrenDTO.getNumber());
        numberOfChildren.setAgeGroup(ageGroupRepository.findById(numberOfChildrenDTO.getAgeGroupId()).get());

       return numberOfChildren;
    }

    public NumberOfChildrenResponseDTO parse(NumberOfChildren numberOfChildren) {

        NumberOfChildrenResponseDTO dto = new NumberOfChildrenResponseDTO();

        dto.setId(numberOfChildren.getId());
        dto.setAgeGroupName(numberOfChildren.getAgeGroup().getName());
        dto.setAgeGroupId(numberOfChildren.getAgeGroup().getId());
        dto.setNumber(numberOfChildren.getNumberOfKids());
        dto.setCreateDate(numberOfChildren.getCreateDate());
        dto.setUpdateDate(numberOfChildren.getUpdateDate());
        return dto;
    }

    public NumberOfChildrenResponseDTO parseNull(AgeGroup ageGroup) {

        NumberOfChildrenResponseDTO dto = new NumberOfChildrenResponseDTO();

        dto.setAgeGroupName(ageGroup.getName());
        dto.setAgeGroupId(ageGroup.getId());
        dto.setCreateDate(new Timestamp(System.currentTimeMillis()));
        dto.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        return dto;
    }
}
