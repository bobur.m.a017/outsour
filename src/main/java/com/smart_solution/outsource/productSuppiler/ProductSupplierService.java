package com.smart_solution.outsource.productSuppiler;


import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.supplier.SupplierRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public record ProductSupplierService(
        ProductSupplierRepository productSupplierRepository,
        UsersRepository usersRepository,
        SupplierRepository supplierRepository,
        ProductRepository productRepository,
        CompanyRepository companyRepository
) {


    public StateMessage add(ProductSupplierDTO productSupplierDTO, ResponseUser responseUser) {

        ProductSupplier productSupplier = productSupplierRepository.findById(productSupplierDTO.getId()).get();

        productSupplier.setSupplier(supplierRepository.findById(productSupplierDTO.getSupplierId()).get());

        productSupplierRepository.save(productSupplier);

        return new StateMessage("Muvaffaqiyatli qo`shildi", true);
    }

    public StateMessage delete(Integer id) {

        Optional<ProductSupplier> byId = productSupplierRepository.findById(id);

        if (byId.isPresent()) {

            return new StateMessage("Muvaffaqiyatli o`chirildi", true);
        } else {
            return new StateMessage("X A T O L I K ! ! !", false);
        }
    }

    public List<ProductSupplierResponseDTO> get(ResponseUser responseUser) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        List<ProductSupplierResponseDTO> list = new ArrayList<>();


        addCompany(company);
        for (ProductSupplier productSupplier : company.getProductSupplier()) {
            list.add(parse(productSupplier));
        }

        list.sort(Comparator.comparing(ProductSupplierResponseDTO::getProductsName));

        return list;
    }

    public ProductSupplierResponseDTO parse(ProductSupplier productSupplier) {
        ProductSupplierResponseDTO dto = new ProductSupplierResponseDTO();

        dto.setId(productSupplier.getId());

        if (productSupplier.getSupplier() != null) {
            dto.setSupplierId(productSupplier.getSupplier().getId());
            dto.setId(productSupplier.getId());
            dto.setSupplierName(productSupplier.getSupplier().getName());
        }


        dto.setProductsId(productSupplier.getProduct().getId());
        dto.setProductsName(productSupplier.getProduct().getName());

        return dto;
    }

    public void addCompany(Company company) {

        for (Product product : productRepository.findAll()) {
            boolean res = false;


            for (ProductSupplier productSupplier : company.getProductSupplier()) {
                if (productSupplier.getProduct().equals(product)) {
                    res = true;
                    break;
                }
            }
            if (!res) {
                ProductSupplier productSupplier = new ProductSupplier();
                productSupplier.setProduct(product);
                productSupplier.setCompany(company);
                List<ProductSupplier> list = company.getProductSupplier();
                list.add(productSupplierRepository.save(productSupplier));
                company.setProductSupplier(list);
                companyRepository.save(company);
            }
        }
    }

}
