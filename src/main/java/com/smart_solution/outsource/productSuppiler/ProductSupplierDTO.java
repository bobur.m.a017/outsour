package com.smart_solution.outsource.productSuppiler;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class ProductSupplierDTO {

    private Integer id;
    private Integer supplierId;
    private Integer productsId;

    public ProductSupplierDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public Integer getProductsId() {
        return productsId;
    }

    public void setProductsId(Integer productsId) {
        this.productsId = productsId;
    }
}
