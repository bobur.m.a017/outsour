package com.smart_solution.outsource.productSuppiler;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ProductSupplierRepository extends JpaRepository<ProductSupplier, Integer> {

    Optional<ProductSupplier> findBySupplierAndProduct_Id(Supplier supplier, Integer productId);

    Optional<ProductSupplier> findByCompanyAndProduct(Company company, Product product);

}
