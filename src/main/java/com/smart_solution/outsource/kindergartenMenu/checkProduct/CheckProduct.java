package com.smart_solution.outsource.kindergartenMenu.checkProduct;


import javax.persistence.*;
import java.math.BigDecimal;


@Entity
public class CheckProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer productId;
    private Integer menuSaveId;
    private Integer kindergartenId;
    private String condition;
    private BigDecimal weight;


    public CheckProduct() {
    }

    public CheckProduct(Integer productId, Integer menuSaveId, Integer kindergartenId, String condition) {
        this.productId = productId;
        this.menuSaveId = menuSaveId;
        this.kindergartenId = kindergartenId;
        this.condition = condition;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public String getCondition() {
        return condition;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getMenuSaveId() {
        return menuSaveId;
    }

    public void setMenuSaveId(Integer menuSaveId) {
        this.menuSaveId = menuSaveId;
    }

    public String isCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
