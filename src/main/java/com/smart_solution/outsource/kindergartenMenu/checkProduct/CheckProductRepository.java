package com.smart_solution.outsource.kindergartenMenu.checkProduct;

import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CheckProductRepository extends JpaRepository<CheckProduct, Integer> {

    CheckProduct findByMenuSaveIdAndProductId(Integer menuId, Integer productId);
    CheckProduct findByKindergartenIdAndMenuSaveIdAndProductIdAndCondition(Integer kindergartenId, Integer menuId, Integer productId, String state);

    List<CheckProduct> findAllByMenuSaveId(Integer id);
    List<CheckProduct> findAllByMenuSaveIdAndCondition(Integer id, String state);
}
