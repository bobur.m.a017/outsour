package com.smart_solution.outsource.kindergartenMenu.menu;

import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardResponseSaveDTO;
import com.smart_solution.outsource.perDay.PerDayResponseDTO;
import com.smart_solution.outsource.order.numberOfKids.NumberOfKidsResponseDTO;

import java.sql.Timestamp;
import java.util.List;

public class MenuSaveResponseDTO {

    private Integer id;
    private String name;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String multiMenuName;
    private Timestamp date;
    private String status;
    private boolean confirmation;
    private PerDayResponseDTO perDayDTO;
    private List<MealTimeStandardResponseSaveDTO> mealTimeStandardResponseSaveDTOList;

    private List<NumberOfKidsResponseDTO> numberToGuess; //Bu yerda ushbu kunda  shu menyu asosida ovqatlanishi taxmin qilingan bolalar soni saqlanadi.


    public MenuSaveResponseDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getMultiMenuName() {
        return multiMenuName;
    }

    public void setMultiMenuName(String multiMenuName) {
        this.multiMenuName = multiMenuName;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public PerDayResponseDTO getPerDayDTO() {
        return perDayDTO;
    }

    public void setPerDayDTO(PerDayResponseDTO perDayDTO) {
        this.perDayDTO = perDayDTO;
    }

    public List<MealTimeStandardResponseSaveDTO> getMealTimeStandardResponseSaveDTOList() {
        return mealTimeStandardResponseSaveDTOList;
    }

    public void setMealTimeStandardResponseSaveDTOList(List<MealTimeStandardResponseSaveDTO> mealTimeStandardResponseSaveDTOList) {
        this.mealTimeStandardResponseSaveDTOList = mealTimeStandardResponseSaveDTOList;
    }

    public List<NumberOfKidsResponseDTO> getNumberToGuess() {
        return numberToGuess;
    }

    public void setNumberToGuess(List<NumberOfKidsResponseDTO> numberToGuess) {
        this.numberToGuess = numberToGuess;
    }
}
