package com.smart_solution.outsource.kindergartenMenu.menu;

import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.perDay.PerDay;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class MenuSave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name; //Menu nomi

    private String multiMenuName; // Ushbu menu qaysi taomnomaga tegishliligi.

    private Timestamp dayDate; // Ushbu menu qaysi kun uchun tuzilganligi.

    private String status; // Menyuning xolati

    private Boolean confirmation = false; // Menyuning MTT mudirasi tomonidan tastiqlanish xolati

    private boolean state;

    private Boolean checkProduct;

    private Integer year;
    private Integer month;
    private Integer day;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToMany(mappedBy = "menuSaveList", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MealTimeStandardSave> mealTimeStandardSaves;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(cascade = CascadeType.ALL)
    private PerDay numberFact; //Bu yerda ushbu kunda xaqiqatda shu menyu asosida ovqatlangan bolalar soni saqlanadi.

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(cascade = CascadeType.ALL)
    private PerDay numberToGuess; //Bu yerda ushbu kunda  shu menyu asosida ovqatlanishi taxmin qilingan bolalar soni saqlanadi.

    @OneToOne
    private MenuWeight menuWeight; //Bu yerda ushbu kunda  shu menyu asosida ovqatlanishi taxmin qilingan bolalar soni saqlanadi.


    @ManyToOne(cascade = CascadeType.ALL)
    private Kindergarten kindergarten; //Ushbu menu qaysi MTT ga biriktirilayotgani.


    public Boolean getCheckProduct() {
        return checkProduct;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Boolean isCheck() {
        return checkProduct;
    }

    public void setCheckProduct(Boolean checkProduct) {
        this.checkProduct = checkProduct;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public MenuWeight getMenuWeight() {
        return menuWeight;
    }

    public void setMenuWeight(MenuWeight menuWeight) {
        this.menuWeight = menuWeight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMultiMenuName() {
        return multiMenuName;
    }

    public void setMultiMenuName(String multiMenuName) {
        this.multiMenuName = multiMenuName;
    }

    public Timestamp getDayDate() {
        return dayDate;
    }

    public void setDayDate(Timestamp dayDate) {
        this.dayDate = dayDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Boolean confirmation) {
        this.confirmation = confirmation;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<MealTimeStandardSave> getMealTimeStandardSaves() {
        return mealTimeStandardSaves;
    }

    public void setMealTimeStandardSaves(List<MealTimeStandardSave> mealTimeStandardSaves) {
        this.mealTimeStandardSaves = mealTimeStandardSaves;
    }

    public PerDay getNumberFact() {
        return numberFact;
    }

    public void setNumberFact(PerDay numberFact) {
        this.numberFact = numberFact;
    }

    public PerDay getNumberToGuess() {
        return numberToGuess;
    }

    public void setNumberToGuess(PerDay numberToGuess) {
        this.numberToGuess = numberToGuess;
    }

    public Kindergarten getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(Kindergarten kindergarten) {
        this.kindergarten = kindergarten;
    }
}
