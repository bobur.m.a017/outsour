package com.smart_solution.outsource.kindergartenMenu.menu;

import com.smart_solution.outsource.kindergarten.Kindergarten;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface MenuSaveRepository extends JpaRepository<MenuSave, Integer> {

    List<MenuSave> findAllByKindergarten_Id(Integer id);

    List<MenuSave> findAllByKindergarten_IdAndState(Integer id, boolean res);

    List<MenuSave> findAllByKindergarten_IdAndYearAndMonth(Integer id, Integer year, Integer month);
    Optional<MenuSave> findAllByKindergarten_IdAndYearAndMonthAndDay(Integer id, Integer year, Integer month, Integer day);

    List<MenuSave> findAllByKindergartenAndStatus(Kindergarten kindergarten, String str);
    List<MenuSave> findAllByCheckProduct(Boolean res);


//        Optional<MenuSave> findByDayDate_YearAndDayDate_MonthAndDayDate_DayAndKindergarten_Id(int dayDate_year, int dayDate_month, int dayDate_day, Integer kindergarten_id);
//
//    @Query( nativeQuery = true , value = "SELECT u FROM MenuSave u WHERE u.date_year = ?1 and u.date_month = ?2 and u.date_day = ?3 and u.kindergarten_id = ?4")
//    Optional<MenuSave> findBy (int date_year, int date_month, int date_day, int kindergarten_id);

}
