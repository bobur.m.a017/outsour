package com.smart_solution.outsource.kindergartenMenu.menu;


import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardResponseSaveDTO;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSaveRepository;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSaveService;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.mealTime.MealTimeRepository;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandard;
import com.smart_solution.outsource.menu.menu.Menu;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.multiMenu.MultiMenu;
import com.smart_solution.outsource.multiMenu.MultiMenuRepository;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import com.smart_solution.outsource.perDay.*;
import com.smart_solution.outsource.order.numberOfKids.NumberOfKids;
import com.smart_solution.outsource.order.numberOfKids.NumberOfKidsResponseDTO;
import com.smart_solution.outsource.order.numberOfKids.NumberOfKidsService;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.productMeal.ProductMeal;
import com.smart_solution.outsource.status.Status;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;

@Service
public record MenuSaveService(
        MenuSaveRepository menuSaveRepository,
        MealTimeStandardSaveService mealTimeStandardSaveService,
        MultiMenuRepository multiMenuRepository,
        MealTimeRepository mealTimeRepository,
        KindergartenRepository kindergartenRepository,
        PerDayService perDayService,
        NumberOfKidsService numberOfKidsService,
        MealTimeStandardSaveRepository mealTimeStandardSaveRepository,
        MealRepository mealRepository) {


    public MenuSave add(Menu menu, Integer multiMenuId) {

        MenuSave menuSave = new MenuSave();
        menuSave.setName(menu.getName());
        menuSave.setState(false);
        menuSave.setStatus(Status.CREATED.getName());
        MultiMenu multiMenu = multiMenuRepository.findById(multiMenuId).get();
        menuSave.setMultiMenuName(multiMenu.getName());
        return menuSave;
    }

    public List<MealTimeStandardSave> addMealTimeStandardSave(Menu menu) {

        List<MealTimeStandardSave> list = new ArrayList<>();

        for (MealTimeStandard mealTimeStandard : menu.getMealTimeStandards()) {
            MealTimeStandardSave add = mealTimeStandardSaveService.add(mealTimeStandard);
            MealTimeStandardSave save = mealTimeStandardSaveRepository.save(add);
            list.add(add);
        }
        return list;
    }


    public MenuSave checkMenu(Integer kindergartenId, Timestamp timestamp) {


        List<MenuSave> list = menuSaveRepository.findAllByKindergarten_Id(kindergartenId);

        for (MenuSave menuSave : list) {

            Date d = new Date(timestamp.getTime());
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            Date dm = new Date(menuSave.getDayDate().getTime());
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int yearM = cm.get(Calendar.YEAR);
            int monthM = cm.get(Calendar.MONTH);
            int dayM = cm.get(Calendar.DAY_OF_MONTH);

            if (year == yearM && month == monthM && day == dayM) {
                return menuSave;
            }
        }
        return null;
    }

    public MenuSaveResponseDTO get(Integer id) {
        Optional<MenuSave> byId = menuSaveRepository.findById(id);

        if (byId.isPresent()) {
            MenuSave menuSave = byId.get();

            MenuSaveResponseDTO dto = parse(menuSave);
            PerDayResponseDTO perDayDTO = perDayService.get(menuSave.getKindergarten().getId(), menuSave.getDayDate());
            dto.setPerDayDTO(perDayDTO);

            return dto;

        } else {
            return null;
        }
    }

    public MenuSaveResponseDTO parse(MenuSave menuSave) {

        MenuSaveResponseDTO dto = new MenuSaveResponseDTO();

        dto.setName(menuSave.getName());
        dto.setMultiMenuName(menuSave.getMultiMenuName());
        dto.setCreateDate(menuSave.getCreateDate());
        dto.setUpdateDate(menuSave.getUpdateDate());
        dto.setDate(menuSave.getDayDate());
        dto.setId(menuSave.getId());
        dto.setConfirmation(menuSave.getConfirmation());
        dto.setStatus(menuSave.getStatus());

        if (menuSave.getNumberFact() != null) {
            dto.setPerDayDTO(perDayService.parse(menuSave.getNumberFact()));
        }


        List<MealTimeStandardResponseSaveDTO> list = new ArrayList<>();
        for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
            MealTimeStandardResponseSaveDTO parse = mealTimeStandardSaveService.parse(mealTimeStandardSave);
            list.add(parse);
        }
        list.sort(Comparator.comparing(MealTimeStandardResponseSaveDTO::getMealTimeName));
        dto.setMealTimeStandardResponseSaveDTOList(list);


        List<NumberOfKidsResponseDTO> guessList = new ArrayList<>();
//        for (NumberOfKids numberToGuess : menuSave.getNumberToGuess()) {
//            guessList.add(numberOfKidsService.parse(numberToGuess));
//        }
//        dto.setNumberToGuess(guessList);


        return dto;
    }

    public StateMessage delete(Integer id) {
        menuSaveRepository.deleteById(id);
        return new StateMessage("O`chirildi qayta tiklab bo`lmaydi", true);
    }

    public StateMessage confirmation(Integer id) {
        Optional<MenuSave> byId = menuSaveRepository.findById(id);

        if (byId.isPresent()) {
            MenuSave menuSave = byId.get();
            menuSave.setConfirmation(true);
            menuSave.setStatus(Status.SUCCESS.getName());
            menuSaveRepository.save(menuSave);

            return new StateMessage("Menyu muvaffaqiyatli tastiqlandi", true);
        } else {
            return new StateMessage("Xatolik menyu topilmadi", false);
        }
    }


    //YANGI TAXRIR
    public List<WeightProduct> calculateMenuProduct(MenuSave ms, MenuWeight menuWeight) {
        MenuSave menuSave = menuSaveRepository.findById(ms.getId()).get();

        List<WeightProduct> list = new ArrayList<>();

        PerDay numberToGuess = menuSave.getNumberToGuess();

        for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
            for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {
                Meal meal = mealRepository.findById(mealAgeStandardSave.getMealId()).get();
                for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
                    if (ageStandardSave.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                        BigDecimal weight = ageStandardSave.getWeight();
                        for (NumberOfChildren numberOfChild : numberToGuess.getNumberOfChildren()) {
                            if (numberOfChild.getAgeGroup().getId().equals(ageStandardSave.getAgeGroupId())){
                                BigDecimal mealWeight = weight.multiply(BigDecimal.valueOf(numberOfChild.getNumberOfKids()));

                                for (ProductMeal productMeal : meal.getProductMeals()) {
                                    BigDecimal mealFactWeight = meal.getWeight();
                                    BigDecimal productWeight = productMeal.getWeight().divide(mealFactWeight, 8, RoundingMode.HALF_UP).multiply(mealWeight);
                                    addListProduct(list,productMeal.getProduct(),productWeight, menuWeight);
                                }
                            }
                        }
                    }
                }
            }
        }

        for (WeightProduct weightProduct : list) {
            weightProduct.setCheckWeight(weightProduct.getWeight());
        }
        return list;
    }

    public void addListProduct(List<WeightProduct> list, Product product, BigDecimal weight, MenuWeight menuWeight){

        boolean res = true;

        for (WeightProduct weightProduct : list) {
            if (weightProduct.getProduct().equals(product)) {
                weightProduct.setWeight(weightProduct.getWeight().add(weight));
//                if (weightProduct.getCheckWeight() == null){
//                    weightProduct.setCheckWeight(BigDecimal.valueOf(0));
//                }
//                weightProduct.setCheckWeight(weightProduct.getCheckWeight().add(weight));
                res = false;
            }
        }
        if (res){
            list.add(new WeightProduct(weight, product, menuWeight,product.getId()));
        }
    }
}
