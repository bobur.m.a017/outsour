package com.smart_solution.outsource.kindergartenMenu.ageStandard;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AgeStandardSaveRepository extends JpaRepository<AgeStandardSave, Integer> {

}
