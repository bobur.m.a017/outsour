package com.smart_solution.outsource.kindergartenMenu.ageStandard;


import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

public class AgeStandardResponseSaveDTO {

    private Integer id;
    private BigDecimal weight;
    private String ageGroupName;

    public AgeStandardResponseSaveDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }
}
