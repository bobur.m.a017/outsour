package com.smart_solution.outsource.kindergartenMenu.ageStandard;

import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AgeStandardSave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Ingredient ingredient;

    private String ageGroupName;
    private Integer ageGroupId;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private MealAgeStandardSave mealAgeStandardSave;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public MealAgeStandardSave getMealAgeStandardSave() {
        return mealAgeStandardSave;
    }

    public void setMealAgeStandardSave(MealAgeStandardSave mealAgeStandardSave) {
        this.mealAgeStandardSave = mealAgeStandardSave;
    }
}
