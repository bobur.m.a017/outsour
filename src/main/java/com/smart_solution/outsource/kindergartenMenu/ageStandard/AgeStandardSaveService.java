package com.smart_solution.outsource.kindergartenMenu.ageStandard;

import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.ingredient.IngredientService;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.menu.ageStandard.AgeStandard;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public record AgeStandardSaveService(
        AgeStandardSaveRepository ageStandardSaveRepository,
        IngredientService ingredientService,
        AgeGroupRepository ageGroupRepository) {


    public AgeStandardSave add(AgeStandard ageStandard) {

        AgeStandardSave ageStandardSave = new AgeStandardSave();

        ageStandardSave.setWeight(ageStandard.getWeight());

        Ingredient mealIngredient = ageStandard.getIngredient();

        Ingredient ingredient = ingredientService.addNoSave(mealIngredient.getProtein(), mealIngredient.getKcal(), mealIngredient.getOil(), mealIngredient.getCarbohydrates());

        ageStandardSave.setIngredient(ingredient);
        ingredient.setAgeStandardSave(ageStandardSave);

        ageStandardSave.setAgeGroupName(ageStandard.getAgeGroup().getName());
        ageStandardSave.setAgeGroupId(ageStandard.getAgeGroup().getId());

        return ageStandardSave;
    }

    public AgeStandardResponseSaveDTO parse(AgeStandardSave ageStandardSave) {

        AgeStandardResponseSaveDTO dto = new AgeStandardResponseSaveDTO();

        dto.setId(ageStandardSave.getId());
        dto.setAgeGroupName(ageStandardSave.getAgeGroupName());
        dto.setWeight(ageStandardSave.getWeight().multiply(BigDecimal.valueOf(1000)));
        return dto;
    }
}
