package com.smart_solution.outsource.kindergartenMenu.mealAgeStandard;


import com.smart_solution.outsource.attachment.Attachment;
import com.smart_solution.outsource.attachment.AttachmentRepository;
import com.smart_solution.outsource.attachment.AttachmentService;
import com.smart_solution.outsource.kindergartenMenu.ageStandard.*;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSaveRepository;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.ageStandard.AgeStandard;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public record MealAgeStandardSaveService(
        MealAgeStandardSaveRepository mealAgeStandardSaveRepository,
        MealRepository mealRepository,
        AgeStandardSaveService ageStandardSaveService,
        AgeStandardSaveRepository ageStandardSaveRepository,
        MealTimeStandardSaveRepository mealTimeStandardSaveRepository,
        AttachmentRepository attachmentRepository,
        AttachmentService attachmentService) {


    public MealAgeStandardSave add(MealAgeStandard mealAgeStandard) {

        MealAgeStandardSave mealAgeStandardSave = new MealAgeStandardSave();

        mealAgeStandardSave.setMealName(mealAgeStandard.getMeal().getName());
        mealAgeStandardSave.setMealId(mealAgeStandard.getMeal().getId());

        List<AgeStandardSave> list = new ArrayList<>();

        for (AgeStandard ageStandard : mealAgeStandard.getAgeStandards()) {

            AgeStandardSave add = ageStandardSaveService.add(ageStandard);
            add.setMealAgeStandardSave(mealAgeStandardSave);
            list.add(add);

        }

        mealAgeStandardSave.setAgeStandardSaves(list);

        return mealAgeStandardSave;
    }

    public MealAgeStandardResponseSaveDTO parse(MealAgeStandardSave mealAgeStandardSave) {

        Meal meal = mealRepository.findByName(mealAgeStandardSave.getMealName()).get();

        MealAgeStandardResponseSaveDTO dto = new MealAgeStandardResponseSaveDTO();
        dto.setId(mealAgeStandardSave.getId());
        dto.setName(mealAgeStandardSave.getMealName());

        Attachment attachment = attachmentRepository.findById(meal.getAttachmentId()).get();

        byte[] bytes = attachmentService.attachmentToBytes(attachment);


        dto.setImage(bytes);

        List<AgeStandardResponseSaveDTO> list = new ArrayList<>();

        for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
            if (ageStandardSave.getWeight().compareTo(BigDecimal.valueOf(0)) > 0){
                list.add(ageStandardSaveService.parse(ageStandardSave));
            }
        }
        dto.setAgeStandardResponseSaveDTOList(list);
        return dto;
    }
}
