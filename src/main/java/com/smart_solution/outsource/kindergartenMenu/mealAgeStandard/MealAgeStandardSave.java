package com.smart_solution.outsource.kindergartenMenu.mealAgeStandard;

import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.meal.Meal;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
public class MealAgeStandardSave {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    private Integer mealId;

    private String mealName;

    @OneToMany(mappedBy = "mealAgeStandardSave",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AgeStandardSave> ageStandardSaves;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private MealTimeStandardSave mealTimeStandardSave;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMealId() {
        return mealId;
    }

    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public List<AgeStandardSave> getAgeStandardSaves() {
        return ageStandardSaves;
    }

    public void setAgeStandardSaves(List<AgeStandardSave> ageStandardSaves) {
        this.ageStandardSaves = ageStandardSaves;
    }

    public MealTimeStandardSave getMealTimeStandardSave() {
        return mealTimeStandardSave;
    }

    public void setMealTimeStandardSave(MealTimeStandardSave mealTimeStandardSave) {
        this.mealTimeStandardSave = mealTimeStandardSave;
    }
}
