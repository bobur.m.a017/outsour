package com.smart_solution.outsource.kindergartenMenu.mealAgeStandard;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MealAgeStandardSaveRepository extends JpaRepository<MealAgeStandardSave, Integer> {

}
