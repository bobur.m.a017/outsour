package com.smart_solution.outsource.kindergartenMenu.mealAgeStandard;

import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardResponseSaveDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.OneToMany;
import java.util.List;

public class MealAgeStandardResponseSaveDTO {

    private Integer id;

    private String name;
    private byte[] image;

    private List<AgeStandardResponseSaveDTO> ageStandardResponseSaveDTOList;


    public MealAgeStandardResponseSaveDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public List<AgeStandardResponseSaveDTO> getAgeStandardResponseSaveDTOList() {
        return ageStandardResponseSaveDTOList;
    }

    public void setAgeStandardResponseSaveDTOList(List<AgeStandardResponseSaveDTO> ageStandardResponseSaveDTOList) {
        this.ageStandardResponseSaveDTOList = ageStandardResponseSaveDTOList;
    }
}
