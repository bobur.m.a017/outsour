package com.smart_solution.outsource.kindergartenMenu.mealTimeStandard;


import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.menu.mealTime.MealTime;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
public class MealTimeStandardSave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    private Integer mealTimeId;
    private String mealTimeName;


    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<MenuSave> menuSaveList;

    @OneToMany(mappedBy = "mealTimeStandardSave",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<MealAgeStandardSave> mealAgeStandardSaves;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMealTimeId() {
        return mealTimeId;
    }

    public void setMealTimeId(Integer mealTimeId) {
        this.mealTimeId = mealTimeId;
    }

    public String getMealTimeName() {
        return mealTimeName;
    }

    public void setMealTimeName(String mealTimeName) {
        this.mealTimeName = mealTimeName;
    }

    public List<MenuSave> getMenuSaveList() {
        return menuSaveList;
    }

    public void setMenuSaveList(List<MenuSave> menuSaveList) {
        this.menuSaveList = menuSaveList;
    }

    public List<MealAgeStandardSave> getMealAgeStandardSaves() {
        return mealAgeStandardSaves;
    }

    public void setMealAgeStandardSaves(List<MealAgeStandardSave> mealAgeStandardSaves) {
        this.mealAgeStandardSaves = mealAgeStandardSaves;
    }
}

