package com.smart_solution.outsource.kindergartenMenu.mealTimeStandard;


import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardResponseSaveDTO;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class MealTimeStandardResponseSaveDTO {

    private Integer id;

    private String mealTimeName;

    private List<MealAgeStandardResponseSaveDTO> mealAgeStandardResponseSaveDTOList;


    public MealTimeStandardResponseSaveDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMealTimeName() {
        return mealTimeName;
    }

    public void setMealTimeName(String mealTimeName) {
        this.mealTimeName = mealTimeName;
    }

    public List<MealAgeStandardResponseSaveDTO> getMealAgeStandardResponseSaveDTOList() {
        return mealAgeStandardResponseSaveDTOList;
    }

    public void setMealAgeStandardResponseSaveDTOList(List<MealAgeStandardResponseSaveDTO> mealAgeStandardResponseSaveDTOList) {
        this.mealAgeStandardResponseSaveDTOList = mealAgeStandardResponseSaveDTOList;
    }
}

