package com.smart_solution.outsource.kindergartenMenu.mealTimeStandard;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MealTimeStandardSaveRepository extends JpaRepository<MealTimeStandardSave, Integer> {

}
