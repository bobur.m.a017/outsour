package com.smart_solution.outsource.kindergartenMenu.mealTimeStandard;

import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardResponseSaveDTO;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSaveService;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupDTO;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupService;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.menu.mealTime.MealTime;
import com.smart_solution.outsource.menu.mealTime.MealTimeRepository;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandard;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public record MealTimeStandardSaveService(
        MealTimeStandardSaveRepository mealTimeStandardSaveRepository,
        MealTimeRepository mealTimeRepository,
        MealAgeStandardSaveService mealAgeStandardSaveService,
        AgeGroupService ageGroupService) {


    public MealTimeStandardSave add(MealTimeStandard mealTimeStandard) {

        MealTimeStandardSave mealTimeStandardSave = new MealTimeStandardSave();

        mealTimeStandardSave.setMealTimeName(mealTimeStandard.getMealTime().getName());
        mealTimeStandardSave.setMealTimeId(mealTimeStandard.getMealTime().getId());

        List<MealAgeStandardSave> list = new ArrayList<>();

        for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandards()) {
            MealAgeStandardSave add = mealAgeStandardSaveService.add(mealAgeStandard);
            add.setMealTimeStandardSave(mealTimeStandardSave);
            list.add(add);

        }

        mealTimeStandardSave.setMealAgeStandardSaves(list);

        return mealTimeStandardSave;
    }

    public MealTimeStandardResponseSaveDTO parse(MealTimeStandardSave mealTimeStandardSave) {

        MealTimeStandardResponseSaveDTO dto = new MealTimeStandardResponseSaveDTO();

        dto.setMealTimeName(mealTimeStandardSave.getMealTimeName());
        dto.setId(mealTimeStandardSave.getId());

        List<MealAgeStandardResponseSaveDTO> list = new ArrayList<>();

        for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {

            MealAgeStandardResponseSaveDTO parse = mealAgeStandardSaveService.parse(mealAgeStandardSave);

            list.add(parse);

        }
        dto.setMealAgeStandardResponseSaveDTOList(list);

        return dto;
    }
}
