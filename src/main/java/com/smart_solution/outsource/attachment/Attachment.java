package com.smart_solution.outsource.attachment;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private byte[] bytes;


//    @Column(nullable = false)
    private String path;  // papkani ichidan topish un

    //CLIENTGA KO'RINADIGAN NOM
    private String fileOriginalName;

    //PAPKADA KO'RINADIGAN NOMI UNIQUE BO'LADI
    private String name;  //izlaganda shu nom bilan izlaniladi

    private Long size;

    private String contentType;


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileOriginalName() {
        return fileOriginalName;
    }

    public void setFileOriginalName(String fileOriginalName) {
        this.fileOriginalName = fileOriginalName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
