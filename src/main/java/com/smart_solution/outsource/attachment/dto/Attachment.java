package com.smart_solution.outsource.attachment.dto;

public class Attachment {

    private int id;
    private byte[] bytes;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
