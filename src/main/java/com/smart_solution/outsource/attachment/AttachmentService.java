package com.smart_solution.outsource.attachment;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public record AttachmentService(
        AttachmentRepository attachmentRepository) {


    public Attachment add(MultipartFile file) throws IOException {

//        Attachment attachment = new Attachment();
//
//        try {
//            attachment.setBytes(file.getBytes());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return attachmentRepository.save(attachment);
////
        try {
            return attachmentRepository.save(uploadSystem(file));
        } catch (Exception e) {
            return null;
        }
    }


    public Attachment edit(MultipartFile file, Integer id) {

        Attachment attachment = attachmentRepository.findById(id).get();

        try {
//            attachment.setBytes(file.getBytes());
//            return attachmentRepository.save(attachment);
            return uploadSystemEdit(file, attachment);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return attachmentRepository.save(attachment);
    }


        public static final String uploadDirectories = "/root/outsource/files";
//    public static final String uploadDirectories = "files";
//    public static final String uploadDirectories = "C:\\Users\\User\\Documents\\GitHub/files";

    public Attachment uploadSystem(MultipartFile file) throws IOException {


        if (file != null) {
            String originalFilename = file.getOriginalFilename();
            Attachment attachment = new Attachment();


            if (originalFilename == null) {

                originalFilename = "123456";
            }

            attachment.setFileOriginalName(originalFilename);
            attachment.setSize(file.getSize());
            attachment.setContentType(file.getContentType());


            //FILENING CONTENTINI OLISH UCHUN KERAK
            String[] split = originalFilename.split("\\.");

            //rasm nameni unique qilish uchun kerak
            String name = UUID.randomUUID().toString() + "." + split[split.length - 1];
            attachment.setName(name);

            //papka saqlanadigan yo'l
            Path path = Paths.get(uploadDirectories + "/" + name);

            attachment.setPath(path.toString());

            Attachment saveAttachment = attachmentRepository.save(attachment);

            Files.copy(file.getInputStream(), path);

            return saveAttachment;
        }
        return null;
    }

    public Attachment uploadSystemEdit(MultipartFile file, Attachment attachment) throws IOException {


        if (file != null) {
            String originalFilename = file.getOriginalFilename();


            if (originalFilename == null) {

                originalFilename = "123456";
            }

            attachment.setFileOriginalName(originalFilename);
            attachment.setSize(file.getSize());
            attachment.setContentType(file.getContentType());


            //FILENING CONTENTINI OLISH UCHUN KERAK
//            String[] split = originalFilename.split("\\.");

            //rasm nameni unique qilish uchun kerak
            String name = UUID.randomUUID().toString();
            attachment.setName(name);

            //papka saqlanadigan yo'l
            Path path = Paths.get(uploadDirectories + "/" + name);

            attachment.setPath(path.toString());

            Attachment saveAttachment = attachmentRepository.save(attachment);

            Files.copy(file.getInputStream(), path);

            return saveAttachment;
        }
        return null;
    }

    public byte[] attachmentToBytes(Attachment attachment) {
        try {
            if (attachment.getBytes() != null) {
                return attachment.getBytes();
            }
            File file = new File(attachment.getPath());
            Path path = Paths.get(file.getPath());
            return Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public com.smart_solution.outsource.attachment.dto.Attachment getAttachment(Integer id) {


        Optional<Attachment> byId = attachmentRepository.findById(id);

        Attachment attachment = byId.orElse(null);

        if (attachment != null) {

            com.smart_solution.outsource.attachment.dto.Attachment dto = new com.smart_solution.outsource.attachment.dto.Attachment();

            dto.setId(attachment.getId());

            if (attachment.getBytes() == null) {

                byte[] bytes = attachmentToBytes(attachment);
                dto.setBytes(bytes);
            } else {
                dto.setBytes(attachment.getBytes());
            }

            return dto;
        }
        return null;
    }

}
