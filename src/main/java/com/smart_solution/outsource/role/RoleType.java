package com.smart_solution.outsource.role;


public enum RoleType {
    ADMIN(1, "ROLE_ADMIN"),
    CHIEF(2, "ROLE_RAHBAR"),
    TECHNOLOGIST(3, "ROLE_TEXNOLOG"),
    STOREKEEPER(4, "ROLE_OMBORCHI"),
    SUPPLIER(5, "ROLE_TA`MINOTCHI"),
    KINDERGARDEN_PRINCIPAL(6, "ROLE_BOG`CHA MUDIRASI"),
    NURSE(7, "ROLE_HAMSHIRA"),
    COOK(8, "ROLE_OSHPAZ"),
    ACCOUNTANT(9, "ROLE_BUXGALTER");

    private Integer id;
    private String name;

    RoleType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
