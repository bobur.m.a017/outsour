package com.smart_solution.outsource.role;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {


    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<RoleDTO> get(String str) {

        List<RoleDTO> list = new ArrayList<>();
        for (Role role : roleRepository.findAll()) {

            if (str.equals("company")) {
                if (role.getName().equals(RoleType.TECHNOLOGIST.getName()) ||
                        role.getName().equals(RoleType.ACCOUNTANT.getName()) ||
                        role.getName().equals(RoleType.STOREKEEPER.getName())) {

                    list.add(new RoleDTO(role.getId(), role.getName()));
                }
            }
            if (str.equals("kindergarten")) {
                if (role.getName().equals(RoleType.COOK.getName()) ||
                        role.getName().equals(RoleType.KINDERGARDEN_PRINCIPAL.getName()) ||
                        role.getName().equals(RoleType.NURSE.getName())) {

                    list.add(new RoleDTO(role.getId(), role.getName()));
                }
            }
        }

        return list;
    }
}
