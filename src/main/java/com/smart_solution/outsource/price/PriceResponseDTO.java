package com.smart_solution.outsource.price;

import java.math.BigDecimal;

public class PriceResponseDTO {
    private String productName;
    private BigDecimal price;

    public PriceResponseDTO(String productName, BigDecimal price) {
        this.productName = productName;
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
