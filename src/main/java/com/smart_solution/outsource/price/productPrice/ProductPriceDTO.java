package com.smart_solution.outsource.price.productPrice;

import com.sun.istack.NotNull;

public class ProductPriceDTO {

    @NotNull
    private Integer id;

    @NotNull
    private Double price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
