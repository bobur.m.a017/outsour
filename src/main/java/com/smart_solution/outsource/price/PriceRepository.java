package com.smart_solution.outsource.price;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PriceRepository extends JpaRepository<Price,Integer> {
    Optional<Price> findByKindergarten_IdAndYearAndMonthAndProduct_Id(Integer kindergartenId, Integer year, Integer month,Integer productId);
    List<Price> findAllByKindergarten_IdAndYearAndMonth(Integer kindergartenId, Integer year, Integer month);
}
