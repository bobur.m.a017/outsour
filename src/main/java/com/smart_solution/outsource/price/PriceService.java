package com.smart_solution.outsource.price;

import com.smart_solution.outsource.address.district.DistrictRepository;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.price.productPrice.ProductPriceDTO;
import com.smart_solution.outsource.product.ProductRepository;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public record PriceService(
        PriceRepository priceRepository,
        KindergartenRepository kindergartenRepository,
        ProductRepository productRepository,
        DistrictRepository districtRepository
) {


    public StateMessage add(PriceDTO dto) {


        List<Price> priceList = new ArrayList<>();
        for (Integer integer : dto.getKindergartenId()) {
            Kindergarten kindergarten = kindergartenRepository.findById(integer).get();

            for (ProductPriceDTO productPriceDTO : dto.getProductPriceDTOList()) {

                Optional<Price> optionalPrice = priceRepository.findByKindergarten_IdAndYearAndMonthAndProduct_Id(kindergarten.getId(), dto.getYear(), dto.getMonth(), productPriceDTO.getId());
                Price price = optionalPrice.orElseGet(Price::new);

                price.setPrice(BigDecimal.valueOf(productPriceDTO.getPrice()));
                price.setDistrict(kindergarten.getAddress().getDistrict());
                price.setProduct(productRepository.findById(productPriceDTO.getId()).get());
                price.setKindergarten(kindergarten);
                price.setMonth(dto.getMonth());
                price.setYear(dto.getYear());
                priceList.add(price);
            }
        }
        priceRepository.saveAll(priceList);
        return new StateMessage("Muvaffaqiyatli bajarildi. Tekshirib ko`ring", true);
    }

    public List<PriceResponseDTO> get(Integer id, Integer year, Integer month) {

        List<PriceResponseDTO> list = new ArrayList<>();

        for (Price price : priceRepository.findAllByKindergarten_IdAndYearAndMonth(id, year, month)) {
            list.add(new PriceResponseDTO(price.getProduct().getName(),price.getPrice()));
        }

        return list;
    }
}
