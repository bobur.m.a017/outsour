package com.smart_solution.outsource.price;

import com.smart_solution.outsource.price.productPrice.ProductPriceDTO;
import com.sun.istack.NotNull;

import java.util.List;

public class PriceDTO {

    private List<Integer> kindergartenId;
    private List<ProductPriceDTO> productPriceDTOList;

    @NotNull
    private Integer year;

    @NotNull
    private Integer month;

    public List<Integer> getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(List<Integer> kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public List<ProductPriceDTO> getProductPriceDTOList() {
        return productPriceDTOList;
    }

    public void setProductPriceDTOList(List<ProductPriceDTO> productPriceDTOList) {
        this.productPriceDTOList = productPriceDTOList;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }
}
