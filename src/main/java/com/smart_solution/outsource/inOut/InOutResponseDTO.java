package com.smart_solution.outsource.inOut;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

public class InOutResponseDTO {

    private UUID id;
    private Date enterDate;
    private BigDecimal price;
    private BigDecimal pack;
    private BigDecimal numberPack;
    private BigDecimal weightPack;
    private String measurementType;
    private String status;
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getEnterDate() {
        return enterDate;
    }

    public void setEnterDate(Date enterDate) {
        this.enterDate = enterDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public BigDecimal getWeightPack() {
        return weightPack;
    }

    public void setWeightPack(BigDecimal weightPack) {
        this.weightPack = weightPack;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }
}
