package com.smart_solution.outsource.inOut;

public class InOutDTO {

    private Double numberPack;
    private Double weight;
    private String comment;

    public InOutDTO() {
    }

    public InOutDTO(Double numberPack, Double weight, String comment) {
        this.numberPack = numberPack;
        this.weight = weight;
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(Double numberPack) {
        this.numberPack = numberPack;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
