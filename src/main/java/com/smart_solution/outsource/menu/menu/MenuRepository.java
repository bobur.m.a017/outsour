package com.smart_solution.outsource.menu.menu;

import org.springframework.data.jpa.repository.JpaRepository;


public interface MenuRepository extends JpaRepository<Menu, Integer> {
    boolean existsByName(String name);
}
