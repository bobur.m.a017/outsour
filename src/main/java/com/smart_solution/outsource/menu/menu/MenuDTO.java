package com.smart_solution.outsource.menu.menu;


import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandard;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandardDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.OneToMany;
import java.util.List;

public class MenuDTO {
    private Integer id;
    private String name;
    private Integer multiMenuId;

    private List<MealTimeStandardDTO> mealTimeStandardDTOList;

    public MenuDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMultiMenuId() {
        return multiMenuId;
    }

    public void setMultiMenuId(Integer multiMenuId) {
        this.multiMenuId = multiMenuId;
    }

    public List<MealTimeStandardDTO> getMealTimeStandardDTOList() {
        return mealTimeStandardDTOList;
    }

    public void setMealTimeStandardDTOList(List<MealTimeStandardDTO> mealTimeStandardDTOList) {
        this.mealTimeStandardDTOList = mealTimeStandardDTOList;
    }
}
