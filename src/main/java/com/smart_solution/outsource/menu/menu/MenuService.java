package com.smart_solution.outsource.menu.menu;


import com.smart_solution.outsource.menu.mealTime.MealTime;
import com.smart_solution.outsource.menu.mealTime.MealTimeRepository;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandard;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandardDTO;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandardResponseDTO;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandardService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.multiMenu.MultiMenu;
import com.smart_solution.outsource.multiMenu.MultiMenuRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public record MenuService(
        MenuRepository menuRepository,
        MealTimeStandardService mealTimeStandardService,
        MultiMenuRepository multiMenuRepository,
        MealTimeRepository mealTimeRepository) {


//    public StateMessage add(Integer id, MenuDTO menuDTO) {
//
//        Menu menu = menuRepository.findById(id).get();
//
//        if (menu.getMealTimeStandards() == null) {
//            return edit(menuDTO, id);
//        } else {
//
//            menu.setName(menuDTO.getName());
//
//            List<MealTimeStandard> list = new ArrayList<>();
//
//            for (MealTimeStandardDTO mealTimeStandardDTO : menuDTO.getMealTimeStandardDTOList()) {
//
//                MealTimeStandard mealTimeStandard = mealTimeStandardService.add(mealTimeStandardDTO);
//
//                list.add(mealTimeStandard);
//
//            }
//            menu.setMealTimeStandards(list);
//            Menu saveMenu = menuRepository.save(menu);
//            MultiMenu multiMenu = multiMenuRepository.findById(menuDTO.getMultiMenuId()).get();
//            multiMenu.setMenuList(List.of(saveMenu));
//            multiMenuRepository.save(multiMenu);
//
//            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
//        }
//
//    }

//    public StateMessage edit(MenuDTO menuDTO, Integer id) {
//
//        Optional<Menu> byId = menuRepository.findById(id);
//        if (byId.isPresent()) {
//            Menu menu = byId.get();
//
//            menu.setName(menuDTO.getName());
//
//            List<MealTimeStandard> list = new ArrayList<>();
//
//            for (MealTimeStandardDTO mealTimeStandardDTO : menuDTO.getMealTimeStandardDTOList()) {
//
//                MealTimeStandard mealTimeStandard = mealTimeStandardService.edit(mealTimeStandardDTO);
//
//                list.add(mealTimeStandard);
//
//            }
//
//            menu.setMealTimeStandards(list);
//            Menu saveMenu = menuRepository.save(menu);
//            MultiMenu multiMenu = multiMenuRepository.findById(menuDTO.getMultiMenuId()).get();
//            multiMenu.setMenuList(List.of(saveMenu));
//            multiMenuRepository.save(multiMenu);
//
//            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
//
//
//        } else {
//            return new StateMessage("X A T O L I K !!!", false);
//        }
//    }

    public MenuResponseDTO parse(Menu menu) {

        MenuResponseDTO dto = new MenuResponseDTO();

        dto.setId(menu.getId());
        dto.setName(menu.getName());
        dto.setCreateDate(menu.getCreateDate());
        dto.setUpdateDate(menu.getUpdateDate());

        List<MealTimeStandardResponseDTO> list = new ArrayList<>();

        for (MealTimeStandard mealTimeStandard : menu.getMealTimeStandards()) {
            list.add(mealTimeStandardService.parse(mealTimeStandard));
        }
        list.sort(Comparator.comparing(MealTimeStandardResponseDTO::getMealTimeName));
        dto.setMealTimeStandardResponseDTOList(list);

        return dto;
    }

    public Menu add(int name) {
        Menu menu = new Menu();
        menu.setName(name + "-kunlik taomnoma");

        List<MealTimeStandard> list = new ArrayList<>();

        for (MealTime mealTime : mealTimeRepository.findAll()) {
            list.add(mealTimeStandardService.add(mealTime));
        }

        menu.setMealTimeStandards(list);

        return menuRepository.save(menu);
    }
}
