package com.smart_solution.outsource.menu.menu;

import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandard;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandardResponseDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

public class MenuResponseDTO {

    private Integer id;
    private String name;
    private Timestamp createDate;
    private Timestamp updateDate;
    private List<MealTimeStandardResponseDTO> mealTimeStandardResponseDTOList;


    public MenuResponseDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<MealTimeStandardResponseDTO> getMealTimeStandardResponseDTOList() {
        return mealTimeStandardResponseDTOList;
    }

    public void setMealTimeStandardResponseDTOList(List<MealTimeStandardResponseDTO> mealTimeStandardResponseDTOList) {
        this.mealTimeStandardResponseDTOList = mealTimeStandardResponseDTOList;
    }
}
