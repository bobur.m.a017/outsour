package com.smart_solution.outsource.menu.mealTime;

import com.smart_solution.outsource.menu.ageGroup.AgeGroupDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.OneToMany;
import java.util.List;


public class MealTimeDTO {

    private Integer id;
    private String name;
    @OneToMany
    private List<AgeGroupDTO> ageGroupsId;


    public MealTimeDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AgeGroupDTO> getAgeGroupsId() {
        return ageGroupsId;
    }

    public void setAgeGroupsId(List<AgeGroupDTO> ageGroupsId) {
        this.ageGroupsId = ageGroupsId;
    }
}
