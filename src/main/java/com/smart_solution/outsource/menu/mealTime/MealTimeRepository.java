package com.smart_solution.outsource.menu.mealTime;

import org.springframework.data.jpa.repository.JpaRepository;


public interface MealTimeRepository extends JpaRepository<MealTime, Integer> {
    boolean existsByName(String name);
}
