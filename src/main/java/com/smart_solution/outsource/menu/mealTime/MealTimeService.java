package com.smart_solution.outsource.menu.mealTime;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupDTO;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupService;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public record MealTimeService(
        AgeGroupService ageGroupService,
        MealTimeRepository mealTimeRepository,
        AgeGroupRepository ageGroupRepository
) {


    public StateMessage add(MealTimeDTO mealTimeDTO) {

        boolean res = mealTimeRepository.existsByName(mealTimeDTO.getName());

        if (!res) {

            MealTime mealTime = new MealTime();
            mealTime.setName(mealTimeDTO.getName());
            mealTime.setDelete(false);

            List<AgeGroup> ageGroupList = new ArrayList<>();
            for (AgeGroupDTO ageGroupDTO : mealTimeDTO.getAgeGroupsId()) {

                Optional<AgeGroup> byId = ageGroupRepository.findById(ageGroupDTO.getAgeGroupId());

                byId.ifPresent(ageGroupList::add);

            }
            mealTime.setAgeGroups(ageGroupList);
            mealTimeRepository.save(mealTime);
            return new StateMessage("Muvaffaqiyatli saqlandi", true);
        } else
            return new StateMessage("Ushbu guruh mavjud!", false);
    }

    public StateMessage edit(MealTimeDTO mealTimeDTO, Integer id) {
        Optional<MealTime> byId1 = mealTimeRepository.findById(id);
        if (byId1.isPresent()) {
            boolean res = false;
            MealTime mealTime = byId1.get();

            if (!mealTime.getName().equals(mealTimeDTO.getName())) {
                res = mealTimeRepository.existsByName(mealTimeDTO.getName());
            }

            if (!res) {

                mealTime.setName(mealTimeDTO.getName());

                List<AgeGroup> ageGroupList = new ArrayList<>();
                for (AgeGroupDTO ageGroupDTO : mealTimeDTO.getAgeGroupsId()) {

                    Optional<AgeGroup> byId = ageGroupRepository.findById(ageGroupDTO.getAgeGroupId());

                    byId.ifPresent(ageGroupList::add);

                }
                mealTime.setAgeGroups(ageGroupList);
                mealTimeRepository.save(mealTime);
                return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);
            }
            return new StateMessage("Ushbu guruh mavjud!", false);
        }
        return new StateMessage("Ma`lumotlar noto`g`ri kiritildi", false);
    }

    public List<MealTimeDTO> get() {

        List<MealTimeDTO> list = new ArrayList<>();

        for (MealTime mealTime : mealTimeRepository.findAll()) {
            if (!mealTime.getDelete()) {

                MealTimeDTO mealTimeDTO = new MealTimeDTO();
                mealTimeDTO.setId(mealTime.getId());
                mealTimeDTO.setName(mealTime.getName());

                List<AgeGroupDTO> ageGroupDTOList = new ArrayList<>();
                for (AgeGroup ageGroup : mealTime.getAgeGroups()) {
                    ageGroupDTOList.add(ageGroupService.parse(ageGroup));
                }
                mealTimeDTO.setAgeGroupsId(ageGroupDTOList);

                list.add(mealTimeDTO);
            }
        }

        return list;

    }

    public StateMessage delete(Integer id) {

        MealTime mealTime = mealTimeRepository.findById(id).get();

        mealTime.setDelete(true);

        mealTimeRepository.save(mealTime);

        return new StateMessage("O`chirildi qayta tiklash uchun adminga murojaat qiling", true);
    }
}
