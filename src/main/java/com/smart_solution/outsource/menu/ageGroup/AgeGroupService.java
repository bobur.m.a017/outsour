package com.smart_solution.outsource.menu.ageGroup;

import com.smart_solution.outsource.message.StateMessage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class AgeGroupService implements AgeGroupParseDTO {

    private final AgeGroupRepository ageGroupRepository;

    public AgeGroupService(AgeGroupRepository ageGroupRepository) {
        this.ageGroupRepository = ageGroupRepository;
    }

    public StateMessage add(AgeGroupDTO ageGroupDTO) {
        boolean exists = ageGroupRepository.existsByName(ageGroupDTO.getName());
        if (!exists) {
            AgeGroup ageGroup = new AgeGroup();
            ageGroup.setDelete(false);
            ageGroup.setName(ageGroupDTO.getName());
            ageGroupRepository.save(ageGroup);
            return new StateMessage("Muvaffaqiyatli saqlandi", true);
        } else
            return new StateMessage("Ushbu guruh mavjud!", false);
    }

    public StateMessage delete(Integer id) {
        Optional<AgeGroup> optionalAgeGroup = ageGroupRepository.findById(id);

        if (optionalAgeGroup.isPresent()) {

            AgeGroup ageGroup = optionalAgeGroup.get();
            ageGroup.setDelete(true);
            ageGroupRepository.save(ageGroup);

            return new StateMessage("Muvafaqqiyatli o'chirildi", true);
        }
        return new StateMessage("Ushbu Obyekt mavjud emas", false);
    }

    public StateMessage edit(AgeGroupDTO ageGroupDTO, Integer id) {
        Optional<AgeGroup> optionalAgeGroup = ageGroupRepository.findById(id);

        if (optionalAgeGroup.isPresent()) {

            AgeGroup ageGroup = optionalAgeGroup.get();
            boolean res = false;

            if (!ageGroup.getName().equals(ageGroupDTO.getName())) {
                res = ageGroupRepository.existsByName(ageGroupDTO.getName());
            }

            if (!res) {
                ageGroup.setName(ageGroupDTO.getName());
                ageGroupRepository.save(ageGroup);
                return new StateMessage("Muvaffaqiyatli qo'shildi", true);
            }
            return new StateMessage("Ushbu guruh mavjud", false);
        }
        return new StateMessage("Xatolik", false);
    }

    public List<AgeGroupDTO> getAll() {

        List<AgeGroupDTO> list = new ArrayList<>();

        for (AgeGroup ageGroup : ageGroupRepository.findAll()) {
            if (!ageGroup.getDelete())
                list.add(parse(ageGroup));
        }

        list.sort(Comparator.comparing(AgeGroupDTO::getAgeGroupId));

        return list;
    }
}
