package com.smart_solution.outsource.menu.ageGroup;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AgeGroupRepository extends JpaRepository<AgeGroup, Integer> {

    boolean existsByName(String name);

    Optional<AgeGroup> findByName(String name);
}
