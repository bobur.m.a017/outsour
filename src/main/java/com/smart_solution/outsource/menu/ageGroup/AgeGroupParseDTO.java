package com.smart_solution.outsource.menu.ageGroup;

public interface AgeGroupParseDTO {

    default AgeGroupDTO parse(AgeGroup ageGroup) {
        AgeGroupDTO dto = new AgeGroupDTO();

        dto.setName(ageGroup.getName());
        dto.setAgeGroupId(ageGroup.getId());
        return dto;
    }
}
