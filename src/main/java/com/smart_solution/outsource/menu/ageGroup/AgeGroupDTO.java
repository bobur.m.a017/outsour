package com.smart_solution.outsource.menu.ageGroup;

import lombok.Getter;
import lombok.Setter;


public class AgeGroupDTO {

    private Integer ageGroupId;
    private String name;

    public AgeGroupDTO() {
    }

    public AgeGroupDTO(String name) {
        this.name = name;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
