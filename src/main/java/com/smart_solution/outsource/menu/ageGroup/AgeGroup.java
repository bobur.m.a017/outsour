package com.smart_solution.outsource.menu.ageGroup;


import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
public class AgeGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Boolean delete;


    @OneToMany(mappedBy = "ageGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NumberOfChildren> numberOfChildrenList;



    public AgeGroup() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }
}
