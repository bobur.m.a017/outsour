package com.smart_solution.outsource.menu.mealTimeStandard;

import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandardDTO;
import com.smart_solution.outsource.menu.mealTime.MealTime;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;


public class MealTimeStandardDTO {

    private Integer id;
    private Integer mealTimeId;
    private String name;

    private List<MealAgeStandardDTO> mealAgeStandardDTOList;


    public MealTimeStandardDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMealTimeId() {
        return mealTimeId;
    }

    public void setMealTimeId(Integer mealTimeId) {
        this.mealTimeId = mealTimeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MealAgeStandardDTO> getMealAgeStandardDTOList() {
        return mealAgeStandardDTOList;
    }

    public void setMealAgeStandardDTOList(List<MealAgeStandardDTO> mealAgeStandardDTOList) {
        this.mealAgeStandardDTOList = mealAgeStandardDTOList;
    }
}
