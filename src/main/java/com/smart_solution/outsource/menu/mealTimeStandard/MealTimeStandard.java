package com.smart_solution.outsource.menu.mealTimeStandard;


import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.menu.mealTime.MealTime;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
public class MealTimeStandard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private MealTime mealTime;

    @OneToMany(cascade = CascadeType.ALL)
    private List<MealAgeStandard> mealAgeStandards;


    public MealTimeStandard() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MealTime getMealTime() {
        return mealTime;
    }

    public void setMealTime(MealTime mealTime) {
        this.mealTime = mealTime;
    }

    public List<MealAgeStandard> getMealAgeStandards() {
        return mealAgeStandards;
    }

    public void setMealAgeStandards(List<MealAgeStandard> mealAgeStandards) {
        this.mealAgeStandards = mealAgeStandards;
    }
}

