package com.smart_solution.outsource.menu.mealTimeStandard;


import com.smart_solution.outsource.menu.ageGroup.AgeGroupDTO;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandardResponseDTO;
import com.smart_solution.outsource.menu.mealTime.MealTime;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

public class MealTimeStandardResponseDTO {

    private Integer id;

    private String mealTimeName;

    private List<MealAgeStandardResponseDTO> mealAgeStandardResponseDTOList;

    private List<AgeGroupDTO> ageGroupDTOList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMealTimeName() {
        return mealTimeName;
    }

    public void setMealTimeName(String mealTimeName) {
        this.mealTimeName = mealTimeName;
    }

    public List<MealAgeStandardResponseDTO> getMealAgeStandardResponseDTOList() {
        return mealAgeStandardResponseDTOList;
    }

    public void setMealAgeStandardResponseDTOList(List<MealAgeStandardResponseDTO> mealAgeStandardResponseDTOList) {
        this.mealAgeStandardResponseDTOList = mealAgeStandardResponseDTOList;
    }

    public List<AgeGroupDTO> getAgeGroupDTOList() {
        return ageGroupDTOList;
    }

    public void setAgeGroupDTOList(List<AgeGroupDTO> ageGroupDTOList) {
        this.ageGroupDTOList = ageGroupDTOList;
    }
}

