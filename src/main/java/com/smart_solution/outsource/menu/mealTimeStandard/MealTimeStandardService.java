package com.smart_solution.outsource.menu.mealTimeStandard;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupDTO;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupService;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandardDTO;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandardResponseDTO;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandardService;
import com.smart_solution.outsource.menu.mealTime.MealTime;
import com.smart_solution.outsource.menu.mealTime.MealTimeRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public record MealTimeStandardService(
        MealTimeStandardRepository mealTimeStandardRepository,
        MealTimeRepository mealTimeRepository,
        MealAgeStandardService mealAgeStandardService,
        AgeGroupService ageGroupService) {


    public MealTimeStandard add(MealTime mealTime) {

        MealTimeStandard mealTimeStandard = new MealTimeStandard();
        mealTimeStandard.setMealTime(mealTime);

        List<MealAgeStandard> list = new ArrayList<>();

        mealTimeStandard.setMealAgeStandards(list);
        MealTimeStandard save = mealTimeStandardRepository.save(mealTimeStandard);

        return save;
    }

//    public MealTimeStandard edit(MealTimeStandardDTO mealTimeStandardDTO) {
//
//        MealTimeStandard mealTimeStandard = mealTimeStandardRepository.findById(mealTimeStandardDTO.getId()).get();
//
//        mealTimeStandard.setMealTime(mealTimeRepository.findById(mealTimeStandardDTO.getMealTimeId()).get());
//
//        List<MealAgeStandard> list = new ArrayList<>();
//
//        for (MealAgeStandardDTO mealAgeStandardDTO : mealTimeStandardDTO.getMealAgeStandardDTOList()) {
//
//            MealAgeStandard mealAgeStandard = mealAgeStandardService.edit(mealAgeStandardDTO);
//            list.add(mealAgeStandard);
//        }
//        mealTimeStandard.setMealAgeStandards(list);
//        mealTimeStandardRepository.save(mealTimeStandard);
//
//        return mealTimeStandard;
//    }

    public MealTimeStandardResponseDTO parse(MealTimeStandard mealTimeStandard) {

        MealTimeStandardResponseDTO dto = new MealTimeStandardResponseDTO();

        dto.setId(mealTimeStandard.getId());
        dto.setMealTimeName(mealTimeStandard.getMealTime().getName());

        List<MealAgeStandardResponseDTO> list = new ArrayList<>();

        for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandards()) {
            list.add(mealAgeStandardService.parse(mealAgeStandard));
        }

        List<AgeGroupDTO> ageGroupDTOList = new ArrayList<>();

        for (AgeGroup ageGroup : mealTimeStandard.getMealTime().getAgeGroups()) {

            ageGroupDTOList.add(ageGroupService.parse(ageGroup));
        }
        dto.setAgeGroupDTOList(ageGroupDTOList);

        dto.setMealAgeStandardResponseDTOList(list);
        return dto;
    }

}
