package com.smart_solution.outsource.menu.mealTimeStandard;

import com.smart_solution.outsource.multiMenu.MultiMenu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MealTimeStandardRepository extends JpaRepository<MealTimeStandard, Integer> {
}
