package com.smart_solution.outsource.menu.mealAgeStandard;

import com.smart_solution.outsource.menu.ageStandard.AgeStandardDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class MealAgeStandardDTO {

    private Integer id;
    private Integer mealId;
    private List<AgeStandardDTO> ageStandardDTOList;
    private Integer multiMenuId;

    public MealAgeStandardDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMealId() {
        return mealId;
    }

    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    public List<AgeStandardDTO> getAgeStandardDTOList() {
        return ageStandardDTOList;
    }

    public void setAgeStandardDTOList(List<AgeStandardDTO> ageStandardDTOList) {
        this.ageStandardDTOList = ageStandardDTOList;
    }

    public Integer getMultiMenuId() {
        return multiMenuId;
    }

    public void setMultiMenuId(Integer multiMenuId) {
        this.multiMenuId = multiMenuId;
    }
}
