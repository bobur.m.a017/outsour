package com.smart_solution.outsource.menu.mealAgeStandard;

import com.smart_solution.outsource.multiMenu.MultiMenu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MealAgeStandardRepository extends JpaRepository<MealAgeStandard, Integer> {

}
