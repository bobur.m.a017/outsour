package com.smart_solution.outsource.menu.mealAgeStandard;

import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.menu.ageStandard.AgeStandard;
import com.smart_solution.outsource.menu.ageStandard.AgeStandardResponseDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

public class MealAgeStandardResponseDTO {

    private Integer id;

    private String name;

    private Integer mealId;

    private List<AgeStandardResponseDTO> ageStandardResponseDTOList;


    public MealAgeStandardResponseDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMealId() {
        return mealId;
    }

    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    public List<AgeStandardResponseDTO> getAgeStandardResponseDTOList() {
        return ageStandardResponseDTOList;
    }

    public void setAgeStandardResponseDTOList(List<AgeStandardResponseDTO> ageStandardResponseDTOList) {
        this.ageStandardResponseDTOList = ageStandardResponseDTOList;
    }
}
