package com.smart_solution.outsource.menu.mealAgeStandard;

import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.menu.ageStandard.AgeStandard;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.util.List;

@Entity
public class MealAgeStandard {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Meal meal;


    @OneToMany(cascade = { CascadeType.ALL })
    private List<AgeStandard> ageStandards;


    public MealAgeStandard() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public List<AgeStandard> getAgeStandards() {
        return ageStandards;
    }

    public void setAgeStandards(List<AgeStandard> ageStandards) {
        this.ageStandards = ageStandards;
    }
}
