package com.smart_solution.outsource.menu.mealAgeStandard;


import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.ageStandard.*;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandard;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandardRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.multiMenu.MultiMenu;
import com.smart_solution.outsource.multiMenu.MultiMenuRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
public record MealAgeStandardService(
        MealAgeStandardRepository mealAgeStandardRepository,
        MealRepository mealRepository,
        AgeStandardService ageStandardService,
        AgeStandardRepository ageStandardRepository,
        MealTimeStandardRepository mealTimeStandardRepository,
        MultiMenuRepository multiMenuRepository) {


    public StateMessage add(MealAgeStandardDTO mealAgeStandardDTO, Integer mealTimeStandardId) {

        if (mealAgeStandardDTO.getId() != null) {

            if (mealAgeStandardDTO.getId() == null) {

            }

            return edit(mealAgeStandardDTO, mealAgeStandardDTO.getId());
        }


        MealAgeStandard mealAgeStandard = new MealAgeStandard();

        Meal meal = mealRepository.findById(mealAgeStandardDTO.getMealId()).get();

        mealAgeStandard.setMeal(meal);

        List<AgeStandard> list = new ArrayList<>();


        for (AgeStandardDTO ageStandardDTO : mealAgeStandardDTO.getAgeStandardDTOList()) {

            AgeStandard ageStandard = ageStandardService.add(ageStandardDTO, meal);

            list.add(ageStandard);
        }

        mealAgeStandard.setAgeStandards(list);
        MealAgeStandard save = mealAgeStandardRepository.save(mealAgeStandard);


        MultiMenu multiMenu = multiMenuRepository.findById(mealAgeStandardDTO.getMultiMenuId()).get();
        List<MealAgeStandard> multiMenuMealAgeStandardList = multiMenu.getMealAgeStandardList();
        multiMenuMealAgeStandardList.add(save);
        multiMenu.setMealAgeStandardList(multiMenuMealAgeStandardList);
        multiMenuRepository.save(multiMenu);


        MealTimeStandard mealTimeStandard = mealTimeStandardRepository.findById(mealTimeStandardId).get();
        List<MealAgeStandard> mealAgeStandardList = mealTimeStandard.getMealAgeStandards();
        mealAgeStandardList.add(save);
        mealTimeStandard.setMealAgeStandards(mealAgeStandardList);
        mealTimeStandardRepository.save(mealTimeStandard);


        return new StateMessage("Qo`shildi", true);
    }

    public StateMessage delete(Integer id) {

        MultiMenu multiMenu = getMultiMenu(id);
        MealTimeStandard mealTimeStandard = getMealTimeStandard(id);


        MealAgeStandard mealAgeStandard = mealAgeStandardRepository.findById(id).get();
        if (mealTimeStandard != null)
            mealTimeStandard.getMealAgeStandards().remove(mealAgeStandard);

        if (multiMenu != null)
            multiMenu.getMealAgeStandardList().remove(mealAgeStandard);


        mealAgeStandardRepository.delete(mealAgeStandard);
        return new StateMessage("O`chirildi", true);
    }

    public StateMessage edit(MealAgeStandardDTO mealAgeStandardDTO, Integer id) {

        MealAgeStandard mealAgeStandard = mealAgeStandardRepository.findById(id).get();

        Meal meal = mealRepository.findById(mealAgeStandardDTO.getMealId()).get();
        mealAgeStandard.setMeal(meal);

        for (AgeStandardDTO ageStandardDTO : mealAgeStandardDTO.getAgeStandardDTOList()) {
            ageStandardService.edit(ageStandardDTO, meal);
        }

        MealAgeStandard save = mealAgeStandardRepository.save(mealAgeStandard);


        return new StateMessage("O`zgartirildi", true);
    }

    public MealAgeStandardResponseDTO parse(MealAgeStandard mealAgeStandard) {

        MealAgeStandardResponseDTO dto = new MealAgeStandardResponseDTO();
        dto.setId(mealAgeStandard.getId());
        dto.setName(mealAgeStandard.getMeal().getName());
        dto.setMealId(mealAgeStandard.getMeal().getId());

        List<AgeStandardResponseDTO> list = new ArrayList<>();

        for (AgeStandard ageStandard : mealAgeStandard.getAgeStandards()) {
            list.add(ageStandardService.parse(ageStandard));
        }
        list.sort(Comparator.comparing(AgeStandardResponseDTO::getName));
        dto.setAgeStandardResponseDTOList(list);
        return dto;
    }

    public MultiMenu getMultiMenu(Integer mealAgeStandardId) {

        for (MultiMenu multiMenu : multiMenuRepository.findAll()) {
            for (MealAgeStandard mealAgeStandard : multiMenu.getMealAgeStandardList()) {
                if (Objects.equals(mealAgeStandard.getId(), mealAgeStandardId)) {
                    return multiMenu;
                }
            }
        }
        return null;
    }

    public MealTimeStandard getMealTimeStandard(Integer mealAgeStandardId) {
        for (MealTimeStandard mealTimeStandard : mealTimeStandardRepository.findAll()) {
            for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandards()) {
                if (mealAgeStandard.getId().equals(mealAgeStandardId)) {
                    return mealTimeStandard;
                }
            }
        }
        return null;
    }
}
