package com.smart_solution.outsource.menu.ageStandard;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

public class AgeStandardResponseDTO {

    private Integer id;
    private BigDecimal weight;
    private String name;
    private Integer ageGroupId;

    public AgeStandardResponseDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }
}
