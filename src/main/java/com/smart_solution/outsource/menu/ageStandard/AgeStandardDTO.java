package com.smart_solution.outsource.menu.ageStandard;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


public class AgeStandardDTO {
    private Integer id;
    private BigDecimal weight;
    private Integer ageGroupId;

    public AgeStandardDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }
}
