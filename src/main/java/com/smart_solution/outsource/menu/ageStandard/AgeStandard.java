package com.smart_solution.outsource.menu.ageStandard;


import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AgeStandard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @OneToOne(cascade = { CascadeType.ALL })
    private Ingredient ingredient;

    @ManyToOne
    private AgeGroup ageGroup;

    public AgeStandard() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }
}
