package com.smart_solution.outsource.menu.ageStandard;

import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.ingredient.IngredientService;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public record AgeStandardService(
        AgeStandardRepository ageStandardRepository,
        IngredientService ingredientService,
        AgeGroupRepository ageGroupRepository) {


    public AgeStandard add(AgeStandardDTO ageStandardDTO, Meal meal) {
        ageStandardDTO.setWeight(ageStandardDTO.getWeight().divide(BigDecimal.valueOf(1000),4,RoundingMode.HALF_UP));


        AgeStandard ageStandard = new AgeStandard();

        ageStandard.setWeight(ageStandardDTO.getWeight());

        Ingredient mealIngredient = meal.getIngredient();

        Ingredient ingredient = ingredientService.add((
                        mealIngredient.getProtein().divide(meal.getWeight(),8, RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()),
                (mealIngredient.getKcal().divide(meal.getWeight(),8,RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()),
                (mealIngredient.getOil().divide(meal.getWeight(),8,RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()),
                (mealIngredient.getCarbohydrates().divide(meal.getWeight(),8,RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()));

        ageStandard.setIngredient(ingredient);

        AgeGroup ageGroup = ageGroupRepository.findById(ageStandardDTO.getAgeGroupId()).get();

        ageStandard.setAgeGroup(ageGroup);

        return ageStandardRepository.save(ageStandard);
    }

    public AgeStandard edit(AgeStandardDTO ageStandardDTO, Meal meal) {
        ageStandardDTO.setWeight(ageStandardDTO.getWeight().divide(BigDecimal.valueOf(1000),4,RoundingMode.HALF_UP));


        AgeStandard ageStandard = ageStandardRepository.findById(ageStandardDTO.getId()).get();

        ageStandard.setWeight(ageStandardDTO.getWeight());

        Ingredient mealIngredient = meal.getIngredient();

        ingredientService.edit(ageStandard.getIngredient(),
                (mealIngredient.getProtein().divide(meal.getWeight(),4, RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()),
                (mealIngredient.getKcal().divide(meal.getWeight(),4,RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()),
                (mealIngredient.getOil().divide(meal.getWeight(),4,RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()),
                (mealIngredient.getCarbohydrates().divide(meal.getWeight(),4,RoundingMode.HALF_UP)).multiply(ageStandardDTO.getWeight()));


        return ageStandardRepository.save(ageStandard);
    }

    public AgeStandardResponseDTO parse(AgeStandard ageStandard) {

        AgeStandardResponseDTO dto = new AgeStandardResponseDTO();
        dto.setId(ageStandard.getId());
        dto.setName(ageStandard.getAgeGroup().getName());
        dto.setWeight(ageStandard.getWeight().multiply(BigDecimal.valueOf(1000)));
        dto.setAgeGroupId(ageStandard.getAgeGroup().getId());
        return dto;
    }
}
