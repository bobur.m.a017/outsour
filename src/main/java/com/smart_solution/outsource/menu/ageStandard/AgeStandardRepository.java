package com.smart_solution.outsource.menu.ageStandard;

import org.springframework.data.jpa.repository.JpaRepository;


public interface AgeStandardRepository extends JpaRepository<AgeStandard, Integer> {

}
