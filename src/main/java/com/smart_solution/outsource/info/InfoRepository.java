package com.smart_solution.outsource.info;

import com.smart_solution.outsource.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InfoRepository extends JpaRepository<Info, Integer> {
}
