package com.smart_solution.outsource.info;

import org.springframework.stereotype.Service;

@Service
public class InfoService {

    private final InfoRepository infoRepository;

    public InfoService(InfoRepository infoRepository) {
        this.infoRepository = infoRepository;
    }


    public Info add(String accountNumber, String mfo, String inn, String phoneNumber) {

        return infoRepository.save(new Info(accountNumber, mfo, inn, phoneNumber));
    }

    public Info edit(Info info, String accountNumber, String mfo, String inn, String phoneNumber) {
        info.setAccountNumber(accountNumber);
        info.setInn(inn);
        info.setMfo(mfo);
        info.setPhoneNumber(phoneNumber);
        return infoRepository.save(info);
    }
}
