package com.smart_solution.outsource.product;

import com.smart_solution.outsource.reducer.ReducerDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

public class ProductDTO {

    private Integer id;
    private String name;
    private String measurementType;
    private Double protein;
    private Double kcal;
    private Double oil;
    private Double carbohydrates;
    private Integer productCategoryId;
    private Integer sanpinCategoryId;
    private Double rounding;
    private Double pack;


    public Double getPack() {
        return pack;
    }

    public void setPack(Double pack) {
        this.pack = pack;
    }

    public ProductDTO() {
    }


    public Double getRounding() {
        return rounding;
    }

    public void setRounding(Double rounding) {
        this.rounding = rounding;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public Double getProtein() {
        return protein;
    }

    public void setProtein(Double protein) {
        this.protein = protein;
    }

    public Double getKcal() {
        return kcal;
    }

    public void setKcal(Double kcal) {
        this.kcal = kcal;
    }

    public Double getOil() {
        return oil;
    }

    public void setOil(Double oil) {
        this.oil = oil;
    }

    public Double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(Double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public Integer getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Integer productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Integer getSanpinCategoryId() {
        return sanpinCategoryId;
    }

    public void setSanpinCategoryId(Integer sanpinCategoryId) {
        this.sanpinCategoryId = sanpinCategoryId;
    }

//    public List<ReducerDTO> getReducerDTOList() {
//        return reducerDTOList;
//    }
//
//    public void setReducerDTOList(List<ReducerDTO> reducerDTOList) {
//        this.reducerDTOList = reducerDTOList;
//    }
}
