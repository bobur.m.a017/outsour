package com.smart_solution.outsource.product;


import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.ingredient.IngredientService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.productSuppiler.ProductSupplierRepository;
import com.smart_solution.outsource.product_category.ProductCategoryRepository;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategoryRepository;
import com.smart_solution.outsource.productSuppiler.ProductSupplier;
import com.smart_solution.outsource.reducer.Reducer;
import com.smart_solution.outsource.reducer.ReducerDTO;
import com.smart_solution.outsource.reducer.ReducerRepository;
import com.smart_solution.outsource.reducer.ReducerService;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class ProductService implements ProductParseDTO {
    private final ProductRepository productRepository;
    private final IngredientService ingredientService;
    private final SanpinCategoryRepository sanpinCategoryRepository;
    private final ProductCategoryRepository productCategoryRepository;
    private final CompanyRepository companyRepository;
    private final ProductSupplierRepository productSupplierRepository;

    public ProductService(ProductRepository productRepository, IngredientService ingredientService, SanpinCategoryRepository sanpinCategoryRepository, ProductCategoryRepository productCategoryRepository, CompanyRepository companyRepository, ProductSupplierRepository productSupplierRepository) {
        this.productRepository = productRepository;
        this.ingredientService = ingredientService;
        this.sanpinCategoryRepository = sanpinCategoryRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.companyRepository = companyRepository;
        this.productSupplierRepository = productSupplierRepository;
    }


    public StateMessage add(ProductDTO productDTO) {

//        if (!checkReducer(productDTO.getReducerDTOList())) {
//            return new StateMessage("Chiqit vaqtlari to`liq kiritilishi lozim", false);
//        }

        Optional<Product> byName = productRepository.findByName(productDTO.getName());
        Product product;


        if (byName.isPresent()) {
            if (byName.get().getDelete()) {
                product = byName.get();
            } else {
                return new StateMessage("Bunday nomli maxsulot avval qo`shilgan", false);
            }
        } else {
            product = new Product();
        }
        product.setName(productDTO.getName());

        product.setRounding(BigDecimal.valueOf(productDTO.getRounding()).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP));

        product.setDelete(false);

        product.setMeasurementType(productDTO.getMeasurementType());

        product.setSanpinCategory(sanpinCategoryRepository.findById(productDTO.getSanpinCategoryId()).get());

        product.setProductCategory(productCategoryRepository.findById(productDTO.getProductCategoryId()).get());

        BigDecimal protain = BigDecimal.valueOf(productDTO.getProtein()).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
        BigDecimal kkal = BigDecimal.valueOf(productDTO.getKcal()).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
        BigDecimal oil = BigDecimal.valueOf(productDTO.getOil()).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
        BigDecimal uglevod = BigDecimal.valueOf(productDTO.getCarbohydrates()).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);


        Ingredient ingredient = ingredientService.add(protain, kkal, oil, uglevod);
        product.setIngredient(ingredient);

//         List<Reducer> reducerList = new ArrayList<>();

//         for (ReducerDTO reducerDTO : productDTO.getReducerDTOList()) {
//             reducerList.add(reducerService.addReducer(reducerDTO));
//         }

//         product.setReducer(reducerList);

        product.setPack(BigDecimal.valueOf(productDTO.getPack()).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP));

        productRepository.save(product);

        return new StateMessage("Muvaffaqiyatli qo`shildi", true);
    }

    public StateMessage edit(ProductDTO productDTO, Integer id) {

        Product product = productRepository.findById(id).get();

        boolean res = false;

        if (!product.getName().equals(productDTO.getName())) {
            res = productRepository.existsByName(productDTO.getName());
        }

        if (!res) {

            product.setName(productDTO.getName());
            product.setMeasurementType(productDTO.getMeasurementType());

            product.setRounding(BigDecimal.valueOf(productDTO.getRounding()));
            product.setPack(BigDecimal.valueOf(productDTO.getPack()));

            product.setSanpinCategory(sanpinCategoryRepository.findById(productDTO.getSanpinCategoryId()).get());

            product.setProductCategory(productCategoryRepository.findById(productDTO.getProductCategoryId()).get());

            Ingredient ingredient = ingredientService.edit(product.getIngredient(), BigDecimal.valueOf(productDTO.getProtein()), BigDecimal.valueOf(productDTO.getKcal()), BigDecimal.valueOf(productDTO.getOil()), BigDecimal.valueOf(productDTO.getCarbohydrates()));

            product.setIngredient(ingredient);

            Product save = productRepository.save(product);


            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);
        } else {
            return new StateMessage("Bunday nomli maxsulot avval qo`shilgan", false);
        }
    }

//    public List<Product> getByCompany(ResponseUser responseUser) {
//
//        List<Product> productList = productRepository.findAll();
//
//        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();
//
//
//        for (ProductSupplier productSupplier : company.getProductSupplier()) {
//            for (Product product : productSupplier.getProduct()) {
//                productList.remove(product);
//            }
//        }
//        return productList;
//    }

    public List<ProductResponseDTO> get() {

        List<ProductResponseDTO> list = new ArrayList<>();

        for (Product product : productRepository.findAll()) {

            if (!product.getDelete()) {

                ProductResponseDTO parse = parse(product);
                list.add(parse);
            }
        }
        return list;
    }

    public List<ProductResponseDTO> getSpecial() {

        List<ProductResponseDTO> list = new ArrayList<>();

        for (Product product : productRepository.findAll()) {

            if (!product.getDelete()) {

                ProductResponseDTO parse = parseSpecial(product);
                list.add(parse);
            }
        }
        return list;
    }

//    public boolean checkReducer(List<ReducerDTO> reducerDTOList) {
//
//        for (ReducerDTO dto : reducerDTOList) {
//
//            if (dto.getStartDate() == null || dto.getEndDate() == null) {
//                return false;
//            }
//
//        }
//
//        return true;
//    }

    public StateMessage delete(Integer id) {
        Product product = productRepository.findById(id).get();
        product.setDelete(true);
        productRepository.save(product);
        return new StateMessage("O`chirildi, tiklash uchun admin bilan bog`laning", true);
    }

    public void addCompany(Product product) {

        for (Company company : companyRepository.findAll()) {
            ProductSupplier productSupplier = new ProductSupplier();
            productSupplier.setProduct(product);

            List<ProductSupplier> list = company.getProductSupplier();
            list.add(productSupplierRepository.save(productSupplier));
            company.setProductSupplier(list);
            companyRepository.save(company);
        }
    }

    public List<TestDTO> getTest() {
        List<TestDTO> list = new ArrayList<>();
        for (Product product : productRepository.findAll()) {
            TestDTO dto = new TestDTO();
            dto.setCarbohydrates(product.getIngredient().getCarbohydrates());
            dto.setKcal(product.getIngredient().getKcal());
            dto.setOil(product.getIngredient().getOil());
            dto.setProtein(product.getIngredient().getProtein());
            dto.setName(product.getName());
            dto.setProductCategoryId(product.getProductCategory().getId());
            dto.setSanpinCategoryId(product.getSanpinCategory().getId());
            dto.setRounding(product.getRounding());
            dto.setPack(product.getPack());
            dto.setMeasurementType("kg");
            list.add(dto);
        }
        return list;
    }

//
//    public List<ProductDTOOO> getProduct() {
//
//        List<ProductDTOOO> list = new ArrayList<>();
//
//        for (Product product : productRepository.findAll()) {
//            list.add(new ProductDTOOO(Double.valueOf(product.getIngredient().getCarbohydrates().toString()), Double.valueOf(product.getIngredient().getKcal().toString()), product.getName(), "KG", product.getIngredient().getOil().doubleValue(), product.getIngredient().getProtein().doubleValue(), getPCID(product.getProductCategory().getId()), getSCID(product.getSanpinCategory().getId())));
//        }
//        return list;
//    }
//
//
//    public Integer getPCID(Integer a) {
//        if (a == 14) {
//            return 11;
//        }
//        if (a == 3) {
//            return 2;
//        }
//        if (a == 6) {
//            return 5;
//        }
//        if (a == 7) {
//            return 6;
//        }
//        if (a == 8) {
//            return 10;
//        }
//        if (a == 13) {
//            return 7;
//        }
//        if (a == 2) {
//            return 1;
//        }
//        if (a == 5) {
//            return 4;
//        }
//        if (a == 9) {
//            return 8;
//        }
//        if (a == 15) {
//            return 12;
//        }
//        if (a == 10) {
//            return 9;
//        }
//        if (a == 16) {
//            return 13;
//        }
//        if (a == 17) {
//            return 14;
//        }
//        if (a == 4) {
//            return 3;
//        }
//        return null;
//    }
//
//    public Integer getSCID(Integer a) {
//        if (a == 2) {
//            return 1;
//        }
//        if (a == 3) {
//            return 2;
//        }
//        if (a == 24) {
//            return 23;
//        }
//        if (a == 7) {
//            return 6;
//        }
//        if (a == 9) {
//            return 8;
//        }
//        if (a == 11) {
//            return 10;
//        }
//        if (a == 13) {
//            return 12;
//        }
//        if (a == 14) {
//            return 21;
//        }
//        if (a == 22) {
//            return 13;
//        }
//        if (a == 15) {
//            return 14;
//        }
//        if (a == 8) {
//            return 7;
//        }
//        if (a == 10) {
//            return 9;
//        }
//        if (a == 12) {
//            return 11;
//        }
//        if (a == 16) {
//            return 15;
//        }
//        if (a == 17) {
//            return 16;
//        }
//        if (a == 25) {
//            return 24;
//        }
//        if (a == 18) {
//            return 17;
//        }
//        if (a == 19) {
//            return 18;
//        }
//        if (a == 21) {
//            return 20;
//        }
//        if (a == 20) {
//            return 19;
//        }
//        if (a == 4) {
//            return 3;
//        }
//        if (a == 23) {
//            return 22;
//        }
//        if (a == 6) {
//            return 5;
//        }
//        if (a == 5) {
//            return 4;
//        }
//        return null;
//    }
}
