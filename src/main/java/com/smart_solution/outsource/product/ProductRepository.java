package com.smart_solution.outsource.product;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface ProductRepository extends JpaRepository<Product, Integer> {

    Optional<Product> findByName(String name);

    boolean existsByName(String name);

    boolean existsByProductCategory_Id(Integer id);

    boolean existsBySanpinCategory_Id(Integer id);
}
