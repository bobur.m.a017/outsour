package com.smart_solution.outsource.product;


import java.math.BigDecimal;

public interface ProductParseDTO {

    default ProductResponseDTO parse(Product product) {

        ProductResponseDTO dto = new ProductResponseDTO();


        dto.setId(product.getId());
        dto.setName(product.getName());
        dto.setMeasurementType(product.getMeasurementType());
        dto.setCarbohydrates(product.getIngredient().getCarbohydrates().multiply(BigDecimal.valueOf(1000)));
        dto.setOil(product.getIngredient().getOil().multiply(BigDecimal.valueOf(1000)));
        dto.setKcal(product.getIngredient().getKcal().multiply(BigDecimal.valueOf(1000)));
        dto.setProtein(product.getIngredient().getProtein().multiply(BigDecimal.valueOf(1000)));
        dto.setProductCategoryId(product.getProductCategory().getId());
        dto.setProductCategoryName(product.getProductCategory().getName());
        dto.setSanpinCategoryId(product.getSanpinCategory().getId());
        dto.setSanpinCategoryName(product.getSanpinCategory().getName());
        dto.setRounding(product.getRounding().multiply(BigDecimal.valueOf(1000)));
        dto.setPack(product.getPack().multiply(BigDecimal.valueOf(1000)));
        return dto;
    }

    default ProductResponseDTO parseSpecial(Product product) {

        ProductResponseDTO dto = new ProductResponseDTO();


        dto.setId(product.getId());
        dto.setName(product.getName());
        dto.setMeasurementType(product.getMeasurementType());
        dto.setCarbohydrates(product.getIngredient().getCarbohydrates().multiply(BigDecimal.valueOf(1000)));
        dto.setOil(product.getIngredient().getOil().multiply(BigDecimal.valueOf(1000)));
        dto.setKcal(product.getIngredient().getKcal().multiply(BigDecimal.valueOf(1000)));
        dto.setProtein(product.getIngredient().getProtein().multiply(BigDecimal.valueOf(1000)));
        dto.setProductCategoryId(product.getProductCategory().getId());
        dto.setProductCategoryName(product.getProductCategory().getName());
        dto.setSanpinCategoryId(product.getSanpinCategory().getId());
        dto.setSanpinCategoryName(product.getSanpinCategory().getName());
        dto.setRounding(product.getRounding());
        dto.setPack(product.getPack());
        return dto;
    }
}
