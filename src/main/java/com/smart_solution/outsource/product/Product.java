package com.smart_solution.outsource.product;

import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.price.Price;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.product_category.ProductCategory;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String measurementType;
    private Boolean delete;
    private BigDecimal rounding;
    @Column(precision = 19,scale = 6)
    private BigDecimal pack;


    @OneToMany(mappedBy = "product" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShipped> productsToBeShippedList;

    @OneToMany(mappedBy = "product" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProducts> shippedProductsList;

    @OneToMany(mappedBy = "product" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShippedCompany> productsToBeShippedCompanyList;

    @OneToMany(mappedBy = "product" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProductsCompany> shippedProductsCompanyList;


    @OneToMany(mappedBy = "product" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutProduct> inOutProductList;


    @OneToMany(mappedBy = "product" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Price> priceList;

    @OneToMany(mappedBy = "product" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductBalancer> productBalancerList;


    @OneToMany(mappedBy = "product" , cascade = CascadeType.ALL)
    private List<WeightProduct> weightProductList;


    @OneToOne
    private Ingredient ingredient;
    @ManyToOne
    private ProductCategory productCategory;
    @ManyToOne
    private SanpinCategory sanpinCategory;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    public BigDecimal getRounding() {
        return rounding;
    }

    public void setRounding(BigDecimal rounding) {
        this.rounding = rounding;
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public List<ProductsToBeShipped> getProductsToBeShippedList() {
        return productsToBeShippedList;
    }

    public void setProductsToBeShippedList(List<ProductsToBeShipped> productsToBeShippedList) {
        this.productsToBeShippedList = productsToBeShippedList;
    }

    public List<ShippedProducts> getShippedProductsList() {
        return shippedProductsList;
    }

    public void setShippedProductsList(List<ShippedProducts> shippedProductsList) {
        this.shippedProductsList = shippedProductsList;
    }

    public List<ProductsToBeShippedCompany> getProductsToBeShippedCompanyList() {
        return productsToBeShippedCompanyList;
    }

    public void setProductsToBeShippedCompanyList(List<ProductsToBeShippedCompany> productsToBeShippedCompanyList) {
        this.productsToBeShippedCompanyList = productsToBeShippedCompanyList;
    }

    public List<ShippedProductsCompany> getShippedProductsCompanyList() {
        return shippedProductsCompanyList;
    }

    public void setShippedProductsCompanyList(List<ShippedProductsCompany> shippedProductsCompanyList) {
        this.shippedProductsCompanyList = shippedProductsCompanyList;
    }

    public List<InOutProduct> getInOutProductList() {
        return inOutProductList;
    }

    public void setInOutProductList(List<InOutProduct> inOutProductList) {
        this.inOutProductList = inOutProductList;
    }

    public List<Price> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<Price> priceList) {
        this.priceList = priceList;
    }

    public List<ProductBalancer> getProductBalancerList() {
        return productBalancerList;
    }

    public void setProductBalancerList(List<ProductBalancer> productBalancerList) {
        this.productBalancerList = productBalancerList;
    }

    public List<WeightProduct> getWeightProductList() {
        return weightProductList;
    }

    public void setWeightProductList(List<WeightProduct> weightProductList) {
        this.weightProductList = weightProductList;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public SanpinCategory getSanpinCategory() {
        return sanpinCategory;
    }

    public void setSanpinCategory(SanpinCategory sanpinCategory) {
        this.sanpinCategory = sanpinCategory;
    }
}
