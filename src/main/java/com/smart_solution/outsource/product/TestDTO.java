package com.smart_solution.outsource.product;

import java.math.BigDecimal;

public class TestDTO {

    private BigDecimal carbohydrates;
    private BigDecimal kcal;
    private BigDecimal oil;
    private BigDecimal protein;
    private BigDecimal pack;
    private BigDecimal rounding;
    private Integer productCategoryId;
    private Integer sanpinCategoryId;
    private String measurementType;
    private String name;


    public BigDecimal getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(BigDecimal carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public BigDecimal getKcal() {
        return kcal;
    }

    public void setKcal(BigDecimal kcal) {
        this.kcal = kcal;
    }

    public BigDecimal getOil() {
        return oil;
    }

    public void setOil(BigDecimal oil) {
        this.oil = oil;
    }

    public BigDecimal getProtein() {
        return protein;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public BigDecimal getRounding() {
        return rounding;
    }

    public void setRounding(BigDecimal rounding) {
        this.rounding = rounding;
    }

    public Integer getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Integer productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Integer getSanpinCategoryId() {
        return sanpinCategoryId;
    }

    public void setSanpinCategoryId(Integer sanpinCategoryId) {
        this.sanpinCategoryId = sanpinCategoryId;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
