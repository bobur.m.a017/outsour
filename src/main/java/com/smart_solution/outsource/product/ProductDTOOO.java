//package com.smart_solution.outsource.product;
//
//public class ProductDTOOO {
//
//    private Double carbohydrates;
//    private Double kcal;
//    private String name;
//    private String measurementType;
//    private Double oil;
//    private Double protein;
//    private Integer productCategoryId;
//    private Integer sanpinCategoryId;
//
//    public ProductDTOOO() {
//    }
//
//    public ProductDTOOO(Double carbohydrates, Double kcal, String name, String measurementType, Double oil, Double protein, Integer productCategoryId, Integer sanpinCategoryId) {
//        this.carbohydrates = carbohydrates;
//        this.kcal = kcal;
//        this.name = name;
//        this.measurementType = measurementType;
//        this.oil = oil;
//        this.protein = protein;
//        this.productCategoryId = productCategoryId;
//        this.sanpinCategoryId = sanpinCategoryId;
//    }
//
//    public Double getCarbohydrates() {
//        return carbohydrates;
//    }
//
//    public void setCarbohydrates(Double carbohydrates) {
//        this.carbohydrates = carbohydrates;
//    }
//
//    public Double getKcal() {
//        return kcal;
//    }
//
//    public void setKcal(Double kcal) {
//        this.kcal = kcal;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getMeasurementType() {
//        return measurementType;
//    }
//
//    public void setMeasurementType(String measurementType) {
//        this.measurementType = measurementType;
//    }
//
//    public Double getOil() {
//        return oil;
//    }
//
//    public void setOil(Double oil) {
//        this.oil = oil;
//    }
//
//    public Double getProtein() {
//        return protein;
//    }
//
//    public void setProtein(Double protein) {
//        this.protein = protein;
//    }
//
//    public Integer getProductCategoryId() {
//        return productCategoryId;
//    }
//
//    public void setProductCategoryId(Integer productCategoryId) {
//        this.productCategoryId = productCategoryId;
//    }
//
//    public Integer getSanpinCategoryId() {
//        return sanpinCategoryId;
//    }
//
//    public void setSanpinCategoryId(Integer sanpinCategoryId) {
//        this.sanpinCategoryId = sanpinCategoryId;
//    }
//}
