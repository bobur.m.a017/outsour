package com.smart_solution.outsource.product;

import com.smart_solution.outsource.reducer.ReducerDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

public class ProductResponseDTO {

    private Integer id;
    private String name;
    private String measurementType;
    private BigDecimal protein;
    private BigDecimal kcal;
    private BigDecimal oil;
    private BigDecimal carbohydrates;
    private Integer productCategoryId;
    private String productCategoryName;
    private Integer sanpinCategoryId;
    private String sanpinCategoryName;
    private List<ReducerDTO> reducerDTOList;
    private BigDecimal rounding;
    private BigDecimal pack;

    public ProductResponseDTO() {
    }


    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public BigDecimal getRounding() {
        return rounding;
    }

    public void setRounding(BigDecimal rounding) {
        this.rounding = rounding;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public BigDecimal getProtein() {
        return protein;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public BigDecimal getKcal() {
        return kcal;
    }

    public void setKcal(BigDecimal kcal) {
        this.kcal = kcal;
    }

    public BigDecimal getOil() {
        return oil;
    }

    public void setOil(BigDecimal oil) {
        this.oil = oil;
    }

    public BigDecimal getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(BigDecimal carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public Integer getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Integer productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public Integer getSanpinCategoryId() {
        return sanpinCategoryId;
    }

    public void setSanpinCategoryId(Integer sanpinCategoryId) {
        this.sanpinCategoryId = sanpinCategoryId;
    }

    public String getSanpinCategoryName() {
        return sanpinCategoryName;
    }

    public void setSanpinCategoryName(String sanpinCategoryName) {
        this.sanpinCategoryName = sanpinCategoryName;
    }

    public List<ReducerDTO> getReducerDTOList() {
        return reducerDTOList;
    }

    public void setReducerDTOList(List<ReducerDTO> reducerDTOList) {
        this.reducerDTOList = reducerDTOList;
    }
}
