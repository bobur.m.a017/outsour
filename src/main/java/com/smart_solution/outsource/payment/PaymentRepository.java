package com.smart_solution.outsource.payment;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment,Integer> {

    List<Payment> findAllByCompanyAndSupplier(Company company, Supplier supplier);
    List<Payment> findAllByCompany(Company company);
}
