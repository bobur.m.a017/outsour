package com.smart_solution.outsource.payment;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.users.Users;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


public class PaymentDTO {

    private String number;
    private Double totalSum;
    private Timestamp timeOfPayment;
    private String comment;

    private Integer supplierId;


    public PaymentDTO() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Double totalSum) {
        this.totalSum = totalSum;
    }

    public Timestamp getTimeOfPayment() {
        return timeOfPayment;
    }

    public void setTimeOfPayment(Timestamp timeOfPayment) {
        this.timeOfPayment = timeOfPayment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }
}
