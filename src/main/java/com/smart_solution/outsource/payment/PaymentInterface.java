package com.smart_solution.outsource.payment;

public interface PaymentInterface {

    default public PaymentResponseDTO parse(Payment payment){
        return new PaymentResponseDTO(
                payment.getId(),
                payment.getNumber(),
                payment.getTotalSum(),
                payment.getTimeOfPayment(),
                payment.getComment(),
                payment.getCreateDate(),
                payment.getUpdateDate(),
                payment.getAddUser().getName()+" "+payment.getAddUser().getSurname()+" "+payment.getAddUser().getFatherName(),
                payment.getEditUser() != null ? payment.getEditUser().getName()+" "+payment.getEditUser().getSurname()+" "+payment.getEditUser().getFatherName() : "O`zgartirilmadi",
                payment.getCompany().getName(),
                payment.getSupplier().getName()
        );
    }
}
