package com.smart_solution.outsource.payment;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.users.Users;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String number;

    @Column(precision = 19, scale = 6)
    private BigDecimal totalSum;

    private Timestamp timeOfPayment;

    @Column(columnDefinition = "text")
    private String comment;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private Users addUser;
    @ManyToOne
    private Users editUser;

    @ManyToOne
    private Company company;

    @ManyToOne
    private Supplier supplier;


    public Payment() {
    }

    public Users getEditUser() {
        return editUser;
    }

    public void setEditUser(Users editUser) {
        this.editUser = editUser;
    }

    public Payment(String number, BigDecimal totalSum, Timestamp timeOfPayment, String comment, Users addUser, Company company, Supplier supplier) {
        this.number = number;
        this.totalSum = totalSum;
        this.timeOfPayment = timeOfPayment;
        this.comment = comment;
        this.addUser = addUser;
        this.company = company;
        this.supplier = supplier;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public Timestamp getTimeOfPayment() {
        return timeOfPayment;
    }

    public void setTimeOfPayment(Timestamp timeOfPayment) {
        this.timeOfPayment = timeOfPayment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public Users getAddUser() {
        return addUser;
    }

    public void setAddUser(Users users) {
        this.addUser = users;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
}
