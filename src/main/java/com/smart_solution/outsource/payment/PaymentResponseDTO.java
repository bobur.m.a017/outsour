package com.smart_solution.outsource.payment;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.users.Users;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


public class PaymentResponseDTO {

    private Integer id;
    private String number;
    private BigDecimal totalSum;
    private Timestamp timeOfPayment;
    private String comment;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String addUser;
    private String editUser;
    private String company;
    private String supplier;

    public PaymentResponseDTO(Integer id, String number, BigDecimal totalSum, Timestamp timeOfPayment, String comment, Timestamp createDate, Timestamp updateDate, String addUser, String editUser, String company, String supplier) {
        this.id = id;
        this.number = number;
        this.totalSum = totalSum;
        this.timeOfPayment = timeOfPayment;
        this.comment = comment;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.addUser = addUser;
        this.editUser = editUser;
        this.company = company;
        this.supplier = supplier;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public Timestamp getTimeOfPayment() {
        return timeOfPayment;
    }

    public void setTimeOfPayment(Timestamp timeOfPayment) {
        this.timeOfPayment = timeOfPayment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getAddUser() {
        return addUser;
    }

    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }

    public String getEditUser() {
        return editUser;
    }

    public void setEditUser(String editUser) {
        this.editUser = editUser;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }
}
