package com.smart_solution.outsource.payment;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.supplier.SupplierRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentService implements PaymentInterface {


    private final PaymentRepository paymentRepository;
    private final UsersRepository usersRepository;
    private final SupplierRepository supplierRepository;
    private final CompanyRepository companyRepository;

    public PaymentService(PaymentRepository paymentRepository, UsersRepository usersRepository, SupplierRepository supplierRepository, CompanyRepository companyRepository) {
        this.paymentRepository = paymentRepository;
        this.usersRepository = usersRepository;
        this.supplierRepository = supplierRepository;
        this.companyRepository = companyRepository;
    }


    public StateMessage add(PaymentDTO dto, ResponseUser responseUser) {
        if (dto.getNumber() == null || dto.getSupplierId() == null || dto.getTimeOfPayment() == null || dto.getTotalSum() == null) {

            return new StateMessage("To`lov kiritilmadi. Barcha kataklar to`ldirilishi shart.", false);

        } else {
            Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
            Company company = users.getCompany();
            Supplier supplier = supplierRepository.findById(dto.getSupplierId()).get();

            Payment payment = paymentRepository.save(new Payment(
                    dto.getNumber(), BigDecimal.valueOf(dto.getTotalSum()), dto.getTimeOfPayment(), dto.getComment(), users, company, supplier
            ));

            List<Payment> paymentList = company.getPaymentList();
            paymentList.add(payment);
            company.setPaymentList(paymentList);
            companyRepository.save(company);


            List<Payment> paymentList1 = supplier.getPaymentList();
            paymentList1.add(payment);
            supplier.setPaymentList(paymentList1);
            supplierRepository.save(supplier);

            return new StateMessage("To`lov muvaffaqiyatli kiritildi", true);
        }
    }

    public StateMessage edit(PaymentDTO dto, Integer id, ResponseUser responseUser) {
        if (dto.getNumber() == null  || dto.getTotalSum() == null) {

            return new StateMessage("To`lov o`zgartirilmadi. Barcha kataklar to`ldirilishi shart.", false);

        } else {
            Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
            Payment payment = paymentRepository.findById(id).get();

            payment.setComment(dto.getComment());
            payment.setTimeOfPayment(dto.getTimeOfPayment());
            payment.setNumber(dto.getNumber());
            payment.setTotalSum(BigDecimal.valueOf(dto.getTotalSum()));
            payment.setEditUser(users);
            paymentRepository.save(payment);

            return new StateMessage("To`lov muvaffaqiyatli o`zgartirildi", true);
        }
    }

    public StateMessage delete(Integer id) {

        Optional<Payment> optional = paymentRepository.findById(id);
        if (optional.isPresent()) {
            Payment payment = optional.get();
            Company company = payment.getCompany();
            List<Payment> paymentList = company.getPaymentList();
            paymentList.remove(payment);
            company.setPaymentList(paymentList);
            companyRepository.save(company);

            Supplier supplier = payment.getSupplier();
            List<Payment> paymentList1 = supplier.getPaymentList();
            paymentList1.remove(payment);
            supplier.setPaymentList(paymentList1);
            supplierRepository.save(supplier);

            paymentRepository.delete(payment);

            return new StateMessage("Muvaffaqiyatli o`chirildi", true);
        }
        return new StateMessage("O`chirish amalga oshmadi.", false);
    }

    public PaymentResponseDTO get(Integer id) {
        Payment payment = paymentRepository.findById(id).get();

        return parse(payment);
    }

    public List<PaymentResponseDTO> getAll(Integer supplierId, Long start, Long end, ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        Company company = users.getCompany();
        List<PaymentResponseDTO> list = new ArrayList<>();

        List<Payment> paymentList = new ArrayList<>();
        if (supplierId != null) {
            Supplier supplier = supplierRepository.findById(supplierId).get();

            paymentList.addAll(paymentRepository.findAllByCompanyAndSupplier(company, supplier));
        } else {
            paymentList.addAll(paymentRepository.findAllByCompany(company));
        }

        for (Payment payment : paymentList) {
            if (start != null && end != null) {
                if (start <= payment.getTimeOfPayment().getTime() && end >= payment.getTimeOfPayment().getTime()) {
                    list.add(parse(payment));
                }
            } else {
                list.add(parse(payment));
            }
        }
        return list;
    }

    public List<PaymentResponseDTO> getAverageNumberOfChildren(Long start, Long end, ResponseUser responseUser) {
        return null;
    }
}
