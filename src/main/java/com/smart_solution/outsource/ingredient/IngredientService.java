package com.smart_solution.outsource.ingredient;


import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class IngredientService {

    private final IngredientRepository ingredientRepository;

    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public Ingredient add(BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates) {

        return ingredientRepository.save(new Ingredient(protein,kcal,oil,carbohydrates));

    }

    public Ingredient addNoSave(BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates) {

        return new Ingredient(protein,kcal,oil,carbohydrates);

    }

    public Ingredient edit(Ingredient ingredient, BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates) {

        ingredient.setCarbohydrates(carbohydrates);
        ingredient.setKcal(kcal);
        ingredient.setOil(oil);
        ingredient.setProtein(protein);

        return ingredientRepository.save(ingredient);
    }

    public IngredientDTO parse(Ingredient ingredient) {
        return new IngredientDTO(
                ingredient.getProtein().multiply(BigDecimal.valueOf(1000)),
                ingredient.getKcal().multiply(BigDecimal.valueOf(1000)),
                ingredient.getOil().multiply(BigDecimal.valueOf(1000)),
                ingredient.getCarbohydrates().multiply(BigDecimal.valueOf(1000))
        );
    }


}
