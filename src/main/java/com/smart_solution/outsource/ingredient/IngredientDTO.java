package com.smart_solution.outsource.ingredient;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

public class IngredientDTO {

    private Integer id;
    private BigDecimal protein;
    private BigDecimal kcal;
    private BigDecimal oil;
    private BigDecimal carbohydrates;

    public IngredientDTO(BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates) {
        this.protein = protein;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
    }

    public IngredientDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getProtein() {
        return protein;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public BigDecimal getKcal() {
        return kcal;
    }

    public void setKcal(BigDecimal kcal) {
        this.kcal = kcal;
    }

    public BigDecimal getOil() {
        return oil;
    }

    public void setOil(BigDecimal oil) {
        this.oil = oil;
    }

    public BigDecimal getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(BigDecimal carbohydrates) {
        this.carbohydrates = carbohydrates;
    }
}
