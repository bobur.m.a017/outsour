package com.smart_solution.outsource.ingredient;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;


public interface IngredientRepository extends JpaRepository<Ingredient, Integer> {
}
