package com.smart_solution.outsource.ingredient;

import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Ingredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(precision = 19, scale = 6)
    private BigDecimal protein;

    @Column(precision = 19, scale = 6)
    private BigDecimal kcal;

    @Column(precision = 19, scale = 6)
    private BigDecimal oil;

    @Column(precision = 19, scale = 6)
    private BigDecimal carbohydrates;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(mappedBy = "ingredient", cascade = CascadeType.ALL)
    private AgeStandardSave ageStandardSave;


    public AgeStandardSave getAgeStandardSave() {
        return ageStandardSave;
    }

    public void setAgeStandardSave(AgeStandardSave ageStandardSave) {
        this.ageStandardSave = ageStandardSave;
    }

    public Ingredient() {
    }

    public Ingredient(BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates) {
        this.protein = protein;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
    }

    public Ingredient(Ingredient ingredient) {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getProtein() {
        return protein;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public BigDecimal getKcal() {
        return kcal;
    }

    public void setKcal(BigDecimal kcal) {
        this.kcal = kcal;
    }

    public BigDecimal getOil() {
        return oil;
    }

    public void setOil(BigDecimal oil) {
        this.oil = oil;
    }

    public BigDecimal getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(BigDecimal carbohydrates) {
        this.carbohydrates = carbohydrates;
    }
}
