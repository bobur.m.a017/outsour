package com.smart_solution.outsource.kindergarten;

import java.util.List;

public class ByDistrict {
    private Integer districtId;
    private String districtName;
    private List<KindergartenResponseDTO> kindergarten;

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public List<KindergartenResponseDTO> getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(List<KindergartenResponseDTO> kindergarten) {
        this.kindergarten = kindergarten;
    }
}
