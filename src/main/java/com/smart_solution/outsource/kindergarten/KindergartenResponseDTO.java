package com.smart_solution.outsource.kindergarten;


import com.smart_solution.outsource.users.UsersResponseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class KindergartenResponseDTO {

    private Integer id;
    private String name;
    private String phoneNumber;
    private String region;
    private String district;
    private String street;
    private String accountNumber;
    private String mfo;
    private String inn;
    private String type;
    private List<UsersResponseDTO> users;


    public KindergartenResponseDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMfo() {
        return mfo;
    }

    public void setMfo(String mfo) {
        this.mfo = mfo;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<UsersResponseDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UsersResponseDTO> users) {
        this.users = users;
    }
}
