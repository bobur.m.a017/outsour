package com.smart_solution.outsource.kindergarten;


public interface KindergartenParseDTO {


    default KindergartenResponseDTO parse(Kindergarten kindergarten) {

        KindergartenResponseDTO dto = new KindergartenResponseDTO();


        dto.setId(kindergarten.getId());
        dto.setName(kindergarten.getName());
        dto.setPhoneNumber(kindergarten.getInfo().getPhoneNumber());
        dto.setRegion(kindergarten.getAddress().getRegion().getName());
        dto.setDistrict(kindergarten.getAddress().getDistrict().getName());
        dto.setStreet(kindergarten.getAddress().getStreet());
        dto.setAccountNumber(kindergarten.getInfo().getAccountNumber());
        dto.setInn(kindergarten.getInfo().getInn());
        dto.setMfo(kindergarten.getInfo().getMfo());
        dto.setType(kindergarten.getType());
        return dto;
    }
}
