package com.smart_solution.outsource.kindergarten.supplier;

import com.smart_solution.outsource.dto.AgreementOrderDTO;

import java.util.List;

public class SupplierKindergartenDTO {

    private String kindergartenName;
    private String district;
    private Integer kindergartenId;
    private List<AgreementOrderDTO> agreementList;


    public SupplierKindergartenDTO() {
    }

    public SupplierKindergartenDTO(String kindergartenName, String district, Integer kindergartenId, List<AgreementOrderDTO> agreementList) {
        this.kindergartenName = kindergartenName;
        this.district = district;
        this.kindergartenId = kindergartenId;
        this.agreementList = agreementList;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public List<AgreementOrderDTO> getAgreementList() {
        return agreementList;
    }

    public void setAgreementList(List<AgreementOrderDTO> agreementList) {
        this.agreementList = agreementList;
    }
}
