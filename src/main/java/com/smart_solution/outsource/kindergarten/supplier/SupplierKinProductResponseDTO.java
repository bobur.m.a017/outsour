package com.smart_solution.outsource.kindergarten.supplier;

import java.math.BigDecimal;

public class SupplierKinProductResponseDTO {
    private Integer productId;
    private String productName;
    private BigDecimal weight;
    private BigDecimal pack;

    public SupplierKinProductResponseDTO(Integer productId, String productName, BigDecimal weight, BigDecimal pack) {
        this.productId = productId;
        this.productName = productName;
        this.weight = weight;
        this.pack = pack;
    }

    public SupplierKinProductResponseDTO() {
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
