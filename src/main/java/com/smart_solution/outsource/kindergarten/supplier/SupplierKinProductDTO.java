package com.smart_solution.outsource.kindergarten.supplier;

import java.math.BigDecimal;

public class SupplierKinProductDTO {
    private Integer productId;
    private Double weight;
    private Double inputWeight;

    public SupplierKinProductDTO(Integer productId, Double weight, Double inputWeight) {
        this.productId = productId;
        this.weight = weight;
        this.inputWeight = inputWeight;
    }

    public SupplierKinProductDTO() {
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getInputWeight() {
        return inputWeight;
    }

    public void setInputWeight(Double inputWeight) {
        this.inputWeight = inputWeight;
    }
}
