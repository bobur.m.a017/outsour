package com.smart_solution.outsource.kindergarten;

public enum KindergartenType {

    OUTSOURCE(1, "AUTSORSING"),
    TAMINOT(2, "TA`MINOT");

    private Integer id;
    private String name;

    KindergartenType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
