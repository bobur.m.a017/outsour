package com.smart_solution.outsource.kindergarten;

import com.smart_solution.outsource.address.Address;
import com.smart_solution.outsource.address.AddressService;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.info.Info;
import com.smart_solution.outsource.info.InfoService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.users.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class KindergartenService implements KindergartenParseDTO {

    private final KindergartenRepository kindergartenRepository;
    private final UsersRepository usersRepository;
    private final CompanyRepository companyRepository;
    private final AddressService addressService;
    private final InfoService infoService;
    private final UserService userService;


    public KindergartenService(KindergartenRepository kindergartenRepository, UsersRepository usersRepository, CompanyRepository companyRepository,  AddressService addressService, InfoService infoService, UserService userService) {
        this.kindergartenRepository = kindergartenRepository;
        this.usersRepository = usersRepository;
        this.companyRepository = companyRepository;
        this.addressService = addressService;
        this.infoService = infoService;
        this.userService = userService;
    }

    public StateMessage add(KindergartenDTO kindergartenDTO, ResponseUser responseUser) {
        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        boolean res = kindergartenRepository.existsByNameAndAddress_District_Id(kindergartenDTO.getName(), kindergartenDTO.getDistrictId());

        if (!res) {
            Kindergarten kindergarten = new Kindergarten();
            kindergarten.setCompanyId(company.getId());

            kindergarten.setName(kindergartenDTO.getName());

            Info info = infoService.add(kindergartenDTO.getAccountNumber(), kindergartenDTO.getMfo(), kindergartenDTO.getInn(), kindergartenDTO.getPhoneNumber());
            kindergarten.setInfo(info);

            Address address = addressService.add(kindergartenDTO.getDistrictId(), kindergartenDTO.getStreet());
            kindergarten.setAddress(address);

            kindergarten.setType(kindergartenDTO.getType() == 1 ? KindergartenType.OUTSOURCE.getName() : KindergartenType.TAMINOT.getName());
            Kindergarten saveKindergarten = kindergartenRepository.save(kindergarten);

            List<Kindergarten> list = company.getKindergarten();
            list.add(saveKindergarten);
            company.setKindergarten(list);
            companyRepository.save(company);
            return new StateMessage("Muvaffaqiyatli qo`shildi", true);

        } else {
            return new StateMessage("Bu MTT avval ro`yxatdan o`tgan va boshqa kompaniyaga biriktirilgan iltimos ma`lumotlarni tekshirib qaytadan kiriting yoki adminga murojaat qiling", false);
        }

    }

    public List<KindergartenResponseDTO> get(Company company) {

        List<KindergartenResponseDTO> list = new ArrayList<>();

        for (Kindergarten kindergarten : company.getKindergarten()) {

            KindergartenResponseDTO kindergartenResponseDTO = parse(kindergarten);
            List<UsersResponseDTO> usersResponseDTOList = userService.parse(kindergarten.getUsers());
            kindergartenResponseDTO.setUsers(usersResponseDTOList);
            list.add(kindergartenResponseDTO);
        }
        return list;
    }

    public List<KindergartenResponseDTO> get(ResponseUser responseUser) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        List<KindergartenResponseDTO> list = new ArrayList<>();

        for (Kindergarten kindergarten : company.getKindergarten()) {

            if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {
                KindergartenResponseDTO kindergartenResponseDTO = parse(kindergarten);
                list.add(kindergartenResponseDTO);
            }
        }
        return list;
    }


    public StateMessage edit(KindergartenDTO kindergartenDTO, ResponseUser responseUser, Integer id) {


        Kindergarten kindergarten = kindergartenRepository.findById(id).get();
        boolean res = true;

        if (!kindergartenDTO.getName().equals(kindergarten.getName())) {
            res = !kindergartenRepository.existsByNameAndAddress_District_Id(kindergartenDTO.getName(), kindergartenDTO.getDistrictId());
        }

        if (res) {

            kindergarten.setName(kindergartenDTO.getName());

            Info info = infoService.edit(kindergarten.getInfo(), kindergartenDTO.getAccountNumber(), kindergartenDTO.getMfo(), kindergartenDTO.getInn(), kindergartenDTO.getPhoneNumber());
            kindergarten.setInfo(info);
            kindergarten.setType(kindergartenDTO.getType() == 1 ? KindergartenType.OUTSOURCE.getName() : KindergartenType.TAMINOT.getName());
            Address address = addressService.edit(kindergarten.getAddress(), kindergartenDTO.getDistrictId(), kindergartenDTO.getStreet());
            kindergarten.setAddress(address);

            kindergartenRepository.save(kindergarten);

            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

        } else {
            return new StateMessage("Bu MTT avval ro`yxatdan o`tgan va boshqa kompaniyaga biriktirilgan iltimos ma`lumotlarni tekshirib qaytadan kiriting yoki adminga murojaat qiling", false);
        }
    }

    public StateMessage delete(Integer id, ResponseUser responseUser) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        Optional<Kindergarten> byId = kindergartenRepository.findById(id);
        if (byId.isPresent()) {

            Kindergarten kindergarten = byId.get();

            List<Kindergarten> kindergartenList = company.getKindergarten();

            kindergartenList.remove(kindergarten);

            company.setKindergarten(kindergartenList);
            companyRepository.save(company);
            return new StateMessage("MTT muvaffaqiyatli olib tashlandi.", true);
        } else {
            return new StateMessage("XATOLIK !!!.", false);
        }
    }

    public List<ByDistrict> getByAddress(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        List<ByDistrict> list = new ArrayList<>();

        Company company = users.getCompany();

        boolean res = true;

        for (Kindergarten kindergarten : company.getKindergarten()) {
            if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {
                KindergartenResponseDTO kindergartenResponseDTO = parse(kindergarten);
                res = true;
                for (ByDistrict byDistrict : list) {
                    if (byDistrict.getDistrictId().equals(kindergarten.getAddress().getDistrict().getId())) {
                        List<KindergartenResponseDTO> kindergartenList = byDistrict.getKindergarten();
                        kindergartenList.add(kindergartenResponseDTO);
                        byDistrict.setKindergarten(kindergartenList);
                        res = false;
                    }
                }
                if (res) {
                    ByDistrict byDistrict = new ByDistrict();
                    byDistrict.setDistrictId(kindergarten.getAddress().getDistrict().getId());
                    byDistrict.setDistrictName(kindergarten.getAddress().getDistrict().getName());

                    List<KindergartenResponseDTO> kindergartenList = new ArrayList<>();
                    kindergartenList.add(kindergartenResponseDTO);
                    byDistrict.setKindergarten(kindergartenList);
                    list.add(byDistrict);
                }
            }
        }
        return list;
    }

    public List<ByDistrict> getByAddressSupplier(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        List<ByDistrict> list = new ArrayList<>();

        Company company = users.getCompany();

        boolean res = true;

        for (Kindergarten kindergarten : company.getKindergarten()) {
            if (kindergarten.getType().equals(KindergartenType.TAMINOT.getName())) {
                KindergartenResponseDTO kindergartenResponseDTO = parse(kindergarten);
                res = true;
                for (ByDistrict byDistrict : list) {
                    if (byDistrict.getDistrictId().equals(kindergarten.getAddress().getDistrict().getId())) {
                        List<KindergartenResponseDTO> kindergartenList = byDistrict.getKindergarten();
                        kindergartenList.add(kindergartenResponseDTO);
                        byDistrict.setKindergarten(kindergartenList);
                        res = false;
                    }
                }
                if (res) {
                    ByDistrict byDistrict = new ByDistrict();
                    byDistrict.setDistrictId(kindergarten.getAddress().getDistrict().getId());
                    byDistrict.setDistrictName(kindergarten.getAddress().getDistrict().getName());

                    List<KindergartenResponseDTO> kindergartenList = new ArrayList<>();
                    kindergartenList.add(kindergartenResponseDTO);
                    byDistrict.setKindergarten(kindergartenList);
                    list.add(byDistrict);
                }
            }
        }
        return list;
    }

    public KindergartenResponseDTO getOne(ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        return parse(kindergartenRepository.findById(users.getKindergartenId()).get());
    }

    public KindergartenResponseDTO getOne(Integer id) {

        Kindergarten kindergarten = kindergartenRepository.findById(id).get();


        return parse(kindergarten);
    }

    public List<KindergartenResponseDTO> getType(Integer type) {

        List<Kindergarten> list = new ArrayList<>();

        if (type == 1) {
            list.addAll(kindergartenRepository.findAllByType(KindergartenType.OUTSOURCE.getName()));
        } else {
            list.addAll(kindergartenRepository.findAllByType(KindergartenType.TAMINOT.getName()));
        }

        List<KindergartenResponseDTO> dtoList = new ArrayList<>();

        for (Kindergarten kindergarten : list) {
            dtoList.add(parse(kindergarten));
        }
        return dtoList;
    }

    public List<ByDistrict> getByAddressAll(ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        List<ByDistrict> list = new ArrayList<>();

        Company company = users.getCompany();

        boolean res = true;

        for (Kindergarten kindergarten : company.getKindergarten()) {
            KindergartenResponseDTO kindergartenResponseDTO = parse(kindergarten);
            res = true;
            for (ByDistrict byDistrict : list) {
                if (byDistrict.getDistrictId().equals(kindergarten.getAddress().getDistrict().getId())) {
                    List<KindergartenResponseDTO> kindergartenList = byDistrict.getKindergarten();
                    kindergartenList.add(kindergartenResponseDTO);
                    byDistrict.setKindergarten(kindergartenList);
                    res = false;
                }
            }
            if (res) {
                ByDistrict byDistrict = new ByDistrict();
                byDistrict.setDistrictId(kindergarten.getAddress().getDistrict().getId());
                byDistrict.setDistrictName(kindergarten.getAddress().getDistrict().getName());

                List<KindergartenResponseDTO> kindergartenList = new ArrayList<>();
                kindergartenList.add(kindergartenResponseDTO);
                byDistrict.setKindergarten(kindergartenList);
                list.add(byDistrict);
            }
        }
        return list;
    }
}
