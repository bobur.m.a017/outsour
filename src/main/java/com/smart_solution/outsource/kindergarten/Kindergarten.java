package com.smart_solution.outsource.kindergarten;

import com.smart_solution.outsource.address.Address;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.info.Info;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.price.Price;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.storage.garbage.GarbageProduct;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import com.smart_solution.outsource.users.Users;

import javax.persistence.*;
import java.util.List;

@Entity
public class Kindergarten {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String type;
    private Integer companyId;


    //Ushbu mttga korxona tomonidan yuboriladigan maxsulotlar
    @OneToMany(mappedBy = "kindergarten" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductsToBeShippedCompany> productsToBeShippedCompanyList;

    //Ushbu mttga korxona tomonidan yuborilgan maxsulotlar
    @OneToMany(mappedBy = "kindergarten" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShippedProductsCompany> shippedProductsCompanyList;

    //Ushbu mttga korxona tomonidan yuborilgan maxsulotlar
    @OneToMany(mappedBy = "kindergarten" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AgreementKindergarten> agreementKindergartenList;

    @OneToOne
    private Address address;

    @OneToOne
    private Info info;

    @OneToMany
    private List<Users> users;

    @OneToMany(mappedBy = "kindergarten" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Price> priceList;

    @OneToMany(mappedBy = "kindergarten" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<OrderKindergarten> orderKindergartens;

    @OneToMany(mappedBy = "kindergarten" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MenuSave> menuSaveList;


    // Aynigan maxsulotlar saqlanadi.
    @OneToMany(mappedBy = "kindergarten", cascade = CascadeType.ALL)
    private List<GarbageProduct> qualityDeteriorates;


    // Omborda mavjud maxsulotlar saqlanadi.
    @OneToMany( cascade = CascadeType.ALL)
    private List<ProductBalancer> consumable;


    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public List<AgreementKindergarten> getAgreementKindergartenList() {
        return agreementKindergartenList;
    }

    public void setAgreementKindergartenList(List<AgreementKindergarten> agreementKindergartenList) {
        this.agreementKindergartenList = agreementKindergartenList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ProductsToBeShippedCompany> getProductsToBeShippedCompanyList() {
        return productsToBeShippedCompanyList;
    }

    public void setProductsToBeShippedCompanyList(List<ProductsToBeShippedCompany> productsToBeShippedCompanyList) {
        this.productsToBeShippedCompanyList = productsToBeShippedCompanyList;
    }

    public List<ShippedProductsCompany> getShippedProductsCompanyList() {
        return shippedProductsCompanyList;
    }

    public void setShippedProductsCompanyList(List<ShippedProductsCompany> shippedProductsCompanyList) {
        this.shippedProductsCompanyList = shippedProductsCompanyList;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public List<Price> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<Price> priceList) {
        this.priceList = priceList;
    }

    public List<OrderKindergarten> getOrderKindergartens() {
        return orderKindergartens;
    }

    public void setOrderKindergartens(List<OrderKindergarten> orderKindergartens) {
        this.orderKindergartens = orderKindergartens;
    }

    public List<MenuSave> getMenuSaveList() {
        return menuSaveList;
    }

    public void setMenuSaveList(List<MenuSave> menuSaveList) {
        this.menuSaveList = menuSaveList;
    }

    public List<GarbageProduct> getQualityDeteriorates() {
        return qualityDeteriorates;
    }

    public void setQualityDeteriorates(List<GarbageProduct> qualityDeteriorates) {
        this.qualityDeteriorates = qualityDeteriorates;
    }

    public List<ProductBalancer> getConsumable() {
        return consumable;
    }

    public void setConsumable(List<ProductBalancer> consumable) {
        this.consumable = consumable;
    }
}
