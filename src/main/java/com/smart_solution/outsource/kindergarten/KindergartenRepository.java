package com.smart_solution.outsource.kindergarten;

import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface KindergartenRepository extends JpaRepository<Kindergarten, Integer> {
    boolean existsByNameAndAddress_District_Id(String name, Integer id);

    List<Kindergarten> findAllByType(String type);

    List<Kindergarten> findAllByTypeAndAddress_District_Id(String type, Integer id);

    Optional<Kindergarten> findByName(String name);
}
