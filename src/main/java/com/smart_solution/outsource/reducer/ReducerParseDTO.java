package com.smart_solution.outsource.reducer;

public interface ReducerParseDTO {

    default ReducerDTO parse(Reducer reducer) {

        ReducerDTO dto = new ReducerDTO();
        dto.setId(reducer.getId());
        dto.setEndDate(reducer.getEndDate());
        dto.setStartDate(reducer.getStartDate());
        dto.setPercentage(Double.valueOf(reducer.getPercentage().toString()));
        dto.setIndex(reducer.getIndex());

        return dto;
    }
}
