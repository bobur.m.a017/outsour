package com.smart_solution.outsource.consumption;

import javax.persistence.Column;
import java.math.BigDecimal;

public class AgeGroupProductDTO {

    private String ageGroupName;
    private String productName;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    public AgeGroupProductDTO() {
    }

    public AgeGroupProductDTO(String ageGroupName, String productName, BigDecimal weight) {
        this.ageGroupName = ageGroupName;
        this.productName = productName;
        this.weight = weight;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
