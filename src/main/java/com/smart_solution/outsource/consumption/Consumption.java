package com.smart_solution.outsource.consumption;

import com.smart_solution.outsource.address.Address;
import com.smart_solution.outsource.consumption.number.ConsumptionByNumberOfChildren;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
public class Consumption {

    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate;

    private Timestamp dayDate;

    private Integer kindergartenId;
    private String kindergartenName;

    @OneToOne
    private Address address;

    private Integer companyId;
    private String companyName;

    private String menuName;
    private Integer menuId;

    private String multiMenuName;

    @OneToMany
    private List<ConsumptionByNumberOfChildren> numberOfChildrenList;

    public Consumption(Timestamp dayDate, Integer kindergartenId, String kindergartenName, Address address, Integer companyId, String companyName, String menuName, Integer menuId, String multiMenuName) {
        this.dayDate = dayDate;
        this.kindergartenId = kindergartenId;
        this.kindergartenName = kindergartenName;
        this.address = address;
        this.companyId = companyId;
        this.companyName = companyName;
        this.menuName = menuName;
        this.menuId = menuId;
        this.multiMenuName = multiMenuName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Consumption() {
    }



    public Timestamp getDayDate() {
        return dayDate;
    }

    public void setDayDate(Timestamp dayDate) {
        this.dayDate = dayDate;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMultiMenuName() {
        return multiMenuName;
    }

    public void setMultiMenuName(String multiMenuName) {
        this.multiMenuName = multiMenuName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<ConsumptionByNumberOfChildren> getNumberOfChildrenList() {
        return numberOfChildrenList;
    }

    public void setNumberOfChildrenList(List<ConsumptionByNumberOfChildren> numberOfChildrenList) {
        this.numberOfChildrenList = numberOfChildrenList;
    }
}
