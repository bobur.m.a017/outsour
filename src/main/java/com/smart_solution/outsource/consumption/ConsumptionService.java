package com.smart_solution.outsource.consumption;

import com.smart_solution.outsource.calculate.CalculateProduct;
import com.smart_solution.outsource.calculate.CalculateService;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.consumption.number.ConsumptionByNumberOfChildren;
import com.smart_solution.outsource.consumption.number.ConsumptionByNumberOfChildrenRepository;
import com.smart_solution.outsource.consumption.number.product.ConsumptionProduct;
import com.smart_solution.outsource.consumption.number.product.ConsumptionProductRepository;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import com.smart_solution.outsource.order.OrderMenuRepository;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeightRepository;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.productBalancer.ProductBalancerRepository;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public record ConsumptionService(
        ConsumptionRepository consumptionRepository,
        MealRepository mealRepository,
        ConsumptionProductRepository consumptionProductRepository,
        ConsumptionByNumberOfChildrenRepository childrenRepository,
        UsersRepository usersRepository,
        OrderMenuRepository orderMenuRepository,
        ProductBalancerRepository productBalancerRepository,
        KindergartenRepository kindergartenRepository,
        AgeGroupRepository ageGroupRepository,
        ProductRepository productRepository,
        MenuWeightRepository menuWeightRepository,
        CalculateService calculateService
) {

    public void add(MenuSave menuSave, Company company) {

        Consumption consumption = new Consumption(
                menuSave.getDayDate(),
                menuSave.getKindergarten().getId(),
                menuSave.getKindergarten().getName(),
                menuSave.getKindergarten().getAddress(),
                company.getId(),
                company.getName(),
                menuSave.getName(),
                menuSave.getId(),
                menuSave.getMultiMenuName()
        );

        Consumption test = consumptionRepository.save(consumption);

//        TestReport testReport = testService.add(test, menuSave.getKindergarten());

        List<ConsumptionByNumberOfChildren> list = new ArrayList<>();

        for (AgeGroup ageGroup : ageGroupRepository.findAll()) {
            ConsumptionByNumberOfChildren children = new ConsumptionByNumberOfChildren(
                    ageGroup.getName(),
                    ageGroup.getId(),
                    getNumberOfKids(menuSave, ageGroup)
            );
            children.setProductList(new ArrayList<>());
            children.setConsumptionId(test.getId());

//            ConsumptionByNumberOfChildren save = childrenRepository.save(children);
            list.add(children);
        }
        consumption.setNumberOfChildrenList(childrenRepository.saveAll(list));

        for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {
            AgeGroup ageGroup = ageGroupRepository.findById(children.getAgeGroupId()).get();
            List<CalculateProduct> calculateProducts = calculateService.calculateProduct(menuSave, ageGroup, children.getNumber().intValue());

            aaaa(children, calculateProducts);
        }
        childrenRepository.saveAll(consumption.getNumberOfChildrenList());
        Consumption save = consumptionRepository.save(consumption);
        addStorage(save, menuSave.getKindergarten(), menuSave);

//        testService.addTest(testReport,menuSave.getKindergarten(),save);
    }

    public void aaaa(ConsumptionByNumberOfChildren children, List<CalculateProduct> calculateProducts) {


        for (CalculateProduct calculateProduct : calculateProducts) {
            if (children.getAgeGroupName().equals(calculateProduct.getAgeGroupName())) {
                bbbb(children, calculateProduct);
            }
        }
    }

    public void bbbb(ConsumptionByNumberOfChildren children, CalculateProduct calculateProduct) {

        boolean res = true;
        List<ConsumptionProduct> productList = children.getProductList() != null ? children.getProductList() : new ArrayList<>();

        for (ConsumptionProduct consumptionProduct : children.getProductList()) {
            if (consumptionProduct.getProductName().equals(calculateProduct.getProductName())) {
                consumptionProduct.setWeight(calculateProduct.getWeight());
                consumptionProduct.setReducerWeight(calculateProduct.getWeight());
                res = false;
            }
        }
        if (res) {
            ConsumptionProduct consumptionProduct = new ConsumptionProduct();
            consumptionProduct.setProductId(calculateProduct.getProduct().getId());
            consumptionProduct.setProductName(calculateProduct.getProductName());

            consumptionProduct.setWeight(calculateProduct.getWeight());
            consumptionProduct.setReducerWeight(calculateProduct.getWeight());
            productList.add(consumptionProduct);
        }
        children.setProductList(consumptionProductRepository.saveAll(productList));
    }

    public long getNumberOfKids(MenuSave menuSave, AgeGroup ageGroup) {
        for (NumberOfChildren numberOfChild : menuSave.getNumberFact().getNumberOfChildren()) {
            if (numberOfChild.getAgeGroup().equals(ageGroup)) {
                return Long.valueOf(numberOfChild.getNumberOfKids());
            }
        }
        return 0;
    }

    public void subtractStorage(Product product, BigDecimal weight, Kindergarten kindergarten) {


        for (ProductBalancer productBalancer : kindergarten.getConsumable()) {
            if (productBalancer.getProduct().equals(product)) {
                    productBalancer.setWeight(productBalancer.getWeight().subtract(weight));
                    BigDecimal pack = product.getPack();
                    if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {
                        pack = BigDecimal.valueOf(1);
                    }
                    productBalancer.setNumberPack(productBalancer.getNumberPack().subtract(weight.divide(pack, 10, RoundingMode.HALF_UP)));
                    productBalancerRepository.save(productBalancer);
//                }

            }
        }
    }

    public void addStorage(Consumption consumption, Kindergarten kindergarten, MenuSave menuSave) {

        Optional<MenuWeight> optionalMenuWeight = menuWeightRepository.findByMenuSave_Id(menuSave.getId());

        if (optionalMenuWeight.isPresent()) {

            MenuWeight menuWeight = optionalMenuWeight.get();

            for (WeightProduct weightProduct : menuWeight.getWeightProducts()) {

                BigDecimal weight = BigDecimal.valueOf(0);

                for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {
                    for (ConsumptionProduct consumptionProduct : children.getProductList()) {
                        if (consumptionProduct.getProductId().equals(weightProduct.getProduct().getId())) {
                            weight = weight.add(consumptionProduct.getWeight());
                        }
                    }
                }
                subtractStorage(weightProduct.getProduct(), weight, kindergarten);
//                BigDecimal subtract = weightProduct.getWeight().subtract(weight);
//                if (subtract.compareTo(BigDecimal.valueOf(0.00099)) > 0) {
//                    List<ProductBalancer> productBalancerList = new ArrayList<>(kindergarten.getConsumable());
//                    for (ProductBalancer productBalancer : productBalancerList) {
//                        if (productBalancer.getProduct().equals(weightProduct.getProduct())) {
//
//                            BigDecimal pack = weightProduct.getProduct().getPack();
//
//                            if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {
//                                pack = BigDecimal.valueOf(1);
//                            }
//
//                            BigDecimal storageWeight = subtractStorage(weightProduct.getProduct(), subtract, kindergarten);
//                            addExisting(kindergarten, weightProduct.getProduct(), subtract, subtract.divide(pack, 10, RoundingMode.HALF_UP));
//                        }
//                    }
//
            }
        }
    }

//    public void addExisting(Kindergarten kindergarten, Product product, BigDecimal weight, BigDecimal numberPack) {
//        boolean res = true;
//        for (ProductBalancer productBalancer : kindergarten.getReserve()) {
//            if (productBalancer.getProduct().equals(product)) {
//                productBalancer.setWeight(productBalancer.getWeight().add(weight));
//                productBalancer.setNumberPack(productBalancer.getNumberPack().add(numberPack));
//                productBalancerRepository.save(productBalancer);
//                res = false;
//            }
//        }
//        if (res) {
//            List<ProductBalancer> reserve = kindergarten.getReserve();
//            reserve.add(productBalancerRepository.save(new ProductBalancer(product, weight, numberPack)));
//            kindergarten.setReserve(reserve);
//            kindergartenRepository.save(kindergarten);
//        }
//    }
}
























