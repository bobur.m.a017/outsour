package com.smart_solution.outsource.consumption.number;

import com.smart_solution.outsource.consumption.number.product.ConsumptionProduct;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class ConsumptionByNumberOfChildren {

    @Id
    private UUID id = UUID.randomUUID();

    private String ageGroupName;
    private Integer ageGroupId;
    private Long number;
    private UUID consumptionId;



    @OneToMany
    private List<ConsumptionProduct> productList;

    public ConsumptionByNumberOfChildren() {
    }

    public ConsumptionByNumberOfChildren(String ageGroupName, Integer ageGroupId, Long number) {
        this.ageGroupName = ageGroupName;
        this.ageGroupId = ageGroupId;
        this.number = number;
    }

    public ConsumptionByNumberOfChildren(String ageGroupName, Integer ageGroupId, List<ConsumptionProduct> productList) {
        this.ageGroupName = ageGroupName;
        this.ageGroupId = ageGroupId;
        this.productList = productList;
    }


    public UUID getConsumptionId() {
        return consumptionId;
    }

    public void setConsumptionId(UUID consumptionId) {
        this.consumptionId = consumptionId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public List<ConsumptionProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ConsumptionProduct> productList) {
        this.productList = productList;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }
}
