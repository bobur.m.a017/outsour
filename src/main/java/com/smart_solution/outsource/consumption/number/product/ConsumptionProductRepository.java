package com.smart_solution.outsource.consumption.number.product;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ConsumptionProductRepository extends JpaRepository<ConsumptionProduct, UUID> {
}
