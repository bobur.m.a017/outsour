package com.smart_solution.outsource.consumption.number;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ConsumptionByNumberOfChildrenRepository extends JpaRepository<ConsumptionByNumberOfChildren, UUID> {

    List<ConsumptionByNumberOfChildren> findAllByConsumptionIdAndAgeGroupId(UUID id, Integer ageGroupId);


}
