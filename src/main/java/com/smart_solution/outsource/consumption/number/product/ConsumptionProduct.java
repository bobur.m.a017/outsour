package com.smart_solution.outsource.consumption.number.product;



import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Entity
public class ConsumptionProduct {

    @Id
    private UUID id = UUID.randomUUID();

    private Integer productId;
    private String productName;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal reducerWeight;

    public ConsumptionProduct() {
    }

    public BigDecimal getReducerWeight() {
        return reducerWeight;
    }

    public void setReducerWeight(BigDecimal reducerWeight) {
        this.reducerWeight = reducerWeight;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
