package com.smart_solution.outsource.consumption;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ConsumptionRepository extends JpaRepository<Consumption, UUID> {
    List<Consumption> findAllByKindergartenId(Integer id);
}
