package com.smart_solution.outsource.multiMenu;

import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.menu.menu.Menu;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNorm;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class MultiMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer daily;
    private BigDecimal percentage;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    @OneToMany
    private List<Menu> menuList;

    @OneToMany
    private List<SanpinMenuNorm> sanpinMenuNormList;

    @OneToMany
    private List<MealAgeStandard> mealAgeStandardList;


    public MultiMenu() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDaily() {
        return daily;
    }

    public void setDaily(Integer daily) {
        this.daily = daily;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public List<SanpinMenuNorm> getSanpinMenuNormList() {
        return sanpinMenuNormList;
    }

    public void setSanpinMenuNormList(List<SanpinMenuNorm> sanpinMenuNormList) {
        this.sanpinMenuNormList = sanpinMenuNormList;
    }

    public List<MealAgeStandard> getMealAgeStandardList() {
        return mealAgeStandardList;
    }

    public void setMealAgeStandardList(List<MealAgeStandard> mealAgeStandardList) {
        this.mealAgeStandardList = mealAgeStandardList;
    }
}
