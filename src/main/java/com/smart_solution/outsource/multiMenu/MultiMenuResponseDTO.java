package com.smart_solution.outsource.multiMenu;

import com.smart_solution.outsource.menu.menu.MenuResponseDTO;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNorm;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormResponseDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class MultiMenuResponseDTO {

    private Integer id;
    private String name;
    private Integer daily;
    private BigDecimal percentage;
    private Timestamp createDate;
    private Timestamp updateDate;

    private List<MenuResponseDTO> menuResponseDTOList;

    private List<SanpinMenuNormResponseDTO> norms;


    public MultiMenuResponseDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDaily() {
        return daily;
    }

    public void setDaily(Integer daily) {
        this.daily = daily;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<MenuResponseDTO> getMenuResponseDTOList() {
        return menuResponseDTOList;
    }

    public void setMenuResponseDTOList(List<MenuResponseDTO> menuResponseDTOList) {
        this.menuResponseDTOList = menuResponseDTOList;
    }

    public List<SanpinMenuNormResponseDTO> getNorms() {
        return norms;
    }

    public void setNorms(List<SanpinMenuNormResponseDTO> norms) {
        this.norms = norms;
    }
}
