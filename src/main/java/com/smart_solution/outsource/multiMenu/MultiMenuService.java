package com.smart_solution.outsource.multiMenu;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.dto.MenuDTO;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.kindergarten.KindergartenType;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProduct;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProductRepository;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveResponseDTO;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveService;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.ageStandard.AgeStandard;
import com.smart_solution.outsource.menu.mealAgeStandard.MealAgeStandard;
import com.smart_solution.outsource.menu.mealTimeStandard.MealTimeStandard;
import com.smart_solution.outsource.menu.menu.Menu;
import com.smart_solution.outsource.menu.menu.MenuRepository;
import com.smart_solution.outsource.menu.menu.MenuResponseDTO;
import com.smart_solution.outsource.menu.menu.MenuService;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.multiMenu.menu.KindergartenByAddressResponseDTO;
import com.smart_solution.outsource.multiMenu.menu.KindergartenResponseDTO;
import com.smart_solution.outsource.multiMenu.menu.MenuAddKindergartenDTO;
import com.smart_solution.outsource.multiMenu.menu.MenuDay;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.productMeal.ProductMeal;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNorm;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormResponseDTO;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormService;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;

@Service
public record MultiMenuService(
        MultiMenuRepository multiMenuRepository,
        MenuService menuService,
        SanpinMenuNormService sanpinMenuNormService,
        MenuSaveService menuSaveService,
        MenuRepository menuRepository,
        UsersRepository usersRepository,
        MenuSaveRepository menuSaveRepository,
        MealRepository mealRepository,
        KindergartenRepository kindergartenRepository,
        CheckProductRepository checkProductRepository
) {


    public StateMessage add(MultiMenuDTO multiMenuDTO) {

        boolean res = multiMenuRepository.existsByName(multiMenuDTO.getName());

        if (!res) {

            MultiMenu multiMenu = new MultiMenu();

            multiMenu.setName(multiMenuDTO.getName());
            multiMenu.setDaily(multiMenuDTO.getDaily());

            List<Menu> menuList = new ArrayList<>();

            for (int i = 1; i < multiMenuDTO.getDaily() + 1; i++) {
                menuList.add(menuService.add(i));
            }
            multiMenu.setSanpinMenuNormList(sanpinMenuNormService.add(multiMenuDTO.getDaily()));
            multiMenu.setMenuList(menuList);

            multiMenuRepository.save(multiMenu);

            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
        }
        return new StateMessage("Xatolik !!!   Bunday nomli menu avval qo`shilgan", false);
    }

    public List<MultiMenuResponseDTO> get() {

        List<MultiMenuResponseDTO> list = new ArrayList<>();

        for (MultiMenu multiMenu : multiMenuRepository.findAll()) {
            sanpinMenuNormService.calculate(multiMenu);
            MultiMenuResponseDTO multiMenuResponseDTO = new MultiMenuResponseDTO();

            multiMenuResponseDTO.setId(multiMenu.getId());
            multiMenuResponseDTO.setName(multiMenu.getName());
            multiMenuResponseDTO.setDaily(multiMenu.getDaily());
            multiMenuResponseDTO.setCreateDate(multiMenu.getCreateDate());
            multiMenuResponseDTO.setUpdateDate(multiMenu.getUpdateDate());
            multiMenuResponseDTO.setPercentage(multiMenu.getPercentage());

            List<SanpinMenuNormResponseDTO> normList = new ArrayList<>();

            for (SanpinMenuNorm sanpinMenuNorm : multiMenu.getSanpinMenuNormList()) {
                normList.add(sanpinMenuNormService.parse(sanpinMenuNorm));
            }

            multiMenuResponseDTO.setNorms(normList);

            List<MenuResponseDTO> menuList = new ArrayList<>();

            for (Menu menu : multiMenu.getMenuList()) {
                menuList.add(menuService.parse(menu));
            }
            multiMenuResponseDTO.setMenuResponseDTOList(menuList);

            list.add(multiMenuResponseDTO);
        }
        return list;
    }

    public StateMessage addMenu(MenuAddKindergartenDTO menuAddKindergartenDTO) {

        List<MenuDay> menuDayList = new ArrayList<>(menuAddKindergartenDTO.getMenuDayList());

        for (MenuDay menuDay : menuDayList) {
            Menu menu = menuRepository.findById(menuDay.getMenuId()).get();
            List<MealTimeStandardSave> mealTimeList = menuSaveService.addMealTimeStandardSave(menu);
            MenuSave menuSave = menuSaveService.add(menu, menuAddKindergartenDTO.getMultiMenuId());

            for (Integer integer : menuAddKindergartenDTO.getKindergartenId()) {

                Kindergarten kindergarten = kindergartenRepository.findById(integer).get();
                Date date = new Date(menuDay.getDate().getTime());
                if (checkMenu(kindergarten,date)){
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);

                    menuSave.setKindergarten(kindergarten);
                    menuSave.setDayDate(menuDay.getDate());
                    menuSave.setYear(c.get(Calendar.YEAR));
                    menuSave.setMonth(c.get(Calendar.MONTH));
                    menuSave.setDay(c.get(Calendar.DAY_OF_MONTH));
                    List<MenuSave> menuSaveList = kindergarten.getMenuSaveList();
                    menuSaveList.add(menuSave);
                    kindergarten.setMenuSaveList(menuSaveList);
                    menuSave.setMealTimeStandardSaves(mealTimeList);

                    for (MealTimeStandardSave mealTimeStandardSave : mealTimeList) {
                        List<MenuSave> menuSaveListTime = mealTimeStandardSave.getMenuSaveList() != null ? mealTimeStandardSave.getMenuSaveList() : new ArrayList<>();
                        menuSaveListTime.add(menuSave);
                        mealTimeStandardSave.setMenuSaveList(menuSaveListTime);
                    }
                    kindergartenRepository.save(kindergarten);
                }
            }
        }
        addCheckProduct();

        return new StateMessage("Muvaffaqiyatli qo`shildi", true);
    }

    public boolean checkMenu(Kindergarten kindergarten, Date date) {

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Optional<MenuSave> optional = menuSaveRepository.findAllByKindergarten_IdAndYearAndMonthAndDay(kindergarten.getId(), year,month,day);
        return optional.isEmpty();
    }

    public List<KindergartenByAddressResponseDTO> getMenu(ResponseUser responseUser, Long num) {

        Timestamp date = new Timestamp(num == null ? System.currentTimeMillis() : num);


        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        Company company = users.getCompany();

        List<KindergartenByAddressResponseDTO> list = new ArrayList<>();

        List<Kindergarten> kindergartens = company.getKindergarten();


        for (Kindergarten kindergarten : kindergartens) {

            if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {

                KindergartenResponseDTO parse = parse(kindergarten, date);

                boolean result = false;
                KindergartenByAddressResponseDTO dto = null;


                for (KindergartenByAddressResponseDTO kindergartenByAddressResponseDTO : list) {

                    if (kindergartenByAddressResponseDTO.getDistrict().equals(kindergarten.getAddress().getDistrict())) {

                        dto = kindergartenByAddressResponseDTO;
                    }
                }

                if (dto != null) {
                    List<KindergartenResponseDTO> kindergartenList = dto.getKindergarten();
                    kindergartenList.add(parse);
                    dto.setKindergarten(kindergartenList);
                    result = true;
                }


                if (list.size() == 0 || !result) {

                    dto = new KindergartenByAddressResponseDTO();

                    dto.setDistrict(kindergarten.getAddress().getDistrict());

                    List<KindergartenResponseDTO> kindergartenList = dto.getKindergarten();

                    if (kindergartenList != null) {
                        kindergartenList.add(parse);
                        dto.setKindergarten(kindergartenList);
                    } else {
                        List<KindergartenResponseDTO> dtoList = new ArrayList<>();
                        dtoList.add(parse);

                        dto.setKindergarten(dtoList);
                    }
                    list.add(dto);
                }
            }
        }
        return list;
    }

    public MenuSaveResponseDTO getMenuKin(ResponseUser responseUser, Long num) {
        Timestamp date = new Timestamp(num == null ? System.currentTimeMillis() : num);
        Integer kindergartenId = usersRepository.findByUserName(responseUser.getUsername()).get().getKindergartenId();

        MenuSave menuSave = menuSaveService.checkMenu(kindergartenId, date);
        if (menuSave != null) {
            MenuSaveResponseDTO parse = menuSaveService.parse(menuSave);

            return parse;
        }

        return null;
    }

    public KindergartenResponseDTO parse(Kindergarten kindergarten, Timestamp date) {

        KindergartenResponseDTO dto = new KindergartenResponseDTO();

        dto.setName(kindergarten.getName());
        dto.setDate(date);
        dto.setKindergartenId(kindergarten.getId());

        MenuSave menuSave = menuSaveService.checkMenu(kindergarten.getId(), date);

        if (menuSave != null) {
            dto.setMenuName(menuSave.getName());
            dto.setMenuId(menuSave.getId());
            dto.setMultiMenuName(menuSave.getMultiMenuName());
        }
        return dto;
    }

    public StateMessage delete(Integer id) {
        multiMenuRepository.deleteById(id);
        return new StateMessage("O`chirildi qayta tiklab bo`lmaydi", true);
    }

    public List<MenuDTO> getMenu(Integer id) {

        MultiMenu multiMenu = multiMenuRepository.findById(id).get();
        List<MenuDTO> list = new ArrayList<>();

        for (Menu menu : multiMenu.getMenuList()) {
            for (MealTimeStandard mealTimeStandard : menu.getMealTimeStandards()) {
                for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandards()) {
                    for (AgeStandard ageStandard : mealAgeStandard.getAgeStandards()) {
                        for (ProductMeal productMeal : mealAgeStandard.getMeal().getProductMeals()) {
                            BigDecimal weight = productMeal.getWeight();
                            BigDecimal divide = weight.divide(mealAgeStandard.getMeal().getWeight(), 7, RoundingMode.HALF_UP);
                            BigDecimal multiply = divide.multiply(ageStandard.getWeight());

                            BigDecimal withoutExit = productMeal.getWithoutExit();
                            BigDecimal withoutExitDivide = withoutExit.divide(mealAgeStandard.getMeal().getWeight(), 7, RoundingMode.HALF_UP);
                            withoutExit = withoutExitDivide.multiply(ageStandard.getWeight());

                            String productWeight = (multiply.divide(BigDecimal.valueOf(1), 6, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(1000)).toString();
                            String productWithoutExit = (withoutExit.divide(BigDecimal.valueOf(1), 6, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(1000)).toString();
                            String ageWeight = (ageStandard.getWeight()).multiply(BigDecimal.valueOf(1000)).toString();

                            String replaceWeight = productWeight.replace('.', ',');
                            String replaceWithoutExit = productWithoutExit.replace('.', ',');
                            String replaceAgeWeight = ageWeight.replace('.', ',');


                            String name = ageStandard.getAgeGroup().getName();
                            if (name.equals("3-4") || name.equals("4-7")) {
                                name = name + " yosh";
                            }


                            list.add(new MenuDTO(
                                    menu.getName(),
                                    mealTimeStandard.getMealTime().getName(),
                                    mealAgeStandard.getMeal().getName(),
                                    name,
                                    productMeal.getProduct().getName(),
                                    replaceWeight,
                                    replaceAgeWeight,
                                    replaceWithoutExit));
                        }
                    }
                }
            }
        }
        return list;
    }

    public void addCheckProduct() {
        List<CheckProduct> checkProductList = new ArrayList<>();
        List<MenuSave> menuSaveList = menuSaveRepository.findAllByCheckProduct(false);
        menuSaveList.addAll(menuSaveRepository.findAllByCheckProduct(null));
        for (MenuSave menuSave : menuSaveList) {
            List<WeightProduct> weightProducts = calculateMenuProduct(menuSave);
            for (WeightProduct weightProduct : weightProducts) {
                checkProductList.add(new CheckProduct(weightProduct.getProduct().getId(), menuSave.getId(), menuSave.getKindergarten().getId(), Status.NEW.getName()));
            }
            menuSave.setCheckProduct(true);
        }
        menuSaveRepository.saveAll(menuSaveList);
        checkProductRepository.saveAll(checkProductList);
    }

    public List<WeightProduct> calculateMenuProduct(MenuSave menuSave) {

        List<WeightProduct> list = new ArrayList<>();
        for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
            for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {
                Meal meal = mealRepository.findById(mealAgeStandardSave.getMealId()).get();
                for (ProductMeal productMeal : meal.getProductMeals()) {
                    addListProduct(list, productMeal.getProduct());
                }
            }

        }
        return list;
    }

    public void addListProduct(List<WeightProduct> list, Product product) {

        boolean res = true;

        for (WeightProduct weightProduct : list) {
            if (weightProduct.getProduct().equals(product)) {
                res = false;
                break;
            }
        }
        if (res) {
            list.add(new WeightProduct(product));
        }
    }

}