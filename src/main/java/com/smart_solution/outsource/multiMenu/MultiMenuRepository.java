package com.smart_solution.outsource.multiMenu;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface MultiMenuRepository extends JpaRepository<MultiMenu, Integer> {
    boolean existsByName(String name);
    Optional<MultiMenu> findByName(String name);
}
