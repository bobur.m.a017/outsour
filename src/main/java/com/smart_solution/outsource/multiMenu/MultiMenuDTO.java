package com.smart_solution.outsource.multiMenu;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class MultiMenuDTO {

    private String name;
    private Integer daily;


    public MultiMenuDTO() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDaily() {
        return daily;
    }

    public void setDaily(Integer daily) {
        this.daily = daily;
    }
}
