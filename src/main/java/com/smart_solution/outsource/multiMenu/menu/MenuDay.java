package com.smart_solution.outsource.multiMenu.menu;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

public class MenuDay {

    private Timestamp date;
    private Integer menuId;

    public MenuDay() {
    }


    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }
}
