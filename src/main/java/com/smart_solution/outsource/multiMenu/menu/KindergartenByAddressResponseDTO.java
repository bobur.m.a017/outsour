package com.smart_solution.outsource.multiMenu.menu;


import com.smart_solution.outsource.address.district.District;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class KindergartenByAddressResponseDTO {
    private District district;
    private String address;
    private List<KindergartenResponseDTO> kindergarten;

    public KindergartenByAddressResponseDTO() {
    }


    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<KindergartenResponseDTO> getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(List<KindergartenResponseDTO> kindergarten) {
        this.kindergarten = kindergarten;
    }
}
