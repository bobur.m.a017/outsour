package com.smart_solution.outsource.multiMenu.menu;


import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

public class KindergartenResponseDTO {

    private String name;
    private Timestamp date;
    private String multiMenuName;
    private String menuName;
    private Integer menuId;
    private Integer kindergartenId;

    public KindergartenResponseDTO() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getMultiMenuName() {
        return multiMenuName;
    }

    public void setMultiMenuName(String multiMenuName) {
        this.multiMenuName = multiMenuName;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }
}
