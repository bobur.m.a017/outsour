package com.smart_solution.outsource.multiMenu.menu;


import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

public class MenuAddKindergartenDTO {
    private List<Integer> kindergartenId;
    private List<MenuDay> menuDayList;
    private Integer multiMenuId;

    public List<Integer> getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(List<Integer> kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public List<MenuDay> getMenuDayList() {
        return menuDayList;
    }

    public void setMenuDayList(List<MenuDay> menuDayList) {
        this.menuDayList = menuDayList;
    }

    public Integer getMultiMenuId() {
        return multiMenuId;
    }

    public void setMultiMenuId(Integer multiMenuId) {
        this.multiMenuId = multiMenuId;
    }
}
