package com.smart_solution.outsource.multiMenu.menu;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

public class MenuSaveEditDTO {

    private Timestamp date;
    private Integer kindergartenId;
    private Integer menuId;
    private Integer multiMenuId;


    public MenuSaveEditDTO() {
    }


    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(Integer kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getMultiMenuId() {
        return multiMenuId;
    }

    public void setMultiMenuId(Integer multiMenuId) {
        this.multiMenuId = multiMenuId;
    }
}
