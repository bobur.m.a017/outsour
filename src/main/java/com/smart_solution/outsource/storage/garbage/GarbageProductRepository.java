package com.smart_solution.outsource.storage.garbage;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GarbageProductRepository extends JpaRepository<GarbageProduct,Integer> {
    List<GarbageProduct> findAllByCompany(Company company);
    List<GarbageProduct> findAllByKindergarten(Kindergarten kindergarten);
}
