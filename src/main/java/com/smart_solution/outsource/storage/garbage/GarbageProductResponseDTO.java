package com.smart_solution.outsource.storage.garbage;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;


public class GarbageProductResponseDTO {


    private Integer id;
    private String productName;
    private BigDecimal weight;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String createdByName;
    private String acceptByName;
    private Timestamp acceptDate;
    private String comment;
    private String status;
    private String kindergartenName;


    public GarbageProductResponseDTO(Integer id, String productName, BigDecimal weight, Timestamp createDate, Timestamp updateDate, String createdByName, String acceptByName, Timestamp acceptDate, String comment, String status, String kindergartenName) {
        this.id = id;
        this.productName = productName;
        this.weight = weight;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.createdByName = createdByName;
        this.acceptByName = acceptByName;
        this.acceptDate = acceptDate;
        this.comment = comment;
        this.status = status;
        this.kindergartenName = kindergartenName;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getAcceptByName() {
        return acceptByName;
    }

    public void setAcceptByName(String acceptByName) {
        this.acceptByName = acceptByName;
    }

    public Timestamp getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(Timestamp acceptDate) {
        this.acceptDate = acceptDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public GarbageProductResponseDTO() {
    }
}
