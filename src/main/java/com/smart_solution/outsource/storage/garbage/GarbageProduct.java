package com.smart_solution.outsource.storage.garbage;


import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.users.Users;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
public class GarbageProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(precision = 19, scale = 6)
    private BigDecimal weight;
    @Column(precision = 19, scale = 6)
    private BigDecimal numberPack;
    @CreationTimestamp
    private Timestamp createDate;
    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private Product product;

    @ManyToOne
    private Users createdByName;

    @ManyToOne
    private Users acceptByName;
    private Timestamp acceptDate;

    private String comment;

    private String status;

    @ManyToOne
    private Company company;

    @ManyToOne
    private Kindergarten kindergarten;

    public GarbageProduct(Product product,BigDecimal weight,BigDecimal numberPack, Users createdByName, String comment, String status, Company company) {

        this.product = product;
        this.weight = weight;
        this.numberPack = numberPack;
        this.createdByName = createdByName;
        this.comment = comment;
        this.status = status;
        this.company = company;
    }

    public GarbageProduct(Product product,BigDecimal weight,BigDecimal numberPack, Users createdByName, String comment, String status, Kindergarten kindergarten) {

        this.product = product;
        this.weight = weight;
        this.numberPack = numberPack;
        this.createdByName = createdByName;
        this.comment = comment;
        this.status = status;
        this.kindergarten = getKindergarten();
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public Timestamp getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(Timestamp acceptDate) {
        this.acceptDate = acceptDate;
    }

    public GarbageProduct(Product product, BigDecimal weight, Users createdByName, String comment, String status, Kindergarten kindergarten) {
        this.product = product;
        this.weight = weight;
        this.createdByName = createdByName;
        this.comment = comment;
        this.status = status;
        this.kindergarten = kindergarten;
    }

    public GarbageProduct() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public Users getCreatedByName() {
        return createdByName;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setCreatedByName(Users createdByName) {
        this.createdByName = createdByName;
    }

    public Users getAcceptByName() {
        return acceptByName;
    }

    public void setAcceptByName(Users acceptByName) {
        this.acceptByName = acceptByName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Kindergarten getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(Kindergarten kindergarten) {
        this.kindergarten = kindergarten;
    }
}
