package com.smart_solution.outsource.storage.garbage;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class GarbageProductDTO {

    private Integer productId;
    private Double weight;
    private Double numberPack;
    private String comment;

    public GarbageProductDTO() {
    }

    public Double getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(Double numberPack) {
        this.numberPack = numberPack;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
