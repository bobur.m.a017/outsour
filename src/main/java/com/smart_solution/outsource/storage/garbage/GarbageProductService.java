package com.smart_solution.outsource.storage.garbage;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.dto.BooleanDto;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.productBalancer.ProductBalancerRepository;
import com.smart_solution.outsource.role.RoleType;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

@Service
public record GarbageProductService(
        GarbageProductRepository garbageProductRepository,
        UsersRepository usersRepository,
        KindergartenRepository kindergartenRepository,
        ProductRepository productRepository,
        ProductBalancerRepository productBalancerRepository,
        CompanyRepository companyRepository
) {


    public List<GarbageProductResponseDTO> get(ResponseUser responseUser, Long start, Long end) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        List<GarbageProduct> arrayList = new ArrayList<>();
        if (users.getCompany() != null) {

            if (users.getRole().getName().equals(RoleType.CHIEF.getName())){
                Company company = users.getCompany();
                for (Kindergarten kindergarten : company.getKindergarten()) {
                    arrayList.addAll(garbageProductRepository.findAllByKindergarten(kindergarten));
                }
            }
           arrayList.addAll(garbageProductRepository.findAllByCompany(users.getCompany()));

        } else if (users.getKindergartenId() != null) {
            Kindergarten kindergarten = kindergartenRepository.findById(users.getKindergartenId()).get();

            arrayList.addAll(garbageProductRepository.findAllByKindergarten(kindergarten));
        }
        List<GarbageProductResponseDTO> list = new ArrayList<>();

        for (GarbageProduct garbageProduct : arrayList) {

            if (start != null && end != null) {

                if (start < garbageProduct.getCreateDate().getTime() && end > garbageProduct.getCreateDate().getTime()) {
                    list.add(parse(garbageProduct));
                }
            } else {
//                if (garbageProduct.getStatus().equals(Status.NEW.getName())) {
                    list.add(parse(garbageProduct));
//                }
            }
        }


        return list;
    }

    public GarbageProductResponseDTO parse(GarbageProduct a) {
        return new GarbageProductResponseDTO(
                a.getId(),
                a.getProduct().getName(),
                a.getWeight(),
                a.getCreateDate(),
                a.getUpdateDate(),
                a.getCreatedByName().getName() + " " + a.getCreatedByName().getSurname(),
                a.getAcceptByName() != null ? a.getAcceptByName().getName() + " " + a.getAcceptByName().getSurname() : "Tasdiqlanmagan",
                a.getAcceptDate(),
                a.getComment(),
                a.getStatus(),
                a.getKindergarten() != null ? a.getKindergarten().getName() + " " + a.getKindergarten().getAddress().getDistrict().getName() : ""
        );
    }

    public StateMessage add(ResponseUser responseUser, GarbageProductDTO dto) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();
        Product product = productRepository.findById(dto.getProductId()).get();
        BigDecimal weight = BigDecimal.valueOf(dto.getWeight());
        BigDecimal numberPack = BigDecimal.valueOf(dto.getNumberPack());
        Kindergarten kindergarten = null;
        List<ProductBalancer> reserve;
//
//        if ((product.getPack().compareTo(BigDecimal.valueOf(0)) > 0) && dto.getNumberPack() % 1 > 0){
//        }

        if (company == null) {
            kindergarten = kindergartenRepository.findById(users.getKindergartenId()).get();
            reserve = kindergarten.getConsumable();
        } else {
            reserve = company.getReserve();
        }

        for (ProductBalancer productBalancer : reserve) {
            if (productBalancer.getProduct().equals(product)) {
                if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(dto.getWeight())) < 0) {
                    return new StateMessage("Xatolik ! ! ! Maxsulot miqdori ko`p kiritilgan", false);
                } else {
                    productBalancer.setWeight(productBalancer.getWeight().subtract(weight));
                    productBalancer.setNumberPack(productBalancer.getNumberPack().subtract(numberPack));
                    productBalancerRepository.save(productBalancer);
                }
            }
        }

        if (kindergarten != null) {
            GarbageProduct garbageProduct = new GarbageProduct(
                    product, weight, numberPack, users, dto.getComment(), Status.NEW.getName(), kindergarten
            );
            garbageProduct.setKindergarten(kindergarten);
            GarbageProduct save = garbageProductRepository.save(garbageProduct);
            List<GarbageProduct> qualityDeteriorates = kindergarten.getQualityDeteriorates() != null ? kindergarten.getQualityDeteriorates() : new ArrayList<>();
            qualityDeteriorates.add(save);
            kindergarten.setQualityDeteriorates(qualityDeteriorates);
            kindergartenRepository.save(kindergarten);
        } else {
            GarbageProduct save = garbageProductRepository.save(new GarbageProduct(
                    product, weight, numberPack, users, dto.getComment(), Status.NEW.getName(), company
            ));

            List<GarbageProduct> qualityDeteriorates = company.getQualityDeteriorates() != null ? company.getQualityDeteriorates() : new ArrayList<>();
            qualityDeteriorates.add(save);
            company.setQualityDeteriorates(qualityDeteriorates);
            companyRepository.save(company);
        }
        return new StateMessage("Muvaffaqiyatli yuborldi", true);
    }

    public StateMessage changeStatus(Integer id, BooleanDto dto, ResponseUser responseUser) {

        GarbageProduct garbageProduct = garbageProductRepository.findById(id).get();
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        if (garbageProduct.getStatus().equals(Status.NEW.getName())) {

            garbageProduct.setStatus(dto.isResult() ? Status.SUCCESS.getName() : Status.REJECTED.getName());

            if (!dto.isResult()) {
                Kindergarten kindergarten = garbageProduct.getKindergarten();
                Company company = garbageProduct.getCompany();
                List<ProductBalancer> reserve;

                if (kindergarten != null) {
                    reserve = kindergarten.getConsumable();
                } else {
                    reserve = company.getReserve();
                }

                for (ProductBalancer productBalancer : reserve) {
                    if (productBalancer.getProduct().equals(garbageProduct.getProduct())) {
                        productBalancer.setWeight(productBalancer.getWeight().add(garbageProduct.getWeight()));
                        productBalancer.setNumberPack(productBalancer.getNumberPack().add(garbageProduct.getNumberPack()));
                        productBalancerRepository.save(productBalancer);
                    }
                }
            }
            garbageProduct.setAcceptDate(new Timestamp(System.currentTimeMillis()));
            garbageProduct.setAcceptByName(users);
            garbageProductRepository.save(garbageProduct);
            return new StateMessage(dto.isResult() ? "Muvaffaqiyatli tasdiqlandi." : "Muvaffaqiyatli rad etildi.", true);
        }
        return new StateMessage("Xatolik ma`lumot mavjud emas.", false);
    }

    public StateMessage delete(Integer id) {

        GarbageProduct garbageProduct = garbageProductRepository.findById(id).get();

        if (garbageProduct.getStatus().equals(Status.NEW.getName())) {
            garbageProduct.setStatus(Status.DELETE.getName());
            garbageProductRepository.save(garbageProduct);

            Kindergarten kindergarten = garbageProduct.getKindergarten();
            Company company = garbageProduct.getCompany();
            List<ProductBalancer> reserve;

            if (kindergarten != null) {
                reserve = kindergarten.getConsumable();
            } else {
                reserve = company.getReserve();
            }

            for (ProductBalancer productBalancer : reserve) {
                if (productBalancer.getProduct().equals(garbageProduct.getProduct())) {
                    productBalancer.setWeight(productBalancer.getWeight().add(garbageProduct.getWeight()));
                    productBalancer.setNumberPack(productBalancer.getNumberPack().add(garbageProduct.getNumberPack()));
                    productBalancerRepository.save(productBalancer);
                }
            }


            return new StateMessage("Muvaffaqiyatli o`chirildi.", true);

        }
        return new StateMessage("Xatolik ma`lumot mavjud emas.", false);
    }
}
