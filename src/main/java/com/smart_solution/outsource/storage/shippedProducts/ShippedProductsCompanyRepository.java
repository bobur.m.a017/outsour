package com.smart_solution.outsource.storage.shippedProducts;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ShippedProductsCompanyRepository extends JpaRepository<ShippedProductsCompany, UUID> {
    List<ShippedProductsCompany> findAllByKindergartenAndStatus(Kindergarten kindergarten, String status);
    List<ShippedProductsCompany> findAllByKindergarten(Kindergarten kindergarten);


    List<ShippedProductsCompany> findAllByCompanyAndStatus(Company company, String status);
    List<ShippedProductsCompany> findAllByCompanyAndStatusAndKindergarten(Company company, String status, Kindergarten kindergarten);
    List<ShippedProductsCompany> findAllByCompany(Company company);
    List<ShippedProductsCompany> findAllByCompanyAndKindergarten(Company company, Kindergarten kindergarten);

}
