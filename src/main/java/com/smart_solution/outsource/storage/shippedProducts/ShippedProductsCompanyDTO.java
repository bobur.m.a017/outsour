package com.smart_solution.outsource.storage.shippedProducts;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

public class ShippedProductsCompanyDTO {

    private UUID id;
    private String productName;
    private Integer productId;
    private String companyName;
    private String orderNumber;
    private String kindergartenName;
    private String kindergartenAddress;
    private BigDecimal sendWeight;
    private BigDecimal successWeight;
    private BigDecimal sendNumberPack;
    private BigDecimal successNumberPack;
    private Timestamp timeOfShipment;
    private Timestamp timeTaken;
    private String theSender;
    private String receiver;
    private String status;
    private BigDecimal pack;
    private String measurementType;
    private String comment;
    private String agreementNumber;
    private BigDecimal price;

    public ShippedProductsCompanyDTO(UUID id, String productName, Integer productId, String companyName, String orderNumber, String kindergartenName, String kindergartenAddress, BigDecimal sendWeight, BigDecimal successWeight, BigDecimal sendNumberPack, BigDecimal successNumberPack, Timestamp timeOfShipment, Timestamp timeTaken, String theSender, String receiver, String status, BigDecimal pack, String measurementType, String comment, String agreementNumber) {
        this.id = id;
        this.productName = productName;
        this.productId = productId;
        this.companyName = companyName;
        this.orderNumber = orderNumber;
        this.kindergartenName = kindergartenName;
        this.kindergartenAddress = kindergartenAddress;
        this.sendWeight = sendWeight;
        this.successWeight = successWeight;
        this.sendNumberPack = sendNumberPack;
        this.successNumberPack = successNumberPack;
        this.timeOfShipment = timeOfShipment;
        this.timeTaken = timeTaken;
        this.theSender = theSender;
        this.receiver = receiver;
        this.status = status;
        this.pack = pack;
        this.measurementType = measurementType;
        this.comment= comment;
        this.agreementNumber= agreementNumber;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getAgreementNumber() {
        return agreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public String getKindergartenAddress() {
        return kindergartenAddress;
    }

    public void setKindergartenAddress(String kindergartenAddress) {
        this.kindergartenAddress = kindergartenAddress;
    }

    public ShippedProductsCompanyDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getSendWeight() {
        return sendWeight;
    }

    public void setSendWeight(BigDecimal sendWeight) {
        this.sendWeight = sendWeight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getSendNumberPack() {
        return sendNumberPack;
    }

    public void setSendNumberPack(BigDecimal sendNumberPack) {
        this.sendNumberPack = sendNumberPack;
    }

    public BigDecimal getSuccessNumberPack() {
        return successNumberPack;
    }

    public void setSuccessNumberPack(BigDecimal successNumberPack) {
        this.successNumberPack = successNumberPack;
    }

    public Timestamp getTimeOfShipment() {
        return timeOfShipment;
    }

    public void setTimeOfShipment(Timestamp timeOfShipment) {
        this.timeOfShipment = timeOfShipment;
    }

    public Timestamp getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Timestamp timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getTheSender() {
        return theSender;
    }

    public void setTheSender(String theSender) {
        this.theSender = theSender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
