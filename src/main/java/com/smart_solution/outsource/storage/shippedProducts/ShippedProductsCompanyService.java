package com.smart_solution.outsource.storage.shippedProducts;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public record ShippedProductsCompanyService() {

    public ShippedProductsCompanyDTO parse(ShippedProductsCompany a){
        return new ShippedProductsCompanyDTO(
                a.getId(),
                a.getProduct().getName(),
                a.getProduct().getId(),
                a.getCompany().getName(),
                a.getOrderMenu() != null ? a.getOrderMenu().getOrderNumber() : " ",
                a.getKindergarten().getName(),
                a.getKindergarten().getAddress().getDistrict().getName(),
                a.getSendWeight(),
                a.getSuccessWeight() != null ? a.getSuccessWeight() : BigDecimal.valueOf(0),
                a.getSendNumberPack(),
                a.getSuccessNumberPack() != null ? a.getSuccessNumberPack() : BigDecimal.valueOf(0),
                a.getTimeOfShipment(),
                a.getTimeTaken(),
                a.getTheSender().getName()+" "+a.getTheSender().getSurname(),
                a.getReceiver() != null ? a.getReceiver().getName()+" "+a.getReceiver().getSurname() : "",
                a.getStatus(),
                a.getProduct().getPack(),
                a.getProduct().getMeasurementType(),
                a.getComment(),
                a.getAgreementNumber() != null ? a.getAgreementNumber() :""
        );
    }
}
