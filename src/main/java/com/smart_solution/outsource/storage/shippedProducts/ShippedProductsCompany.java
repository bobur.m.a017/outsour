package com.smart_solution.outsource.storage.shippedProducts;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.users.Users;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
public class ShippedProductsCompany {

    @Id
    private UUID id = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private OrderMenu orderMenu;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Kindergarten kindergarten;


    @Column(precision = 19, scale = 6)
    private BigDecimal sendWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal successWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal sendNumberPack;

    @Column(precision = 19, scale = 6)
    private BigDecimal successNumberPack;


    private Timestamp timeOfShipment;

    private String agreementNumber;

    private String comment;

    private Timestamp timeTaken;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Users theSender;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Users receiver;

    public ShippedProductsCompany() {
    }

    public ShippedProductsCompany(Product product, Company company, OrderMenu orderMenu, Kindergarten kindergarten, BigDecimal sendWeight, BigDecimal successWeight, BigDecimal sendNumberPack, BigDecimal successNumberPack, Timestamp timeOfShipment, String comment, Timestamp timeTaken, Users theSender, Users receiver, String status, String agreementNumber) {
        this.product = product;
        this.company = company;
        this.orderMenu = orderMenu;
        this.kindergarten = kindergarten;
        this.sendWeight = sendWeight;
        this.successWeight = successWeight;
        this.sendNumberPack = sendNumberPack;
        this.successNumberPack = successNumberPack;
        this.timeOfShipment = timeOfShipment;
        this.comment = comment;
        this.timeTaken = timeTaken;
        this.theSender = theSender;
        this.receiver = receiver;
        this.status = status;
        this.agreementNumber = agreementNumber;
    }

    public ShippedProductsCompany(Product product, Company company, OrderMenu orderMenu, Kindergarten kindergarten, BigDecimal sendWeight, BigDecimal sendNumberPack, Timestamp timeOfShipment, Users theSender, String status, String comment,String agreementNumber) {
        this.product = product;
        this.company = company;
        this.orderMenu = orderMenu;
        this.kindergarten = kindergarten;
        this.sendWeight = sendWeight;
        this.sendNumberPack = sendNumberPack;
        this.timeOfShipment = timeOfShipment;
        this.theSender = theSender;
        this.status = status;
        this.comment = comment;
        this.agreementNumber = agreementNumber;
    }

    public String getAgreementNumber() {
        return agreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private String status;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public OrderMenu getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(OrderMenu orderMenu) {
        this.orderMenu = orderMenu;
    }

    public Kindergarten getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(Kindergarten kindergarten) {
        this.kindergarten = kindergarten;
    }

    public BigDecimal getSendWeight() {
        return sendWeight;
    }

    public void setSendWeight(BigDecimal sendWeight) {
        this.sendWeight = sendWeight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getSendNumberPack() {
        return sendNumberPack;
    }

    public void setSendNumberPack(BigDecimal sendNumberPack) {
        this.sendNumberPack = sendNumberPack;
    }

    public BigDecimal getSuccessNumberPack() {
        return successNumberPack;
    }

    public void setSuccessNumberPack(BigDecimal successNumberPack) {
        this.successNumberPack = successNumberPack;
    }

    public Timestamp getTimeOfShipment() {
        return timeOfShipment;
    }

    public void setTimeOfShipment(Timestamp timeOfShipment) {
        this.timeOfShipment = timeOfShipment;
    }

    public Timestamp getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Timestamp timeTaken) {
        this.timeTaken = timeTaken;
    }

    public Users getTheSender() {
        return theSender;
    }

    public void setTheSender(Users theSender) {
        this.theSender = theSender;
    }

    public Users getReceiver() {
        return receiver;
    }

    public void setReceiver(Users receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
