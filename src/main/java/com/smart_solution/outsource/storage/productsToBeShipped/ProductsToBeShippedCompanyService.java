package com.smart_solution.outsource.storage.productsToBeShipped;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public record ProductsToBeShippedCompanyService (){

    public ProductsToBeShippedCompanyDTO parse(ProductsToBeShippedCompany a){
        return new ProductsToBeShippedCompanyDTO(
                a.getId(),
                a.getProduct().getName(),
                a.getProduct().getId(),
                a.getCompany().getName(),
                a.getKindergarten().getName(),
                a.getKindergarten().getAddress().getDistrict().getName(),
                a.getOrderMenu().getOrderNumber(),
                a.getWeight(),
                a.getSuccessWeight()!= null ? a.getSuccessWeight() : BigDecimal.valueOf(0),
                a.getNumberPack(),
                a.getSuccessNumberPack()!= null ? a.getSuccessNumberPack() : BigDecimal.valueOf(0),
                a.getRequestDate(),
                a.getStatus(),
                a.getProduct().getPack(),
                a.getProduct().getMeasurementType()
        );
    }
}
