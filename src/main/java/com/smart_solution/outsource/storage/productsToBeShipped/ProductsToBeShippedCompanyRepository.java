package com.smart_solution.outsource.storage.productsToBeShipped;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ProductsToBeShippedCompanyRepository extends JpaRepository<ProductsToBeShippedCompany, UUID> {
    List<ProductsToBeShippedCompany> findAllByCompanyAndKindergartenAndStatus(Company company, Kindergarten kindergarten, String status);
    List<ProductsToBeShippedCompany> findAllByCompanyAndKindergarten(Company company, Kindergarten kindergarten);
}
