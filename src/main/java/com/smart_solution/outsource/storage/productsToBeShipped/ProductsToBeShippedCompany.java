package com.smart_solution.outsource.storage.productsToBeShipped;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.product.Product;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
public class ProductsToBeShippedCompany {

    @Id
    private UUID id = UUID.randomUUID();


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Kindergarten kindergarten;


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private OrderMenu orderMenu;


    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal successWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal numberPack;

    @Column(precision = 19, scale = 6)
    private BigDecimal successNumberPack;

    private Timestamp requestDate;

    private String status;

    private String agreementNumber;



    public ProductsToBeShippedCompany() {
    }

    public ProductsToBeShippedCompany(Product product, Company company, Kindergarten kindergarten, OrderMenu orderMenu, BigDecimal weight, BigDecimal numberPack, Timestamp requestDate, String status, String agreementNumber) {
        this.product = product;
        this.company = company;
        this.kindergarten = kindergarten;
        this.orderMenu = orderMenu;
        this.weight = weight;
        this.numberPack = numberPack;
        this.requestDate = requestDate;
        this.status = status;
        this.agreementNumber = agreementNumber;
    }

    public String getAgreementNumber() {
        return agreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Kindergarten getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(Kindergarten kindergarten) {
        this.kindergarten = kindergarten;
    }

    public OrderMenu getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(OrderMenu orderMenu) {
        this.orderMenu = orderMenu;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public BigDecimal getSuccessNumberPack() {
        return successNumberPack;
    }

    public void setSuccessNumberPack(BigDecimal successNumberPack) {
        this.successNumberPack = successNumberPack;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
