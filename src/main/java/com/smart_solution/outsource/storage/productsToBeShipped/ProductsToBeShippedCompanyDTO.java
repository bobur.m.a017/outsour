package com.smart_solution.outsource.storage.productsToBeShipped;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

public class ProductsToBeShippedCompanyDTO {

    private UUID id;
    private String productName;
    private Integer productId;
    private String companyName;
    private String kindergartenName;
    private String kindergartenAddress;
    private String orderNumber;
    private BigDecimal weight;
    private BigDecimal successWeight;
    private BigDecimal numberPack;
    private BigDecimal successNumberPack;
    private Timestamp requestDate;
    private String status;
    private BigDecimal pack;
    private String measurementType;

    public ProductsToBeShippedCompanyDTO(UUID id, String productName, Integer productId, String companyName, String kindergartenName, String kindergartenAddress, String orderNumber, BigDecimal weight, BigDecimal successWeight, BigDecimal numberPack, BigDecimal successNumberPack, Timestamp requestDate, String status, BigDecimal pack, String measurementType) {
        this.id = id;
        this.productName = productName;
        this.productId = productId;
        this.companyName = companyName;
        this.kindergartenName = kindergartenName;
        this.kindergartenAddress = kindergartenAddress;
        this.orderNumber = orderNumber;
        this.weight = weight;
        this.successWeight = successWeight;
        this.numberPack = numberPack;
        this.successNumberPack = successNumberPack;
        this.requestDate = requestDate;
        this.status = status;
        this.pack = pack;
        this.measurementType = measurementType;
    }

    public ProductsToBeShippedCompanyDTO() {
    }

    public BigDecimal getPack() {
        return pack;
    }

    public void setPack(BigDecimal pack) {
        this.pack = pack;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public String getKindergartenAddress() {
        return kindergartenAddress;
    }

    public void setKindergartenAddress(String kindergartenAddress) {
        this.kindergartenAddress = kindergartenAddress;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getNumberPack() {
        return numberPack;
    }

    public void setNumberPack(BigDecimal numberPack) {
        this.numberPack = numberPack;
    }

    public BigDecimal getSuccessNumberPack() {
        return successNumberPack;
    }

    public void setSuccessNumberPack(BigDecimal successNumberPack) {
        this.successNumberPack = successNumberPack;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
