package com.smart_solution.outsource.storage.report;

import java.math.BigDecimal;

public class StorageReportDTO {


    private Integer productId;
    private String productName;
    private String measurementType;
    private BigDecimal sendWeight;
    private BigDecimal inputWeight;
    private BigDecimal kunOxiriga;
    private BigDecimal kunBoshiga;


    public StorageReportDTO(Integer productId, String productName, String measurementType, BigDecimal sendWeight, BigDecimal inputWeight, BigDecimal kunOxiriga, BigDecimal kunBoshiga) {
        this.productId = productId;
        this.productName = productName;
        this.measurementType = measurementType;
        this.sendWeight = sendWeight;
        this.inputWeight = inputWeight;
        this.kunOxiriga = kunOxiriga;
        this.kunBoshiga = kunBoshiga;
    }

    public StorageReportDTO() {
    }

    public BigDecimal getKunOxiriga() {
        return kunOxiriga;
    }

    public void setKunOxiriga(BigDecimal kunOxiriga) {
        this.kunOxiriga = kunOxiriga;
    }

    public BigDecimal getKunBoshiga() {
        return kunBoshiga;
    }

    public void setKunBoshiga(BigDecimal kunBoshiga) {
        this.kunBoshiga = kunBoshiga;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getSendWeight() {
        return sendWeight;
    }

    public void setSendWeight(BigDecimal sendWeight) {
        this.sendWeight = sendWeight;
    }

    public BigDecimal getInputWeight() {
        return inputWeight;
    }

    public void setInputWeight(BigDecimal inputWeight) {
        this.inputWeight = inputWeight;
    }
}
