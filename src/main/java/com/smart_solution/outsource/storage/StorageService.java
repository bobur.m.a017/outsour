package com.smart_solution.outsource.storage;

import com.smart_solution.outsource.agreement.price.AgreementPrice;
import com.smart_solution.outsource.agreement.price.AgreementPriceRepository;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.company.CompanyService;
import com.smart_solution.outsource.dto.CheckProductDTO;
import com.smart_solution.outsource.dto.ReceiveDto;
import com.smart_solution.outsource.dto.SendProductDTO;
import com.smart_solution.outsource.dto.ShippingLetter;
import com.smart_solution.outsource.inOut.*;
import com.smart_solution.outsource.inOutProduct.InOutProduct;
import com.smart_solution.outsource.inOutProduct.InOutProductRepository;
import com.smart_solution.outsource.inOutProduct.InOutProductService;
import com.smart_solution.outsource.kindergarten.*;
import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProduct;
import com.smart_solution.outsource.kindergartenMenu.checkProduct.CheckProductRepository;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveService;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import com.smart_solution.outsource.order.OrderMenu;
import com.smart_solution.outsource.order.orderKindergarten.OrderKindergarten;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProductRepository;
import com.smart_solution.outsource.perDay.PerDay;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productBalancer.ProductBalancer;
import com.smart_solution.outsource.productBalancer.ProductBalancerRepository;
import com.smart_solution.outsource.productBalancer.ProductBalancerResponseDTO;
import com.smart_solution.outsource.productBalancer.ProductBalancerService;
import com.smart_solution.outsource.productMeal.ProductMeal;
import com.smart_solution.outsource.productSuppiler.ProductSupplierRepository;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompanyDTO;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompanyRepository;
import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompanyService;
import com.smart_solution.outsource.storage.report.StorageReportDTO;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompanyDTO;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompanyRepository;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompanyService;
import com.smart_solution.outsource.storage.shipping.ShippingCompany;
import com.smart_solution.outsource.storage.shipping.ShippingCompanyRepository;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShipped;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedDTO;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedRepository;
import com.smart_solution.outsource.supplier.productsToBeShipped.ProductsToBeShippedService;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsDTO;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsRepository;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsService;
import com.smart_solution.outsource.supplier.shipping.Shipping;
import com.smart_solution.outsource.supplier.shipping.ShippingRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;

@Service
public record StorageService(
        UsersRepository usersRepository,
        KindergartenRepository kindergartenRepository,
        InOutProductRepository inOutProductRepository,
        CompanyRepository companyRepository,
        InOutProductService inOutProductService,
        ProductBalancerRepository productBalancerRepository,
        ProductBalancerService productBalancerService,
        ShippedProductsCompanyService shippedProductsCompanyService,
        ProductsToBeShippedService productsToBeShippedService,
        ProductsToBeShippedCompanyRepository productsToBeShippedCompanyRepository,
        ProductsToBeShippedRepository productsToBeShippedRepository,
        ShippedProductsCompanyRepository shippedProductsCompanyRepository,
        ProductsToBeShippedCompanyService productsToBeShippedCompanyService,
        ShippedProductsRepository shippedProductsRepository,
        ShippedProductsService shippedProductsService,
        ShippingRepository shippingRepository,
        ShippingCompanyRepository shippingCompanyRepository,
        WeightProductRepository weightProductRepository,
        ProductSupplierRepository productSupplierRepository,
        ProductRepository productRepository,
        CompanyService companyService,
        KindergartenService kindergartenService,
        AgreementPriceRepository agreementPriceRepository,
        MenuSaveRepository menuSaveRepository,
        MenuSaveService menuSaveService,
        MealRepository mealRepository,
        CheckProductRepository checkProductRepository
) {

    //Zayafka yuborilayotganda zaxiradagi ombordagi maxsulotlar asosiy omborga o`tkaziladi.
    public void addShouldBeSentProduct(OrderMenu orderMenu, Company company) {
        List<ProductBalancer> list = new ArrayList<>();

        for (InOutProduct inOutProduct : orderMenu.getExistingProduct()) {
            if (inOutProduct.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                boolean res = true;
                for (ProductBalancer productBalancer : company.getShouldBeSent()) {
                    if (productBalancer.getProduct().equals(inOutProduct.getProduct())) {
                        productBalancer.setWeight(productBalancer.getWeight().add(inOutProduct.getWeight()));
                        productBalancer.setNumberPack(productBalancer.getNumberPack().add(inOutProduct.getNumberPack()));
                        list.add(productBalancer);
                        res = false;
                        break;
                    }
                }
                if (res) {
                    list.add(new ProductBalancer(
                            inOutProduct.getProduct(),
                            inOutProduct.getWeight(),
                            inOutProduct.getNumberPack()
                    ));
                }
            }
        }
        List<ProductBalancer> saveAll = productBalancerRepository.saveAll(list);
        company.setShouldBeSent(saveAll);
        companyRepository.save(company);
    }

    //Zayafka yuborilayotganda zaxiradagi ombordagi maxsulotlar asosiy omborga o`tkaziladi. MTT
//    public void addShouldBeSentProductKin(OrderKindergarten orderKindergarten, Kindergarten kindergarten) {
//        List<ProductBalancer> list = new ArrayList<>();
//
//        for (InOutProduct inOutProduct : orderKindergarten.getExistingProduct()) {
//            if (inOutProduct.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
//
//                boolean res = true;
//                for (ProductBalancer productBalancer : kindergarten.getConsumable()) {
//                    if (productBalancer.getProduct().equals(inOutProduct.getProduct())) {
//                        productBalancer.setWeight(productBalancer.getWeight().add(inOutProduct.getWeight()));
//                        list.add(productBalancer);
//                        res = false;
//                        break;
//                    }
//                }
//                if (res) {
//                    list.add(new ProductBalancer(
//                            inOutProduct.getProduct(),
//                            inOutProduct.getWeight()
//                    ));
//                }
//            }
//        }
//        List<ProductBalancer> saveAll = productBalancerRepository.saveAll(list);
//        kindergarten.setConsumable(saveAll);
//        kindergartenRepository.save(kindergarten);
//    }

    public List<ProductBalancerResponseDTO> getExistingProductKin(ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Kindergarten kindergarten = kindergartenRepository.findById(users.getKindergartenId()).get();

        List<ProductBalancerResponseDTO> list = new ArrayList<>();
        for (ProductBalancer productBalancer : kindergarten.getConsumable()) {    /////O`ZGARTIRILDI
            if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(0.003)) > 0) {
                ProductBalancerResponseDTO parse = productBalancerService.parse(productBalancer);

                BigDecimal weight = productBalancer.getWeight();
                List<MenuSave> menuList = menuSaveRepository.findAllByKindergartenAndStatus(kindergarten, Status.MAXSULOT_YUBORILDI.getName());

                for (MenuSave menuSave : menuList) {
                    CheckProduct checkProduct = checkProductRepository.findByKindergartenIdAndMenuSaveIdAndProductIdAndCondition(kindergarten.getId(), menuSave.getId(), productBalancer.getProduct().getId(), Status.MAXSULOT_YUBORILDI.getName());

                    weight = weight.subtract(checkProduct.getWeight());

                }

                if (weight.compareTo(BigDecimal.valueOf(0)) > 0) {
                    parse.setWeight(weight);
                    BigDecimal pack = parse.getPack();
                    if (pack.compareTo(BigDecimal.valueOf(0)) > 0) {
                        parse.setNumberPack(weight.divide(pack, 2, RoundingMode.HALF_UP));
                    } else {
                        parse.setNumberPack(weight);
                    }
                    list.add(parse);
                }
            }
        }
        list.sort(Comparator.comparing(ProductBalancerResponseDTO::getProductName));
        return list;
    }

    public List<ProductBalancerResponseDTO> getConsumableKin(ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Kindergarten kindergarten = kindergartenRepository.findById(users.getKindergartenId()).get();

        List<ProductBalancerResponseDTO> list = new ArrayList<>();
        for (ProductBalancer productBalancer : kindergarten.getConsumable()) {
//            if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(0.003)) > 0) {
            list.add(productBalancerService.parse(productBalancer));
//            }
        }

//        for (ProductBalancerResponseDTO dto : list) {
//            for (ProductBalancer productBalancer : kindergarten.getReserve()) {   /////O`ZGARTIRILDI
//                if (dto.getProductId().equals(productBalancer.getProduct().getId())) {
//                    dto.setNumberPack(dto.getNumberPack().add(productBalancer.getNumberPack()));
//                    dto.setWeight(dto.getWeight().add(productBalancer.getWeight()));
//                }
//            }
//        }
//
//        for (ProductBalancer productBalancer : kindergarten.getReserve()) {
//            boolean res = true;
//            for (ProductBalancerResponseDTO productBalancerResponseDTO : list) {
//                if (productBalancer.getProduct().getId().equals(productBalancerResponseDTO.getProductId())) {
//                    res = false;
//                    break;
//                }
//            }
//            if (res) {
//                list.add(new ProductBalancerResponseDTO(productBalancer.getProduct().getName(),
//                        productBalancer.getProduct().getId(),
//                        productBalancer.getWeight(),
//                        productBalancer.getNumberPack(),
//                        productBalancer.getProduct().getPack()));
//            }
//        }
        list.sort(Comparator.comparing(ProductBalancerResponseDTO::getProductName));
        return list;
    }

    public List<ProductBalancerResponseDTO> getConsumableKindergarten(Integer id) {

        Kindergarten kindergarten = kindergartenRepository.findById(id).get();

        List<ProductBalancerResponseDTO> list = new ArrayList<>();
        for (ProductBalancer productBalancer : kindergarten.getConsumable()) {
//            if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(0.003)) > 0) {
            list.add(productBalancerService.parse(productBalancer));
//            }
        }

//        for (ProductBalancerResponseDTO dto : list) {
//            for (ProductBalancer productBalancer : kindergarten.getReserve()) {
//                if (dto.getProductId().equals(productBalancer.getProduct().getId())) {
//                    dto.setNumberPack(dto.getNumberPack().add(productBalancer.getNumberPack()));
//                    dto.setWeight(dto.getWeight().add(productBalancer.getWeight()));
//                }
//            }
//        }
//
//        for (ProductBalancer productBalancer : kindergarten.getReserve()) {
//            boolean res = true;
//            for (ProductBalancerResponseDTO productBalancerResponseDTO : list) {
//                if (productBalancer.getProduct().getId().equals(productBalancerResponseDTO.getProductId())) {
//                    res = false;
//                    break;
//                }
//            }
//            if (res) {
//                list.add(new ProductBalancerResponseDTO(productBalancer.getProduct().getName(),
//                        productBalancer.getProduct().getId(),
//                        productBalancer.getWeight(),
//                        productBalancer.getNumberPack(),
//                        productBalancer.getProduct().getPack()));
//            }
//        }
        list.sort(Comparator.comparing(ProductBalancerResponseDTO::getProductName));
        return list;
    }

    public List<ShippedProductsCompanyDTO> getInOutProductKin(ResponseUser responseUser, Long start, Long end) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Kindergarten kindergarten = kindergartenRepository.findById(users.getKindergartenId()).get();

        List<ShippedProductsCompanyDTO> list = new ArrayList<>();
        List<ShippedProductsCompany> arrayList;

        if (start != null && end != null) {
            arrayList = shippedProductsCompanyRepository.findAllByKindergarten(kindergarten);
        } else {
            arrayList = shippedProductsCompanyRepository.findAllByKindergartenAndStatus(kindergarten, Status.NEW.getName());
//            arrayList = shippedProductsCompanyRepository.findAllByKindergarten(kindergarten);
        }


        for (ShippedProductsCompany shippedProductsCompany : arrayList) {
            if (start != null && end != null) {
                if (shippedProductsCompany.getTimeOfShipment().getTime() > start && end > shippedProductsCompany.getTimeOfShipment().getTime()) {
                    list.add(shippedProductsCompanyService.parse(shippedProductsCompany));
                }
            } else {
                list.add(shippedProductsCompanyService.parse(shippedProductsCompany));
            }
        }

        list.sort(Comparator.comparing(ShippedProductsCompanyDTO::getTimeOfShipment));
        return list;
    }

    public List<ProductBalancerResponseDTO> getExistingProductCom(ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();

        List<ProductBalancerResponseDTO> list = new ArrayList<>();
        for (ProductBalancer productBalancer : company.getReserve()) {
            if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(0.003)) > 0) {
                list.add(productBalancerService.parse(productBalancer));
            }
        }
        list.sort(Comparator.comparing(ProductBalancerResponseDTO::getProductName));
        return list;
    }

    public List<ProductBalancerResponseDTO> getShouldBeSentCom(ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();

        List<ProductBalancerResponseDTO> list = new ArrayList<>();
        for (ProductBalancer productBalancer : company.getShouldBeSent()) {
            if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(0.003)) > 0) {
                list.add(productBalancerService.parse(productBalancer));
            }
        }
        list.sort(Comparator.comparing(ProductBalancerResponseDTO::getProductName));
        return list;
    }

    public List<ShippedProductsDTO> getInOut(ResponseUser responseUser, Long start, Long end) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();

        List<ShippedProductsDTO> list = new ArrayList<>();

        List<ShippedProducts> all = shippedProductsRepository.findAllByCompany(company);
        for (ShippedProducts shippedProducts : all) {

            if (start == null || end == null) {
                if (shippedProducts.getStatus().equals(Status.NEW.getName()))
                    list.add(shippedProductsService.parse(shippedProducts));
            } else if (start < shippedProducts.getTimeOfShipment().getTime() && end > shippedProducts.getTimeOfShipment().getTime()) {
                list.add(shippedProductsService.parse(shippedProducts));
            }
        }

        list.sort(Comparator.comparing(ShippedProductsDTO::getProductName));
        return list;
    }

    public StateMessage addShippedProductCom(ResponseUser responseUser, InOutDTO dto, UUID requiredId) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        boolean res = false;

        ProductsToBeShippedCompany productsToBeShippedCompany = productsToBeShippedCompanyRepository.findById(requiredId).get();

        if (productsToBeShippedCompany.getStatus().equals(Status.FULLY_SENT.getName())) {
            return new StateMessage("XATOLIK. Maxsulot yuborib bo`lingan", false);
        }
        if (dto.getWeight() == 0) {
            return new StateMessage("XATOLIK. 0 qiymatni yuborib bo`lmaydi", false);
        }
        if ((productsToBeShippedCompany.getProduct().getPack().compareTo(BigDecimal.valueOf(0)) > 0) && dto.getNumberPack() % 1 > 0) {
            return new StateMessage(productsToBeShippedCompany.getProduct().getName() + "ni yarimta yuborib bo`lmaydi", false);
        }
        if (productsToBeShippedCompany.getWeight().compareTo(BigDecimal.valueOf(dto.getWeight())) < 0) {
            return new StateMessage("XATOLIK. Talab qilingan miqdordan ko`p kiritildi", false);
        }

        for (ProductBalancer productBalancer : productsToBeShippedCompany.getCompany().getShouldBeSent()) {
            if (productBalancer.getProduct().equals(productsToBeShippedCompany.getProduct())) {
                if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(dto.getWeight())) < 0) {
                    return new StateMessage("XATOLIK. Ushbu maxsulotdan yuborilayotgan miqdorda mavjud emas.", false);
                } else {
                    productBalancer.setWeight(productBalancer.getWeight().subtract(BigDecimal.valueOf(dto.getWeight())));
                    productBalancer.setNumberPack(productBalancer.getNumberPack().subtract(BigDecimal.valueOf(dto.getNumberPack())));
                    productBalancerRepository.save(productBalancer);
                    res = true;
                }
            }
        }

        if (res) {

            Kindergarten kindergarten = productsToBeShippedCompany.getKindergarten();
            ShippedProductsCompany shippedProductsCompany;

            if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {
                shippedProductsCompany = shippedProductsCompanyRepository.save(new ShippedProductsCompany(
                        productsToBeShippedCompany.getProduct(),
                        productsToBeShippedCompany.getCompany(),
                        productsToBeShippedCompany.getOrderMenu(),
                        productsToBeShippedCompany.getKindergarten(),
                        BigDecimal.valueOf(dto.getWeight()),
                        BigDecimal.valueOf(dto.getNumberPack()),
                        new Timestamp(System.currentTimeMillis()),
                        users,
                        Status.NEW.getName(),
                        dto.getComment(),
                        productsToBeShippedCompany.getAgreementNumber()
                ));

            } else {

                shippedProductsCompany = shippedProductsCompanyRepository.save(new ShippedProductsCompany(
                        productsToBeShippedCompany.getProduct(),
                        productsToBeShippedCompany.getCompany(),
                        productsToBeShippedCompany.getOrderMenu(),
                        productsToBeShippedCompany.getKindergarten(),
                        BigDecimal.valueOf(dto.getWeight()),
                        BigDecimal.valueOf(dto.getWeight()),
                        BigDecimal.valueOf(dto.getNumberPack()),
                        BigDecimal.valueOf(dto.getNumberPack()),
                        new Timestamp(System.currentTimeMillis()),
                        dto.getComment(),
                        new Timestamp(System.currentTimeMillis()),
                        users,
                        users,
                        Status.ACCEPTED.getName(),
                        productsToBeShippedCompany.getAgreementNumber()
                ));

            }

            productsToBeShippedCompany.setSuccessWeight((productsToBeShippedCompany.getSuccessWeight() != null ? productsToBeShippedCompany.getSuccessWeight() : BigDecimal.valueOf(0)).add(BigDecimal.valueOf(dto.getWeight())));
            productsToBeShippedCompany.setSuccessNumberPack((productsToBeShippedCompany.getSuccessNumberPack() != null ? productsToBeShippedCompany.getSuccessNumberPack() : BigDecimal.valueOf(0)).add(BigDecimal.valueOf(dto.getNumberPack())));

            productsToBeShippedCompany.setWeight(productsToBeShippedCompany.getWeight().subtract(BigDecimal.valueOf(dto.getWeight())));
            productsToBeShippedCompany.setNumberPack(productsToBeShippedCompany.getNumberPack().subtract(BigDecimal.valueOf(dto.getNumberPack())));
            if (productsToBeShippedCompany.getWeight().compareTo(BigDecimal.valueOf(0)) == 0) {
                productsToBeShippedCompany.setStatus(Status.FULLY_SENT.getName());
            } else {
                productsToBeShippedCompany.setStatus(Status.QISMAN_YUBORILDI.getName());
            }
            shippingCompanyRepository.save(new ShippingCompany(productsToBeShippedCompany, shippedProductsCompany));
            productsToBeShippedCompanyRepository.save(productsToBeShippedCompany);
            return new StateMessage("Muvaffaqiyatli yuborildi", true);
        }
        return new StateMessage("XATOLIK. Ushbu maxsulotdan yuborilayotgan miqdorda mavjud emas.", false);
    }

    public List<ProductsToBeShippedCompanyDTO> getRequiredProductCom(Integer id, ResponseUser responseUser, Long start, Long end) {


        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();
        Kindergarten kindergarten = kindergartenRepository.findById(id).get();
        List<ProductsToBeShippedCompany> kinList;

        if (start == null || end == null) {
            kinList = productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.NEW.getName());
            kinList.addAll(productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.QISMAN_YUBORILDI.getName()));
        } else {
            kinList = productsToBeShippedCompanyRepository.findAllByCompanyAndKindergarten(company, kindergarten);
        }

        List<ProductsToBeShippedCompanyDTO> list = new ArrayList<>();

        for (ProductsToBeShippedCompany productsToBeShippedCompany : kinList) {
            if (start != null && end != null) {
                if (start < productsToBeShippedCompany.getRequestDate().getTime() && end > productsToBeShippedCompany.getRequestDate().getTime()) {
                    list.add(productsToBeShippedCompanyService.parse(productsToBeShippedCompany));
                }
            } else {
                list.add(productsToBeShippedCompanyService.parse(productsToBeShippedCompany));
            }
        }
        list.sort(Comparator.comparing(ProductsToBeShippedCompanyDTO::getProductName));
        return list;
    }

    public List<ShippedProductsCompanyDTO> getShippedProductCom(Integer id, ResponseUser responseUser, Long start, Long end) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();

        Kindergarten kindergarten = kindergartenRepository.findById(id).get();


        List<ShippedProductsCompany> allByCompany;

        if (start != null && end != null) {
            allByCompany = shippedProductsCompanyRepository.findAllByCompanyAndKindergarten(company, kindergarten);
        } else {
            allByCompany = shippedProductsCompanyRepository.findAllByCompanyAndStatusAndKindergarten(company, Status.NEW.getName(), kindergarten);
        }

        List<ShippedProductsCompanyDTO> list = new ArrayList<>();

        for (ShippedProductsCompany shippedProductsCompany : allByCompany) {
            if (start != null && end != null) {
                if (start < shippedProductsCompany.getTimeOfShipment().getTime() && end > shippedProductsCompany.getTimeOfShipment().getTime()) {
                    list.add(shippedProductsCompanyService.parse(shippedProductsCompany));
                }
            } else {
                list.add(shippedProductsCompanyService.parse(shippedProductsCompany));
            }
        }


        list.sort(Comparator.comparing(ShippedProductsCompanyDTO::getProductName));
        return list;
    }

    public StateMessage receiveCom(UUID id, ReceiveDto dto, ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        BigDecimal zero = BigDecimal.valueOf(0);
        ShippedProducts shippedProducts = shippedProductsRepository.findById(id).get();

        if ((shippedProducts.getProduct().getPack().compareTo(zero) > 0) && (dto.getNumberPack() % 1 > 0)) {
            return new StateMessage(shippedProducts.getProduct() + " ni yarimta qabul qilib bo`lmaydi", false);
        }
        if ((shippedProducts.getSendWeight().subtract(shippedProducts.getSuccessWeight() != null ? shippedProducts.getSuccessWeight() : zero)).compareTo(BigDecimal.valueOf(dto.getWeight())) < 0) {
            return new StateMessage("Siz yuborilgan miqdordan ko`p qabul qila olmaysiz", false);
        }


        Optional<Shipping> optional = shippingRepository.findByShippedProducts(shippedProducts);
        if (optional.isPresent()) {
            Shipping shipping = optional.get();
            ProductsToBeShipped productsToBeShipped = shipping.getProductsToBeShipped();


            if (dto.getWeight() == 0) {
                productsToBeShipped.setSuccessWeight(productsToBeShipped.getSuccessWeight().subtract(shippedProducts.getSendWeight()));
                productsToBeShipped.setSuccessNumberPack(productsToBeShipped.getSuccessNumberPack().subtract(shippedProducts.getSendNumberPack()));
                productsToBeShipped.setNumberPack(productsToBeShipped.getNumberPack().add(shippedProducts.getSendNumberPack()));
                productsToBeShipped.setWeight(productsToBeShipped.getWeight().add(shippedProducts.getSendWeight()));


                shippedProducts.setStatus(Status.REJECTED.getName());
                shippedProducts.setReceiver(users);
                shippedProducts.setSuccessNumberPack(zero);
                shippedProducts.setSendWeight(zero);

                if (productsToBeShipped.getSuccessWeight().compareTo(zero) == 0) {
                    productsToBeShipped.setStatus(Status.NEW.getName());
                } else {
                    productsToBeShipped.setStatus(Status.QISMAN_YUBORILDI.getName());
                }
            } else if (BigDecimal.valueOf(dto.getWeight()).compareTo(shippedProducts.getSendWeight()) == 0) {
                shippedProducts.setStatus(Status.ACCEPTED.getName());
                shippedProducts.setSuccessNumberPack(shippedProducts.getSendNumberPack());
                shippedProducts.setSuccessWeight(shippedProducts.getSendWeight());

                addStorage(shippedProducts.getCompany(), shippedProducts.getProduct(), shippedProducts.getSendWeight(), shippedProducts.getSendNumberPack(), shippedProducts.getOrderMenu());

            } else {
                shippedProducts.setStatus(Status.PARTIALLY_ACCEPTED.getName());
                shippedProducts.setSuccessNumberPack(BigDecimal.valueOf(dto.getNumberPack()));
                shippedProducts.setSuccessWeight(BigDecimal.valueOf(dto.getWeight()));


                productsToBeShipped.setSuccessWeight(productsToBeShipped.getSuccessWeight().subtract(shippedProducts.getSendWeight().subtract(shippedProducts.getSuccessWeight())));
                productsToBeShipped.setSuccessNumberPack(productsToBeShipped.getSuccessNumberPack().subtract(shippedProducts.getSendNumberPack().subtract(shippedProducts.getSuccessNumberPack())));
                productsToBeShipped.setNumberPack(productsToBeShipped.getNumberPack().add(shippedProducts.getSendNumberPack().subtract(shippedProducts.getSuccessNumberPack())));
                productsToBeShipped.setWeight(productsToBeShipped.getWeight().add(shippedProducts.getSendWeight().subtract(shippedProducts.getSuccessWeight())));

                if (productsToBeShipped.getSuccessWeight().compareTo(zero) == 0) {
                    productsToBeShipped.setStatus(Status.NEW.getName());
                } else {
                    productsToBeShipped.setStatus(Status.QISMAN_YUBORILDI.getName());
                }

                addStorage(shippedProducts.getCompany(), shippedProducts.getProduct(), shippedProducts.getSuccessWeight(), shippedProducts.getSuccessNumberPack(), shippedProducts.getOrderMenu());
            }

            productsToBeShippedRepository.save(productsToBeShipped);

            shippedProducts.setReceiver(users);
            shippedProducts.setTimeTaken(new Timestamp(System.currentTimeMillis()));
            shippedProductsRepository.save(shippedProducts);

            return new StateMessage("Muvaffaqiyatli qabul qilindi", true);
        } else {

            return new StateMessage("XATOLIK", false);
        }
    }

    public void addStorage(Company company, Product product, BigDecimal weight, BigDecimal numberPack, OrderMenu orderMenu) {

        List<ProductBalancer> list = new ArrayList<>();

        if (orderMenu == null) {
            list.addAll(company.getReserve());
        } else {
            list.addAll(company.getShouldBeSent());
        }
        boolean res = true;
        for (ProductBalancer productBalancer : list) {
            if (productBalancer.getProduct().equals(product)) {
                productBalancer.setWeight(productBalancer.getWeight().add(weight));
                productBalancer.setNumberPack(productBalancer.getNumberPack().add(numberPack));
                res = false;
                productBalancerRepository.save(productBalancer);
                break;
            }
        }
        if (res) {
            list.add(productBalancerRepository.save(new ProductBalancer(product, weight, numberPack)));
            if (orderMenu == null) {
                company.setReserve(list);
            } else {
                company.setShouldBeSent(list);
            }
            companyRepository.save(company);
        }
    }

    public void addStorageKin(Kindergarten kindergarten, Product product, BigDecimal weight, BigDecimal numberPack) {


        List<ProductBalancer> list = new ArrayList<>(kindergarten.getConsumable());

        boolean res = true;
        for (ProductBalancer productBalancer : list) {
            if (productBalancer.getProduct().equals(product)) {
                productBalancer.setWeight(productBalancer.getWeight().add(weight));
                productBalancer.setNumberPack(productBalancer.getNumberPack().add(numberPack));
                res = false;
                productBalancerRepository.save(productBalancer);
                break;
            }
        }
        if (res) {
            list.add(productBalancerRepository.save(new ProductBalancer(product, weight, numberPack)));

            kindergarten.setConsumable(list);

            kindergartenRepository.save(kindergarten);
        }
    }

//    public void addStorageKinZaxira(Kindergarten kindergarten, Product product, BigDecimal weight, BigDecimal numberPack) {
//
//
//        List<ProductBalancer> list = new ArrayList<>(kindergarten.getConsumable());  /////O`ZGARTIRILDI
//
//        boolean res = true;
//        for (ProductBalancer productBalancer : list) {
//            if (productBalancer.getProduct().equals(product)) {
//                productBalancer.setWeight(productBalancer.getWeight().add(weight));
//                productBalancer.setNumberPack(productBalancer.getNumberPack().add(numberPack));
//                res = false;
//                productBalancerRepository.save(productBalancer);
//                break;
//            }
//        }
//        if (res) {
//            list.add(productBalancerRepository.save(new ProductBalancer(product, weight, numberPack)));
//            kindergarten.setConsumable(list);  /////O`ZGARTIRILDI
//            kindergartenRepository.save(kindergarten);
//        }
//    }

    public List<KindergartenRequiredNumberDTO> gerKinderNumber(ResponseUser responseUser) {


        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();

        List<KindergartenRequiredNumberDTO> list = new ArrayList<>();

        for (Kindergarten kindergarten : company.getKindergarten()) {

            if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {


                KindergartenRequiredNumberDTO dto = new KindergartenRequiredNumberDTO();
                dto.setDistrict(kindergarten.getAddress().getDistrict().getName());
                dto.setName(kindergarten.getName());
                dto.setId(kindergarten.getId());
                List<ProductsToBeShippedCompany> kinList = productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.NEW.getName());
                kinList.addAll(productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.QISMAN_YUBORILDI.getName()));

                dto.setNumber(kinList.size());
                list.add(dto);
            }

        }
        list.sort(Comparator.comparing(KindergartenRequiredNumberDTO::getDistrict));
        return list;
    }

    public StateMessage receiveKin(UUID id, ReceiveDto dto, ResponseUser responseUser) {
        ShippedProductsCompany shippedProductsCompany = shippedProductsCompanyRepository.findById(id).get();

        if (shippedProductsCompany.getOrderMenu() == null) {

            if (shippedProductsCompany.getStatus().equals(Status.NEW.getName())) {

                Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
                BigDecimal zero = BigDecimal.valueOf(0);

                if ((shippedProductsCompany.getProduct().getPack().compareTo(zero) > 0) && (dto.getNumberPack() % 1 > 0)) {
                    return new StateMessage(shippedProductsCompany.getProduct() + " ni yarimta qabul qilib bo`lmaydi", false);
                }
                if ((shippedProductsCompany.getSendWeight().subtract(shippedProductsCompany.getSuccessWeight() != null ? shippedProductsCompany.getSuccessWeight() : zero)).compareTo(BigDecimal.valueOf(dto.getWeight())) < 0) {
                    return new StateMessage("Siz yuborilgan miqdordan ko`p qabul qila olmaysiz", false);
                }

                if (dto.getWeight() == 0) {

                    Company company = shippedProductsCompany.getCompany();

                    shippedProductsCompany.setStatus(Status.REJECTED.getName());
                    shippedProductsCompany.setReceiver(users);
                    shippedProductsCompany.setSuccessNumberPack(zero);
                    shippedProductsCompany.setSuccessWeight(zero);

                    addStorage(shippedProductsCompany.getCompany(), shippedProductsCompany.getProduct(), shippedProductsCompany.getSendWeight(), shippedProductsCompany.getSendNumberPack(), shippedProductsCompany.getOrderMenu());

                } else if (BigDecimal.valueOf(dto.getWeight()).compareTo(shippedProductsCompany.getSendWeight()) == 0) {
                    shippedProductsCompany.setStatus(Status.ACCEPTED.getName());
                    shippedProductsCompany.setSuccessNumberPack(shippedProductsCompany.getSendNumberPack());
                    shippedProductsCompany.setSuccessWeight(shippedProductsCompany.getSendWeight());

                    BigDecimal weightReserve;
                    BigDecimal numberPackReserve;

                    weightReserve = shippedProductsCompany.getSendWeight();

                    BigDecimal pack = shippedProductsCompany.getProduct().getPack();

                    if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {
                        pack = BigDecimal.valueOf(1);
                    }
                    numberPackReserve = weightReserve.divide(pack, 6, RoundingMode.HALF_UP);
                    addStorageKin(shippedProductsCompany.getKindergarten(), shippedProductsCompany.getProduct(), weightReserve, numberPackReserve);

                } else {
                    shippedProductsCompany.setStatus(Status.PARTIALLY_ACCEPTED.getName());
                    shippedProductsCompany.setSuccessNumberPack(BigDecimal.valueOf(dto.getNumberPack()));
                    shippedProductsCompany.setSuccessWeight(BigDecimal.valueOf(dto.getWeight()));

                    addStorage(shippedProductsCompany.getCompany(), shippedProductsCompany.getProduct(), shippedProductsCompany.getSendWeight().subtract(shippedProductsCompany.getSuccessWeight()), shippedProductsCompany.getSendNumberPack().subtract(shippedProductsCompany.getSuccessNumberPack()), shippedProductsCompany.getOrderMenu());

                    BigDecimal weightReserve;

                    BigDecimal numberPackReserve;

                    BigDecimal pack = shippedProductsCompany.getProduct().getPack();

                    if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {
                        pack = BigDecimal.valueOf(1);
                    }
                    weightReserve = shippedProductsCompany.getSuccessWeight();
                    numberPackReserve = weightReserve.divide(pack, 6, RoundingMode.HALF_UP);

                    addStorageKin(shippedProductsCompany.getKindergarten(), shippedProductsCompany.getProduct(), weightReserve, numberPackReserve);
                }
                shippedProductsCompany.setReceiver(users);
                shippedProductsCompany.setTimeTaken(new Timestamp(System.currentTimeMillis()));
                shippedProductsCompanyRepository.save(shippedProductsCompany);

                return new StateMessage("Muvaffaqiyatli qabul qilindi", true);
            }
            return new StateMessage("Qabul qilingan", false);
        } else {
            String a = shippedProductsCompany.getStatus();
            String b = Status.NEW.getName();

            if (a.equals(b)) {
//
//            List<WeightProduct> list = new ArrayList<>();
//
//            for (WeightProduct weightProduct : weightProductRepository.findAll()) {
//                if (weightProduct.getCheckWeight() == null) {
//                    weightProduct.setCheckWeight(weightProduct.getWeight());
//                    list.add(weightProduct);
//                }
//            }
//            weightProductRepository.saveAll(list);
                Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
                BigDecimal zero = BigDecimal.valueOf(0);

                if ((shippedProductsCompany.getProduct().getPack().compareTo(zero) > 0) && (dto.getNumberPack() % 1 > 0)) {
                    return new StateMessage(shippedProductsCompany.getProduct() + " ni yarimta qabul qilib bo`lmaydi", false);
                }
                if ((shippedProductsCompany.getSendWeight().subtract(shippedProductsCompany.getSuccessWeight() != null ? shippedProductsCompany.getSuccessWeight() : zero)).compareTo(BigDecimal.valueOf(dto.getWeight())) < 0) {
                    return new StateMessage("Siz yuborilgan miqdordan ko`p qabul qila olmaysiz", false);
                }


                Optional<ShippingCompany> optional = shippingCompanyRepository.findByShippedProductsCompany(shippedProductsCompany);
                if (optional.isPresent()) {
                    ShippingCompany shipping = optional.get();
                    ProductsToBeShippedCompany productsToBeShippedCompany = shipping.getProductsToBeShippedCompany();


                    if (dto.getWeight() == 0) {
                        productsToBeShippedCompany.setSuccessWeight(productsToBeShippedCompany.getSuccessWeight().subtract(shippedProductsCompany.getSendWeight()));
                        productsToBeShippedCompany.setSuccessNumberPack(productsToBeShippedCompany.getSuccessNumberPack().subtract(shippedProductsCompany.getSendNumberPack()));
                        productsToBeShippedCompany.setNumberPack(productsToBeShippedCompany.getNumberPack().add(shippedProductsCompany.getSendNumberPack()));
                        productsToBeShippedCompany.setWeight(productsToBeShippedCompany.getWeight().add(shippedProductsCompany.getSendWeight()));


                        shippedProductsCompany.setStatus(Status.REJECTED.getName());
                        shippedProductsCompany.setReceiver(users);
                        shippedProductsCompany.setSuccessNumberPack(zero);
                        shippedProductsCompany.setSuccessWeight(zero);

                        if (productsToBeShippedCompany.getSuccessWeight().compareTo(zero) == 0) {
                            productsToBeShippedCompany.setStatus(Status.NEW.getName());
                        } else {
                            productsToBeShippedCompany.setStatus(Status.QISMAN_YUBORILDI.getName());
                        }

//                addStorage(shippedProductsCompany.getCompany(), shippedProductsCompany.getProduct(), shippedProductsCompany.getSendWeight(), shippedProductsCompany.getSendNumberPack(), shippedProductsCompany.getOrderMenu());
                        addStorage(shippedProductsCompany.getCompany(), shippedProductsCompany.getProduct(), shippedProductsCompany.getSendWeight(), shippedProductsCompany.getSendNumberPack(), shippedProductsCompany.getOrderMenu());

                    } else if (BigDecimal.valueOf(dto.getWeight()).compareTo(shippedProductsCompany.getSendWeight()) == 0) {
                        shippedProductsCompany.setStatus(Status.ACCEPTED.getName());
                        shippedProductsCompany.setSuccessNumberPack(shippedProductsCompany.getSendNumberPack());
                        shippedProductsCompany.setSuccessWeight(shippedProductsCompany.getSendWeight());

                        BigDecimal weightConsumable;

                        BigDecimal numberPackConsumable;
//                    if (shippedProductsCompany.getOrderMenu() != null) {
                        weightConsumable = shippedProductsCompany.getSuccessWeight();

                        BigDecimal pack = shippedProductsCompany.getProduct().getPack();

                        if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {
                            pack = BigDecimal.valueOf(1);
                        }
                        numberPackConsumable = weightConsumable.divide(pack, 6, RoundingMode.HALF_UP);
                        addStorageKin(shippedProductsCompany.getKindergarten(), shippedProductsCompany.getProduct(), weightConsumable, numberPackConsumable);


                    } else {
                        shippedProductsCompany.setStatus(Status.PARTIALLY_ACCEPTED.getName());
                        shippedProductsCompany.setSuccessNumberPack(BigDecimal.valueOf(dto.getNumberPack()));
                        shippedProductsCompany.setSuccessWeight(BigDecimal.valueOf(dto.getWeight()));


                        productsToBeShippedCompany.setSuccessWeight(productsToBeShippedCompany.getSuccessWeight().subtract(shippedProductsCompany.getSendWeight().subtract(shippedProductsCompany.getSuccessWeight())));
                        productsToBeShippedCompany.setSuccessNumberPack(productsToBeShippedCompany.getSuccessNumberPack().subtract(shippedProductsCompany.getSendNumberPack().subtract(shippedProductsCompany.getSuccessNumberPack())));
                        productsToBeShippedCompany.setNumberPack(productsToBeShippedCompany.getNumberPack().add(shippedProductsCompany.getSendNumberPack().subtract(shippedProductsCompany.getSuccessNumberPack())));
                        productsToBeShippedCompany.setWeight(productsToBeShippedCompany.getWeight().add(shippedProductsCompany.getSendWeight().subtract(shippedProductsCompany.getSuccessWeight())));


                        if (productsToBeShippedCompany.getSuccessWeight().compareTo(zero) == 0) {
                            productsToBeShippedCompany.setStatus(Status.NEW.getName());
                        } else {
                            productsToBeShippedCompany.setStatus(Status.QISMAN_YUBORILDI.getName());
                        }
                        addStorage(shippedProductsCompany.getCompany(), shippedProductsCompany.getProduct(), shippedProductsCompany.getSendWeight().subtract(shippedProductsCompany.getSuccessWeight()), shippedProductsCompany.getSendNumberPack().subtract(shippedProductsCompany.getSuccessNumberPack()), shippedProductsCompany.getOrderMenu());

                        BigDecimal weightConsumable;

                        BigDecimal numberPackConsumable;

                        BigDecimal pack = shippedProductsCompany.getProduct().getPack();

                        if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {
                            pack = BigDecimal.valueOf(1);
                        }

                        weightConsumable = shippedProductsCompany.getSuccessWeight();
                        numberPackConsumable = weightConsumable.divide(pack, 6, RoundingMode.HALF_UP);
                        addStorageKin(shippedProductsCompany.getKindergarten(), shippedProductsCompany.getProduct(), weightConsumable, numberPackConsumable);

                    }

                    productsToBeShippedCompanyRepository.save(productsToBeShippedCompany);

                    shippedProductsCompany.setReceiver(users);
                    shippedProductsCompany.setTimeTaken(new Timestamp(System.currentTimeMillis()));
                    shippedProductsCompanyRepository.save(shippedProductsCompany);

                    return new StateMessage("Muvaffaqiyatli qabul qilindi", true);
                }
            }
        }
        return new StateMessage("Qabul qilingan", false);
    }


    public List<CheckProductDTO> checkProduct(Integer id, ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        Company company = users.getCompany();
        Kindergarten kindergarten = kindergartenRepository.findById(id).get();


        List<ProductsToBeShippedCompany> kinList = productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.NEW.getName());
        kinList.addAll(productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.QISMAN_YUBORILDI.getName()));


        List<CheckProductDTO> list = new ArrayList<>(0);


        for (ProductsToBeShippedCompany productsToBeShippedCompany : kinList) {

            boolean res = true;
            for (ProductBalancer productBalancer : company.getShouldBeSent()) {
                if (productBalancer.getProduct().equals(productsToBeShippedCompany.getProduct())) {
                    if (productBalancer.getWeight().compareTo(productsToBeShippedCompany.getWeight()) >= 0) {
                        res = false;
                    }
                }
            }
            if (res) {
                list.add(new CheckProductDTO(productsToBeShippedCompany.getProduct().getName(), productsToBeShippedCompany.getWeight()));
            }
        }
        return list;
    }

    public StateMessage addShippedProductAllKin(Integer id, ResponseUser responseUser) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        Company company = users.getCompany();
        Kindergarten kindergarten = kindergartenRepository.findById(id).get();

        List<ProductsToBeShippedCompany> kinList = productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.NEW.getName());

        if (kinList.size() == 0) {
            return new StateMessage("Yuboriladigan maxsulotlar soni 0 ga teng", false);
        }

        kinList.addAll(productsToBeShippedCompanyRepository.findAllByCompanyAndKindergartenAndStatus(company, kindergarten, Status.QISMAN_YUBORILDI.getName()));

        List<ProductBalancer> productBalancerList = new ArrayList<>();
        List<ProductsToBeShippedCompany> productsToBeShippedCompanyList = new ArrayList<>();
        List<ShippedProductsCompany> shippedProductsCompanyList = new ArrayList<>();
        List<ShippingCompany> shippingCompanyList = new ArrayList<>();


        for (ProductsToBeShippedCompany productsToBeShippedCompany : kinList) {
            for (ProductBalancer productBalancer : company.getShouldBeSent()) {
                if (productBalancer.getProduct().equals(productsToBeShippedCompany.getProduct())) {
                    if (productBalancer.getWeight().compareTo(productsToBeShippedCompany.getWeight()) >= 0) {
                        BigDecimal numberPack = productsToBeShippedCompany.getNumberPack();
                        BigDecimal weight = productsToBeShippedCompany.getWeight();

                        productBalancer.setWeight(productBalancer.getWeight().subtract(weight));
                        productBalancer.setNumberPack(productBalancer.getNumberPack().subtract(numberPack));


                        productsToBeShippedCompany.setSuccessWeight((productsToBeShippedCompany.getSuccessWeight() != null ? productsToBeShippedCompany.getSuccessWeight() : BigDecimal.valueOf(0)).add(weight));
                        productsToBeShippedCompany.setSuccessNumberPack((productsToBeShippedCompany.getSuccessNumberPack() != null ? productsToBeShippedCompany.getSuccessNumberPack() : BigDecimal.valueOf(0)).add(numberPack));

                        productsToBeShippedCompany.setWeight(BigDecimal.valueOf(0));
                        productsToBeShippedCompany.setNumberPack(BigDecimal.valueOf(0));
                        productsToBeShippedCompany.setStatus(Status.FULLY_SENT.getName());

                        ShippedProductsCompany shippedProductsCompany = addShippedProductComALLKin(responseUser, numberPack, weight, productsToBeShippedCompany);
                        shippedProductsCompanyList.add(shippedProductsCompany);
                        productsToBeShippedCompanyList.add(productsToBeShippedCompany);
                        productBalancerList.add(productBalancer);

                        shippingCompanyList.add(new ShippingCompany(productsToBeShippedCompany, shippedProductsCompany));
                    }
                }
            }
        }
        productBalancerRepository.saveAll(productBalancerList);
        productsToBeShippedCompanyRepository.saveAll(productsToBeShippedCompanyList);
        shippedProductsCompanyRepository.saveAll(shippedProductsCompanyList);
        shippingCompanyRepository.saveAll(shippingCompanyList);
        return new StateMessage("Muvaffaqiyatli yuborildi", true);
    }

    public ShippedProductsCompany addShippedProductComALLKin(ResponseUser responseUser, BigDecimal numberPack, BigDecimal weight, ProductsToBeShippedCompany productsToBeShippedCompany) {
        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();


        Kindergarten kindergarten = productsToBeShippedCompany.getKindergarten();
        ShippedProductsCompany shippedProductsCompany;

        if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {
            shippedProductsCompany = new ShippedProductsCompany(
                    productsToBeShippedCompany.getProduct(),
                    productsToBeShippedCompany.getCompany(),
                    productsToBeShippedCompany.getOrderMenu(),
                    productsToBeShippedCompany.getKindergarten(),
                    weight,
                    numberPack,
                    new Timestamp(System.currentTimeMillis()),
                    users,
                    Status.NEW.getName(),
                    "AVTO YUBORISH",
                    productsToBeShippedCompany.getAgreementNumber()
            );

        } else {
            shippedProductsCompany = new ShippedProductsCompany(
                    productsToBeShippedCompany.getProduct(),
                    productsToBeShippedCompany.getCompany(),
                    productsToBeShippedCompany.getOrderMenu(),
                    productsToBeShippedCompany.getKindergarten(),
                    weight,
                    weight,
                    numberPack,
                    numberPack,
                    new Timestamp(System.currentTimeMillis()),
                    "AVTO YUBORISH",
                    new Timestamp(System.currentTimeMillis()),
                    users,
                    users,
                    Status.ACCEPTED.getName(),
                    productsToBeShippedCompany.getAgreementNumber()
            );
        }


        return shippedProductsCompany;

    }

//    public BigDecimal calculateWeight(OrderMenu orderMenu, Kindergarten kindergarten, Product product, BigDecimal weight) {
//
//        BigDecimal weightCheck = BigDecimal.valueOf(0);
//        BigDecimal weightOld = weight;
//
//        List<WeightProduct> save = new ArrayList<>();
//
//        for (OrderKindergarten orderKindergarten : orderMenu.getOrderKindergartenList()) {
//            if (orderKindergarten.getKindergarten().equals(kindergarten)) {
//
//                for (MenuWeight menuWeight : orderKindergarten.getMenuWeightList()) {
//                    for (WeightProduct weightProduct : menuWeight.getWeightProducts()) {
//                        if (weightProduct.getProduct().equals(product)) {
//                            weightCheck = weightCheck.add(weightProduct.getCheckWeight());
//                            if (weight.compareTo(BigDecimal.valueOf(0)) > 0) {
//                                if (weightProduct.getCheckWeight().compareTo(weight) > 0) {
//                                    weightProduct.setCheckWeight(weightProduct.getCheckWeight().subtract(weight));
//                                } else {
//                                    weightProduct.setCheckWeight(BigDecimal.valueOf(0));
//                                }
//                                save.add(weightProduct);
//                                weight.subtract(weightProduct.getCheckWeight());
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        weightProductRepository.saveAll(save);
//        return weightOld.subtract(weightCheck);
//    }

    public List<ShippingLetter> getShippingLetter(Integer id, ResponseUser responseUser, Long start, Long end) {

        List<ShippedProductsCompanyDTO> shippedProductCom = getShippedProductCom(id, responseUser, start, end);

        List<ShippingLetter> list = new ArrayList<>();
        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();
        Kindergarten kindergarten = kindergartenRepository.findById(id).get();

        for (ShippedProductsCompanyDTO shippedProductsCompanyDTO : shippedProductCom) {

            shippedProductsCompanyDTO.setPrice(getPrice(shippedProductsCompanyDTO.getAgreementNumber(), shippedProductsCompanyDTO.getProductId(), kindergarten));

            boolean res = true;


            for (ShippingLetter shippingLetter : list) {
                if (shippingLetter.getAgreementNumber().equals(shippedProductsCompanyDTO.getAgreementNumber())) {

                    List<ShippedProductsCompanyDTO> productList = shippingLetter.getProductList();
                    productList.add(shippedProductsCompanyDTO);
                    shippingLetter.setProductList(productList);
                    res = false;

                }
            }
            if (res) {
                ShippingLetter shippingLetter = new ShippingLetter();
                shippingLetter.setCompany(companyService.parse(company));
                shippingLetter.setKindergarten(kindergartenService.parse(kindergarten));
                shippingLetter.setAgreementNumber(shippedProductsCompanyDTO.getAgreementNumber());

                List<ShippedProductsCompanyDTO> shippedProductsCompanyDTOList = new ArrayList<>();
                shippedProductsCompanyDTOList.add(shippedProductsCompanyDTO);
                shippingLetter.setProductList(shippedProductsCompanyDTOList);
                list.add(shippingLetter);
            }
        }


        return list;
    }

    public BigDecimal getPrice(String agreementNumber, Integer productId, Kindergarten kindergarten) {

        if (kindergarten.getType().equals(KindergartenType.TAMINOT.getName())) {

            Optional<AgreementPrice> optional = agreementPriceRepository.findByAgreement_NumberAndProduct_Id(agreementNumber, productId);

            if (optional.isPresent()) {
                return optional.get().getPrice();
            }
        }
        return BigDecimal.valueOf(0);
    }

//    public StateMessage sengProduct(SendProductDTO dto, ResponseUser responseUser) {
//        Product product = productRepository.findById(dto.getProductId()).get();
//
//        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
//        Company company = users.getCompany();
//
//        ProductSupplier productSupplier = productSupplierRepository.findByCompanyAndProduct(company, product).get();
//
//
//        ProductsToBeShipped save = productsToBeShippedRepository.save(new ProductsToBeShipped(
//                productSupplier.getSupplier(),
//                product,
//                company,
//                null,
//                BigDecimal.valueOf(dto.getWeight()),
//                BigDecimal.valueOf(dto.getNumberPack()),
//                new Timestamp(System.currentTimeMillis()),
//                Status.NEW.getName()
//        ));
//
//        return new StateMessage("Muvafaqiyatli yuborildi",true);
//    }


    public StateMessage sendProductKindergarten(ResponseUser responseUser, Integer kindergartenId, List<SendProductDTO> list) {


        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();

        for (SendProductDTO sendProductDTO : list) {

            boolean res = false;

            for (ProductBalancer productBalancer : users.getCompany().getReserve()) {
                if (productBalancer.getProduct().getId().equals(sendProductDTO.getProductId())) {


                    if ((productBalancer.getProduct().getPack().compareTo(BigDecimal.valueOf(0)) > 0) && (sendProductDTO.getNumberPack() % 1 > 0)) {

                    } else {
                        if (!(productBalancer.getWeight().compareTo(BigDecimal.valueOf(sendProductDTO.getWeight())) < 0)) {
                            productBalancer.setWeight(productBalancer.getWeight().subtract(BigDecimal.valueOf(sendProductDTO.getWeight())));
                            productBalancer.setNumberPack(productBalancer.getNumberPack().subtract(BigDecimal.valueOf(sendProductDTO.getNumberPack())));
                            productBalancerRepository.save(productBalancer);
                            res = true;
                        }
                    }
                }
            }

            if (res) {

                Kindergarten kindergarten = kindergartenRepository.findById(kindergartenId).get();

                Product product = productRepository.findById(sendProductDTO.getProductId()).get();

                if (kindergarten.getType().equals(KindergartenType.OUTSOURCE.getName())) {
                    ShippedProductsCompany shippedProductsCompany = shippedProductsCompanyRepository.save(new ShippedProductsCompany(
                            product,
                            users.getCompany(),
                            null,
                            kindergarten,
                            BigDecimal.valueOf(sendProductDTO.getWeight()),
                            BigDecimal.valueOf(sendProductDTO.getNumberPack()),
                            new Timestamp(System.currentTimeMillis()),
                            users,
                            Status.NEW.getName(),
                            "MAXSUS BUYURTMA",
                            null
                    ));
                }
            }
        }
        return new StateMessage("Muvaffaqiyatli yuborildi", true);
    }

    public List<ProductBalancerResponseDTO> getNotExistingProduct(Integer id) {
        Kindergarten kindergarten = kindergartenRepository.findById(id).get();

//        List<MenuSave> menuList = menuSaveRepository.findAllByKindergartenAndStatus(kindergarten, Status.MAXSULOT_YUBORILDI.getName());
//        menuList.addAll(menuSaveRepository.findAllByKindergartenAndStatus(kindergarten, Status.SUCCESS.getName()));
//
//        List<ProductBalancerResponseDTO> list = new ArrayList<>();
//
//        for (MenuSave menuSave : menuList) {
//            List<WeightProduct> weightProductList = calculateMenuProduct(menuSave, null);
//            for (WeightProduct weightProduct : weightProductList) {
//
//                boolean res = true;
//
//                for (ProductBalancerResponseDTO productBalancerResponseDTO : list) {
//
//                    if (productBalancerResponseDTO.getProductId().equals(weightProduct.getProduct().getId())) {
//                        productBalancerResponseDTO.setWeight(productBalancerResponseDTO.getWeight().add(weightProduct.getWeight()));
//                        productBalancerResponseDTO.setNumberPack(productBalancerResponseDTO.getNumberPack().add(weightProduct.getWeight()));
//                        res = false;
//                    }
//                }
//
//                if (res) {
//                    ProductBalancerResponseDTO productBalancerResponseDTO = new ProductBalancerResponseDTO(weightProduct.getProduct().getName(), weightProduct.getProduct().getId(), weightProduct.getWeight(), weightProduct.getWeight(), weightProduct.getProduct().getPack());
//                    list.add(productBalancerResponseDTO);
//                }
//            }
//        }
//
//
//        for (ProductBalancerResponseDTO dto : list) {
//
//            if (dto.getPack().compareTo(BigDecimal.valueOf(0)) != 0) {
//                BigDecimal pack = dto.getPack();
//
//                BigDecimal numberPack = dto.getWeight().divide(pack, 0, RoundingMode.UP);
//                dto.setNumberPack(numberPack);
//                dto.setWeight(numberPack.multiply(pack));
//            }
//        }
//
//
//        List<ProductBalancerResponseDTO> returnList = new ArrayList<>();
//        List<ProductBalancerResponseDTO> consumableKindergarten = getConsumableKindergarten(kindergarten.getId());
//
//        for (ProductBalancerResponseDTO balancerResponseDTO : list) {
//
//            boolean res = true;
//
//            for (ProductBalancerResponseDTO productBalancerResponseDTO : consumableKindergarten) {
//
//                if (balancerResponseDTO.getProductId().equals(productBalancerResponseDTO.getProductId())) {
//                    if (balancerResponseDTO.getProductId() == 109) {
//                        System.out.println(balancerResponseDTO.getNumberPack());
//                    }
//
//                    if (balancerResponseDTO.getNumberPack().compareTo(productBalancerResponseDTO.getNumberPack()) > 0) {
//
//                        returnList.add(new ProductBalancerResponseDTO(productBalancerResponseDTO.getProductName(), productBalancerResponseDTO.getProductId(), balancerResponseDTO.getWeight().subtract(productBalancerResponseDTO.getWeight()), balancerResponseDTO.getNumberPack().subtract(productBalancerResponseDTO.getNumberPack()), productBalancerResponseDTO.getPack()));
//                    }
//                    res = false;
//                }
//
//
//            }
//            if (balancerResponseDTO.getProductId() == 109) {
//                System.out.println(balancerResponseDTO.getNumberPack());
//            }
//
//            if (res) {
//                returnList.add(new ProductBalancerResponseDTO(balancerResponseDTO.getProductName(), balancerResponseDTO.getProductId(), balancerResponseDTO.getWeight(), balancerResponseDTO.getNumberPack(), balancerResponseDTO.getPack()));
//            }
//        }
        List<ProductBalancerResponseDTO> list = new ArrayList<>();
        for (ProductBalancer productBalancer : kindergarten.getConsumable()) {    /////O`ZGARTIRILDI
            if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(0.003)) > 0) {
                ProductBalancerResponseDTO parse = productBalancerService.parse(productBalancer);

                BigDecimal weight = productBalancer.getWeight();
                List<MenuSave> menuList = menuSaveRepository.findAllByKindergartenAndStatus(kindergarten, Status.MAXSULOT_YUBORILDI.getName());

                for (MenuSave menuSave : menuList) {
                    CheckProduct checkProduct = checkProductRepository.findByKindergartenIdAndMenuSaveIdAndProductIdAndCondition(kindergarten.getId(), menuSave.getId(), productBalancer.getProduct().getId(), Status.MAXSULOT_YUBORILDI.getName());

                    weight = weight.subtract(checkProduct.getWeight());

                }

                if (weight.compareTo(BigDecimal.valueOf(0)) < 0) {
                    parse.setWeight(weight.multiply(BigDecimal.valueOf(1)));
                    BigDecimal pack = parse.getPack();
                    if (pack.compareTo(BigDecimal.valueOf(0)) > 0) {
                        parse.setNumberPack((weight.multiply(BigDecimal.valueOf(1))).divide(pack, 2, RoundingMode.HALF_UP));
                    } else {
                        parse.setNumberPack(weight.multiply(BigDecimal.valueOf(1)));
                    }
                    list.add(parse);
                }
            }
        }
        return list;
    }

    public List<ProductBalancerResponseDTO> getNotExistingProductAll(ResponseUser responseUser) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        List<ProductBalancerResponseDTO> list = new ArrayList<>();

        for (Kindergarten kindergarten : company.getKindergarten()) {


            for (ProductBalancer productBalancer : kindergarten.getConsumable()) {    /////O`ZGARTIRILDI
                if (productBalancer.getWeight().compareTo(BigDecimal.valueOf(0.003)) > 0) {

                    ProductBalancerResponseDTO parse = productBalancerService.parse(productBalancer);

                    BigDecimal weight = productBalancer.getWeight();
                    List<MenuSave> menuList = menuSaveRepository.findAllByKindergartenAndStatus(kindergarten, Status.MAXSULOT_YUBORILDI.getName());

                    for (MenuSave menuSave : menuList) {
                        CheckProduct checkProduct = checkProductRepository.findByKindergartenIdAndMenuSaveIdAndProductIdAndCondition(kindergarten.getId(), menuSave.getId(), productBalancer.getProduct().getId(), Status.MAXSULOT_YUBORILDI.getName());
                        weight = weight.subtract(checkProduct.getWeight());
                    }

                    if (weight.compareTo(BigDecimal.valueOf(0)) < 0) {
                        parse.setWeight(weight.multiply(BigDecimal.valueOf(1)));
                        BigDecimal pack = parse.getPack();
                        if (pack.compareTo(BigDecimal.valueOf(0)) > 0) {
                            parse.setNumberPack((weight.multiply(BigDecimal.valueOf(1))).divide(pack, 2, RoundingMode.HALF_UP));
                        } else {
                            parse.setNumberPack(weight.multiply(BigDecimal.valueOf(1)));
                        }

                        boolean res = true;
                        for (ProductBalancerResponseDTO productBalancerResponseDTO : list) {
                            if (productBalancerResponseDTO.getProductId().equals(productBalancer.getProduct().getId())) {
                                productBalancerResponseDTO.setWeight(productBalancerResponseDTO.getWeight().add(parse.getWeight()));
                                productBalancerResponseDTO.setNumberPack(productBalancerResponseDTO.getNumberPack().add(parse.getNumberPack()));
                                res = false;
                            }
                        }

                        if (res) {
                            list.add(parse);
                        }
                    }
                }
            }
        }

        return list;
    }

//    public List<WeightProduct> calculateMenuProduct(MenuSave ms, MenuWeight menuWeight) {
//        MenuSave menuSave = menuSaveRepository.findById(ms.getId()).get();
//
//        List<WeightProduct> list = new ArrayList<>();
//        PerDay numberToGuess;
//
//        if (menuSave.getNumberFact() != null) {
//            numberToGuess = menuSave.getNumberFact();
//        } else {
//
//            numberToGuess = menuSave.getNumberToGuess();
//        }
//
//
//        for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
//            for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {
//                Meal meal = mealRepository.findById(mealAgeStandardSave.getMealId()).get();
//                for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
//                    if (ageStandardSave.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
//                        BigDecimal weight = ageStandardSave.getWeight();
//                        for (NumberOfChildren numberOfChild : numberToGuess.getNumberOfChildren()) {
//                            if (numberOfChild.getAgeGroup().getId().equals(ageStandardSave.getAgeGroupId())) {
//                                BigDecimal mealWeight = weight.multiply(BigDecimal.valueOf(numberOfChild.getNumberOfKids()));
//
//                                for (ProductMeal productMeal : meal.getProductMeals()) {
//                                    BigDecimal mealFactWeight = meal.getWeight();
//                                    BigDecimal productWeight = productMeal.getWeight().divide(mealFactWeight, 8, RoundingMode.HALF_UP).multiply(mealWeight);
//                                    addListProduct(list, productMeal.getProduct(), productWeight, menuWeight);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        for (WeightProduct weightProduct : list) {
//            weightProduct.setCheckWeight(weightProduct.getWeight());
//        }
//        return list;
//    }

//    public void addListProduct(List<WeightProduct> list, Product product, BigDecimal weight, MenuWeight menuWeight) {
//
//        boolean res = true;
//
//        for (WeightProduct weightProduct : list) {
//            if (weightProduct.getProduct().equals(product)) {
//                weightProduct.setWeight(weightProduct.getWeight().add(weight));
////                if (weightProduct.getCheckWeight() == null){
////                    weightProduct.setCheckWeight(BigDecimal.valueOf(0));
////                }
////                weightProduct.setCheckWeight(weightProduct.getCheckWeight().add(weight));
//                res = false;
//            }
//        }
//        if (res) {
//            list.add(new WeightProduct(weight, product, menuWeight));
//        }
//    }

    public StateMessage sendingSpecifiedProducts(List<UUID> list, ResponseUser responseUser) {

        StringBuilder str = new StringBuilder();


        for (UUID id : list) {

            ProductsToBeShippedCompany productsToBeShippedCompany = productsToBeShippedCompanyRepository.findById(id).get();
            StateMessage stateMessage = addShippedProductCom(responseUser, new InOutDTO(Double.valueOf(productsToBeShippedCompany.getNumberPack().toString()), Double.valueOf(productsToBeShippedCompany.getWeight().toString()), "AVTO YUBORISH"), id);
            if (!stateMessage.isSuccess()) {
                str.append(productsToBeShippedCompany.getProduct().getName()).append(", ");
            }

        }

        if (str.length() > 0) {
            return new StateMessage("Ushbu maxsulotlarni yuborishning imkoni yo`q " + str, false);
        }

        return new StateMessage("Maxsulotlar to`liq yuborildi", true);
    }

    public List<StorageReportDTO> getStorageReport(Long start, Long end, ResponseUser responseUser) {

        start = start - 18000000;


        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();

        BigDecimal zero = BigDecimal.valueOf(0);

        List<StorageReportDTO> list = new ArrayList<>();

        for (ProductBalancer productBalancer : company.getReserve()) {
            StorageReportDTO dto = new StorageReportDTO(productBalancer.getProduct().getId(), productBalancer.getProduct().getName(), productBalancer.getProduct().getMeasurementType(), zero, zero, productBalancer.getNumberPack(), zero);
            list.add(dto);
        }


        for (ProductBalancer productBalancer : company.getShouldBeSent()) {
            boolean res = true;
            for (StorageReportDTO dto : list) {
                if (dto.getProductId().equals(productBalancer.getProduct().getId())) {
                    dto.setKunOxiriga(dto.getKunOxiriga().add(productBalancer.getNumberPack()));
                    res = false;
                }
            }
            if (res) {
                StorageReportDTO dto = new StorageReportDTO(productBalancer.getProduct().getId(), productBalancer.getProduct().getName(), productBalancer.getProduct().getMeasurementType(), zero, zero, productBalancer.getNumberPack(), zero);
                list.add(dto);
            }
        }


        List<ShippedProductsCompany> allByCompany = shippedProductsCompanyRepository.findAllByCompany(company);
        List<ShippedProductsCompany> sendList = new ArrayList<>();

        for (ShippedProductsCompany shippedProductsCompany : allByCompany) {

            Date startD = new Date(start);
            Date endD = new Date(end);
            Date shDate = new Date(shippedProductsCompany.getTimeOfShipment().getTime());


            if (start <= shippedProductsCompany.getTimeOfShipment().getTime() && end >= shippedProductsCompany.getTimeOfShipment().getTime()) {

                if (shippedProductsCompany.getSendNumberPack().compareTo(zero) > 0) {

                    if (shippedProductsCompany.getProduct().getName().equals("Makaron")) {
                        System.out.println("djfin");
                    }

                    sendList.add(shippedProductsCompany);
                }
            }
        }


        for (ShippedProductsCompany shippedProductsCompany : sendList) {
            boolean res = true;
            for (StorageReportDTO dto : list) {
                if (dto.getProductId().equals(shippedProductsCompany.getProduct().getId())) {
                    dto.setSendWeight(dto.getSendWeight().add(shippedProductsCompany.getSendNumberPack()));
                    res = false;
                }
            }

            if (res) {
                StorageReportDTO dto = new StorageReportDTO(shippedProductsCompany.getProduct().getId(), shippedProductsCompany.getProduct().getName(), shippedProductsCompany.getProduct().getMeasurementType(), shippedProductsCompany.getSendNumberPack(), zero, zero, zero);
                list.add(dto);
            }
        }

        List<ShippedProducts> inputList = new ArrayList<>();

        for (ShippedProducts shippedProducts : shippedProductsRepository.findAllByCompany(company)) {

            if (start <= shippedProducts.getTimeOfShipment().getTime() && end >= shippedProducts.getTimeOfShipment().getTime()) {
                inputList.add(shippedProducts);
            }
        }

        for (ShippedProducts shippedProducts : inputList) {
            boolean res = true;
            for (StorageReportDTO dto : list) {

                if (dto.getInputWeight() == null) {
                    dto.setInputWeight(zero);
                }

                if (shippedProducts.getProduct().getId().equals(dto.getProductId())) {
                    if (shippedProducts.getSuccessNumberPack() != null) {
                        dto.setInputWeight(dto.getInputWeight().add(shippedProducts.getSuccessNumberPack()));
                    } else {
                        dto.setInputWeight(dto.getInputWeight().add(zero));
                    }
                    res = false;
                }
            }
            if (res) {
                StorageReportDTO dto = new StorageReportDTO(shippedProducts.getProduct().getId(), shippedProducts.getProduct().getName(), shippedProducts.getProduct().getMeasurementType(), zero, shippedProducts.getSuccessNumberPack(), zero, zero);
                list.add(dto);
            }
        }


        for (StorageReportDTO dto : list) {
            dto.setKunBoshiga(dto.getKunOxiriga().add(dto.getSendWeight()).subtract(dto.getInputWeight()));
        }

        list.sort(Comparator.comparing(StorageReportDTO::getProductName));

        return list;
    }

    public List<ProductsToBeShippedDTO> getRequiredProduct(ResponseUser responseUser, Long start, Long end) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        List<ProductsToBeShippedDTO> list = new ArrayList<>();
        Company company = users.getCompany();

        if (start != null && end != null) {
            List<ProductsToBeShipped> allBySupplier = productsToBeShippedRepository.findAllByCompanyAndOrderMenu(company, null);
            for (ProductsToBeShipped productsToBeShipped : allBySupplier) {
                if (start < productsToBeShipped.getRequestDate().getTime() && end > productsToBeShipped.getRequestDate().getTime()) {
                    list.add(productsToBeShippedService.parse(productsToBeShipped));
                }
            }

        } else {

            List<ProductsToBeShipped> shippedList = productsToBeShippedRepository.findAllByCompanyAndStatusAndOrderMenu(company, Status.NEW.getName(), null);
            shippedList.addAll(productsToBeShippedRepository.findAllByCompanyAndStatusAndOrderMenu(company, Status.QISMAN_YUBORILDI.getName(), null));

            for (ProductsToBeShipped productsToBeShipped : shippedList) {
                list.add(productsToBeShippedService.parse(productsToBeShipped));
            }
        }

        list.sort(Comparator.comparing(ProductsToBeShippedDTO::getProductName));
        return list;
    }
}
