package com.smart_solution.outsource.storage.shipping;

import com.smart_solution.outsource.storage.productsToBeShipped.ProductsToBeShippedCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.UUID;

@Entity
public class ShippingCompany {

    @Id
    private UUID id = UUID.randomUUID();

    @OneToOne
    private ProductsToBeShippedCompany productsToBeShippedCompany;

    @OneToOne
    private ShippedProductsCompany shippedProductsCompany;

    public ShippingCompany(ProductsToBeShippedCompany productsToBeShippedCompany, ShippedProductsCompany shippedProductsCompany) {
        this.productsToBeShippedCompany = productsToBeShippedCompany;
        this.shippedProductsCompany = shippedProductsCompany;
    }

    public ShippingCompany() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ProductsToBeShippedCompany getProductsToBeShippedCompany() {
        return productsToBeShippedCompany;
    }

    public void setProductsToBeShippedCompany(ProductsToBeShippedCompany productsToBeShippedCompany) {
        this.productsToBeShippedCompany = productsToBeShippedCompany;
    }

    public ShippedProductsCompany getShippedProductsCompany() {
        return shippedProductsCompany;
    }

    public void setShippedProductsCompany(ShippedProductsCompany shippedProductsCompany) {
        this.shippedProductsCompany = shippedProductsCompany;
    }
}
