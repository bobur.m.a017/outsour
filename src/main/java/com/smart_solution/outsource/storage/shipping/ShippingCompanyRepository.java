package com.smart_solution.outsource.storage.shipping;

import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ShippingCompanyRepository extends JpaRepository<ShippingCompany, Integer> {
    Optional<ShippingCompany> findByShippedProductsCompany(ShippedProductsCompany shippedProductsCompany);

}
