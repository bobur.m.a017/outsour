package com.smart_solution.outsource.act;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class SupplierReport {
    private BigDecimal calculated;
    private BigDecimal paid;
    private BigDecimal balance;
    private Timestamp date;


    public SupplierReport(BigDecimal calculated, BigDecimal paid, BigDecimal balance, Timestamp date) {
        this.calculated = calculated;
        this.paid = paid;
        this.balance = balance;
        this.date = date;
    }

    public SupplierReport() {
    }

    public BigDecimal getCalculated() {
        return calculated;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setCalculated(BigDecimal calculated) {
        this.calculated = calculated;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
