package com.smart_solution.outsource.act;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class Accruals {

    private BigDecimal startTotalBalance;
    private Timestamp start;
    private Timestamp end;
    private List<SupplierReport> supplierReportList;
    private BigDecimal totalCalculated;
    private BigDecimal totalPaid;
    private BigDecimal endTotalBalance;


    private String supplierName;
    private String companyName;

    public Accruals(Timestamp start, Timestamp end, String supplierName, String companyName) {
        this.start = start;
        this.end = end;
        this.supplierName = supplierName;
        this.companyName = companyName;
    }

    public Accruals(Timestamp start, Timestamp end, List<SupplierReport> supplierReportList, BigDecimal totalCalculated, BigDecimal totalPaid, BigDecimal endTotalBalance, String supplierName, String companyName) {
        this.start = start;
        this.end = end;
        this.supplierReportList = supplierReportList;
        this.totalCalculated = totalCalculated;
        this.totalPaid = totalPaid;
        this.endTotalBalance = endTotalBalance;
        this.supplierName = supplierName;
        this.companyName = companyName;
    }

    public BigDecimal getStartTotalBalance() {
        return startTotalBalance;
    }

    public void setStartTotalBalance(BigDecimal startTotalBalance) {
        this.startTotalBalance = startTotalBalance;
    }

    public Accruals() {
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public List<SupplierReport> getSupplierReportList() {
        return supplierReportList;
    }

    public void setSupplierReportList(List<SupplierReport> supplierReportList) {
        this.supplierReportList = supplierReportList;
    }

    public BigDecimal getTotalCalculated() {
        return totalCalculated;
    }

    public void setTotalCalculated(BigDecimal totalCalculated) {
        this.totalCalculated = totalCalculated;
    }

    public BigDecimal getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(BigDecimal totalPaid) {
        this.totalPaid = totalPaid;
    }

    public BigDecimal getEndTotalBalance() {
        return endTotalBalance;
    }

    public void setEndTotalBalance(BigDecimal endTotalBalance) {
        this.endTotalBalance = endTotalBalance;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
