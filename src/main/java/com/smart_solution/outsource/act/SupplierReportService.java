package com.smart_solution.outsource.act;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.payment.Payment;
import com.smart_solution.outsource.payment.PaymentRepository;
import com.smart_solution.outsource.supplier.Supplier;
import com.smart_solution.outsource.supplier.SupplierRepository;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public record SupplierReportService(
        UsersRepository usersRepository,
        SupplierRepository supplierRepository,
        ShippedProductsRepository shippedProductsRepository,
        PaymentRepository paymentRepository
) {

    public Accruals getReport(Long start, Long end, Integer supplierId, ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();
        Company company = users.getCompany();
        Supplier supplier = supplierRepository.findById(supplierId).get();

        Accruals accruals = new Accruals(new Timestamp(start), new Timestamp(end), supplier.getName(), company.getName());
        BigDecimal calculated = addCalculated(accruals, start, end, company, supplier);
        BigDecimal payment = addPayment(start, end, company, supplier, accruals);
        BigDecimal zero = BigDecimal.valueOf(0);

        BigDecimal totalCalculated = zero;
        BigDecimal totalPayment = zero;
        BigDecimal totalBalance = zero;

        for (SupplierReport supplierReport : accruals.getSupplierReportList()) {
            if (supplierReport.getPaid() == null) {
                supplierReport.setPaid(zero);
            }
            if (supplierReport.getCalculated() == null) {
                supplierReport.setCalculated(zero);
            }
            supplierReport.setBalance(supplierReport.getCalculated().subtract(supplierReport.getPaid()));

            totalCalculated = totalCalculated.add(supplierReport.getCalculated());
            totalPayment = totalPayment.add(supplierReport.getPaid());
            totalBalance = totalBalance.add(supplierReport.getBalance());
        }
        accruals.setStartTotalBalance((calculated.subtract(payment)).subtract(totalBalance));
        accruals.setTotalPaid(totalPayment);
        accruals.setTotalCalculated(totalCalculated);
        accruals.setEndTotalBalance((calculated.subtract(payment)));

        return accruals;
    }


    public BigDecimal addPayment(Long start, Long end, Company company, Supplier supplier, Accruals accruals) {

        BigDecimal sum = BigDecimal.valueOf(0);

        List<Payment> list = paymentRepository.findAllByCompanyAndSupplier(company, supplier);
        List<SupplierReport> reportList = accruals.getSupplierReportList();

        for (Payment payment : list) {
            if (start <= payment.getTimeOfPayment().getTime() && end >= payment.getTimeOfPayment().getTime()) {
                addPaymentList(reportList, payment);
            }
            sum = sum.add(payment.getTotalSum());
        }

        accruals.setSupplierReportList(reportList);
        return sum;
    }

    public void addPaymentList(List<SupplierReport> reportList, Payment payment) {

        boolean res = true;
        for (SupplierReport supplierReport : reportList) {
            Timestamp date = supplierReport.getDate();
            Timestamp timeOfPayment = payment.getTimeOfPayment();
            if (timeOfPayment.getYear() == date.getYear() && date.getMonth() == timeOfPayment.getMonth() && date.getDate() == timeOfPayment.getDate()) {

                if (supplierReport.getPaid()==null) {
                    supplierReport.setPaid(BigDecimal.valueOf(0));
                }

                supplierReport.setPaid(supplierReport.getPaid().add(payment.getTotalSum()));
                res = false;
            }
        }
        if (res) {
            SupplierReport supplierReport = new SupplierReport();
            supplierReport.setDate(payment.getTimeOfPayment());
            supplierReport.setPaid(payment.getTotalSum());
            reportList.add(supplierReport);
        }
    }


    public BigDecimal addCalculated(Accruals accruals, Long start, Long end, Company company, Supplier supplier) {
        List<ShippedProducts> list = shippedProductsRepository.findAllByCompanyAndSupplier(company, supplier);

        BigDecimal sum = BigDecimal.valueOf(0);

        List<SupplierReport> reportList = new ArrayList<>();

        for (ShippedProducts shippedProducts : list) {
            if (start <= shippedProducts.getTimeTaken().getTime() && end >= shippedProducts.getTimeTaken().getTime()) {
                addCalculatedList(reportList, shippedProducts);
            }
            sum = sum.add(shippedProducts.getSuccessNumberPack().multiply(shippedProducts.getPrice()));
        }
        accruals.setSupplierReportList(reportList);
        return sum;
    }

    public void addCalculatedList(List<SupplierReport> list, ShippedProducts shippedProducts) {


        boolean res = true;
        for (SupplierReport supplierReport : list) {
            Timestamp date = supplierReport.getDate();
            Timestamp timeTaken = shippedProducts.getTimeTaken();
            if (timeTaken.getYear() == date.getYear() && date.getMonth() == timeTaken.getMonth() && date.getDate() == timeTaken.getDate()) {
                supplierReport.setCalculated(supplierReport.getCalculated().add(shippedProducts.getSuccessNumberPack().multiply(shippedProducts.getPrice())));
                res = false;
            }
        }
        if (res) {
            SupplierReport supplierReport = new SupplierReport();
            supplierReport.setDate(shippedProducts.getTimeTaken());
            supplierReport.setCalculated(shippedProducts.getSuccessNumberPack().multiply(shippedProducts.getPrice()));
            list.add(supplierReport);
        }
    }
}
