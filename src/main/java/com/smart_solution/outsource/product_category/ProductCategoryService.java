package com.smart_solution.outsource.product_category;

import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.product.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public record ProductCategoryService(
        ProductCategoryRepository productCategoryRepository,
        ProductRepository productRepository
) {

    public StateMessage add(ProductCategoryDTO productCategoryDTO) {

        boolean res = productCategoryRepository.existsByName(productCategoryDTO.getName());

        if (!res) {
            productCategoryRepository.save(new ProductCategory(productCategoryDTO.getName()));

            return new StateMessage("Muvaffaqiyatli qo`shildi", true);

        } else {
            return new StateMessage("Bunday nomli sanpin categoriya mavjud iltimos tekshirib qaytadan kiriting", false);
        }
    }

    public StateMessage edit(ProductCategoryDTO productCategoryDTO, Integer id) {

        boolean res = productCategoryRepository.existsByName(productCategoryDTO.getName());

        if (!res) {
            ProductCategory productCategory = productCategoryRepository.findById(id).get();
            productCategory.setName(productCategoryDTO.getName());
            productCategoryRepository.save(productCategory);

            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

        } else {
            return new StateMessage("Bunday nomli sanpin categoriya mavjud iltimos tekshirib qaytadan kiriting", false);
        }
    }

    public List<ProductCategoryDTO> get() {

        List<ProductCategoryDTO> list = new ArrayList<>();

        for (ProductCategory productCategory : productCategoryRepository.findAll()) {
            list.add(new ProductCategoryDTO(productCategory.getId(), productCategory.getName()));
        }
        return list;
    }

    public StateMessage delete(Integer id) {
        boolean res = productRepository.existsByProductCategory_Id(id);

        if (!res){
            productCategoryRepository.deleteById(id);
        }

        return res ? new StateMessage("O`chirilmadi ushbu kategoriyaga maxsulot biriktirilgan",false) : new StateMessage("O`chirildi, qayta tiklab bo`lmaydi",true);
    }


}
