package com.smart_solution.outsource.product_category;


import lombok.Getter;
import lombok.Setter;


public class ProductCategoryDTO {


    private Integer id;
    private String name;

    public ProductCategoryDTO() {
    }

    public ProductCategoryDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
