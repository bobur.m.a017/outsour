package com.smart_solution.outsource.product_category;

import java.util.List;

public class TestPCDTO {

    private String name;
    private List<TestAGSDTO> sanpinAgeNormDTOList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TestAGSDTO> getSanpinAgeNormDTOList() {
        return sanpinAgeNormDTOList;
    }

    public void setSanpinAgeNormDTOList(List<TestAGSDTO> sanpinAgeNormDTOList) {
        this.sanpinAgeNormDTOList = sanpinAgeNormDTOList;
    }
}
