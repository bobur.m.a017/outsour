package com.smart_solution.outsource.report;

import com.smart_solution.outsource.company.CompanyResponseDTO;
import com.smart_solution.outsource.report.dto.DateAgeGroupSum;
import com.smart_solution.outsource.report.dto.ReportByProduct;
import com.smart_solution.outsource.report.dto.ReportDate;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNorm;

import java.util.List;

public class Report {
    private List<ReportByProduct> reportByProduct;          //1
    private List<ReportByProduct> reportByProduct_3_4;      //2
    private List<ReportByProduct> reportByProduct_4_7;      //3
    private List<ReportByProduct> reportByProduct_3_7;      //4
    private List<ReportByProduct> reportByProduct_xodim;    //5
    private List<ReportByProduct> reportByProduct_qisqa;    //6
    private List<DateAgeGroupSum> ageGroup_3_4;
    private List<DateAgeGroupSum> ageGroup_4_7;
    private List<DateAgeGroupSum> ageGroup_q_m;
    private List<DateAgeGroupSum> ageGroup_jami;
    private List<ReportDate> table_2;
    private CompanyResponseDTO company;
    private String nameString;
    private String name;
    private String district;


    public List<ReportByProduct> getReportByProduct_3_4() {
        return reportByProduct_3_4;
    }

    public void setReportByProduct_3_4(List<ReportByProduct> reportByProduct_3_4) {
        this.reportByProduct_3_4 = reportByProduct_3_4;
    }

    public List<ReportByProduct> getReportByProduct_4_7() {
        return reportByProduct_4_7;
    }

    public void setReportByProduct_4_7(List<ReportByProduct> reportByProduct_4_7) {
        this.reportByProduct_4_7 = reportByProduct_4_7;
    }

    public List<ReportByProduct> getReportByProduct_3_7() {
        return reportByProduct_3_7;
    }

    public void setReportByProduct_3_7(List<ReportByProduct> reportByProduct_3_7) {
        this.reportByProduct_3_7 = reportByProduct_3_7;
    }

    public List<ReportByProduct> getReportByProduct_xodim() {
        return reportByProduct_xodim;
    }

    public void setReportByProduct_xodim(List<ReportByProduct> reportByProduct_xodim) {
        this.reportByProduct_xodim = reportByProduct_xodim;
    }

    public List<ReportByProduct> getReportByProduct_qisqa() {
        return reportByProduct_qisqa;
    }

    public void setReportByProduct_qisqa(List<ReportByProduct> reportByProduct_qisqa) {
        this.reportByProduct_qisqa = reportByProduct_qisqa;
    }

    private List<SanpinMenuNorm> sanpinMenuNorms;


    public Report(List<ReportByProduct> reportByProduct, List<SanpinMenuNorm> sanpinMenuNorms) {
        this.reportByProduct = reportByProduct;
        this.sanpinMenuNorms = sanpinMenuNorms;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ReportDate> getTable_2() {
        return table_2;
    }

    public void setTable_2(List<ReportDate> table_2) {
        this.table_2 = table_2;
    }

    public CompanyResponseDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyResponseDTO company) {
        this.company = company;
    }

    public String getNameString() {
        return nameString;
    }

    public void setNameString(String nameString) {
        this.nameString = nameString;
    }

    public List<DateAgeGroupSum> getAgeGroup_3_4() {
        return ageGroup_3_4;
    }

    public void setAgeGroup_3_4(List<DateAgeGroupSum> ageGroup_3_4) {
        this.ageGroup_3_4 = ageGroup_3_4;
    }

    public List<DateAgeGroupSum> getAgeGroup_4_7() {
        return ageGroup_4_7;
    }

    public void setAgeGroup_4_7(List<DateAgeGroupSum> ageGroup_4_7) {
        this.ageGroup_4_7 = ageGroup_4_7;
    }

    public List<DateAgeGroupSum> getAgeGroup_q_m() {
        return ageGroup_q_m;
    }

    public void setAgeGroup_q_m(List<DateAgeGroupSum> ageGroup_q_m) {
        this.ageGroup_q_m = ageGroup_q_m;
    }

    public List<DateAgeGroupSum> getAgeGroup_jami() {
        return ageGroup_jami;
    }

    public void setAgeGroup_jami(List<DateAgeGroupSum> ageGroup_jami) {
        this.ageGroup_jami = ageGroup_jami;
    }

    public List<SanpinMenuNorm> getSanpinMenuNorms() {
        return sanpinMenuNorms;
    }

    public void setSanpinMenuNorms(List<SanpinMenuNorm> sanpinMenuNorms) {
        this.sanpinMenuNorms = sanpinMenuNorms;
    }

    public Report() {
    }

    public List<ReportByProduct> getReportByProduct() {
        return reportByProduct;
    }

    public void setReportByProduct(List<ReportByProduct> reportByProduct) {
        this.reportByProduct = reportByProduct;
    }
}
