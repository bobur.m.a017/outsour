package com.smart_solution.outsource.report;

import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.company.CompanyRepository;
import com.smart_solution.outsource.company.CompanyService;
import com.smart_solution.outsource.consumption.Consumption;
import com.smart_solution.outsource.consumption.ConsumptionRepository;
import com.smart_solution.outsource.consumption.number.ConsumptionByNumberOfChildren;
import com.smart_solution.outsource.consumption.number.ConsumptionByNumberOfChildrenRepository;
import com.smart_solution.outsource.consumption.number.product.ConsumptionProduct;
import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import com.smart_solution.outsource.kindergarten.KindergartenService;
import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.multiMenu.MultiMenu;
import com.smart_solution.outsource.multiMenu.MultiMenuRepository;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import com.smart_solution.outsource.order.orderKindergarten.menuWeight.MenuWeight;
import com.smart_solution.outsource.order.orderKindergarten.weightProduct.WeightProduct;
import com.smart_solution.outsource.perDay.PerDay;
import com.smart_solution.outsource.price.Price;
import com.smart_solution.outsource.price.PriceRepository;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productMeal.ProductMeal;
import com.smart_solution.outsource.report.dto.*;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNorm;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormRepository;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormResponseDTO;
import com.smart_solution.outsource.sanpinMenuNorm.SanpinMenuNormService;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormRepository;
import com.smart_solution.outsource.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormService;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategory;
import com.smart_solution.outsource.sanpin_catecory.SanpinCategoryRepository;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNorm;
import com.smart_solution.outsource.sanpin_catecory.sanpinAgeNorm.SanpinAgeNormRepository;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompany;
import com.smart_solution.outsource.storage.shippedProducts.ShippedProductsCompanyRepository;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProducts;
import com.smart_solution.outsource.supplier.shippedProducts.ShippedProductsRepository;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;

@Service
public record ReportService(
        ConsumptionRepository consumptionRepository,
        KindergartenRepository kindergartenRepository,
        ProductRepository productRepository,
        SanpinCategoryRepository sanpinCategoryRepository,
        AgeGroupSanpinNormRepository ageGroupSanpinNormRepository,
        SanpinMenuNormRepository sanpinMenuNormRepository,
        AgeGroupSanpinNormService ageGroupSanpinNormService,
        MenuSaveRepository menuSaveRepository,
        MealRepository mealRepository,
        PriceRepository priceRepository,
        CompanyService companyService,
        UsersRepository usersRepository,
        KindergartenService kindergartenService,
        AgeGroupRepository ageGroupRepository,
        SanpinAgeNormRepository sanpinAgeNormRepository,
        MultiMenuRepository multiMenuRepository,
        SanpinMenuNormService sanpinMenuNormService,
        CompanyRepository companyRepository,
        ShippedProductsCompanyRepository shippedProductsCompanyRepository,
        ShippedProductsRepository shippedProductsRepository,
        ConsumptionByNumberOfChildrenRepository consumptionByNumberOfChildrenRepository
) {


    public Report get(Long start, Long end, List<Integer> list, ResponseUser responseUser) {

        Users users = usersRepository.findByUserName(responseUser.getUsername()).get();


        List<Consumption> consumptions = new ArrayList<>();

        for (Integer id : list) {
            for (Consumption consumption : consumptionRepository.findAllByKindergartenId(id)) {
                if (start <= consumption.getDayDate().getTime() && end >= consumption.getDayDate().getTime()) {
                    consumptions.add(consumption);
                }
            }
        }
        String str = "";

        Report report = getOne(consumptions);
        if (report != null) {
            if (list.size() > 0) {
                Integer integer = list.get(0);
                Kindergarten kindergarten = kindergartenRepository.findById(integer).get();

                Date startDate = new Date(start);
                Date endDate = new Date(end);

                Calendar c = Calendar.getInstance();
                c.setTime(startDate);
                int yearS = c.get(Calendar.YEAR);
                int monthS = c.get(Calendar.MONTH) + 1;
                int dayS = c.get(Calendar.DAY_OF_MONTH);


                Calendar cm = Calendar.getInstance();
                cm.setTime(endDate);
                int yearE = cm.get(Calendar.YEAR);
                int monthE = cm.get(Calendar.MONTH) + 1;
                int dayE = cm.get(Calendar.DAY_OF_MONTH);

                if (list.size() > 1) {
                    str = kindergarten.getAddress().getDistrict().getName() + " DMTTlarida " + (dayS < 10 ? "0" + dayS : dayS) + "." + (monthS < 10 ? "0" + monthS : monthS) + "." + yearS + " - " + (dayE < 10 ? "0" + dayE : dayE) + "." + (monthE < 10 ? "0" + monthE : monthE) + "." + yearE + "  vaqt oralig`ida";


                    report.setName(kindergarten.getAddress().getDistrict().getName() + " MTB");
                } else {
                    str = kindergarten.getAddress().getDistrict().getName() + " " + kindergarten.getName() + "da " + (dayS < 10 ? "0" + dayS : dayS) + "." + (monthS < 10 ? "0" + monthS : monthS) + "." + yearS + " - " + (dayE < 10 ? "0" + dayE : dayE) + "." + (monthE < 10 ? "0" + monthE : monthE) + "." + yearE + "  vaqt oralig`ida";
                    report.setName(kindergarten.getName());
                }
                report.setNameString(str);
                report.setCompany(companyService.parse(users.getCompany()));
                report.setDistrict(kindergarten.getAddress().getDistrict().getName());


                Consumption consumption = consumptions.get(0);
                String multiMenuName = consumption.getMultiMenuName();
                Optional<MultiMenu> byName = multiMenuRepository.findByName(multiMenuName);
                if (byName.isPresent()) {
                    MultiMenu multiMenu = byName.get();
//                    List<SanpinMenuNormResponseDTO> normList = new ArrayList<>();
//
//                    for (SanpinMenuNorm sanpinMenuNorm : multiMenu.getSanpinMenuNormList()) {
//                        normList.add(sanpinMenuNormService.parse(sanpinMenuNorm));
//                    }
                    report.setSanpinMenuNorms(multiMenu.getSanpinMenuNormList());
                }


//                for (SanpinMenuNorm sanpinMenuNorm : report.getSanpinMenuNorms()) {
//                    for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {
////                        ageGroupSanpinNorm.se
//                    }
//                }
            }
        }


        return sort(report);
    }

    public Report getOne(List<Consumption> consumptions) {

        List<ReportByProduct> reportByProduct = new ArrayList<>(getList(consumptions));
        List<ReportByProduct> reportByProduct_3_4 = new ArrayList<>(getList(consumptions));
        List<ReportByProduct> reportByProduct_4_7 = new ArrayList<>(getList(consumptions));
        List<ReportByProduct> reportByProduct_3_7 = new ArrayList<>(getList(consumptions));
        List<ReportByProduct> reportByProduct_xodim = new ArrayList<>(getList(consumptions));
        List<ReportByProduct> reportByProduct_qisqa = new ArrayList<>(getList(consumptions));

        for (Consumption consumption : consumptions) {
            calculate(consumption, reportByProduct);
            calculate_3_4(consumption, reportByProduct_3_4);
            calculate_4_7(consumption, reportByProduct_4_7);
            calculate_3_7(consumption, reportByProduct_3_7);
            calculate_xodim(consumption, reportByProduct_xodim);
            calculate_qisqa(consumption, reportByProduct_qisqa);
        }

        List<SanpinMenuNorm> sanpinMenuNorms = null;

//        if (consumptions.size() > 0) {
//            sanpinMenuNorms = addSanPin(getSanpinDay(consumptions));
//            calculate(sanpinMenuNorms, consumptions);
//        }


        if (consumptions.size() > 0) {
            Report report = new Report(getPrice(consumptions.get(0), sortProduct(reportByProduct)), sanpinMenuNorms);

            report.setReportByProduct_3_4(getPrice(consumptions.get(0), sortProduct(reportByProduct_3_4)));
            report.setReportByProduct_4_7(getPrice(consumptions.get(0), sortProduct(reportByProduct_4_7)));
            report.setReportByProduct_3_7(getPrice(consumptions.get(0), sortProduct(reportByProduct_3_7)));
            report.setReportByProduct_xodim(getPrice(consumptions.get(0), sortProduct(reportByProduct_xodim)));
            report.setReportByProduct_qisqa(getPrice(consumptions.get(0), sortProduct(reportByProduct_qisqa)));

//            report.setAgeGroup_3_4(calculate456NEW(consumptions, 1));
//            report.setAgeGroup_4_7(calculate456NEW(consumptions, 2));
//            report.setAgeGroup_q_m(calculate456NEW(consumptions, 3));

            calculate7(report);
            calculateQ_M(report);
            calculate3_4(report);
            calculate4_7(report);


            calculateTable_2(report, consumptions);
            return report;
        }
        return null;
    }

    public List<ReportByProduct> getList(List<Consumption> consumptions) {
        List<ReportByProduct> list = new ArrayList<>();


        for (Product product : productRepository.findAll()) {
            ReportByProduct reportByProduct = new ReportByProduct();

            reportByProduct.setProductName(product.getName());
            reportByProduct.setTotalPrice(BigDecimal.valueOf(0));
            reportByProduct.setTotalWeight(BigDecimal.valueOf(0));
            reportByProduct.setPrice(BigDecimal.valueOf(0));

            List<DateWeight> dateWeightList = new ArrayList<>();
            for (Consumption consumption : consumptions) {

                boolean res = true;

                for (DateWeight dateWeight : dateWeightList) {
                    Date d = new Date(dateWeight.getDate().getTime());
                    Calendar c = Calendar.getInstance();
                    c.setTime(d);
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);


                    Date dm = new Date(consumption.getDayDate().getTime());
                    Calendar cm = Calendar.getInstance();
                    cm.setTime(dm);
                    int yearM = cm.get(Calendar.YEAR);
                    int monthM = cm.get(Calendar.MONTH);
                    int dayM = cm.get(Calendar.DAY_OF_MONTH);

                    if (year == yearM && month == monthM && day == dayM) {
                        res = false;
                    }
                }

                if (res) {
                    DateWeight dateWeight = new DateWeight();
                    dateWeight.setDate(consumption.getDayDate());
                    dateWeight.setWeight(BigDecimal.valueOf(0));
                    dateWeightList.add(dateWeight);
                }
            }
            reportByProduct.setDateWeightList(dateWeightList);


            list.add(reportByProduct);
        }

        return list;
    }

    public List<ReportByProduct> sortProduct(List<ReportByProduct> list) {

        for (ReportByProduct report : list) {
            BigDecimal weight = BigDecimal.valueOf(0);
            for (DateWeight dateWeight : report.getDateWeightList()) {
                weight = weight.add(dateWeight.getWeight());
            }
            report.setTotalWeight(weight);
            report.setTotalPrice(weight.multiply(report.getPrice()));
        }


        List<ReportByProduct> returnList = new ArrayList<>(list);
        for (ReportByProduct reportByProduct : list) {
            if (reportByProduct.getTotalWeight().compareTo(BigDecimal.valueOf(0)) == 0) {
                returnList.remove(reportByProduct);
            }
        }

        return returnList;
    }

//    public Report getOneTest(Consumption consumptionTest) {
//
//        List<ReportByProduct> list = new ArrayList<>();
//
//        List<Consumption> consumptionList = new ArrayList<>();
//
//
//        List<Consumption> consumptions = new ArrayList<>();
//        consumptions.add(consumptionTest);
//        consumptionList.add(consumptionTest);
//
//        for (Product product : productRepository.findAll()) {
//            ReportByProduct reportByProduct = new ReportByProduct();
//            reportByProduct.setProductName(product.getName());
//            reportByProduct.setTotalPrice(BigDecimal.valueOf(0));
//            reportByProduct.setTotalWeight(BigDecimal.valueOf(0));
//            reportByProduct.setPrice(BigDecimal.valueOf(0));
//
//            List<DateWeight> dateWeightList = new ArrayList<>();
//            for (Consumption consumption : consumptions) {
//
//                DateWeight dateWeight = new DateWeight();
//                dateWeight.setDate(consumption.getDayDate());
//                dateWeight.setWeight(BigDecimal.valueOf(0));
//                dateWeightList.add(dateWeight);
//            }
//            reportByProduct.setDateWeightList(dateWeightList);
//            list.add(reportByProduct);
//        }
//
//        for (Consumption consumption : consumptions) {
//            calculate(consumption, list);
//        }
//
//        for (ReportByProduct report : list) {
//            BigDecimal weight = BigDecimal.valueOf(0);
//            for (DateWeight dateWeight : report.getDateWeightList()) {
//                weight = weight.add(dateWeight.getWeight());
//            }
//            report.setTotalWeight(weight);
//            report.setTotalPrice(weight.multiply(report.getPrice()));
//        }
//        List<SanpinMenuNorm> sanpinMenuNorms = null;
//
//        if (consumptionList.size() > 0) {
//
//            sanpinMenuNorms = addSanPin(getSanpinDay(consumptionList));
//            calculate(sanpinMenuNorms, consumptionList);
//        }
//
//        List<ReportByProduct> returnList = new ArrayList<>(list);
//        for (ReportByProduct reportByProduct : list) {
//            if (reportByProduct.getTotalWeight().compareTo(BigDecimal.valueOf(0)) == 0) {
//                returnList.remove(reportByProduct);
//            }
//        }
//
//
//        Report report = new Report(returnList, sanpinMenuNorms);
//
//        return report;
//    }

    public void calculate(Consumption consumption, List<ReportByProduct> list) {
        for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {
            if (!children.getAgeGroupName().equals("Xodim")) {
                for (ConsumptionProduct consumptionProduct : children.getProductList()) {
                    Product product = productRepository.findById(consumptionProduct.getProductId()).get();
                    addList(list, product, consumptionProduct.getWeight(), BigDecimal.valueOf(0), consumption.getDayDate());
                }
            }
        }
    }

    public void calculate_3_4(Consumption consumption, List<ReportByProduct> list) {
        for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {

            if (children.getAgeGroupName().equals("3-4")) {

                for (ConsumptionProduct consumptionProduct : children.getProductList()) {
                    Product product = productRepository.findById(consumptionProduct.getProductId()).get();
                    addList(list, product, consumptionProduct.getWeight(), BigDecimal.valueOf(0), consumption.getDayDate());
                }
            }


        }
    }

    public void calculate_4_7(Consumption consumption, List<ReportByProduct> list) {
        for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {

            if (children.getAgeGroupName().equals("4-7")) {

                for (ConsumptionProduct consumptionProduct : children.getProductList()) {
                    Product product = productRepository.findById(consumptionProduct.getProductId()).get();
                    addList(list, product, consumptionProduct.getWeight(), BigDecimal.valueOf(0), consumption.getDayDate());
                }
            }
        }
    }

    public void calculate_qisqa(Consumption consumption, List<ReportByProduct> list) {
        for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {

            if (children.getAgeGroupName().equals("Qisqa muddatli")) {

                for (ConsumptionProduct consumptionProduct : children.getProductList()) {
                    Product product = productRepository.findById(consumptionProduct.getProductId()).get();
                    addList(list, product, consumptionProduct.getWeight(), BigDecimal.valueOf(0), consumption.getDayDate());
                }
            }


        }
    }

    public void calculate_xodim(Consumption consumption, List<ReportByProduct> list) {
        for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {

            if (children.getAgeGroupName().equals("Xodim")) {

                for (ConsumptionProduct consumptionProduct : children.getProductList()) {
                    Product product = productRepository.findById(consumptionProduct.getProductId()).get();
                    addList(list, product, consumptionProduct.getWeight(), BigDecimal.valueOf(0), consumption.getDayDate());
                }
            }


        }
    }

    public void calculate_3_7(Consumption consumption, List<ReportByProduct> list) {
        for (ConsumptionByNumberOfChildren children : consumption.getNumberOfChildrenList()) {

            if ((!children.getAgeGroupName().equals("Xodim")) && (!children.getAgeGroupName().equals("Qisqa muddatli"))) {

                for (ConsumptionProduct consumptionProduct : children.getProductList()) {
                    Product product = productRepository.findById(consumptionProduct.getProductId()).get();
                    addList(list, product, consumptionProduct.getWeight(), BigDecimal.valueOf(0), consumption.getDayDate());
                }
            }


        }
    }

    public List<ReportByProduct> getPrice(Consumption consumption, List<ReportByProduct> list) {

        Integer kindergartenId = consumption.getKindergartenId();

        for (ReportByProduct reportByProduct : list) {
            Product product = productRepository.findByName(reportByProduct.getProductName()).get();

            Date dm = new Date(consumption.getDayDate().getTime());
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int year = cm.get(Calendar.YEAR);
            int month = cm.get(Calendar.MONTH);

            Optional<Price> optional = priceRepository.findByKindergarten_IdAndYearAndMonthAndProduct_Id(kindergartenId, year, month, product.getId());
            if (optional.isPresent()) {
                Price price = optional.get();
                reportByProduct.setPrice(price.getPrice());
                reportByProduct.setTotalPrice(reportByProduct.getPrice().multiply(reportByProduct.getTotalWeight()));
            }
        }
        return list;
    }

    public BigDecimal getPrice(Consumption consumption, Product product) {

        Integer kindergartenId = consumption.getKindergartenId();

        Date dm = new Date(consumption.getDayDate().getTime());
        Calendar cm = Calendar.getInstance();
        cm.setTime(dm);
        int year = cm.get(Calendar.YEAR);
        int month = cm.get(Calendar.MONTH);

        Optional<Price> optional = priceRepository.findByKindergarten_IdAndYearAndMonthAndProduct_Id(kindergartenId, year, month, product.getId());
        if (optional.isPresent()) {
            Price price = optional.get();
            return price.getPrice();
        }


        return BigDecimal.valueOf(0);
    }

    public void addList(List<ReportByProduct> list, Product product, BigDecimal weight, BigDecimal price, Timestamp date) {

        boolean res = true;
        boolean ans = true;

        for (ReportByProduct reportByProduct : list) {
            List<DateWeight> dateWeightList = new ArrayList<>(reportByProduct.getDateWeightList());
            if (reportByProduct.getProductName().equals(product.getName())) {
                reportByProduct.setPrice(reportByProduct.getPrice().add(price).divide(BigDecimal.valueOf(2), 10, RoundingMode.HALF_UP));

                for (DateWeight dateWeight : dateWeightList) {
                    Date d = new Date(dateWeight.getDate().getTime());
                    Calendar c = Calendar.getInstance();
                    c.setTime(d);
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);


                    Date dm = new Date(date.getTime());
                    Calendar cm = Calendar.getInstance();
                    cm.setTime(dm);
                    int yearM = cm.get(Calendar.YEAR);
                    int monthM = cm.get(Calendar.MONTH);
                    int dayM = cm.get(Calendar.DAY_OF_MONTH);

                    if (year == yearM && month == monthM && day == dayM) {

                        if (product.getMeasurementType().equals("dona")) {
                            BigDecimal divide = weight.divide(product.getPack(), 10, RoundingMode.HALF_UP);
                            dateWeight.setWeight(dateWeight.getWeight().add(divide));
                        } else {
                            dateWeight.setWeight(dateWeight.getWeight().add(weight));
                        }


                        res = false;
                    }
                }
                if (res) {
                    dateWeightList.add(new DateWeight(date, weight));

                    res = false;
                }
                ans = false;
            }
            reportByProduct.setDateWeightList(dateWeightList);
        }

        if (res) {
            ReportByProduct report = new ReportByProduct();

            report.setProductName(product.getName());
            report.setPrice(price);
            List<DateWeight> dateWeightList = new ArrayList<>();

            dateWeightList.add(new DateWeight(date, weight));
            report.setDateWeightList(dateWeightList);
            list.add(report);
        }

    }

    public List<ReportByAgeGroup> calculateTotal(List<Consumption> consumptionList) {

        List<ReportByAgeGroup> list = new ArrayList<>();

        for (Consumption consumption : consumptionList) {
            boolean res = false;
            BigDecimal totalPrice = BigDecimal.valueOf(0);

            for (ReportByAgeGroup report : list) {
                Date d = new Date(report.getDate().getTime());
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);


                Date dm = new Date(consumption.getDayDate().getTime());
                Calendar cm = Calendar.getInstance();
                cm.setTime(dm);
                int yearM = cm.get(Calendar.YEAR);
                int monthM = cm.get(Calendar.MONTH);
                int dayM = cm.get(Calendar.DAY_OF_MONTH);

                if (year == yearM && month == monthM && day == dayM) {
                    report.setTotalPrice(report.getTotalPrice().add(totalPrice));

                    BigDecimal totalPriceVAT = report.getTotalPrice().divide(BigDecimal.valueOf(115), 10, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
                    report.setTotalPriceVAT(totalPriceVAT);

                    BigDecimal wages = totalPriceVAT.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(19));
                    report.setWages(wages);

                    report.setTotal(wages.add(totalPriceVAT));
                    res = true;
                }
            }

            if (!res) {

                ReportByAgeGroup report = new ReportByAgeGroup();

                report.setDate(consumption.getDayDate());

                report.setTotalPrice(totalPrice);

                BigDecimal totalPriceVAT = totalPrice.divide(BigDecimal.valueOf(115), 10, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
                report.setTotalPriceVAT(totalPriceVAT);

                BigDecimal wages = totalPriceVAT.divide(BigDecimal.valueOf(100), 10, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(19));
                report.setWages(wages);

                report.setTotal(wages.add(totalPriceVAT));

                report.setName("jami");

                list.add(report);
            }
        }
        return list;
    }

    public List<SanpinMenuNorm> addSanPin(Integer daily) {

        List<SanpinMenuNorm> list = new ArrayList<>();

        for (SanpinCategory sanpinCategory : sanpinCategoryRepository.findAll()) {
            SanpinMenuNorm sanpinMenuNorm = new SanpinMenuNorm();
            sanpinMenuNorm.setSanpinCategory(sanpinCategory);
            sanpinMenuNorm.setAgeGroupSanpinNormList(ageGroupSanpinNormService.add(daily, sanpinCategory));
            list.add(sanpinMenuNormRepository.save(sanpinMenuNorm));
        }
        return list;
    }

    public void calculate(List<SanpinMenuNorm> sanpinMenuNorms, List<Consumption> consumptionList) {

        for (SanpinMenuNorm sanpinMenuNorm : sanpinMenuNorms) {
            for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {

                IngredientReportDTO ingredient = calculateWeight(ageGroupSanpinNorm, consumptionList, sanpinMenuNorm.getSanpinCategory());
                ageGroupSanpinNorm.setDoneWeight(ingredient.getWeight());
                ageGroupSanpinNorm.setDoneProtein(ingredient.getProtein());
                ageGroupSanpinNorm.setDoneOil(ingredient.getOil());
                ageGroupSanpinNorm.setDoneKcal(ingredient.getKcal());
                ageGroupSanpinNorm.setDoneCarbohydrates(ingredient.getCarbohydrates());
                ageGroupSanpinNormRepository.save(ageGroupSanpinNorm);
            }
        }
    }

    public IngredientReportDTO calculateWeight(AgeGroupSanpinNorm ageGroupSanpinNorm, List<Consumption> consumptionList, SanpinCategory sanpinCategory) {

        BigDecimal weight = BigDecimal.valueOf(0);
        BigDecimal protein = BigDecimal.valueOf(0);

        BigDecimal kcal = BigDecimal.valueOf(0);

        BigDecimal oil = BigDecimal.valueOf(0);

        BigDecimal carbohydrates = BigDecimal.valueOf(0);

        MenuSave menuSave = null;

        for (Consumption consumption : consumptionList) {
            menuSave = menuSaveRepository.findById(consumption.getMenuId()).get();
            for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
                for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {
                    for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
                        if (ageStandardSave.getAgeGroupId().equals(ageGroupSanpinNorm.getAgeGroup().getId())) {

                            Meal meal = mealRepository.findById(mealAgeStandardSave.getMealId()).get();

                            for (ProductMeal productMeal : meal.getProductMeals()) {
                                if (productMeal.getProduct().getSanpinCategory().equals(sanpinCategory)) {

                                    if (productMeal.getWeight().compareTo(BigDecimal.valueOf(0)) != 0 && meal.getWeight().compareTo(BigDecimal.valueOf(0)) != 0) {
                                        BigDecimal weight1 = productMeal.getWeight().divide(meal.getWeight(), 10, RoundingMode.HALF_UP);
                                        weight = weight.add(weight1.multiply(ageStandardSave.getWeight()));


                                        Ingredient ingredient = productMeal.getIngredient();
                                        BigDecimal kcal1 = ingredient.getKcal().divide(meal.getWeight(), 10, RoundingMode.HALF_UP);
                                        kcal = kcal.add(kcal1.multiply(ageStandardSave.getWeight()));


                                        BigDecimal carbohydrates1 = ingredient.getCarbohydrates().divide(meal.getWeight(), 10, RoundingMode.HALF_UP);
                                        ;
                                        carbohydrates = carbohydrates.add(carbohydrates1.multiply(ageStandardSave.getWeight()));

                                        BigDecimal oil1 = ingredient.getOil().divide(meal.getWeight(), 10, RoundingMode.HALF_UP);
                                        ;
                                        oil = oil.add(oil1.multiply(ageStandardSave.getWeight()));

                                        BigDecimal protein1 = ingredient.getProtein().divide(meal.getWeight(), 10, RoundingMode.HALF_UP);
                                        ;
                                        protein = protein.add(protein1.multiply(ageStandardSave.getWeight()));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return new IngredientReportDTO(protein, kcal, oil, carbohydrates, weight);
    }

    public Integer getSanpinDay(List<Consumption> consumptionList) {

        List<Consumption> list = new ArrayList<>();


        for (Consumption consumption : consumptionList) {
            boolean res = false;
            for (Consumption consumption1 : list) {


                Date d = new Date(consumption.getDayDate().getTime());
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);


                Date dm = new Date(consumption1.getDayDate().getTime());
                Calendar cm = Calendar.getInstance();
                cm.setTime(dm);
                int yearM = cm.get(Calendar.YEAR);
                int monthM = cm.get(Calendar.MONTH);
                int dayM = cm.get(Calendar.DAY_OF_MONTH);

                if (year == yearM && month == monthM && day == dayM) {
                    res = true;
                }
            }
            if (!res) {
                list.add(consumption);
            }
        }
        return list.size();
    }

    public void calculate7(Report report) {
        List<DateAgeGroupSum> list = new ArrayList<>();
        for (ReportByProduct reportByProduct : report.getReportByProduct()) {

            for (DateWeight dateWeight : reportByProduct.getDateWeightList()) {

                boolean res = true;
                for (DateAgeGroupSum dateAgeGroupSum : list) {

                    Date d = new Date(dateWeight.getDate().getTime());
                    Calendar c = Calendar.getInstance();
                    c.setTime(d);
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);

                    Date dm = new Date(dateAgeGroupSum.getDate().getTime());
                    Calendar cm = Calendar.getInstance();
                    cm.setTime(dm);
                    int yearM = cm.get(Calendar.YEAR);
                    int monthM = cm.get(Calendar.MONTH);
                    int dayM = cm.get(Calendar.DAY_OF_MONTH);
                    if (year == yearM && month == monthM && day == dayM) {
                        dateAgeGroupSum.setSum(dateAgeGroupSum.getSum().add(dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                        res = false;
                        break;
                    }
                }
                if (res) {
                    list.add(new DateAgeGroupSum(dateWeight.getDate(), dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                }
            }
        }
        report.setAgeGroup_jami(list);
    }

    public void calculate3_4(Report report) {
        List<DateAgeGroupSum> list = new ArrayList<>();
        for (ReportByProduct reportByProduct : report.getReportByProduct_3_4()) {

            for (DateWeight dateWeight : reportByProduct.getDateWeightList()) {

                boolean res = true;
                for (DateAgeGroupSum dateAgeGroupSum : list) {

                    Date d = new Date(dateWeight.getDate().getTime());
                    Calendar c = Calendar.getInstance();
                    c.setTime(d);
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);

                    Date dm = new Date(dateAgeGroupSum.getDate().getTime());
                    Calendar cm = Calendar.getInstance();
                    cm.setTime(dm);
                    int yearM = cm.get(Calendar.YEAR);
                    int monthM = cm.get(Calendar.MONTH);
                    int dayM = cm.get(Calendar.DAY_OF_MONTH);
                    if (year == yearM && month == monthM && day == dayM) {
                        dateAgeGroupSum.setSum(dateAgeGroupSum.getSum().add(dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                        res = false;
                        break;
                    }
                }
                if (res) {
                    list.add(new DateAgeGroupSum(dateWeight.getDate(), dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                }
            }
        }
        report.setAgeGroup_3_4(list);
    }

    public void calculate4_7(Report report) {
        List<DateAgeGroupSum> list = new ArrayList<>();
        for (ReportByProduct reportByProduct : report.getReportByProduct_4_7()) {

            for (DateWeight dateWeight : reportByProduct.getDateWeightList()) {

                boolean res = true;
                for (DateAgeGroupSum dateAgeGroupSum : list) {

                    Date d = new Date(dateWeight.getDate().getTime());
                    Calendar c = Calendar.getInstance();
                    c.setTime(d);
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);

                    Date dm = new Date(dateAgeGroupSum.getDate().getTime());
                    Calendar cm = Calendar.getInstance();
                    cm.setTime(dm);
                    int yearM = cm.get(Calendar.YEAR);
                    int monthM = cm.get(Calendar.MONTH);
                    int dayM = cm.get(Calendar.DAY_OF_MONTH);
                    if (year == yearM && month == monthM && day == dayM) {
                        dateAgeGroupSum.setSum(dateAgeGroupSum.getSum().add(dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                        res = false;
                        break;
                    }
                }
                if (res) {
                    list.add(new DateAgeGroupSum(dateWeight.getDate(), dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                }
            }
        }
        report.setAgeGroup_4_7(list);
    }

    public void calculateQ_M(Report report) {
        List<DateAgeGroupSum> list = new ArrayList<>();
        for (ReportByProduct reportByProduct : report.getReportByProduct_qisqa()) {

            for (DateWeight dateWeight : reportByProduct.getDateWeightList()) {

                boolean res = true;
                for (DateAgeGroupSum dateAgeGroupSum : list) {

                    Date d = new Date(dateWeight.getDate().getTime());
                    Calendar c = Calendar.getInstance();
                    c.setTime(d);
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);

                    Date dm = new Date(dateAgeGroupSum.getDate().getTime());
                    Calendar cm = Calendar.getInstance();
                    cm.setTime(dm);
                    int yearM = cm.get(Calendar.YEAR);
                    int monthM = cm.get(Calendar.MONTH);
                    int dayM = cm.get(Calendar.DAY_OF_MONTH);
                    if (year == yearM && month == monthM && day == dayM) {
                        dateAgeGroupSum.setSum(dateAgeGroupSum.getSum().add(dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                        res = false;
                        break;
                    }
                }
                if (res) {
                    list.add(new DateAgeGroupSum(dateWeight.getDate(), dateWeight.getWeight().multiply(reportByProduct.getPrice())));
                }
            }
        }
        report.setAgeGroup_q_m(list);
    }

    public List<DateAgeGroupSum> calculate456(List<Consumption> consumptionList, Integer ageGroupId) {

        List<DateAgeGroupSum> list = new ArrayList<>();

        for (Consumption consumption : consumptionList) {
            for (ConsumptionByNumberOfChildren consumptionByNumberOfChildren : consumption.getNumberOfChildrenList()) {
                if (consumptionByNumberOfChildren.getAgeGroupId().equals(ageGroupId)) {
                    for (ConsumptionProduct consumptionProduct : consumptionByNumberOfChildren.getProductList()) {

                        Date d = new Date(consumption.getDayDate().getTime());
                        Calendar c = Calendar.getInstance();
                        c.setTime(d);
                        int year = c.get(Calendar.YEAR);
                        int month = c.get(Calendar.MONTH);
                        BigDecimal weight = consumptionProduct.getWeight();

                        Optional<Price> optional = priceRepository.findByKindergarten_IdAndYearAndMonthAndProduct_Id(consumption.getKindergartenId(), year, month, consumptionProduct.getProductId());
                        if (optional.isEmpty()) {
                            addListSum(consumption.getDayDate(), BigDecimal.valueOf(0), list);
                        } else {
                            Price price = optional.get();
                            addListSum(consumption.getDayDate(), weight.multiply(price.getPrice()), list);
                        }
                    }
                }
            }
        }
        return list;
    }


    public List<DateAgeGroupSum> calculate456NEW(List<Consumption> consumptionList, Integer ageGroupId) {

        List<DateAgeGroupSum> list = new ArrayList<>();

        for (Consumption consumption : consumptionList) {

            List<ConsumptionByNumberOfChildren> numberOfChildrenList = consumptionByNumberOfChildrenRepository.findAllByConsumptionIdAndAgeGroupId(consumption.getId(), ageGroupId);

            for (ConsumptionByNumberOfChildren consumptionByNumberOfChildren : numberOfChildrenList) {
                for (ConsumptionProduct consumptionProduct : consumptionByNumberOfChildren.getProductList()) {

                    Date d = new Date(consumption.getDayDate().getTime());
                    Calendar c = Calendar.getInstance();
                    c.setTime(d);
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    BigDecimal weight = consumptionProduct.getWeight();

                    Optional<Price> optional = priceRepository.findByKindergarten_IdAndYearAndMonthAndProduct_Id(consumption.getKindergartenId(), year, month, consumptionProduct.getProductId());
                    if (optional.isEmpty()) {
                        addListSum(consumption.getDayDate(), BigDecimal.valueOf(0), list);
                    } else {
                        Price price = optional.get();
                        addListSum(consumption.getDayDate(), weight.multiply(price.getPrice()), list);
                    }
                }
            }
        }
        return list;
    }


    public void addListSum(Timestamp date, BigDecimal sum, List<DateAgeGroupSum> list) {
        boolean res = true;
        for (DateAgeGroupSum dateAgeGroupSum : list) {

            Date d = new Date(date.getTime());
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            Date dm = new Date(dateAgeGroupSum.getDate().getTime());
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int yearM = cm.get(Calendar.YEAR);
            int monthM = cm.get(Calendar.MONTH);
            int dayM = cm.get(Calendar.DAY_OF_MONTH);
            if (year == yearM && month == monthM && day == dayM) {
                dateAgeGroupSum.setSum(dateAgeGroupSum.getSum().add(sum));
                res = false;
                break;
            }
        }
        if (res) {
            list.add(new DateAgeGroupSum(date, sum));
        }
    }

    public void calculateTable_2(Report report, List<Consumption> consumptions) {

        List<ReportDate> list = new ArrayList<>();

        for (DateAgeGroupSum dateAgeGroupSum : report.getAgeGroup_3_4()) {
            list.add(new ReportDate(dateAgeGroupSum.getDate()));
        }

//        for (Consumption consumption : consumptions) {
//            for (ConsumptionByNumberOfChildren consumptionByNumberOfChildren : consumption.getNumberOfChildrenList()) {
//                addList_buyurtma_boyicha(list, consumption.getDayDate(), consumptionByNumberOfChildren.getNumber(), consumptionByNumberOfChildren.getAgeGroupName(), consumptionByNumberOfChildren.getAgeGroupId());
//            }
//        }

        for (Consumption consumption : consumptions) {
            for (ConsumptionByNumberOfChildren consumptionByNumberOfChildren : consumption.getNumberOfChildrenList()) {

                if (!consumptionByNumberOfChildren.getAgeGroupName().equals("Xodim")) {
//
//                    for (ConsumptionProduct consumptionProduct : consumptionByNumberOfChildren.getProductList()) {
//                        Integer productId = consumptionProduct.getProductId();
//                        Product product = productRepository.findById(productId).get();
//                        BigDecimal price = getPrice(consumption, product);
////                        BigDecimal number = (consumptionProduct.getWeight().divide(BigDecimal.valueOf(consumptionByNumberOfChildren.getNumber()), 2, RoundingMode.HALF_UP)).multiply(price);
//
//
//                        BigDecimal number = (consumptionProduct.getWeight()).multiply(price);
//
//                        addList_bir_nafar_bolaga(list, consumption.getDayDate(), number, consumptionByNumberOfChildren.getAgeGroupName(), consumptionByNumberOfChildren.getAgeGroupId());
//                    }
                    addList_buyurtma_boyicha(list, consumption.getDayDate(), consumptionByNumberOfChildren.getNumber(), consumptionByNumberOfChildren.getAgeGroupName(), consumptionByNumberOfChildren.getAgeGroupId());
                }
            }
        }

//        for (ReportDate reportDate : list) {
//            for (ReportAgeGroup reportAgeGroup : reportDate.getBir_nafar_bolaga()) {
//
//                for (ReportAgeGroup ageGroup : reportDate.getBuyurtma_boyicha()) {
//                    if (ageGroup.getId().equals(reportAgeGroup.getId())) {
//
//                        if (reportAgeGroup.getNumber().compareTo(BigDecimal.valueOf(0)) != 0) {
//
//                            reportAgeGroup.setNumber(reportAgeGroup.getNumber().divide(ageGroup.getNumber(), 10, RoundingMode.HALF_UP));
//                        }
//
//                    }
//                }
//            }
//        }


        for (DateAgeGroupSum dateAgeGroupSum : report.getAgeGroup_3_4()) {

            AgeGroup ageGroup = ageGroupRepository.findById(1).get();

            for (ReportDate reportDate : list) {
                Date d = new Date(reportDate.getDate().getTime());
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);


                Date dm = new Date(dateAgeGroupSum.getDate().getTime());
                Calendar cm = Calendar.getInstance();
                cm.setTime(dm);
                int yearM = cm.get(Calendar.YEAR);
                int monthM = cm.get(Calendar.MONTH);
                int dayM = cm.get(Calendar.DAY_OF_MONTH);

                if (year == yearM && month == monthM && day == dayM) {
                    List<ReportAgeGroup> bir_nafar_bolaga = reportDate.getBir_nafar_bolaga() != null ? reportDate.getBir_nafar_bolaga() : new ArrayList<>();
                    bir_nafar_bolaga.add(new ReportAgeGroup(ageGroup.getId(), ageGroup.getName(), dateAgeGroupSum.getSum()));
                    reportDate.setBir_nafar_bolaga(bir_nafar_bolaga);
                }
            }
        }
        for (DateAgeGroupSum dateAgeGroupSum : report.getAgeGroup_4_7()) {

            AgeGroup ageGroup = ageGroupRepository.findById(2).get();

            for (ReportDate reportDate : list) {
                Date d = new Date(reportDate.getDate().getTime());
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);


                Date dm = new Date(dateAgeGroupSum.getDate().getTime());
                Calendar cm = Calendar.getInstance();
                cm.setTime(dm);
                int yearM = cm.get(Calendar.YEAR);
                int monthM = cm.get(Calendar.MONTH);
                int dayM = cm.get(Calendar.DAY_OF_MONTH);

                if (year == yearM && month == monthM && day == dayM) {
                    List<ReportAgeGroup> bir_nafar_bolaga = reportDate.getBir_nafar_bolaga() != null ? reportDate.getBir_nafar_bolaga() : new ArrayList<>();
                    bir_nafar_bolaga.add(new ReportAgeGroup(ageGroup.getId(), ageGroup.getName(), dateAgeGroupSum.getSum()));
                    reportDate.setBir_nafar_bolaga(bir_nafar_bolaga);
                }
            }
        }
        for (DateAgeGroupSum dateAgeGroupSum : report.getAgeGroup_q_m()) {

            AgeGroup ageGroup = ageGroupRepository.findById(3).get();

            for (ReportDate reportDate : list) {
                Date d = new Date(reportDate.getDate().getTime());
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);


                Date dm = new Date(dateAgeGroupSum.getDate().getTime());
                Calendar cm = Calendar.getInstance();
                cm.setTime(dm);
                int yearM = cm.get(Calendar.YEAR);
                int monthM = cm.get(Calendar.MONTH);
                int dayM = cm.get(Calendar.DAY_OF_MONTH);

                if (year == yearM && month == monthM && day == dayM) {
                    List<ReportAgeGroup> bir_nafar_bolaga = reportDate.getBir_nafar_bolaga() != null ? reportDate.getBir_nafar_bolaga() : new ArrayList<>();
                    bir_nafar_bolaga.add(new ReportAgeGroup(ageGroup.getId(), ageGroup.getName(), dateAgeGroupSum.getSum()));
                    reportDate.setBir_nafar_bolaga(bir_nafar_bolaga);
                }
            }
        }


        for (ReportDate reportDate : list) {
            for (ReportAgeGroup reportAgeGroup : reportDate.getBir_nafar_bolaga()) {

                for (ReportAgeGroup ageGroup : reportDate.getBuyurtma_boyicha()) {
                    if (ageGroup.getId().equals(reportAgeGroup.getId())) {

                        if (reportAgeGroup.getNumber().compareTo(BigDecimal.valueOf(0)) != 0) {

                            reportAgeGroup.setNumber(reportAgeGroup.getNumber().divide(ageGroup.getNumber(), 10, RoundingMode.HALF_UP));
                        }

                    }
                }
            }
        }


        for (ReportDate reportDate : list) {
            reportDate.getBuyurtma_boyicha().sort(Comparator.comparing(ReportAgeGroup::getName));
            reportDate.getBir_nafar_bolaga().sort(Comparator.comparing(ReportAgeGroup::getName));
        }
        list.sort(Comparator.comparing(ReportDate::getDate));


        report.setTable_2(list);
    }

    public void addList_buyurtma_boyicha(List<ReportDate> list, Timestamp date, Long number, String name, Integer id) {

        for (ReportDate reportDate : list) {

            Date d = new Date(reportDate.getDate().getTime());
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            Date dm = new Date(date.getTime());
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int yearM = cm.get(Calendar.YEAR);
            int monthM = cm.get(Calendar.MONTH);
            int dayM = cm.get(Calendar.DAY_OF_MONTH);

            if (year == yearM && month == monthM && day == dayM) {
                boolean ans = true;
                if (reportDate.getBuyurtma_boyicha() != null) {
                    for (ReportAgeGroup reportAgeGroup : reportDate.getBuyurtma_boyicha()) {
                        if (reportAgeGroup.getId().equals(id)) {
                            reportAgeGroup.setNumber(reportAgeGroup.getNumber().add(BigDecimal.valueOf(number)));
                            ans = false;
                        }
                    }
                }

                if (ans) {
                    List<ReportAgeGroup> buyurtma_boyicha = reportDate.getBuyurtma_boyicha() != null ? reportDate.getBuyurtma_boyicha() : new ArrayList<>();
                    buyurtma_boyicha.add(new ReportAgeGroup(id, name, BigDecimal.valueOf(number)));

                    reportDate.setBuyurtma_boyicha(buyurtma_boyicha);
                }
            }
        }
    }

    public void addList_bir_nafar_bolaga(List<ReportDate> list, Timestamp date, BigDecimal number, String name, Integer id) {

        for (ReportDate reportDate : list) {

            Date d = new Date(reportDate.getDate().getTime());
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            Date dm = new Date(date.getTime());
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int yearM = cm.get(Calendar.YEAR);
            int monthM = cm.get(Calendar.MONTH);
            int dayM = cm.get(Calendar.DAY_OF_MONTH);

            if (year == yearM && month == monthM && day == dayM) {
                boolean ans = true;

                if (reportDate.getBir_nafar_bolaga() != null) {
                    for (ReportAgeGroup reportAgeGroup : reportDate.getBir_nafar_bolaga()) {
                        if (reportAgeGroup.getId().equals(id)) {
                            reportAgeGroup.setNumber((reportAgeGroup.getNumber().add(number)));
                            ans = false;
                        }
                    }
                }

                if (ans) {

                    List<ReportAgeGroup> bir_nafar_bolaga = reportDate.getBir_nafar_bolaga() != null ? reportDate.getBir_nafar_bolaga() : new ArrayList<>();
                    bir_nafar_bolaga.add(new ReportAgeGroup(id, name, number));

                    reportDate.setBir_nafar_bolaga(bir_nafar_bolaga);
                }
            }
        }
    }

    //MENYU XISOBOTI
    public List<MenuDTO> getMenu(ResponseUser responseUser, Integer id, Long start, Long end) {

        Company company = usersRepository.findByUserName(responseUser.getUsername()).get().getCompany();

        Date d = new Date(start);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);


        Date dm = new Date(end);
        Calendar cm = Calendar.getInstance();
        cm.setTime(dm);
        int yearM = c.get(Calendar.YEAR);
        int monthM = cm.get(Calendar.MONTH);

        List<MenuSave> list = new ArrayList<>();
        if (year != yearM) {
            list.addAll(menuSaveRepository.findAllByKindergarten_IdAndYearAndMonth(id, year, month));
            list.addAll(menuSaveRepository.findAllByKindergarten_IdAndYearAndMonth(id, yearM, monthM));
        }
        if (year == yearM && month != monthM) {
            list.addAll(menuSaveRepository.findAllByKindergarten_IdAndYearAndMonth(id, year, month));
            list.addAll(menuSaveRepository.findAllByKindergarten_IdAndYearAndMonth(id, year, monthM));
        }
        if (year == yearM && month == monthM) {
            list.addAll(menuSaveRepository.findAllByKindergarten_IdAndYearAndMonth(id, year, month));
        }

        List<MenuDTO> returnList = new ArrayList<>();

        for (MenuSave menuSave : list) {

            if (start <= menuSave.getDayDate().getTime() && end >= menuSave.getDayDate().getTime()) {
                if (menuSave.getNumberFact() != null) {
                    returnList.add(menuParse(menuSave, company));
                }
            }
        }

        for (MenuDTO menuDTO : returnList) {
            for (MealTimeDTO mealTimeDTO : menuDTO.getMealTimeList()) {
                for (MealDTO mealDTO : mealTimeDTO.getMealList()) {
                    List<ProductDTO> productList = mealDTO.getProductList();
                    productList.sort(Comparator.comparing(ProductDTO::getName));
                    mealDTO.setProductList(productList);

                    for (AgeGroupDTO ageGroupDTO : mealDTO.getAgeGroupList()) {

                        List<ProductDTO> productList1 = ageGroupDTO.getProductList();
                        productList1.sort(Comparator.comparing(ProductDTO::getName));
                        ageGroupDTO.setProductList(productList1);
                    }

                    List<AgeGroupDTO> ageGroupList = mealDTO.getAgeGroupList();
                    ageGroupList.sort(Comparator.comparing(AgeGroupDTO::getName));
                    mealDTO.setAgeGroupList(ageGroupList);
                }
                List<MealDTO> mealList = mealTimeDTO.getMealList();
                mealList.sort(Comparator.comparing(MealDTO::getName));
                mealTimeDTO.setMealList(mealList);


                menuDTO.getMaxsulot_narxi().sort(Comparator.comparing(ProductDTO::getName));
                menuDTO.getJami_sarflangan_maxsulot().sort(Comparator.comparing(ProductDTO::getName));
                for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {

                    menuAgeGroupDTO.getBir_nafar_bolaga().sort(Comparator.comparing(ProductDTO::getName));
                    menuAgeGroupDTO.getEnergetik_quvvati().sort(Comparator.comparing(ProductDTO::getName));
                    menuAgeGroupDTO.getSan_pin().sort(Comparator.comparing(ProductDTO::getName));
                    menuAgeGroupDTO.getAmalda_istemol_qilindi().sort(Comparator.comparing(ProductDTO::getName));
                    menuAgeGroupDTO.getJami_maxsulot_narxi().sort(Comparator.comparing(ProductDTO::getName));
                }

                menuDTO.getMenuAgeGroupList().sort(Comparator.comparing(MenuAgeGroupDTO::getAgeGroupId));

            }
            List<MealTimeDTO> mealTimeList = menuDTO.getMealTimeList();
            mealTimeList.sort(Comparator.comparing(MealTimeDTO::getName));
            menuDTO.setMealTimeList(mealTimeList);


            for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
                if ((menuAgeGroupDTO.getAgeGroupName().equals("Xodim") || menuAgeGroupDTO.getAgeGroupName().equals("Qisqa muddatli"))) {
                    menuAgeGroupDTO.setEnergetik_quvvati(null);
                    menuAgeGroupDTO.setSan_pin(null);
                    menuAgeGroupDTO.setKcal(null);
                }
            }
        }

        returnList.sort(Comparator.comparing(MenuDTO::getDate));


        return returnList;
    }

    public MenuDTO menuParse(MenuSave menuSave, Company company) {


        MenuDTO menuDTO = new MenuDTO();
        menuDTO.setDate(menuSave.getDayDate());
        menuDTO.setName(menuSave.getName());
        menuDTO.setAgeGroupList(addNumberOfChildren(menuSave.getNumberFact()));

        for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {

            menuDTO.setMealTimeList(addMealTime(menuDTO, mealTimeStandardSave.getMealTimeId(), mealTimeStandardSave.getMealTimeName()));

            for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {

                addMeal(menuDTO, mealAgeStandardSave.getMealId(), mealAgeStandardSave.getMealName(), mealTimeStandardSave.getMealTimeId());

                for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
                    if (!ageStandardSave.getAgeGroupName().equals("Xodim")) {
                        BigDecimal weight = ageStandardSave.getWeight();
                        addAgeStandard(menuDTO, ageStandardSave.getAgeGroupId(), ageStandardSave.getAgeGroupName(), mealAgeStandardSave.getMealId(), menuSave, weight);
                    }
                }

            }
        }
        addMeal(menuDTO, menuSave);


        if (company == null) {
            company = companyRepository.findById(menuSave.getKindergarten().getCompanyId()).get();
        }

        menuDTO.setCompany(companyService.parse(company));
        menuDTO.setKindergarten(kindergartenService.parse(menuSave.getKindergarten()));

        calculateSanPin(menuDTO, menuSave);

        BigDecimal total = BigDecimal.valueOf(0);

        for (ProductDTO productDTO : menuDTO.getJami_sarflangan_maxsulot_narxi()) {
            total = total.add(productDTO.getWeight());
        }
        menuDTO.setTotalSum(total);

        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
            BigDecimal totalKcal = BigDecimal.valueOf(0);
            if (menuAgeGroupDTO.getEnergetik_quvvati() != null) {

                for (ProductDTO productDTO : menuAgeGroupDTO.getEnergetik_quvvati()) {
                    totalKcal = totalKcal.add(productDTO.getWeight());
                }
                menuAgeGroupDTO.setKcal(totalKcal);
            }


            BigDecimal totalSum = BigDecimal.valueOf(0);

            for (ProductDTO productDTO : menuAgeGroupDTO.getJami_maxsulot_narxi()) {
                totalSum = totalSum.add(productDTO.getWeight());
            }
            if (totalSum.compareTo(BigDecimal.valueOf(0)) == 0) {

                menuAgeGroupDTO.setTotal(BigDecimal.valueOf(0));
            } else {

                menuAgeGroupDTO.setTotal(totalSum.divide((getNumber(menuAgeGroupDTO.getAgeGroupId(), menuDTO)), 6, RoundingMode.HALF_UP));
            }
        }


//        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
//            menuAgeGroupDTO.setAgeGroupName(menuAgeGroupDTO.getAgeGroupName() + "123131");
//        }


        return menuDTO;
    }


    public List<MealTimeDTO> addMealTime(MenuDTO menuDTO, Integer id, String name) {

        List<MealTimeDTO> mealTimeList = menuDTO.getMealTimeList() != null ? menuDTO.getMealTimeList() : new ArrayList<>();
        boolean res = true;


        for (MealTimeDTO mealTimeDTO : mealTimeList) {
            if (mealTimeDTO.getId().equals(id)) {
                res = false;
                break;
            }
        }
        if (res) {
            mealTimeList.add(new MealTimeDTO(id, name));
        }
        return mealTimeList;
    }

    public void addMeal(MenuDTO menuDTO, Integer id, String name, Integer mealTimeId) {


        for (MealTimeDTO mealTimeDTO : menuDTO.getMealTimeList()) {
            if (mealTimeDTO.getId().equals(mealTimeId)) {
                List<MealDTO> mealList = mealTimeDTO.getMealList() != null ? mealTimeDTO.getMealList() : new ArrayList<>();
                boolean res = true;


                for (MealDTO mealDTO : mealList) {
                    if (mealDTO.getId().equals(id)) {
                        res = false;
                        break;
                    }
                }
                if (res) {
                    mealList.add(new MealDTO(id, name));
                }
                mealTimeDTO.setMealList(mealList);
            }
        }
    }

    public void addMeal(MenuDTO menuDTO, MenuSave menuSave) {


        for (MealTimeDTO mealTimeDTO : menuDTO.getMealTimeList()) {
            for (MealDTO mealDTO : mealTimeDTO.getMealList()) {

                BigDecimal weight = BigDecimal.valueOf(0);

                for (AgeGroupDTO ageGroupDTO : mealDTO.getAgeGroupList()) {

                    BigDecimal number = getNumber(ageGroupDTO.getId(), menuDTO);

                    weight = weight.add((ageGroupDTO.getMealWeight().divide(BigDecimal.valueOf(1000), 10, RoundingMode.HALF_UP)).multiply(number));
                }
                mealDTO.setMealWeight(weight);
            }
        }

        for (MealTimeDTO mealTimeDTO : menuDTO.getMealTimeList()) {
            for (MealDTO mealDTO : mealTimeDTO.getMealList()) {
                List<ProductDTO> productList = mealDTO.getProductList() != null ? mealDTO.getProductList() : new ArrayList<>();
                for (AgeGroupDTO ageGroupDTO : mealDTO.getAgeGroupList()) {
                    for (ProductDTO productDTO : ageGroupDTO.getProductList()) {
                        boolean res = true;

                        for (ProductDTO dto : productList) {
                            if (dto.getId().equals(productDTO.getId())) {
                                dto.setWeight(dto.getWeight().add((productDTO.getWeight().divide(BigDecimal.valueOf(1000), 10, RoundingMode.HALF_UP)).multiply(getNumber(ageGroupDTO.getId(), menuDTO))));
                                res = false;
                                break;
                            }
                        }
                        if (res) {
                            ProductDTO product = new ProductDTO(productDTO.getId(), productDTO.getName(), (productDTO.getWeight().divide(BigDecimal.valueOf(1000), 10, RoundingMode.HALF_UP)).multiply(getNumber(ageGroupDTO.getId(), menuDTO)));
                            productList.add(product);
                        }
                    }
                }
                mealDTO.setProductList(productList);
            }
        }


    }

    public void addAgeStandard(MenuDTO menuDTO, Integer id, String name, Integer mealId, MenuSave menuSave, BigDecimal weight) {

        for (MealTimeDTO mealTimeDTO : menuDTO.getMealTimeList()) {
            for (MealDTO mealDTO : mealTimeDTO.getMealList()) {
                if (mealDTO.getId().equals(mealId)) {
                    List<AgeGroupDTO> ageGroupList = mealDTO.getAgeGroupList() != null ? mealDTO.getAgeGroupList() : new ArrayList<>();

                    boolean res = true;
                    for (AgeGroupDTO ageGroupDTO : ageGroupList) {
                        if (ageGroupDTO.getId().equals(id)) {
                            res = false;
                            ageGroupDTO.setMealWeight(ageGroupDTO.getMealWeight().add(weight));
                            addProduct(mealId, weight, ageGroupDTO, menuSave);
                            break;
                        }
                    }
                    if (res) {

                        String replace = name.replace("-", " -- ");

                        AgeGroupDTO ageGroupDTO = new AgeGroupDTO(id, replace);

                        addProduct(mealId, weight, ageGroupDTO, menuSave);
                        ageGroupDTO.setMealWeight(weight.multiply(BigDecimal.valueOf(1000)));

                        ageGroupList.add(ageGroupDTO);
                        mealDTO.setAgeGroupList(ageGroupList);
                    }
                }
            }
        }
    }

    public BigDecimal getNumber(Integer ageGroupId, MenuDTO menuDTO) {
        for (ReportAgeGroup reportAgeGroup : menuDTO.getAgeGroupList()) {
            if (reportAgeGroup.getId().equals(ageGroupId)) {
                return reportAgeGroup.getNumber();
            }
        }
        return BigDecimal.valueOf(0);
    }

    public void addProduct(Integer mealId, BigDecimal weight, AgeGroupDTO ageGroupDTO, MenuSave menuSave) {

        if (ageGroupDTO.getProductList() == null) {
            ageGroupDTO.setProductList(getProductList(menuSave));
        }

        Meal meal = mealRepository.findById(mealId).get();


        for (ProductMeal productMeal : meal.getProductMeals()) {
            BigDecimal mealFactWeight = meal.getWeight();
            BigDecimal productWeight = productMeal.getWeight().divide(mealFactWeight, 10, RoundingMode.HALF_UP).multiply((weight));

            for (ProductDTO productDTO : ageGroupDTO.getProductList()) {
                if (productDTO.getId().equals(productMeal.getProduct().getId())) {
                    productDTO.setWeight(productDTO.getWeight().add(productWeight.multiply(BigDecimal.valueOf(1000))));
                }
            }
        }

    }


    //MENYU XISOBOTI

    public List<ProductDTO> getProductList(MenuSave ms) {
        MenuSave menuSave = menuSaveRepository.findById(ms.getId()).get();

        List<WeightProduct> list = new ArrayList<>();

        PerDay numberToGuess = menuSave.getNumberToGuess();

        if (numberToGuess == null){
            numberToGuess = menuSave.getNumberFact();
        }

        if (numberToGuess != null) {
            for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
                for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {
                    Meal meal = mealRepository.findById(mealAgeStandardSave.getMealId()).get();
                    for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
                        if (ageStandardSave.getWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                            for (NumberOfChildren numberOfChild : numberToGuess.getNumberOfChildren()) {
                                if (numberOfChild.getAgeGroup().getId().equals(ageStandardSave.getAgeGroupId())) {
                                    for (ProductMeal productMeal : meal.getProductMeals()) {
                                        addListProduct(list, productMeal.getProduct());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        List<ProductDTO> productList = new ArrayList<>();

        for (WeightProduct weightProduct : list) {
            productList.add(new ProductDTO(weightProduct.getProduct().getId(), weightProduct.getProduct().getName(), BigDecimal.valueOf(0)));
        }


        return productList;
    }

    public void addListProduct(List<WeightProduct> list, Product product) {

        boolean res = true;

        for (WeightProduct weightProduct : list) {
            if (weightProduct.getProduct().equals(product)) {
                res = false;
            }
        }
        if (res) {
            list.add(new WeightProduct(null, product, null,product.getId()));
        }
    }

    public List<ReportAgeGroup> addNumberOfChildren(PerDay perDay) {

        List<ReportAgeGroup> list = new ArrayList<>();
        if (perDay != null) {
            if (perDay.getNumberOfChildren() != null) {
                for (NumberOfChildren numberOfChild : perDay.getNumberOfChildren()) {
                    list.add(new ReportAgeGroup(numberOfChild.getAgeGroup().getId(), numberOfChild.getAgeGroup().getName(), BigDecimal.valueOf(numberOfChild.getNumberOfKids())));
                }
            }
        }
        return list;
    }

    public Report sort(Report report) {

        if (report != null) {
            for (ReportByProduct reportByProduct : report.getReportByProduct()) {
                reportByProduct.getDateWeightList().sort(Comparator.comparing(DateWeight::getDate));
            }
            for (ReportByProduct reportByProduct : report.getReportByProduct_3_4()) {
                reportByProduct.getDateWeightList().sort(Comparator.comparing(DateWeight::getDate));
            }
            for (ReportByProduct reportByProduct : report.getReportByProduct_4_7()) {
                reportByProduct.getDateWeightList().sort(Comparator.comparing(DateWeight::getDate));
            }
            for (ReportByProduct reportByProduct : report.getReportByProduct_3_7()) {
                reportByProduct.getDateWeightList().sort(Comparator.comparing(DateWeight::getDate));
            }
            for (ReportByProduct reportByProduct : report.getReportByProduct_xodim()) {
                reportByProduct.getDateWeightList().sort(Comparator.comparing(DateWeight::getDate));
            }
            for (ReportByProduct reportByProduct : report.getReportByProduct_qisqa()) {
                reportByProduct.getDateWeightList().sort(Comparator.comparing(DateWeight::getDate));
            }

            report.getReportByProduct_qisqa().sort(Comparator.comparing(ReportByProduct::getProductName));
            report.getReportByProduct().sort(Comparator.comparing(ReportByProduct::getProductName));
            report.getReportByProduct_3_7().sort(Comparator.comparing(ReportByProduct::getProductName));
            report.getReportByProduct_3_4().sort(Comparator.comparing(ReportByProduct::getProductName));
            report.getReportByProduct_4_7().sort(Comparator.comparing(ReportByProduct::getProductName));
            report.getReportByProduct_xodim().sort(Comparator.comparing(ReportByProduct::getProductName));

            report.getAgeGroup_3_4().sort(Comparator.comparing(DateAgeGroupSum::getDate));
            report.getAgeGroup_4_7().sort(Comparator.comparing(DateAgeGroupSum::getDate));
            report.getAgeGroup_jami().sort(Comparator.comparing(DateAgeGroupSum::getDate));
            report.getAgeGroup_q_m().sort(Comparator.comparing(DateAgeGroupSum::getDate));
            report.getTable_2().sort(Comparator.comparing(ReportDate::getDate));
        }

        return report;
    }

    public void calculateSanPin(MenuDTO menuDTO, MenuSave menuSave) {

        List<MenuAgeGroupDTO> list = new ArrayList<>();

        for (AgeGroup ageGroup : ageGroupRepository.findAll()) {

            MenuAgeGroupDTO menuAgeGroupDTO = new MenuAgeGroupDTO(ageGroup.getName());

            if (!(ageGroup.getName().equals("Xodim") || ageGroup.getName().equals("Qisqa muddatli"))) {
                menuAgeGroupDTO.setSan_pin(getProductList(menuSave));
                menuAgeGroupDTO.setEnergetik_quvvati(getProductList(menuSave));

            }
            menuAgeGroupDTO.setAgeGroupId(ageGroup.getId());
            menuAgeGroupDTO.setAgeGroupName(ageGroup.getName());
            menuAgeGroupDTO.setJami_maxsulot_narxi(getProductList(menuSave));
            menuAgeGroupDTO.setAmalda_istemol_qilindi(getProductList(menuSave));
            menuAgeGroupDTO.setBir_nafar_bolaga(getProductList(menuSave));
            list.add(menuAgeGroupDTO);
        }

        for (MenuAgeGroupDTO menuAgeGroupDTO : list) {

            if (menuAgeGroupDTO.getSan_pin() == null) {
                menuAgeGroupDTO.setSan_pin(getProductList(menuSave));
            }

            for (ProductDTO productDTO : menuAgeGroupDTO.getSan_pin()) {

                Product product = productRepository.findById(productDTO.getId()).get();
                SanpinCategory sanpinCategory = product.getSanpinCategory();
                AgeGroup ageGroup = ageGroupRepository.findByName(menuAgeGroupDTO.getAgeGroupName()).get();

                Optional<SanpinAgeNorm> optional = sanpinAgeNormRepository.findByAgeGroupAndSanpinCategory(ageGroup, sanpinCategory);
                if (optional.isPresent()) {
                    SanpinAgeNorm sanpinAgeNorm = optional.get();
                    productDTO.setWeight(sanpinAgeNorm.getWeight());
                }
            }
        }


        menuDTO.setMenuAgeGroupList(list);
        addPrice(menuDTO, menuSave);
        bir_nafar_bolaga(menuDTO, menuSave);
        amalda_istemol_qilindi(menuDTO);
        jami_maxsulot_narxi(menuDTO);
        jami_sarflangan_maxsulot(menuDTO, menuSave);
//        maxsulot_narxi(menuDTO, menuSave);
        jami_sarflangan_maxsulot_narxi(menuDTO, menuSave);
    }

    public void bir_nafar_bolaga(MenuDTO menuDTO, MenuSave menuSave) {
        for (MealTimeDTO mealTimeDTO : menuDTO.getMealTimeList()) {
            for (MealDTO mealDTO : mealTimeDTO.getMealList()) {
                for (AgeGroupDTO ageGroupDTO : mealDTO.getAgeGroupList()) {
                    for (ProductDTO productDTO : ageGroupDTO.getProductList()) {
                        bir_nafar_bolaga_add_product(menuDTO, ageGroupDTO.getId(), productDTO.getWeight(), productDTO.getId());
                        energetik_quvvati_add_product(menuDTO, ageGroupDTO.getId(), productDTO.getWeight(), productDTO.getId(), menuSave);
                    }
                }
            }
        }


    }

    public void bir_nafar_bolaga_add_product(MenuDTO menuDTO, Integer ageGroupId, BigDecimal weight, Integer productId) {
        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
            if (menuAgeGroupDTO.getAgeGroupId().equals(ageGroupId)) {
                for (ProductDTO productDTO : menuAgeGroupDTO.getBir_nafar_bolaga()) {
                    if (productDTO.getId().equals(productId)) {
                        productDTO.setWeight(productDTO.getWeight().add(weight));
                    }
                }
            }
        }
    }

    public void energetik_quvvati_add_product(MenuDTO menuDTO, Integer ageGroupId, BigDecimal weight, Integer productId, MenuSave menuSave) {


        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {

            if (menuAgeGroupDTO.getEnergetik_quvvati() == null) {
                menuAgeGroupDTO.setEnergetik_quvvati(getProductList(menuSave));
            }

            if (menuAgeGroupDTO.getAgeGroupId().equals(ageGroupId)) {
                for (ProductDTO productDTO : menuAgeGroupDTO.getEnergetik_quvvati()) {
                    if (productDTO.getId().equals(productId)) {
                        Product product = productRepository.findById(productId).get();
                        Ingredient ingredient = product.getIngredient();

                        BigDecimal kcal = ingredient.getKcal();
                        BigDecimal multiply = (kcal.divide(BigDecimal.valueOf(0.100), 10, RoundingMode.HALF_UP)).multiply(weight);

                        productDTO.setWeight(productDTO.getWeight().add(multiply));
                    }
                }
            }
        }
    }

    public void amalda_istemol_qilindi(MenuDTO menuDTO) {
        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
            for (ProductDTO productDTO : menuAgeGroupDTO.getBir_nafar_bolaga()) {

                BigDecimal weight = productDTO.getWeight().divide(BigDecimal.valueOf(1000), 10, RoundingMode.HALF_UP);

                amalda_istemol_qilindi_add_product(menuDTO, menuAgeGroupDTO.getAgeGroupId(), weight, productDTO.getId());
            }
        }
    }

    public void amalda_istemol_qilindi_add_product(MenuDTO menuDTO, Integer ageGroupId, BigDecimal weight, Integer productId) {
        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
            if (menuAgeGroupDTO.getAgeGroupId().equals(ageGroupId)) {
                for (ProductDTO productDTO : menuAgeGroupDTO.getAmalda_istemol_qilindi()) {
                    if (productDTO.getId().equals(productId)) {

                        BigDecimal multiply = weight.multiply(getNumber(ageGroupId, menuDTO));
                        productDTO.setWeight(productDTO.getWeight().add(multiply));
                    }
                }
            }
        }
    }


    public void jami_maxsulot_narxi(MenuDTO menuDTO) {
        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
            for (ProductDTO productDTO : menuAgeGroupDTO.getAmalda_istemol_qilindi()) {
                jami_maxsulot_narxi_add_product(menuDTO, menuAgeGroupDTO.getAgeGroupId(), productDTO.getWeight(), productDTO.getId());
            }
        }
    }

    public void jami_maxsulot_narxi_add_product(MenuDTO menuDTO, Integer ageGroupId, BigDecimal weight, Integer productId) {
        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
            if (menuAgeGroupDTO.getAgeGroupId().equals(ageGroupId)) {
                for (ProductDTO productDTO : menuAgeGroupDTO.getJami_maxsulot_narxi()
                ) {
                    if (productDTO.getId().equals(productId)) {
                        productDTO.setWeight(productDTO.getWeight().add(weight.multiply(getPrice(menuDTO, productId))));
                    }
                }
            }
        }
    }

    public void jami_sarflangan_maxsulot(MenuDTO menuDTO, MenuSave menuSave) {
        for (MenuAgeGroupDTO menuAgeGroupDTO : menuDTO.getMenuAgeGroupList()) {
            for (ProductDTO productDTO : menuAgeGroupDTO.getAmalda_istemol_qilindi()) {
                jami_sarflangan_maxsulot(menuDTO, productDTO.getWeight(), productDTO.getId(), menuSave);
            }
        }
    }

    public void jami_sarflangan_maxsulot(MenuDTO menuDTO, BigDecimal weight, Integer productId, MenuSave menuSave) {
        if (menuDTO.getJami_sarflangan_maxsulot() == null) {
            List<ProductDTO> productList = getProductList(menuSave);
                    menuDTO.setJami_sarflangan_maxsulot(productList);
        }
        for (ProductDTO productDTO : menuDTO.getJami_sarflangan_maxsulot()) {
            if (productDTO.getId().equals(productId)) {
                productDTO.setWeight(productDTO.getWeight().add(weight));
            }
        }
    }

//    public void maxsulot_narxi(MenuDTO menuDTO, MenuSave menuSave) {
//        if (menuDTO.getMaxsulot_narxi() == null) {
//            menuDTO.setMaxsulot_narxi(getProductList(menuSave));
//        }
//
//
//        for (ProductDTO productDTO : menuDTO.getJami_sarflangan_maxsulot()) {
//            for (ProductDTO dto : menuDTO.getMaxsulot_narxi()) {
//                if (productDTO.getId().equals(dto.getId())) {
//                    productDTO.setWeight(productDTO.getWeight().multiply(getPrice(menuDTO, productDTO.getId())));
//                }
//            }
//        }
//    }

    public void addPrice(MenuDTO menuDTO, MenuSave menuSave) {


        List<ProductDTO> productList = getProductList(menuSave);
        for (ProductDTO productDTO : productList) {
            productDTO.setWeight(getPriceMenu(menuSave, productDTO.getId()));
        }
        menuDTO.setMaxsulot_narxi(productList);
    }

    public BigDecimal getPriceMenu(MenuSave menuSave, Integer productId) {

        Integer kindergartenId = menuSave.getKindergarten().getId();

        Date dm = new Date(menuSave.getDayDate().getTime());
        Calendar cm = Calendar.getInstance();
        cm.setTime(dm);
        int year = cm.get(Calendar.YEAR);
        int month = cm.get(Calendar.MONTH);

        Optional<Price> optional = priceRepository.findByKindergarten_IdAndYearAndMonthAndProduct_Id(kindergartenId, year, month, productId);
        if (optional.isPresent()) {
            Price price = optional.get();
            return price.getPrice();
        }


        return BigDecimal.valueOf(0);
    }

    public BigDecimal getPrice(MenuDTO menuDTO, Integer productId) {

        for (ProductDTO productDTO : menuDTO.getMaxsulot_narxi()) {
            if (productDTO.getId().equals(productId)) {
                return productDTO.getWeight();
            }
        }
        return BigDecimal.valueOf(0);
    }

    public void jami_sarflangan_maxsulot_narxi(MenuDTO menuDTO, MenuSave menuSave) {

        if (menuDTO.getJami_sarflangan_maxsulot_narxi() == null) {
            menuDTO.setJami_sarflangan_maxsulot_narxi(getProductList(menuSave));
        }

//        if (menuDTO.getJami_sarflangan_maxsulot() != null) {

            for (ProductDTO productDTO : menuDTO.getJami_sarflangan_maxsulot()) {

                for (ProductDTO dto : menuDTO.getJami_sarflangan_maxsulot_narxi()) {
                    if (dto.getId().equals(productDTO.getId())) {
                        dto.setWeight(productDTO.getWeight().multiply(getPrice(menuDTO, productDTO.getId())));
                    }
                }
            }
//        }
    }

    public List<ReportPriceDTO> getPriceReport(ResponseUser responseUser, Integer kindergartenId, Long start, Long end) {

        List<ReportPriceDTO> list = new ArrayList<>();


        Kindergarten kindergarten = kindergartenRepository.findById(kindergartenId).get();

        Company company = companyRepository.findById(kindergarten.getCompanyId()).get();

        List<ShippedProductsCompany> listKin1 = shippedProductsCompanyRepository.findAllByCompanyAndKindergarten(company, kindergarten);

        List<ShippedProducts> listCompany1 = shippedProductsRepository.findAllByCompany(company);
        List<ShippedProducts> listCompany = new ArrayList<>();

        for (ShippedProducts shippedProducts : listCompany1) {
            if (shippedProducts.getTimeTaken().getTime() >= start && shippedProducts.getTimeTaken().getTime() <= end) {
                listCompany.add(shippedProducts);
            }
        }

        List<ShippedProductsCompany> listKin = new ArrayList<>();

        for (ShippedProductsCompany shippedProductsCompany : listKin1) {
            if (shippedProductsCompany.getTimeOfShipment().getTime() >= start && shippedProductsCompany.getTimeOfShipment().getTime() <= end) {
                listKin.add(shippedProductsCompany);
            }
        }

        for (ShippedProductsCompany shippedProductsCompany : listKin) {
            boolean res = true;
            for (ReportPriceDTO reportPriceDTO : list) {
                if (reportPriceDTO.getProductId().equals(shippedProductsCompany.getProduct().getId())) {
                    reportPriceDTO.setWeight(reportPriceDTO.getWeight().add(shippedProductsCompany.getSuccessWeight()));
                    res = false;
                }
            }
            if (res) {
                ReportPriceDTO reportPriceDTO = new ReportPriceDTO();
                reportPriceDTO.setWeight(shippedProductsCompany.getSuccessWeight());
                reportPriceDTO.setProductName(shippedProductsCompany.getProduct().getName());
                reportPriceDTO.setProductId(shippedProductsCompany.getProduct().getId());
                reportPriceDTO.setMeasurementType(shippedProductsCompany.getProduct().getMeasurementType());
                list.add(reportPriceDTO);
            }
        }

        for (ReportPriceDTO reportPriceDTO : list) {
            Date dm = new Date(start);
            Calendar cm = Calendar.getInstance();
            cm.setTime(dm);
            int year = cm.get(Calendar.YEAR);
            int month = cm.get(Calendar.MONTH);

            Optional<Price> priceOptional = priceRepository.findByKindergarten_IdAndYearAndMonthAndProduct_Id(kindergartenId, year, month, reportPriceDTO.getProductId());

            if (priceOptional.isPresent()) {
                Price price = priceOptional.get();
                reportPriceDTO.setPriceXokim(price.getPrice());
            } else {
                reportPriceDTO.setPriceXokim(BigDecimal.valueOf(0));
            }
        }

        for (ShippedProducts shippedProducts : listCompany) {
            for (ReportPriceDTO reportPriceDTO : list) {
                if (shippedProducts.getProduct().getId().equals(reportPriceDTO.getProductId())) {

                    if (reportPriceDTO.getPriceBozor() == null) {
                        reportPriceDTO.setPriceBozor(BigDecimal.valueOf(0));
                    }

                    reportPriceDTO.setPriceBozor(reportPriceDTO.getPriceBozor().add(shippedProducts.getPrice()));
                    reportPriceDTO.setNumber(reportPriceDTO.getNumber() + 1);
                }
            }
        }

        for (ReportPriceDTO reportPriceDTO : list) {
            if (reportPriceDTO.getPriceBozor() != null) {
                reportPriceDTO.setPriceBozor(reportPriceDTO.getPriceBozor().divide(BigDecimal.valueOf(reportPriceDTO.getNumber()), 2, RoundingMode.HALF_UP));
            } else {
                reportPriceDTO.setPriceBozor(BigDecimal.valueOf(0));
            }
        }

        list.sort(Comparator.comparing(ReportPriceDTO::getProductName));

        return list;
    }
}





