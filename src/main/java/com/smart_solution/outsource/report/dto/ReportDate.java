package com.smart_solution.outsource.report.dto;

import java.sql.Timestamp;
import java.util.List;

public class ReportDate {


    private Timestamp date;
    private List<ReportAgeGroup> buyurtma_boyicha;
    private List<ReportAgeGroup> bir_nafar_bolaga;

    public ReportDate() {
    }

    public ReportDate(Timestamp date) {
        this.date = date;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public List<ReportAgeGroup> getBuyurtma_boyicha() {
        return buyurtma_boyicha;
    }

    public void setBuyurtma_boyicha(List<ReportAgeGroup> buyurtma_boyicha) {
        this.buyurtma_boyicha = buyurtma_boyicha;
    }

    public List<ReportAgeGroup> getBir_nafar_bolaga() {
        return bir_nafar_bolaga;
    }

    public void setBir_nafar_bolaga(List<ReportAgeGroup> bir_nafar_bolaga) {
        this.bir_nafar_bolaga = bir_nafar_bolaga;
    }
}
