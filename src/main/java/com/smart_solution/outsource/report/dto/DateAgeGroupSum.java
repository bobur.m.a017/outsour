package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class DateAgeGroupSum {

    private Timestamp date;
    private BigDecimal sum;

    public DateAgeGroupSum(Timestamp date, BigDecimal sum) {
        this.date = date;
        this.sum = sum;
    }

    public DateAgeGroupSum() {
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }
}
