package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;
import java.util.List;

public class ReportByProduct {

    private String productName;

    private List<DateWeight> dateWeightList;

    private BigDecimal price;
    private BigDecimal totalPrice;
    private BigDecimal totalWeight;


    public String getProductName() {
        return productName;
    }

    public BigDecimal getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(BigDecimal totalWeight) {
        this.totalWeight = totalWeight;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<DateWeight> getDateWeightList() {
        return dateWeightList;
    }

    public void setDateWeightList(List<DateWeight> dateWeightList) {
        this.dateWeightList = dateWeightList;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
