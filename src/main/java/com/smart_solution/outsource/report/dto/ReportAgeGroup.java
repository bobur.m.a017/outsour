package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;

public class ReportAgeGroup {

    private Integer id;
    private String name;
    private BigDecimal number;

    public ReportAgeGroup(Integer id, String name, BigDecimal number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }


    public ReportAgeGroup() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getNumber() {
        return number;
    }

    public void setNumber(BigDecimal number) {
        this.number = number;
    }
}
