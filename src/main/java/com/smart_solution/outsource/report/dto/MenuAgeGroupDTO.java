package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;
import java.util.List;

public class MenuAgeGroupDTO {

    private String ageGroupName;
    private Integer ageGroupId;
    private BigDecimal kcal;
    private List<ProductDTO> san_pin;
    private List<ProductDTO> bir_nafar_bolaga;
    private List<ProductDTO> energetik_quvvati;
    private List<ProductDTO> amalda_istemol_qilindi;
    private List<ProductDTO> jami_maxsulot_narxi;
    private BigDecimal total;




    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public BigDecimal getKcal() {
        return kcal;
    }

    public void setKcal(BigDecimal kcal) {
        this.kcal = kcal;
    }

    public MenuAgeGroupDTO() {
    }

    public MenuAgeGroupDTO(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public List<ProductDTO> getSan_pin() {
        return san_pin;
    }

    public void setSan_pin(List<ProductDTO> san_pin) {
        this.san_pin = san_pin;
    }

    public List<ProductDTO> getBir_nafar_bolaga() {
        return bir_nafar_bolaga;
    }

    public void setBir_nafar_bolaga(List<ProductDTO> bir_nafar_bolaga) {
        this.bir_nafar_bolaga = bir_nafar_bolaga;
    }

    public List<ProductDTO> getEnergetik_quvvati() {
        return energetik_quvvati;
    }

    public void setEnergetik_quvvati(List<ProductDTO> energetik_quvvati) {
        this.energetik_quvvati = energetik_quvvati;
    }

    public List<ProductDTO> getAmalda_istemol_qilindi() {
        return amalda_istemol_qilindi;
    }

    public void setAmalda_istemol_qilindi(List<ProductDTO> amalda_istemol_qilindi) {
        this.amalda_istemol_qilindi = amalda_istemol_qilindi;
    }

    public List<ProductDTO> getJami_maxsulot_narxi() {
        return jami_maxsulot_narxi;
    }

    public void setJami_maxsulot_narxi(List<ProductDTO> jami_maxsulot_narxi) {
        this.jami_maxsulot_narxi = jami_maxsulot_narxi;
    }
}
