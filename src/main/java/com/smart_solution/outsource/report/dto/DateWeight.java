package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class DateWeight {

    private Timestamp date;
    private BigDecimal weight;

    public DateWeight(Timestamp date, BigDecimal weight) {
        this.date = date;
        this.weight = weight;
    }

    public DateWeight() {
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
