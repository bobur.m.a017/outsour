package com.smart_solution.outsource.report.dto;

import com.smart_solution.outsource.company.CompanyResponseDTO;
import com.smart_solution.outsource.kindergarten.KindergartenResponseDTO;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class MenuDTO {
    private String name;
    private Timestamp date;
    private List<ReportAgeGroup> ageGroupList;
    private List<MealTimeDTO> mealTimeList;
    private CompanyResponseDTO company;
    private KindergartenResponseDTO kindergarten;
    private List<MenuAgeGroupDTO> menuAgeGroupList;
    private List<ProductDTO> maxsulot_narxi;
    private List<ProductDTO> jami_sarflangan_maxsulot;
    private List<ProductDTO> jami_sarflangan_maxsulot_narxi;
    private BigDecimal totalSum;

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public List<ProductDTO> getJami_sarflangan_maxsulot_narxi() {
        return jami_sarflangan_maxsulot_narxi;
    }

    public void setJami_sarflangan_maxsulot_narxi(List<ProductDTO> jami_sarflangan_maxsulot_narxi) {
        this.jami_sarflangan_maxsulot_narxi = jami_sarflangan_maxsulot_narxi;
    }

    public List<ProductDTO> getMaxsulot_narxi() {
        return maxsulot_narxi;
    }

    public void setMaxsulot_narxi(List<ProductDTO> maxsulot_narxi) {
        this.maxsulot_narxi = maxsulot_narxi;
    }

    public List<ProductDTO> getJami_sarflangan_maxsulot() {
        return jami_sarflangan_maxsulot;
    }

    public void setJami_sarflangan_maxsulot(List<ProductDTO> jami_sarflangan_maxsulot) {
        this.jami_sarflangan_maxsulot = jami_sarflangan_maxsulot;
    }

    public List<MenuAgeGroupDTO> getMenuAgeGroupList() {
        return menuAgeGroupList;
    }

    public void setMenuAgeGroupList(List<MenuAgeGroupDTO> menuAgeGroupList) {
        this.menuAgeGroupList = menuAgeGroupList;
    }

    public CompanyResponseDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyResponseDTO company) {
        this.company = company;
    }

    public KindergartenResponseDTO getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(KindergartenResponseDTO kindergarten) {
        this.kindergarten = kindergarten;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public List<ReportAgeGroup> getAgeGroupList() {
        return ageGroupList;
    }

    public void setAgeGroupList(List<ReportAgeGroup> ageGroupList) {
        this.ageGroupList = ageGroupList;
    }

    public List<MealTimeDTO> getMealTimeList() {
        return mealTimeList;
    }

    public void setMealTimeList(List<MealTimeDTO> mealTimeList) {
        this.mealTimeList = mealTimeList;
    }
}
