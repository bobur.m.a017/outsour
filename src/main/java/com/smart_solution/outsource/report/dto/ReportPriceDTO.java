package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;

public class ReportPriceDTO {
    private String productName;
    private String measurementType;
    private Integer productId;
    private BigDecimal weight;
    private BigDecimal priceBozor;
    private BigDecimal priceXokim;
    private int number;

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getPriceBozor() {
        return priceBozor;
    }

    public void setPriceBozor(BigDecimal priceBozor) {
        this.priceBozor = priceBozor;
    }

    public BigDecimal getPriceXokim() {
        return priceXokim;
    }

    public void setPriceXokim(BigDecimal priceXokim) {
        this.priceXokim = priceXokim;
    }
}
