package com.smart_solution.outsource.report.dto;

import java.util.List;

public class MealTimeDTO {

    private Integer id;
    private String name;
    private List<MealDTO> mealList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MealDTO> getMealList() {
        return mealList;
    }

    public void setMealList(List<MealDTO> mealList) {
        this.mealList = mealList;
    }

    public MealTimeDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public MealTimeDTO() {
    }
}
