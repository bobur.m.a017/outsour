package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;
import java.util.List;

public class AgeGroupDTO {

    private Integer id;
    private String name;
    private BigDecimal mealWeight;
    private List<ProductDTO> productList;

    public AgeGroupDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public AgeGroupDTO() {
    }

    public BigDecimal getMealWeight() {
        return mealWeight;
    }

    public void setMealWeight(BigDecimal mealWeight) {
        this.mealWeight = mealWeight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDTO> productList) {
        this.productList = productList;
    }
}
