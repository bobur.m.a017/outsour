package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ReportByAgeGroup {
    private Timestamp date;
    private BigDecimal totalPrice;
    private BigDecimal totalPriceVAT;
    private BigDecimal wages;
    private BigDecimal total;
    private String name;

    public Timestamp getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalPriceVAT() {
        return totalPriceVAT;
    }

    public void setTotalPriceVAT(BigDecimal totalPriceVAT) {
        this.totalPriceVAT = totalPriceVAT;
    }

    public BigDecimal getWages() {
        return wages;
    }

    public void setWages(BigDecimal wages) {
        this.wages = wages;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
