package com.smart_solution.outsource.report.dto;

import java.math.BigDecimal;
import java.util.List;

public class MealDTO {

    private Integer id;
    private String name;
    private BigDecimal mealWeight;
    private List<AgeGroupDTO> ageGroupList;
    private List<ProductDTO> productList;

    public MealDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public MealDTO(Integer id, String name, BigDecimal mealWeight) {
        this.id = id;
        this.name = name;
        this.mealWeight = mealWeight;
    }

    public MealDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMealWeight() {
        return mealWeight;
    }

    public void setMealWeight(BigDecimal mealWeight) {
        this.mealWeight = mealWeight;
    }

    public List<AgeGroupDTO> getAgeGroupList() {
        return ageGroupList;
    }

    public void setAgeGroupList(List<AgeGroupDTO> ageGroupList) {
        this.ageGroupList = ageGroupList;
    }

    public List<ProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDTO> productList) {
        this.productList = productList;
    }
}
