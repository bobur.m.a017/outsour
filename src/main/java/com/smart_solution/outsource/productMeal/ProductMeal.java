package com.smart_solution.outsource.productMeal;

import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.product.Product;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class ProductMeal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(precision = 19,scale = 6)
    private BigDecimal weight;
    @Column(precision = 19,scale = 6)
    private BigDecimal withoutExit;

    @OneToOne
    private Ingredient ingredient;

    @OneToOne
    private Product product;


    public ProductMeal() {
    }

    public BigDecimal getWithoutExit() {
        return withoutExit;
    }

    public void setWithoutExit(BigDecimal withoutExit) {
        this.withoutExit = withoutExit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}


