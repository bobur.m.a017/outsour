package com.smart_solution.outsource.productMeal;


import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.ingredient.IngredientRepository;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductMealService {

    private final ProductMealRepository productMealRepository;
    private final ProductRepository productRepository;
    private final IngredientRepository ingredientRepository;

    public ProductMealService(ProductMealRepository productMealRepository, ProductRepository productRepository, IngredientRepository ingredientRepository) {
        this.productMealRepository = productMealRepository;
        this.productRepository = productRepository;
        this.ingredientRepository = ingredientRepository;
    }


    public ProductMeal add(ProductMealDTO productMealDTO) {

        ProductMeal productMeal = new ProductMeal();

        Product product = productRepository.findById(productMealDTO.getProductId()).get();

        productMeal.setProduct(product);
        productMeal.setWeight(BigDecimal.valueOf(productMealDTO.getWeight()).divide(BigDecimal.valueOf(1000),6,RoundingMode.HALF_UP));
        productMeal.setWithoutExit(BigDecimal.valueOf(productMealDTO.getWithoutExit()).divide(BigDecimal.valueOf(1000),6,RoundingMode.HALF_UP));
        productMeal.setIngredient(calculateIngredient(product, productMeal.getWeight()));

        return productMealRepository.save(productMeal);
    }

    public Ingredient calculateIngredient(Product product, BigDecimal weight) {

        Ingredient ingredient = new Ingredient();
        ingredient.setProtein(((product.getIngredient().getProtein().divide(BigDecimal.valueOf(0.1),8, RoundingMode.HALF_UP)).multiply(weight)));
        ingredient.setOil(((product.getIngredient().getOil().divide(BigDecimal.valueOf(0.1),8, RoundingMode.HALF_UP)).multiply(weight)));
        ingredient.setKcal(((product.getIngredient().getKcal().divide(BigDecimal.valueOf(0.1),8, RoundingMode.HALF_UP)).multiply(weight)));
        ingredient.setCarbohydrates(((product.getIngredient().getCarbohydrates().divide(BigDecimal.valueOf(0.1),8, RoundingMode.HALF_UP)).multiply(weight)));

        return ingredientRepository.save(ingredient);
    }

    public ProductMealDTO parse(ProductMeal productMeal) {

        return new ProductMealDTO(
                productMeal.getId(),
                Double.valueOf(productMeal.getWeight().multiply(BigDecimal.valueOf(1000)).toString()),
                Double.valueOf(productMeal.getWithoutExit().multiply(BigDecimal.valueOf(1000)).toString()),
                productMeal.getProduct().getId(),
                productMeal.getProduct().getName()
        );
    }

    public ProductMeal edit(ProductMealDTO productMealDTO) {

        ProductMeal productMeal = productMealRepository.findById(productMealDTO.getId()).get();

        Product product = productRepository.findById(productMealDTO.getProductId()).get();

        productMeal.setProduct(product);

        productMeal.setWeight(BigDecimal.valueOf(productMealDTO.getWeight()).divide(BigDecimal.valueOf(1000),4,RoundingMode.HALF_UP));

        productMeal.setWithoutExit(BigDecimal.valueOf(productMealDTO.getWithoutExit()).divide(BigDecimal.valueOf(1000),4,RoundingMode.HALF_UP));

        productMeal.setIngredient(calculateIngredient(product, BigDecimal.valueOf(productMealDTO.getWeight())));

        return productMealRepository.save(productMeal);
    }

    public void delete(ProductMeal productMeal) {

        Integer id = productMeal.getIngredient().getId();
        productMeal.setIngredient(null);
        productMealRepository.save(productMeal);

        ingredientRepository.deleteById(id);
        productMealRepository.delete(productMeal);
    }
}
