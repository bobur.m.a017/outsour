package com.smart_solution.outsource.productMeal;

public class ProductMealDTO {
    private Integer id;
    private Double weight;

    private Double withoutExit;
    private Integer productId;
    private String name;

    public ProductMealDTO(Integer id, Double weight, Double withoutExit, Integer productId, String name) {
        this.id = id;
        this.weight = weight;
        this.withoutExit = withoutExit;
        this.productId = productId;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductMealDTO() {
    }

    public Double getWithoutExit() {
        return withoutExit;
    }

    public void setWithoutExit(Double withoutExit) {
        this.withoutExit = withoutExit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

}
