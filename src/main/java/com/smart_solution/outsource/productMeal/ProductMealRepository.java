package com.smart_solution.outsource.productMeal;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductMealRepository extends JpaRepository<ProductMeal, Integer> {
}
