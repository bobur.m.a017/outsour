package com.smart_solution.outsource.agreement.price;

import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AgreementPriceRepository extends JpaRepository<AgreementPrice, Integer> {

    Optional<AgreementPrice> findByAgreementAndProduct(Agreement agreement, Product product);
    Optional<AgreementPrice> findByAgreement_NumberAndProduct_Id(String number, Integer productId);
}
