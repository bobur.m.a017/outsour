package com.smart_solution.outsource.agreement.price;


import java.math.BigDecimal;

public class AgreementPriceResponseDTO {
    private BigDecimal weight;
    private BigDecimal successWeight;
    private BigDecimal residue;
    private BigDecimal price;
    private BigDecimal totalSumWeight;
    private BigDecimal totalSumResidue;
    private BigDecimal totalSumSuccessWeight;
    private String productName;
    private Integer productId;
    private Integer id;

    public AgreementPriceResponseDTO(BigDecimal weight, BigDecimal successWeight, BigDecimal residue, BigDecimal price, BigDecimal totalSumWeight, String productName, Integer productId, Integer id, BigDecimal totalSumResidue, BigDecimal totalSumSuccessWeight) {
        this.weight = weight;
        this.successWeight = successWeight;
        this.residue = residue;
        this.price = price;
        this.totalSumWeight = totalSumWeight;
        this.productName = productName;
        this.productId = productId;
        this.id = id;
        this.totalSumResidue = totalSumResidue;
        this.totalSumSuccessWeight = totalSumSuccessWeight;
    }

    public AgreementPriceResponseDTO() {
    }

    public BigDecimal getTotalSumResidue() {
        return totalSumResidue;
    }

    public void setTotalSumResidue(BigDecimal totalSumResidue) {
        this.totalSumResidue = totalSumResidue;
    }

    public BigDecimal getTotalSumSuccessWeight() {
        return totalSumSuccessWeight;
    }

    public void setTotalSumSuccessWeight(BigDecimal totalSumSuccessWeight) {
        this.totalSumSuccessWeight = totalSumSuccessWeight;
    }

    public AgreementPriceResponseDTO(BigDecimal weight, BigDecimal successWeight, BigDecimal residue, BigDecimal price, BigDecimal totalSumWeight, String productName, Integer productId) {
        this.weight = weight;
        this.successWeight = successWeight;
        this.residue = residue;
        this.price = price;
        this.totalSumWeight = totalSumWeight;
        this.productName = productName;
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getResidue() {
        return residue;
    }

    public void setResidue(BigDecimal residue) {
        this.residue = residue;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalSumWeight() {
        return totalSumWeight;
    }

    public void setTotalSumWeight(BigDecimal totalSumWeight) {
        this.totalSumWeight = totalSumWeight;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}
