package com.smart_solution.outsource.agreement.price;


import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.product.Product;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AgreementPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal successWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal residue;

    @Column(precision = 19, scale = 6)
    private BigDecimal price;

    @ManyToOne
    private Product product;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(cascade = CascadeType.ALL)
    private Agreement agreement;

    public AgreementPrice() {
    }

    public AgreementPrice(BigDecimal weight, BigDecimal successWeight, BigDecimal residue, BigDecimal price, Product product, Agreement agreement) {
        this.weight = weight;
        this.successWeight = successWeight;
        this.residue = residue;
        this.price = price;
        this.product = product;
        this.agreement = agreement;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getResidue() {
        return residue;
    }

    public void setResidue(BigDecimal residue) {
        this.residue = residue;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Agreement getAgreement() {
        return agreement;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }
}
