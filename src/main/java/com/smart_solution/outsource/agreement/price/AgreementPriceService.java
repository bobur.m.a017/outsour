package com.smart_solution.outsource.agreement.price;


import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public record AgreementPriceService(
        ProductRepository productRepository,
        AgreementPriceRepository agreementPriceRepository
) {


    public AgreementPrice add(Agreement agreement, Product product, BigDecimal weight, BigDecimal successWeight, BigDecimal residue) {
        Optional<AgreementPrice> optional = agreementPriceRepository.findByAgreementAndProduct(agreement, product);

        if (optional.isPresent()){
            AgreementPrice agreementPrice = optional.get();
            agreementPrice.setWeight(agreementPrice.getWeight().add(weight));
            agreementPrice.setSuccessWeight(agreementPrice.getSuccessWeight().add(successWeight));
            agreementPrice.setResidue(agreementPrice.getResidue().add(residue));
            return agreementPrice;
        }else {

            AgreementPrice agreementPrice = new AgreementPrice();
            agreementPrice.setAgreement(agreement);
            agreementPrice.setPrice(BigDecimal.valueOf(0));
            agreementPrice.setProduct(product);
            agreementPrice.setWeight(weight);
            agreementPrice.setSuccessWeight(successWeight);
            agreementPrice.setResidue(residue);
            return agreementPrice;
        }
    }

    public AgreementPrice subtract(Agreement agreement, Product product, BigDecimal weight, BigDecimal successWeight, BigDecimal residue) {
        Optional<AgreementPrice> optional = agreementPriceRepository.findByAgreementAndProduct(agreement, product);

        BigDecimal zero = BigDecimal.valueOf(0);

        if (optional.isPresent()){
            AgreementPrice agreementPrice = optional.get();
            agreementPrice.setWeight(agreementPrice.getWeight().subtract(weight));
            agreementPrice.setSuccessWeight(agreementPrice.getSuccessWeight().subtract(successWeight));
            agreementPrice.setResidue(agreementPrice.getResidue().subtract(residue));
            return agreementPrice;
        }else {

            AgreementPrice agreementPrice = new AgreementPrice();
            agreementPrice.setAgreement(agreement);
            agreementPrice.setPrice(zero);
            agreementPrice.setProduct(product);
            agreementPrice.setWeight(zero);
            agreementPrice.setSuccessWeight(zero);
            agreementPrice.setResidue(zero);
            return agreementPrice;
        }
    }

    public AgreementPriceResponseDTO parse(AgreementPrice agreementPrice) {
        return new AgreementPriceResponseDTO(
                agreementPrice.getWeight(),
                agreementPrice.getSuccessWeight(),
                agreementPrice.getResidue(),
                agreementPrice.getPrice(),
                agreementPrice.getWeight().multiply(agreementPrice.getPrice()),
                agreementPrice.getProduct().getName(),
                agreementPrice.getProduct().getId(),
                agreementPrice.getId(),
                agreementPrice.getResidue().multiply(agreementPrice.getPrice()),
                agreementPrice.getSuccessWeight().multiply(agreementPrice.getPrice())
        );
    }
}
