package com.smart_solution.outsource.agreement;

import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergartenResponseDTO;
import com.smart_solution.outsource.agreement.price.AgreementPriceResponseDTO;
import com.smart_solution.outsource.agreement.product.AgreementProductResponseDTO;
import io.swagger.models.auth.In;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class AgreementResponseDTO {

    private Integer id;
    private String number;
    private String name;
    private Timestamp date;
    private String status;
    private String districtName;
    private Integer districtId;
    private List<AgreementKindergartenResponseDTO> kindergartenList;
    private List<AgreementPriceResponseDTO> productList;
    private BigDecimal totalSumWeight;
    private BigDecimal totalSumResidue;
    private BigDecimal totalSumSuccessWeight;


    public AgreementResponseDTO(Integer id, String number, Timestamp date, String status, String districtName, Integer districtId, String name) {
        this.id = id;
        this.number = number;
        this.date = date;
        this.status = status;
        this.districtName = districtName;
        this.districtId = districtId;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalSumWeight() {
        return totalSumWeight;
    }

    public void setTotalSumWeight(BigDecimal totalSumWeight) {
        this.totalSumWeight = totalSumWeight;
    }

    public BigDecimal getTotalSumResidue() {
        return totalSumResidue;
    }

    public void setTotalSumResidue(BigDecimal totalSumResidue) {
        this.totalSumResidue = totalSumResidue;
    }

    public BigDecimal getTotalSumSuccessWeight() {
        return totalSumSuccessWeight;
    }

    public void setTotalSumSuccessWeight(BigDecimal totalSumSuccessWeight) {
        this.totalSumSuccessWeight = totalSumSuccessWeight;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public List<AgreementPriceResponseDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<AgreementPriceResponseDTO> productList) {
        this.productList = productList;
    }

    public List<AgreementKindergartenResponseDTO> getKindergartenList() {
        return kindergartenList;
    }

    public void setKindergartenList(List<AgreementKindergartenResponseDTO> kindergartenList) {
        this.kindergartenList = kindergartenList;
    }

    public AgreementResponseDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}