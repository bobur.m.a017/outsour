package com.smart_solution.outsource.agreement;


import com.smart_solution.outsource.address.district.District;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.agreement.price.AgreementPrice;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.users.Users;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Agreement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "agreement", cascade = CascadeType.ALL)
    private List<AgreementKindergarten> agreementKindergartenList;

    private String status;

    @Column(unique = true)
    private String number;

    private String name;

    private Timestamp date;

    @ManyToOne
    private Users createBy;

    @ManyToOne
    private Users updateBy;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private Company company;

    @ManyToOne
    private District district;

    @OneToMany(mappedBy = "agreement", cascade = CascadeType.ALL)
    List<AgreementPrice> agreementPriceList;

    public List<AgreementPrice> getAgreementPriceList() {
        return agreementPriceList;
    }

    public void setAgreementPriceList(List<AgreementPrice> agreementPriceList) {
        this.agreementPriceList = agreementPriceList;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Agreement(String status, String number, Timestamp date, Users createBy, Company company, District district, String name) {
        this.status = status;
        this.number = number;
        this.date = date;
        this.createBy = createBy;
        this.company = company;
        this.district = district;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Agreement() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AgreementKindergarten> getAgreementKindergartenList() {
        return agreementKindergartenList;
    }

    public void setAgreementKindergartenList(List<AgreementKindergarten> agreementKindergartenList) {
        this.agreementKindergartenList = agreementKindergartenList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Users getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Users createBy) {
        this.createBy = createBy;
    }

    public Users getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Users updateBy) {
        this.updateBy = updateBy;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}