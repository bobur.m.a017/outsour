package com.smart_solution.outsource.agreement.product;


import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public record AgreementProductService(
        ProductRepository productRepository,
        AgreementProductRepository agreementProductRepository
        ) {


//    public AgreementProduct add(AgreementProductDTO dto, AgreementKindergarten agreementKindergarten){
//        Product product = productRepository.findById(dto.getProductId()).get();
//
//        return (new AgreementProduct(BigDecimal.valueOf(dto.getWeight()), product, agreementKindergarten, BigDecimal.valueOf(0),  BigDecimal.valueOf(0)));
//    }

    public AgreementProductResponseDTO parse(AgreementProduct agreementProduct){
        return new AgreementProductResponseDTO(agreementProduct.getWeight(),agreementProduct.getSuccessWeight(),agreementProduct.getResidue(),agreementProduct.getProduct().getName(),agreementProduct.getProduct().getId());
    }
}
