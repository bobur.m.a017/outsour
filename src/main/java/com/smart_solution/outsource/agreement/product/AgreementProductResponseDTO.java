package com.smart_solution.outsource.agreement.product;


import javax.persistence.Column;
import java.math.BigDecimal;

public class AgreementProductResponseDTO {
    private BigDecimal weight;
    private BigDecimal successWeight;
    private BigDecimal residue;
    private String productName;
    private Integer productId;

    public AgreementProductResponseDTO() {
    }

    public AgreementProductResponseDTO(BigDecimal weight, BigDecimal successWeight, BigDecimal residue, String productName, Integer productId) {
        this.weight = weight;
        this.successWeight = successWeight;
        this.residue = residue;
        this.productName = productName;
        this.productId = productId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public AgreementProductResponseDTO(BigDecimal weight, BigDecimal successWeight, BigDecimal residue, String productName) {
        this.weight = weight;
        this.successWeight = successWeight;
        this.residue = residue;
        this.productName = productName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getResidue() {
        return residue;
    }

    public void setResidue(BigDecimal residue) {
        this.residue = residue;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
