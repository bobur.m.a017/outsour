package com.smart_solution.outsource.agreement.product;


import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergartenDTO;
import com.smart_solution.outsource.kindergarten.KindergartenDTO;
import com.smart_solution.outsource.product.Product;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

public class AgreementProductDTO {
    private Integer productId;
    private String productName;
    private List<AgreementKindergartenDTO> kindergartenList;

    public AgreementProductDTO(Integer productId, String productName) {
        this.productId = productId;
        this.productName = productName;
    }

    public AgreementProductDTO(Integer productId, String productName, List<AgreementKindergartenDTO> kindergartenList) {
        this.productId = productId;
        this.productName = productName;
        this.kindergartenList = kindergartenList;
    }

    public AgreementProductDTO() {
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<AgreementKindergartenDTO> getKindergartenList() {
        return kindergartenList;
    }

    public void setKindergartenList(List<AgreementKindergartenDTO> kindergartenList) {
        this.kindergartenList = kindergartenList;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}
