package com.smart_solution.outsource.agreement.product;


import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.product.Product;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AgreementProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal successWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal residue;

    @ManyToOne
    private Product product;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(cascade = CascadeType.ALL)
    private AgreementKindergarten agreementKindergarten;


    public AgreementProduct() {
    }

    public AgreementProduct(BigDecimal weight, Product product, AgreementKindergarten agreementKindergarten, BigDecimal successWeight, BigDecimal residue) {
        this.weight = weight;
        this.product = product;
        this.agreementKindergarten = agreementKindergarten;
        this.successWeight = successWeight;
        this.residue = residue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSuccessWeight() {
        return successWeight;
    }

    public void setSuccessWeight(BigDecimal successWeight) {
        this.successWeight = successWeight;
    }

    public BigDecimal getResidue() {
        return this.weight.subtract(this.successWeight);
    }

    public void setResidue(BigDecimal residue) {
        this.residue = residue;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public AgreementKindergarten getAgreementKindergarten() {
        return agreementKindergarten;
    }

    public void setAgreementKindergarten(AgreementKindergarten agreementKindergarten) {
        this.agreementKindergarten = agreementKindergarten;
    }
}
