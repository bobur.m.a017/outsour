package com.smart_solution.outsource.agreement.product;

import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.agreement.kindergarten.AgreementKindergarten;
import com.smart_solution.outsource.agreement.product.AgreementProduct;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AgreementProductRepository extends JpaRepository<AgreementProduct, Integer> {

    Optional<AgreementProduct> findByAgreementKindergarten_KindergartenAndProductAndAgreementKindergarten_Agreement(Kindergarten kindergarten, Product product, Agreement agreement);

    Optional<AgreementProduct> findByAgreementKindergartenAndProduct(AgreementKindergarten agreementKindergarten, Product product);

    Optional<AgreementProduct> findByAgreementKindergartenAndProduct_Id(AgreementKindergarten agreementKindergarten, Integer id);

    List<AgreementProduct> findAllByAgreementKindergarten_Agreement(Agreement agreement);

    Optional<AgreementProduct> findByAgreementKindergarten_AgreementAndAgreementKindergarten_KindergartenAndProduct(Agreement agreement, Kindergarten kindergarten,Product product);
}
