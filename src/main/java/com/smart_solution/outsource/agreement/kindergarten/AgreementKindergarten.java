package com.smart_solution.outsource.agreement.kindergarten;


import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.agreement.product.AgreementProduct;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
public class AgreementKindergarten {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Kindergarten kindergarten;


    @OneToMany(mappedBy = "agreementKindergarten", cascade = CascadeType.ALL)
    private List<AgreementProduct> agreementProductList;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(cascade = CascadeType.ALL)
    private Agreement agreement;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Kindergarten getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(Kindergarten kindergarten) {
        this.kindergarten = kindergarten;
    }

    public List<AgreementProduct> getAgreementProductList() {
        return agreementProductList;
    }

    public void setAgreementProductList(List<AgreementProduct> agreementProductList) {
        this.agreementProductList = agreementProductList;
    }

    public Agreement getAgreement() {
        return agreement;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }
}