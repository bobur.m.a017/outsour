package com.smart_solution.outsource.agreement.kindergarten;

import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AgreementKindergartenRepository extends JpaRepository<AgreementKindergarten, Integer> {
    Optional<AgreementKindergarten> findByAgreementAndKindergarten(Agreement agreement, Kindergarten kindergarten);
    List<AgreementKindergarten> findAllByKindergarten(Kindergarten kindergarten);
}
