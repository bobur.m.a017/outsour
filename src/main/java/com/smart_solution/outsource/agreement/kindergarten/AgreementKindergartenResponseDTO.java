package com.smart_solution.outsource.agreement.kindergarten;


import com.smart_solution.outsource.agreement.product.AgreementProductDTO;
import com.smart_solution.outsource.agreement.product.AgreementProductResponseDTO;

import java.util.List;

public class AgreementKindergartenResponseDTO {

    private Integer id;
    private String kindergartenName;
    private String districtName;
    private List<AgreementProductResponseDTO> productList;

    public AgreementKindergartenResponseDTO() {
    }

    public AgreementKindergartenResponseDTO(String kindergartenName, String districtName) {
        this.kindergartenName = kindergartenName;
        this.districtName = districtName;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AgreementProductResponseDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<AgreementProductResponseDTO> productList) {
        this.productList = productList;
    }
}