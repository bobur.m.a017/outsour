package com.smart_solution.outsource.agreement.kindergarten;


import com.smart_solution.outsource.agreement.Agreement;
import com.smart_solution.outsource.agreement.product.AgreementProduct;
import com.smart_solution.outsource.agreement.product.AgreementProductDTO;
import com.smart_solution.outsource.agreement.product.AgreementProductResponseDTO;
import com.smart_solution.outsource.agreement.product.AgreementProductService;
import com.smart_solution.outsource.kindergarten.Kindergarten;
import com.smart_solution.outsource.kindergarten.KindergartenRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public record AgreementKindergartenService(
        AgreementKindergartenRepository agreementKindergartenRepository,
        KindergartenRepository kindergartenRepository,
        AgreementProductService agreementProductService
) {

//    public AgreementKindergarten add(AgreementKindergartenDTO dto, Agreement agreement) {
//        Kindergarten kindergarten = kindergartenRepository.findById(dto.getId()).get();
//        AgreementKindergarten agreementKindergarten = new AgreementKindergarten();
//        agreementKindergarten.setKindergarten(kindergarten);
//        agreementKindergarten.setAgreement(agreement);
//
//        List<AgreementProduct> list = new ArrayList<>();
//
//        for (AgreementProductDTO agreementProductDTO : dto.getAgreementProductDTOListList()) {
//            list.add(agreementProductService.add(agreementProductDTO,agreementKindergarten));
//        }
//        agreementKindergarten.setAgreementProductList(list);
//        return agreementKindergarten;
//    }

    public AgreementKindergarten add(Integer id, Agreement agreement) {
        Kindergarten kindergarten = kindergartenRepository.findById(id).get();
        AgreementKindergarten agreementKindergarten = new AgreementKindergarten();
        agreementKindergarten.setKindergarten(kindergarten);
        agreementKindergarten.setAgreement(agreement);
        return agreementKindergarten;
    }

    public AgreementKindergartenResponseDTO parse(AgreementKindergarten agreementKindergarten) {
        AgreementKindergartenResponseDTO dto = new AgreementKindergartenResponseDTO(agreementKindergarten.getKindergarten().getName(), agreementKindergarten.getKindergarten().getAddress().getDistrict().getName());

        dto.setId(agreementKindergarten.getId());
        List<AgreementProductResponseDTO> list = new ArrayList<>();

        for (AgreementProduct agreementProduct : agreementKindergarten.getAgreementProductList()) {
            list.add(agreementProductService.parse(agreementProduct));
        }
        dto.setProductList(list);
        return dto;

    }

//    public AgreementKindergarten edit(AgreementKindergarten agreementKindergarten, AgreementKindergartenDTO dto) {
//
//        List<AgreementProduct> list = new ArrayList<>();
//
//
//        for (AgreementProductDTO productDTO : dto.getAgreementProductDTOListList()) {
//            boolean res = true;
//            for (AgreementProduct agreementProduct : agreementKindergarten.getAgreementProductList()) {
//                if (productDTO.getProductId().equals(agreementProduct.getProduct().getId())) {
//                    agreementProduct.setWeight(BigDecimal.valueOf(productDTO.getWeight()));
//                    list.add(agreementProduct);
//                    res = false;
//                }
//            }
//            if (res) {
//                list.add(agreementProductService.add(productDTO, agreementKindergarten));
//            }
//        }
//        agreementKindergarten.setAgreementProductList(list);
//        return  agreementKindergartenRepository.save(agreementKindergarten);
//    }
}