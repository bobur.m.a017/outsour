package com.smart_solution.outsource.agreement.kindergarten;

public class AgreementKindergartenDTO {

    private Integer id;
    private String kindergartenName;
    private Double weight;

    public AgreementKindergartenDTO() {
    }

    public AgreementKindergartenDTO(Integer id, String kindergartenName, Double weight) {
        this.id = id;
        this.kindergartenName = kindergartenName;
        this.weight = weight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }
}