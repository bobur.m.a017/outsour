package com.smart_solution.outsource.agreement;

import com.smart_solution.outsource.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AgreementRepository extends JpaRepository<Agreement, Integer> {

 boolean existsByNumber(String number);
 List<Agreement> findAllByCompany(Company company);
}
