package com.smart_solution.outsource.agreement;

import com.smart_solution.outsource.agreement.product.AgreementProductDTO;

import java.sql.Timestamp;
import java.util.List;

public class AgreementDTOByProduct {
    private Integer id;
    private String number;
    private Timestamp date;
    private String status;
    private String districtName;
    private Integer districtId;
    private List<AgreementProductDTO> kinProductList;


    public AgreementDTOByProduct() {
    }

    public AgreementDTOByProduct(Integer id, String number, Timestamp date, String status, String districtName) {
        this.id = id;
        this.number = number;
        this.date = date;
        this.status = status;
        this.districtName = districtName;
    }

    public AgreementDTOByProduct(Integer id, String number, Timestamp date, String status, String districtName, Integer districtId, List<AgreementProductDTO> kinProductList) {
        this.id = id;
        this.number = number;
        this.date = date;
        this.status = status;
        this.districtName = districtName;
        this.districtId = districtId;
        this.kinProductList = kinProductList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public List<AgreementProductDTO> getKinProductList() {
        return kinProductList;
    }

    public void setKinProductList(List<AgreementProductDTO> kinProductList) {
        this.kinProductList = kinProductList;
    }
}
