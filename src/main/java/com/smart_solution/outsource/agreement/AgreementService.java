package com.smart_solution.outsource.agreement;

import com.smart_solution.outsource.address.district.District;
import com.smart_solution.outsource.address.district.DistrictRepository;
import com.smart_solution.outsource.agreement.kindergarten.*;
import com.smart_solution.outsource.agreement.price.*;
import com.smart_solution.outsource.agreement.product.*;
import com.smart_solution.outsource.company.Company;
import com.smart_solution.outsource.kindergarten.*;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.status.Status;
import com.smart_solution.outsource.users.ResponseUser;
import com.smart_solution.outsource.users.Users;
import com.smart_solution.outsource.users.UsersRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public record AgreementService(
        AgreementRepository agreementRepository,
        UsersRepository usersRepository,
        AgreementKindergartenService agreementKindergartenService,
        KindergartenRepository kindergartenRepository,
        AgreementProductService agreementProductService,
        AgreementKindergartenRepository agreementKindergartenRepository,
        AgreementPriceService agreementPriceService,
        AgreementPriceRepository agreementPriceRepository,
        DistrictRepository districtRepository,
        KindergartenService kindergartenService,
        ProductRepository productRepository,
        AgreementProductRepository agreementProductRepository
) {


    public StateMessage add(AgreementDTO dto, ResponseUser responseUser) {

        Users users = checkUser(responseUser);
        if (users == null) {
            return userNodFound();
        }

        Company company = users.getCompany();
        District district = districtRepository.findById(dto.getDistrictId()).get();

        boolean res = agreementRepository.existsByNumber(dto.getNumber());

        if (!res) {
            agreementRepository.save(new Agreement(Status.NEW.getName(), dto.getNumber(), new Timestamp(dto.getDate()), users, company, district, dto.getName()));

            return new StateMessage("Shartnoma muvaffaqiyatli qo`shildi.", true);
        } else {
            return new StateMessage("Shartnoma qo`shilmadi. Ushbu raqam bilan avval shartnoma kiritilgan", false);
        }
    }

    public StateMessage edit(Integer id, AgreementDTO agreementDTO, ResponseUser responseUser) {
        Users users = checkUser(responseUser);
        if (users == null) {
            return userNodFound();
        }

        Optional<Agreement> optional = agreementRepository.findById(id);
        if (optional.isEmpty()) {
            return noInformation();
        }

        Agreement agreement = optional.get();

        if (agreement.getStatus().equals(Status.NEW.getName()) || agreement.getStatus().equals(Status.SEND.getName())) {
            District district = districtRepository.findById(agreementDTO.getDistrictId()).get();

            if (!agreement.getNumber().equals(agreementDTO.getNumber())) {

                boolean res = agreementRepository.existsByNumber(agreementDTO.getNumber());
                if (res) {
                    return new StateMessage("O`zgartirishni imkoni yo`q. Bunday raqamli shartnoma mavjud.", false);
                }
                agreement.setNumber(agreementDTO.getNumber());
            }
            agreement.setName(agreementDTO.getName());
            agreement.setDistrict(district);
            agreement.setDate(new Timestamp(agreementDTO.getDate()));
            agreement.setUpdateBy(users);
            agreementRepository.save(agreement);
            return new StateMessage("Muvaffaqiyatli o`zgartirildi.", true);
        }
        return new StateMessage("O`zgartirishni imkoni yo`q", false);

    }

//    public StateMessage addKin(ResponseUser responseUser, Integer id, AgreementKindergartenDTO dto) {
//        Optional<Kindergarten> optionalKin = kindergartenRepository.findById(dto.getId());
//        if (optionalKin.isEmpty()) {
//            return noInformation();
//        }
//        Kindergarten kindergarten = optionalKin.get();
//
//        Users users = checkUser(responseUser);
//        if (users == null) {
//            return userNodFound();
//        }
//
//        if (kindergarten.getType().equals(KindergartenType.TAMINOT.getName())) {
//
//            Optional<Agreement> optionalAgreement = agreementRepository.findById(id);
//            if (optionalAgreement.isEmpty()) {
//                return noInformation();
//            }
//            Agreement agreement = optionalAgreement.get();
//
//            Optional<AgreementKindergarten> optional = agreementKindergartenRepository.findByAgreementAndKindergarten(agreement, kindergarten);
//            if (optional.isPresent()) {
//                AgreementKindergarten agreementKindergarten = optional.get();
//                return editKin(responseUser, agreementKindergarten.getId(), dto);
//            }
//
//            agreement.setUpdateBy(users);
//
//            AgreementKindergarten agreementKindergarten = agreementKindergartenService.add(dto, agreement);
//
////            agreement.setAgreementPriceList(addPrice(agreementKindergarten));
//
//            List<AgreementKindergarten> agreementKindergartenList = agreement.getAgreementKindergartenList();
//            agreementKindergartenList.add(agreementKindergarten);
//            agreement.setAgreementKindergartenList(agreementKindergartenList);
//            calculateProduct(agreementRepository.save(agreement));
//
//            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
//        }
//        return new StateMessage("Shartnoma qo`shilmadi. MTT toifasi boshqa", false);
//    }

    public StateMessage addKin(ResponseUser responseUser, Integer id, List<Integer> list) {

        Optional<Agreement> optionalAgreement = agreementRepository.findById(id);
        if (optionalAgreement.isEmpty()) {
            return noInformation();
        }
        Agreement agreement = optionalAgreement.get();

        if (agreement.getStatus().equals(Status.NEW.getName()) || agreement.getStatus().equals(Status.SEND.getName())) {

            Users users = checkUser(responseUser);
            if (users == null) {
                return userNodFound();
            }

            List<AgreementKindergarten> kinList = new ArrayList<>();


            for (Integer integer : list) {
                Kindergarten kindergarten = kindergartenRepository.findById(integer).get();
                if (kindergarten.getType().equals(KindergartenType.TAMINOT.getName())) {
                    Optional<AgreementKindergarten> optional = agreementKindergartenRepository.findByAgreementAndKindergarten(agreement, kindergarten);
                    if (optional.isEmpty()) {
                        kinList.add(agreementKindergartenService.add(integer, agreement));
                    }
                }
            }

            agreement.setUpdateBy(users);

            List<AgreementKindergarten> agreementKindergartenList = agreement.getAgreementKindergartenList();
            agreementKindergartenList.addAll(agreementKindergartenRepository.saveAll(kinList));
            agreement.setAgreementKindergartenList(agreementKindergartenList);
            calculateProduct(agreementRepository.save(agreement));

            return new StateMessage("Muvaffaqiyatli qo`shildi", true);
        }
        return new StateMessage("O`zgartirishni imkoni yo`q", false);

    }

    public StateMessage addPrice(List<AgreementPriceDTO> list) {

        List<AgreementPrice> priceList = new ArrayList<>();
        if (list.size() > 0) {
            AgreementPrice price = agreementPriceRepository.findById(list.get(0).getId()).get();
            Agreement agreement = price.getAgreement();

            if (agreement.getStatus().equals(Status.NEW.getName()) || agreement.getStatus().equals(Status.SEND.getName())) {
                for (AgreementPriceDTO agreementPriceDTO : list) {
                    AgreementPrice agreementPrice = agreementPriceRepository.findById(agreementPriceDTO.getId()).get();
                    agreementPrice.setPrice(BigDecimal.valueOf(agreementPriceDTO.getPrice()));
                    priceList.add(agreementPrice);
                }
                agreementPriceRepository.saveAll(priceList);
                return new StateMessage("Muvaffaqiyatli kiritildi.", true);
            }
        }
        return new StateMessage("O`zgartirishni imkoni yo`q", false);
    }

    public List<AgreementPrice> addPrice(AgreementKindergarten agreementKindergarten) {

        List<AgreementPrice> list = new ArrayList<>();
        for (AgreementProduct agreementProduct : agreementKindergarten.getAgreementProductList()) {
            list.add(agreementPriceService.add(agreementKindergarten.getAgreement(), agreementProduct.getProduct(), agreementProduct.getWeight(), agreementProduct.getSuccessWeight(), agreementProduct.getResidue()));
        }
        return agreementPriceRepository.saveAll(list);

    }

    public void subtractPrice(AgreementKindergarten agreementKindergarten) {

        List<AgreementPrice> list = new ArrayList<>();
        for (AgreementProduct agreementProduct : agreementKindergarten.getAgreementProductList()) {
            list.add(agreementPriceService.subtract(agreementKindergarten.getAgreement(), agreementProduct.getProduct(), agreementProduct.getWeight(), agreementProduct.getSuccessWeight(), agreementProduct.getResidue()));
        }
        agreementPriceRepository.saveAll(list);
    }

//    public StateMessage editKin(ResponseUser responseUser, Integer id, AgreementKindergartenDTO dto) {
//        Users users = checkUser(responseUser);
//        if (users == null) {
//            return userNodFound();
//        }
//        Optional<AgreementKindergarten> optional = agreementKindergartenRepository.findById(id);
//        if (optional.isEmpty()) {
//            return noInformation();
//        }
//        AgreementKindergarten agreementKindergarten = optional.get();
//
//        Agreement agreement = agreementKindergarten.getAgreement();
//        agreement.setUpdateBy(users);
//        Agreement save = agreementRepository.save(agreement);
//
//        if (!agreement.getStatus().equals(Status.TOLIQ_YETKAZIB_BERILDI.getName())) {
//            AgreementKindergarten edit = agreementKindergartenService.edit(agreementKindergarten, dto);
////            agreement.setAgreementPriceList(addPrice(edit));
//            calculateProduct(save);
//            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);
//
//        } else {
//            return new StateMessage("O`zgartirib bo`lmaydi maxsulot to`liq yetkazib berilgan", false);
//        }
//    }

    public StateMessage delete(Integer id) {
        Optional<Agreement> optional = agreementRepository.findById(id);

        if (optional.isEmpty()) {
            return noInformation();
        }
        Agreement agreement = optional.get();
        if (agreement.getStatus().equals(Status.NEW.getName())) {

            agreementRepository.deleteById(id);
            return new StateMessage("Muvaffaqtiyatli o`chirildi", true);
        } else {
            return new StateMessage("O`chirishni imkoni yo`q ushbu shartnomaga asosan maxsulot yuborilgan.", false);
        }
    }

    public ResponseEntity<?> getOne(Integer id) {
        Optional<Agreement> optional = agreementRepository.findById(id);

        if (optional.isPresent()) {
            Agreement agreement = optional.get();
            AgreementResponseDTO dto = parse(agreement);

            List<AgreementKindergartenResponseDTO> list = new ArrayList<>();
            for (AgreementKindergarten agreementKindergarten : agreement.getAgreementKindergartenList()) {
                list.add(agreementKindergartenService.parse(agreementKindergarten));
            }
            dto.setKindergartenList(list);

            List<AgreementPriceResponseDTO> priceList = new ArrayList<>();
            for (AgreementPrice agreementPrice : agreement.getAgreementPriceList()) {
                priceList.add(agreementPriceService.parse(agreementPrice));
            }
            dto.setProductList(priceList);

            BigDecimal totalSumWeight = BigDecimal.valueOf(0);
            BigDecimal totalSumResidue = BigDecimal.valueOf(0);
            BigDecimal totalSumSuccessWeight = BigDecimal.valueOf(0);


            for (AgreementPriceResponseDTO agreementPriceResponseDTO : priceList) {
                totalSumWeight = totalSumWeight.add(agreementPriceResponseDTO.getTotalSumWeight());
                totalSumResidue = totalSumResidue.add(agreementPriceResponseDTO.getTotalSumResidue());
                totalSumSuccessWeight = totalSumSuccessWeight.add(agreementPriceResponseDTO.getTotalSumSuccessWeight());
            }
//            priceList.sort(Comparator.comparing(AgreementPriceResponseDTO::getProductName));
//            dto.setProductList(priceList);
//            Banan	Karam	Karam qizil	Kartoshka	Loviya	Makaron	Mol go`shti	Mosh	Piyoz	Yongʼoq magʼzi
            dto.setTotalSumWeight(totalSumWeight);
            dto.setTotalSumSuccessWeight(totalSumSuccessWeight);
            dto.setTotalSumResidue(totalSumResidue);

            return ResponseEntity.status(200).body(sort(dto));
        }
        return ResponseEntity.status(200).body(noInformation());
    }

    public List<AgreementResponseDTO> getAll(ResponseUser responseUser) {
        Users users = checkUser(responseUser);
        if (users == null) {
            return new ArrayList<>();
        }
        Company company = users.getCompany();

        List<AgreementResponseDTO> list = new ArrayList<>();

        for (Agreement agreement : agreementRepository.findAllByCompany(company)) {
            list.add(parse(agreement));
        }
        return list;
    }

    public AgreementResponseDTO parse(Agreement agreement) {
        return new AgreementResponseDTO(agreement.getId(),
                agreement.getNumber(), agreement.getCreateDate(), agreement.getStatus(), agreement.getDistrict().getName(), agreement.getDistrict().getId(),agreement.getName());
    }

    public void calculateProduct(Agreement agreement) {
        BigDecimal zero = BigDecimal.valueOf(0);

        List<AgreementPrice> priceList = agreement.getAgreementPriceList();

        for (AgreementPrice agreementPrice : priceList) {
            agreementPrice.setWeight(zero);
            agreementPrice.setSuccessWeight(zero);
            agreementPrice.setResidue(zero);
            agreementPrice.setAgreement(null);
        }

        agreement.setAgreementPriceList(null);
        Agreement save = agreementRepository.save(agreement);
        agreementPriceRepository.deleteAll(priceList);

        List<AgreementPrice> agreementPriceList = new ArrayList<>();

        for (AgreementKindergarten agreementKindergarten : save.getAgreementKindergartenList()) {
            if (agreementKindergarten.getAgreementProductList() != null) {

                for (AgreementProduct agreementProduct : agreementKindergarten.getAgreementProductList()) {
                    boolean res = true;
                    for (AgreementPrice agreementPrice : agreementPriceList) {
                        if (agreementPrice.getProduct().equals(agreementProduct.getProduct())) {
                            agreementPrice.setWeight(agreementPrice.getWeight().add(agreementProduct.getWeight()));
                            agreementPrice.setResidue(agreementPrice.getResidue().add(agreementProduct.getResidue()));
                            agreementPrice.setSuccessWeight(agreementPrice.getSuccessWeight().add(agreementProduct.getSuccessWeight()));
                            res = false;
                            break;
                        }
                    }
                    if (res) {
                        agreementPriceList.add(new AgreementPrice(agreementProduct.getWeight(), agreementProduct.getSuccessWeight(), agreementProduct.getResidue(), zero, agreementProduct.getProduct(), agreement));
                    }
                }
            }
        }
        agreement.setAgreementPriceList(agreementPriceRepository.saveAll(agreementPriceList));

        agreementRepository.save(save);

    }

    public void addList(List<AgreementProductResponseDTO> list, AgreementProductResponseDTO dto) {
        boolean res = true;
        for (AgreementProductResponseDTO product : list) {
            if (product.getProductName().equals(dto.getProductName())) {
                product.setWeight(product.getWeight().add(dto.getWeight()));
                product.setResidue(product.getResidue().add(dto.getResidue()));
                product.setSuccessWeight(product.getSuccessWeight().add(dto.getSuccessWeight()));
                res = false;
                break;
            }
        }
        if (res) {
            list.add(dto);
        }
    }

    public AgreementResponseDTO sort(AgreementResponseDTO agreement) {
        BigDecimal zero = BigDecimal.valueOf(0);

        for (AgreementPriceResponseDTO product : agreement.getProductList()) {
            for (AgreementKindergartenResponseDTO dto : agreement.getKindergartenList()) {
                boolean res = true;
                List<AgreementProductResponseDTO> productList = dto.getProductList();
                for (AgreementProductResponseDTO productDTO : productList) {
                    if (productDTO.getProductName().equals(product.getProductName())) {
                        res = false;
                        break;
                    }
                }
                if (res) {
                    productList.add(new AgreementProductResponseDTO(zero, zero, zero, product.getProductName()));
                }
                productList.sort(Comparator.comparing(AgreementProductResponseDTO::getProductName));
                dto.setProductList(productList);
            }
        }


        for (AgreementKindergartenResponseDTO dto : agreement.getKindergartenList()) {
            dto.getProductList().sort(Comparator.comparing(AgreementProductResponseDTO::getProductName));
        }
        agreement.getProductList().sort(Comparator.comparing(AgreementPriceResponseDTO::getProductName));




        return agreement;
    }

    public StateMessage confirm(Integer id, ResponseUser responseUser, boolean confirm) {
        Users users = checkUser(responseUser);
        if (users == null) {
            return userNodFound();
        }

        Optional<Agreement> optional = agreementRepository.findById(id);
        if (optional.isEmpty()) {
            return noInformation();
        }

        Agreement agreement = optional.get();

        if (confirm) {
            agreement.setStatus(Status.SUCCESS.getName());
        } else {
            agreement.setStatus(Status.NEW.getName());
        }
        agreementRepository.save(agreement);
        return new StateMessage("Muvaffaqtiyatli " + (confirm ? "tasdiqlandi." : "o`zgartirish uchun yuborildi"), true);
    }

    public StateMessage deleteKin(List<Integer> list, Integer id) {

        Agreement agreement = agreementRepository.findById(id).get();
        List<AgreementKindergarten> agreementKindergartenList = agreement.getAgreementKindergartenList();
        List<AgreementKindergarten> kindergartenList = new ArrayList<>();

        if (agreement.getStatus().equals(Status.TOLIQ_YETKAZIB_BERILDI.getName()))
            return new StateMessage("O`chirib bo`lmaydi. Maxsulotlar to`liq yetkazib berilgan.", false);


        for (Integer integer : list) {
            Optional<AgreementKindergarten> optional = agreementKindergartenRepository.findById(integer);
            if (optional.isPresent()) {
                AgreementKindergarten agreementKindergarten = optional.get();
                agreementKindergartenList.remove(agreementKindergarten);
                agreementKindergarten.setAgreement(null);
                kindergartenList.add(agreementKindergarten);
            }
        }
        agreement.setAgreementKindergartenList(agreementKindergartenList);
        calculateProduct(agreementRepository.save(agreement));
        List<AgreementKindergarten> saveList = agreementKindergartenRepository.saveAll(kindergartenList);
        agreementKindergartenRepository.deleteAll(saveList);
        return new StateMessage("Muvaffaqiyatli o`chirildi.", true);
    }

    public StateMessage addProduct(ResponseUser responseUser, Integer agreementId, List<AgreementProductDTO> list) {

        Optional<Agreement> optional = agreementRepository.findById(agreementId);

        if (optional.isEmpty())
            return noInformation();
        Agreement agreement = optional.get();

        if (agreement.getStatus().equals(Status.NEW.getName())) {


            agreement.setUpdateBy(checkUser(responseUser));
            Agreement save = agreementRepository.save(agreement);


            List<AgreementKindergarten> kindergartenList = new ArrayList<>();

            List<AgreementProduct> delete = agreementProductRepository.findAllByAgreementKindergarten_Agreement(agreement);
            for (AgreementProduct agreementProduct : delete) {
                agreementProduct.setAgreementKindergarten(null);
            }

            agreementProductRepository.deleteAll(agreementProductRepository.saveAll(delete));

            for (AgreementProductDTO dto : list) {

                Product product = productRepository.findById(dto.getProductId()).get();

                for (AgreementKindergartenDTO kindergartenDTO : dto.getKindergartenList()) {
                    AgreementKindergarten agreementKindergarten = agreementKindergartenRepository.findById(kindergartenDTO.getId()).get();

                    Optional<AgreementProduct> optionalProduct = agreementProductRepository.findByAgreementKindergartenAndProduct(agreementKindergarten, product);

                    if (optionalProduct.isEmpty()) {
                        List<AgreementProduct> agreementProductList = agreementKindergarten.getAgreementProductList();
                        agreementProductList.add(new AgreementProduct(BigDecimal.valueOf(kindergartenDTO.getWeight()), product, agreementKindergarten, BigDecimal.valueOf(0), BigDecimal.valueOf(kindergartenDTO.getWeight())));
                    } else {
                        AgreementProduct agreementProduct = optionalProduct.get();
                        agreementProduct.setWeight(BigDecimal.valueOf(kindergartenDTO.getWeight()));
                        agreementProductRepository.save(agreementProduct);
                    }
                    kindergartenList.add(agreementKindergarten);
                }
            }
            agreementKindergartenRepository.saveAll(kindergartenList);
            calculateProduct(save);

            return new StateMessage("Muvaffaqiyatli qo`shildi.", true);
        }
        return new StateMessage("O`zgartirishni imkoni yo`q", false);

    }

    public List<KindergartenResponseDTO> getKindergartenByAgreement(Integer id) {
        Optional<Agreement> optional = agreementRepository.findById(id);

        List<KindergartenResponseDTO> list = new ArrayList<>();

        if (optional.isEmpty())
            return new ArrayList<>();


        Agreement agreement = optional.get();
        for (Kindergarten kindergarten : kindergartenRepository.findAllByTypeAndAddress_District_Id(KindergartenType.TAMINOT.getName(), agreement.getDistrict().getId())) {
            boolean res = true;
            if (agreement.getAgreementKindergartenList() != null) {
                for (AgreementKindergarten agreementKindergarten : agreement.getAgreementKindergartenList()) {
                    if (agreementKindergarten.getKindergarten().equals(kindergarten)) {
                        res = false;
                        break;
                    }
                }
            }
            if (res) {
                list.add(kindergartenService.parse(kindergarten));
            }
        }

        return list;
    }

    public ResponseEntity<?> getOneByProduct(Integer id) {

        Optional<Agreement> optional = agreementRepository.findById(id);
        if (optional.isEmpty())
            return ResponseEntity.status(400).body(noInformation());

        Agreement agreement = optional.get();
//        calculateProduct(agreement);

        AgreementDTOByProduct dto = new AgreementDTOByProduct(agreement.getId(), agreement.getNumber(), agreement.getDate(), agreement.getStatus(), agreement.getDistrict().getName());

        List<AgreementProductDTO> productList = new ArrayList<>();
        for (AgreementPrice agreementPrice : agreement.getAgreementPriceList()) {
            productList.add(new AgreementProductDTO(agreementPrice.getProduct().getId(), agreementPrice.getProduct().getName(), new ArrayList<>()));
        }


        for (AgreementKindergarten agreementKindergarten : agreement.getAgreementKindergartenList()) {
            for (AgreementProductDTO agreementProductDTO : productList) {
                Optional<AgreementProduct> productOptional = agreementProductRepository.findByAgreementKindergartenAndProduct_Id(agreementKindergarten, agreementProductDTO.getProductId());
                if (productOptional.isPresent()) {
                    AgreementProduct agreementProduct = productOptional.get();
                    List<AgreementKindergartenDTO> kindergartenList = agreementProductDTO.getKindergartenList();
                    kindergartenList.add(new AgreementKindergartenDTO(agreementKindergarten.getId(), agreementKindergarten.getKindergarten().getName(), Double.parseDouble(agreementProduct.getWeight().toString())));
                    agreementProductDTO.setKindergartenList(kindergartenList);
                } else {
                    List<AgreementKindergartenDTO> kindergartenList = agreementProductDTO.getKindergartenList();
                    kindergartenList.add(new AgreementKindergartenDTO(agreementKindergarten.getId(), agreementKindergarten.getKindergarten().getName(), 0.0));
                    agreementProductDTO.setKindergartenList(kindergartenList);
                }
            }
        }

        for (AgreementProductDTO agreementProductDTO : productList) {
            List<AgreementKindergartenDTO> kindergartenList = agreementProductDTO.getKindergartenList();
            kindergartenList.sort(Comparator.comparing(AgreementKindergartenDTO::getKindergartenName));
            agreementProductDTO.setKindergartenList(kindergartenList);
        }

        productList.sort(Comparator.comparing(AgreementProductDTO::getProductName));

        dto.setKinProductList(productList);

        productList.add(new AgreementProductDTO(null, null, new ArrayList<>()));

        for (AgreementKindergarten agreementKindergarten : agreement.getAgreementKindergartenList()) {
            for (AgreementProductDTO agreementProductDTO : productList) {
                if (agreementProductDTO.getProductId() == null && agreementProductDTO.getProductName() == null) {
                    List<AgreementKindergartenDTO> kindergartenList = agreementProductDTO.getKindergartenList();
                    kindergartenList.add(new AgreementKindergartenDTO(agreementKindergarten.getId(), agreementKindergarten.getKindergarten().getName(), 0.0));
                    agreementProductDTO.setKindergartenList(kindergartenList);
                }
            }
        }

        return ResponseEntity.status(200).body(dto);
    }


    public Users checkUser(ResponseUser responseUser) {
        Optional<Users> optionalUsers = usersRepository.findByUserName(responseUser.getUsername());
        return optionalUsers.isEmpty() ? null : optionalUsers.get();
    }

    public StateMessage userNodFound() {
        return new StateMessage("User ma`lumotlari topilmadi.", false);
    }

    public StateMessage noInformation() {
        return new StateMessage("Ma`lumot mavjud emas.", false);
    }

    public StateMessage send(Integer id) {

        Optional<Agreement> optional = agreementRepository.findById(id);
        if (optional.isEmpty())
            return noInformation();

        Agreement agreement = optional.get();

        if (agreement.getStatus().equals(Status.NEW.getName())) {
            agreement.setStatus(Status.SEND.getName());
            agreementRepository.save(agreement);
            return new StateMessage("Shartnoma tasdiqlash uchun muvaffaqiyatli yuborildi", true);
        }
        return new StateMessage("Shartnoma tasdiqlash uchun yuborib bo`lingan", false);
    }
}
