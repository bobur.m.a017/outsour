package com.smart_solution.outsource.meal.mealCategory;


import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.message.StateMessage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public record MealCategoryService(
        MealCategoryRepository mealCategoryRepository,
        MealRepository mealRepository
) {


    public StateMessage add(MealCategoryDTO mealCategoryDTO) {

        boolean res = mealCategoryRepository.existsByName(mealCategoryDTO.getName());

        if (!res) {
            mealCategoryRepository.save(new MealCategory(mealCategoryDTO.getName()));
            return new StateMessage("Muvaffaqiyatli qo`shildi", true);

        } else {
            return new StateMessage("Bunday nomli categoriya mavjud", false);
        }
    }

    public StateMessage edit(MealCategoryDTO mealCategoryDTO, Integer id) {

        MealCategory mealCategory = mealCategoryRepository.findById(id).get();
        boolean res = false;

        if (!mealCategory.getName().equals(mealCategoryDTO.getName())) {
            res = mealCategoryRepository.existsByName(mealCategoryDTO.getName());
        }

        if (!res) {
            mealCategory.setName(mealCategoryDTO.getName());
            mealCategoryRepository.save(mealCategory);
            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

        } else {
            return new StateMessage("Bunday nomli categoriya mavjud", false);
        }
    }

    public List<MealCategoryDTO> get() {

        List<MealCategoryDTO> list = new ArrayList<>();

        for (MealCategory mealCategory : mealCategoryRepository.findAll()) {

            list.add(new MealCategoryDTO(mealCategory.getId(), mealCategory.getName()));

        }
        return list;
    }

    public StateMessage delete(Integer id) {
        boolean res = mealRepository.existsByMealCategory_Id(id);

        if (!res){
            mealCategoryRepository.deleteById(id);
        }
        return res ? new StateMessage("O`chirilmadi ushbu kategoriyaga ovqat biriktirilgan",false) : new StateMessage("O`chirildi, qayta tiklab bo`lmaydi",true);
    }
}
