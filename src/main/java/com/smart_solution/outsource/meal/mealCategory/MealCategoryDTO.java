package com.smart_solution.outsource.meal.mealCategory;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class MealCategoryDTO {

    private Integer id;
    private String name;

    public MealCategoryDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public MealCategoryDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
