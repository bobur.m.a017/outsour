package com.smart_solution.outsource.meal.mealCategory;

import com.smart_solution.outsource.meal.mealCategory.MealCategory;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MealCategoryRepository extends JpaRepository<MealCategory, Integer> {

    boolean existsByName(String name);

}
