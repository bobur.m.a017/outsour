package com.smart_solution.outsource.meal;

import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.ingredient.IngredientDTO;
import com.smart_solution.outsource.productMeal.ProductMealDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.List;


public class MealDTO {

    private String name;
    private Double weight;
    private Integer mealCategoryId;
    private List<ProductMealDTO> productMeals;
    private String comment;
    private Integer attachmentId;


    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public MealDTO() {
    }



    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getMealCategoryId() {
        return mealCategoryId;
    }

    public void setMealCategoryId(Integer mealCategoryId) {
        this.mealCategoryId = mealCategoryId;
    }

    public List<ProductMealDTO> getProductMeals() {
        return productMeals;
    }

    public void setProductMeals(List<ProductMealDTO> productMeals) {
        this.productMeals = productMeals;
    }
}
