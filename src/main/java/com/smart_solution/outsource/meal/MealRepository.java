package com.smart_solution.outsource.meal;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface MealRepository extends JpaRepository<Meal, Integer> {

    boolean existsByName(String name);

    Optional<Meal> findByName(String name);

    boolean existsByMealCategory_Id(Integer id);
}
