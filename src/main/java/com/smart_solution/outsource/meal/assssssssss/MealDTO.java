//package com.smart_solution.outsource.meal.assssssssss;
//
//import java.util.List;
//
//public class MealDTO {
//
//    private String name;
//    private Double weight;
//    private String comment;
//    private Integer mealCategoryId;
//    private List<ProductMealDTO> productMealList;
//
//    public MealDTO(String name, Double weight, String comment, Integer mealCategoryId, List<ProductMealDTO> productMealList) {
//        this.name = name;
//        this.weight = weight;
//        this.comment = comment;
//        this.mealCategoryId = mealCategoryId;
//        this.productMealList = productMealList;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Double getWeight() {
//        return weight;
//    }
//
//    public void setWeight(Double weight) {
//        this.weight = weight;
//    }
//
//    public String getComment() {
//        return comment;
//    }
//
//    public void setComment(String comment) {
//        this.comment = comment;
//    }
//
//    public Integer getMealCategory() {
//        return mealCategoryId;
//    }
//
//    public void setMealCategory(Integer mealCategoryId) {
//        this.mealCategoryId = mealCategoryId;
//    }
//
//    public List<ProductMealDTO> getProductMealList() {
//        return productMealList;
//    }
//
//    public void setProductMealList(List<ProductMealDTO> productMealList) {
//        this.productMealList = productMealList;
//    }
//}
