package com.smart_solution.outsource.meal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.smart_solution.outsource.attachment.Attachment;
import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.meal.mealCategory.MealCategory;
import com.smart_solution.outsource.productMeal.ProductMeal;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Meal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(precision = 19,scale = 6)
    private BigDecimal weight;
    private Boolean delete;
    private String comment;
    private Integer attachmentId;

    @ManyToOne
    private MealCategory mealCategory;

    @OneToMany
    private List<ProductMeal> productMeals;

    @OneToOne
    private Ingredient ingredient;

//(fetch = FetchType.LAZY)
//    @OneToOne
//    private Attachment attachment;


    public Meal(String name, BigDecimal weight, MealCategory mealCategory, List<ProductMeal> productMeals, Ingredient ingredient) {
        this.name = name;
        this.weight = weight;
        this.mealCategory = mealCategory;
        this.productMeals = productMeals;
        this.ingredient = ingredient;
    }

    public Meal() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    public MealCategory getMealCategory() {
        return mealCategory;
    }

    public void setMealCategory(MealCategory mealCategory) {
        this.mealCategory = mealCategory;
    }

    public List<ProductMeal> getProductMeals() {
        return productMeals;
    }

    public void setProductMeals(List<ProductMeal> productMeals) {
        this.productMeals = productMeals;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }
    //    public Attachment getAttachment() {
//        return attachment;
//    }
//
//    public void setAttachment(Attachment attachment) {
//        this.attachment = attachment;
//    }
}
