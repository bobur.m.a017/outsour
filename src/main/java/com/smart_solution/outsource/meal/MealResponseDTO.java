package com.smart_solution.outsource.meal;

import com.smart_solution.outsource.attachment.Attachment;
import com.smart_solution.outsource.ingredient.IngredientDTO;
import com.smart_solution.outsource.perDay.PerDayResponseDTO;
import com.smart_solution.outsource.productMeal.ProductMealDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

public class MealResponseDTO {

    private Integer id;
    private String name;
    private BigDecimal weight;
    private Integer mealCategoryId;
    private String mealCategoryName;
    private List<ProductMealDTO> productMeals;
    private IngredientDTO ingredientDTO;
    private Integer attachmentId;
    private String comment;
    private PerDayResponseDTO perDayDTO;

    public MealResponseDTO() {
    }

    public PerDayResponseDTO getPerDayDTO() {
        return perDayDTO;
    }

    public void setPerDayDTO(PerDayResponseDTO perDayDTO) {
        this.perDayDTO = perDayDTO;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getMealCategoryId() {
        return mealCategoryId;
    }

    public void setMealCategoryId(Integer mealCategoryId) {
        this.mealCategoryId = mealCategoryId;
    }

    public String getMealCategoryName() {
        return mealCategoryName;
    }

    public void setMealCategoryName(String mealCategoryName) {
        this.mealCategoryName = mealCategoryName;
    }

    public List<ProductMealDTO> getProductMeals() {
        return productMeals;
    }

    public void setProductMeals(List<ProductMealDTO> productMeals) {
        this.productMeals = productMeals;
    }

    public IngredientDTO getIngredientDTO() {
        return ingredientDTO;
    }

    public void setIngredientDTO(IngredientDTO ingredientDTO) {
        this.ingredientDTO = ingredientDTO;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }
}
