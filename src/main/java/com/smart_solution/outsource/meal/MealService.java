package com.smart_solution.outsource.meal;

import com.smart_solution.outsource.attachment.AttachmentService;
import com.smart_solution.outsource.ingredient.Ingredient;
import com.smart_solution.outsource.ingredient.IngredientService;
import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSaveRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveService;
import com.smart_solution.outsource.meal.mealCategory.MealCategoryRepository;
import com.smart_solution.outsource.message.StateMessage;
import com.smart_solution.outsource.numberOfChildren.NumberOfChildren;
import com.smart_solution.outsource.perDay.PerDay;
import com.smart_solution.outsource.perDay.PerDayService;
import com.smart_solution.outsource.productMeal.ProductMeal;
import com.smart_solution.outsource.productMeal.ProductMealDTO;
import com.smart_solution.outsource.productMeal.ProductMealService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public record MealService(
        MealRepository mealRepository,
        MealCategoryRepository mealCategoryRepository,
        ProductMealService productMealService,
        IngredientService ingredientService,
        AttachmentService attachmentService,
        MenuSaveRepository menuSaveRepository,
        MealAgeStandardSaveRepository mealAgeStandardSaveRepository,
        PerDayService perDayService
) {

    public StateMessage add(MealDTO mealDTO, MultipartFile file) throws IOException {

        Optional<Meal> byName = mealRepository.findByName(mealDTO.getName());
        Meal meal;

        if (byName.isPresent()) {
            if (byName.get().getDelete()) {
                meal = byName.get();
            } else {
                return new StateMessage("Bunday nomli ovqat mavjud", false);
            }
        } else {
            meal = new Meal();
        }

        meal.setName(mealDTO.getName());
        meal.setDelete(false);
        meal.setWeight(BigDecimal.valueOf(mealDTO.getWeight()).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP));
        meal.setMealCategory(mealCategoryRepository.findById(mealDTO.getMealCategoryId()).get());
        meal.setComment(mealDTO.getComment());

        List<ProductMeal> productMealList = new ArrayList<>();
        for (ProductMealDTO productMealDTO : mealDTO.getProductMeals()) {
            ProductMeal productMeal = productMealService.add(productMealDTO);
            productMealList.add(productMeal);
        }

        meal.setProductMeals(productMealList);
        meal.setIngredient(calculateIngredient(meal.getProductMeals()));
        meal.setAttachmentId(attachmentService.add(file).getId());
//        meal.setAttachmentId(attachmentService.add(mealDTO.getBytes()).getId());

        mealRepository.save(meal);
        return new StateMessage("Muvaffaqiyatli qo`shildi", true);

    }

    public Ingredient calculateIngredient(List<ProductMeal> productMealList) {

        BigDecimal protein = BigDecimal.valueOf(0);
        BigDecimal kcal = BigDecimal.valueOf(0);
        BigDecimal oil = BigDecimal.valueOf(0);
        BigDecimal carbohydrates = BigDecimal.valueOf(0);

        for (ProductMeal productMeal : productMealList) {

            protein = protein.add(productMeal.getIngredient().getProtein());
            kcal = kcal.add(productMeal.getIngredient().getKcal());
            oil = oil.add(productMeal.getIngredient().getOil());
            carbohydrates = carbohydrates.add(productMeal.getIngredient().getCarbohydrates());

        }

        return ingredientService.add(protein, kcal, oil, carbohydrates);

    }

    public StateMessage edit(MealDTO mealDTO, Integer id, MultipartFile file) throws IOException {

        boolean res = true;
        Meal meal = mealRepository.findById(id).get();

        if (!mealDTO.getName().equals(meal.getName())) {
            res = !mealRepository.existsByName(mealDTO.getName());
        }

        if (res) {
            meal.setName(mealDTO.getName());
            meal.setWeight(BigDecimal.valueOf(mealDTO.getWeight() / 1000));
            meal.setMealCategory(mealCategoryRepository.findById(mealDTO.getMealCategoryId()).get());
            meal.setComment(mealDTO.getComment());

            List<ProductMeal> productMealList = new ArrayList<>();

            for (ProductMealDTO productMealDTO : mealDTO.getProductMeals()) {

                if (productMealDTO.getId() != null) {
                    productMealList.add(productMealService.edit(productMealDTO));
                } else {
                    productMealList.add(productMealService.add(productMealDTO));
                }
            }

            meal.setProductMeals(productMealList);
            meal.setIngredient(calculateIngredient(meal.getProductMeals()));

            if (file != null) {
                if (meal.getAttachmentId() == null) {
                    meal.setAttachmentId(attachmentService.add(file).getId());
                } else {
                    meal.setAttachmentId(attachmentService.edit(file, meal.getAttachmentId()).getId());
                }
            }

            mealRepository.save(meal);
            return new StateMessage("Muvaffaqiyatli o`zgartirildi", true);

        } else {
            return new StateMessage("Bunday nomli ovqat mavjud", false);
        }
    }

    public List<MealResponseDTO> get() {

        List<MealResponseDTO> list = new ArrayList<>();

        for (Meal meal : mealRepository.findAll()) {

            if (!meal.getDelete()) {

                MealResponseDTO mealResponseDTO = new MealResponseDTO();
                mealResponseDTO.setId(meal.getId());
                mealResponseDTO.setWeight(meal.getWeight().multiply(BigDecimal.valueOf(1000)));
                mealResponseDTO.setName(meal.getName());
                mealResponseDTO.setMealCategoryId(meal.getMealCategory().getId());
                mealResponseDTO.setMealCategoryName(meal.getMealCategory().getName());
                mealResponseDTO.setIngredientDTO(ingredientService.parse(meal.getIngredient()));
                mealResponseDTO.setAttachmentId(meal.getAttachmentId());
                mealResponseDTO.setComment(meal.getComment());


                List<ProductMealDTO> productMealDTOList = new ArrayList<>();

                for (ProductMeal productMeal : meal.getProductMeals()) {

                    productMealDTOList.add(productMealService.parse(productMeal));

                }
                mealResponseDTO.setProductMeals(productMealDTOList);

                list.add(mealResponseDTO);
            }
        }
        list.sort(Comparator.comparing(MealResponseDTO::getName));
        return list;
    }

    public StateMessage delete(Integer id) {

        Meal meal = mealRepository.findById(id).get();
        meal.setDelete(true);
        mealRepository.save(meal);
        return new StateMessage("O`chirildi qayta tiklash uchun admin bilan bog`laning", true);
    }

    public MealResponseDTO getMeal(Integer menuId, Integer mealAgeStandardId) {

        MealAgeStandardSave mealAgeStandardSave = mealAgeStandardSaveRepository.findById(mealAgeStandardId).get();

        MenuSave menuSave = menuSaveRepository.findById(menuId).get();

        PerDay numberFact = menuSave.getNumberFact();

        if (numberFact == null) {
            return null;
        }

        Meal meal = mealRepository.findById(mealAgeStandardSave.getMealId()).get();


        MealResponseDTO mealResponseDTO = new MealResponseDTO();
        mealResponseDTO.setId(meal.getId());
        mealResponseDTO.setName(meal.getName());
        mealResponseDTO.setMealCategoryId(meal.getMealCategory().getId());
        mealResponseDTO.setMealCategoryName(meal.getMealCategory().getName());
        mealResponseDTO.setIngredientDTO(ingredientService.parse(meal.getIngredient()));
        mealResponseDTO.setAttachmentId(meal.getAttachmentId());
        mealResponseDTO.setComment(meal.getComment());

        mealResponseDTO.setPerDayDTO(perDayService.parse(numberFact));

        List<ProductMealDTO> productMealDTOList = new ArrayList<>();

        for (ProductMeal productMeal : meal.getProductMeals()) {
            ProductMealDTO parse = productMealService.parse(productMeal);

            BigDecimal weight = getWeight(mealAgeStandardSave, meal, productMeal.getWeight(), numberFact);
            BigDecimal withoutExit = getWeight(mealAgeStandardSave, meal, productMeal.getWithoutExit(), numberFact);
            parse.setWeight(Double.valueOf(weight.toString()));
            parse.setWithoutExit(Double.valueOf(withoutExit.toString()));

            productMealDTOList.add(parse);
        }
        mealResponseDTO.setProductMeals(productMealDTOList);
        mealResponseDTO.setWeight(getMealWeight(mealAgeStandardSave, numberFact));

        return mealResponseDTO;
    }

    public BigDecimal getWeight(MealAgeStandardSave mealAgeStandardSave, Meal meal, BigDecimal productWeight, PerDay perDay) {

        BigDecimal weight = BigDecimal.valueOf(0);

        for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
            for (NumberOfChildren numberOfChild : perDay.getNumberOfChildren()) {
                if (numberOfChild.getAgeGroup().getId().equals(ageStandardSave.getAgeGroupId())) {
                    weight = weight.add(ageStandardSave.getWeight().multiply(BigDecimal.valueOf(numberOfChild.getNumberOfKids())));
                }
            }
        }
        productWeight = productWeight.divide(meal.getWeight(), 7, RoundingMode.HALF_UP);

        return (productWeight.multiply(weight)).divide(BigDecimal.valueOf(1), 2, RoundingMode.HALF_UP);
    }

    public BigDecimal getMealWeight(MealAgeStandardSave mealAgeStandardSave, PerDay perDay) {

        BigDecimal weight = BigDecimal.valueOf(0);

        for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {
            for (NumberOfChildren numberOfChild : perDay.getNumberOfChildren()) {
                if (numberOfChild.getAgeGroup().getId().equals(ageStandardSave.getAgeGroupId())) {
                    weight = weight.add(ageStandardSave.getWeight().multiply(BigDecimal.valueOf(numberOfChild.getNumberOfKids())));
                }
            }
        }

        return weight;
    }


    public List<MealDTO> getAll() {

        List<MealDTO> list = new ArrayList<>();

        for (Meal meal : mealRepository.findAll()) {
            MealDTO dto = new MealDTO();
            dto.setWeight(Double.parseDouble(meal.getWeight().toString()));
            dto.setComment(meal.getComment());
            dto.setName(meal.getName());
            dto.setMealCategoryId(meal.getMealCategory().getId());
            dto.setAttachmentId(meal.getAttachmentId());

            List<ProductMealDTO> dtoList = new ArrayList<>();

            for (ProductMeal productMeal : meal.getProductMeals()) {
                ProductMealDTO mealDTO = new ProductMealDTO();
                mealDTO.setWithoutExit(Double.parseDouble(productMeal.getWithoutExit().toString()));
                mealDTO.setProductId(productMeal.getProduct().getId());
                mealDTO.setWeight(Double.parseDouble(productMeal.getWeight().toString()));
                mealDTO.setName(productMeal.getProduct().getName());
                dtoList.add(mealDTO);
            }
            dto.setProductMeals(dtoList);
            list.add(dto);
        }
        return list;
    }


//    public List<com.smart_solution.outsource.meal.assssssssss.MealDTO> getAllMeal() {
//        List<com.smart_solution.outsource.meal.assssssssss.MealDTO> list = new ArrayList<>();
//
//
//        for (Meal meal : mealRepository.findAll()) {
//
//            if (!meal.getDelete()) {
//                List<com.smart_solution.outsource.meal.assssssssss.ProductMealDTO> productMealDTOList = new ArrayList<>();
//                for (ProductMeal productMeal : meal.getProductMeals()) {
//                    productMealDTOList.add(new com.smart_solution.outsource.meal.assssssssss.ProductMealDTO(productMeal.getWeight().doubleValue(), productMeal.getProduct().getName()));
//                }
//
//                list.add(new com.smart_solution.outsource.meal.assssssssss.MealDTO(meal.getName(),
//                        meal.getWeight().doubleValue(), meal.getComment(), meal.getMealCategory().getId(), productMealDTOList));
//            }
//        }
//        return list;
//    }
}
