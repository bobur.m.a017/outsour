package com.smart_solution.outsource.test;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
public class TestReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    private UUID consumptionId;
    private String kindergartenName;
    private Timestamp date;

    @OneToMany
    private List<ProductReport> productReportList;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getConsumptionId() {
        return consumptionId;
    }

    public void setConsumptionId(UUID consumptionId) {
        this.consumptionId = consumptionId;
    }

    public List<ProductReport> getProductReportList() {
        return productReportList;
    }

    public void setProductReportList(List<ProductReport> productReportList) {
        this.productReportList = productReportList;
    }

    public String getKindergartenName() {
        return kindergartenName;
    }

    public void setKindergartenName(String kindergartenName) {
        this.kindergartenName = kindergartenName;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
