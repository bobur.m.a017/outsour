package com.smart_solution.outsource.test;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class ProductReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    private Integer productId;
    private String productName;
    @Column(precision = 19, scale = 6)
    private BigDecimal ombordaMavjud;
    @Column(precision = 19, scale = 6)
    private BigDecimal zaxiradaMavjud;
    @Column(precision = 19, scale = 6)
    private BigDecimal xisobotRasxod;
    @Column(precision = 19, scale = 6)
    private BigDecimal ombordaMavjudXisobotdanKeyin;
    @Column(precision = 19, scale = 6)
    private BigDecimal zaxiradaMavjudXisobotdanKeyin;

    public ProductReport() {
    }

    public ProductReport(Integer productId, String productName, BigDecimal ombordaMavjud) {
        this.productId = productId;
        this.productName = productName;
        this.ombordaMavjud = ombordaMavjud;
    }


    public ProductReport(Integer productId, String productName, BigDecimal ombordaMavjud, BigDecimal zaxiradaMavjud) {
        this.productId = productId;
        this.productName = productName;
        this.ombordaMavjud = ombordaMavjud;
        this.zaxiradaMavjud = zaxiradaMavjud;
    }

    public ProductReport(Integer productId, String productName, BigDecimal ombordaMavjud, BigDecimal zaxiradaMavjud, BigDecimal ombordaMavjudXisobotdanKeyin) {
        this.productId = productId;
        this.productName = productName;
        this.ombordaMavjud = ombordaMavjud;
        this.zaxiradaMavjud = zaxiradaMavjud;
        this.ombordaMavjudXisobotdanKeyin = ombordaMavjudXisobotdanKeyin;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public BigDecimal getOmbordaMavjud() {
        return ombordaMavjud;
    }

    public void setOmbordaMavjud(BigDecimal ombordaMavjud) {
        this.ombordaMavjud = ombordaMavjud;
    }

    public BigDecimal getZaxiradaMavjud() {
        return zaxiradaMavjud;
    }

    public void setZaxiradaMavjud(BigDecimal zaxiradaMavjud) {
        this.zaxiradaMavjud = zaxiradaMavjud;
    }

    public BigDecimal getXisobotRasxod() {
        return xisobotRasxod;
    }

    public void setXisobotRasxod(BigDecimal xisobotRasxod) {
        this.xisobotRasxod = xisobotRasxod;
    }

    public BigDecimal getOmbordaMavjudXisobotdanKeyin() {
        return ombordaMavjudXisobotdanKeyin;
    }

    public void setOmbordaMavjudXisobotdanKeyin(BigDecimal ombordaMavjudXisobotdanKeyin) {
        this.ombordaMavjudXisobotdanKeyin = ombordaMavjudXisobotdanKeyin;
    }

    public BigDecimal getZaxiradaMavjudXisobotdanKeyin() {
        return zaxiradaMavjudXisobotdanKeyin;
    }

    public void setZaxiradaMavjudXisobotdanKeyin(BigDecimal zaxiradaMavjudXisobotdanKeyin) {
        this.zaxiradaMavjudXisobotdanKeyin = zaxiradaMavjudXisobotdanKeyin;
    }
}
