package com.smart_solution.outsource.test;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductReportRepository extends JpaRepository<ProductReport,Integer> {
}
