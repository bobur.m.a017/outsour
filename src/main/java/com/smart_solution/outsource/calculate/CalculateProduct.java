package com.smart_solution.outsource.calculate;

import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.product.Product;

import java.math.BigDecimal;

public class CalculateProduct {


    private Product product;
    private String productName;
    private BigDecimal weight;
    private AgeGroup ageGroup;
    private String ageGroupName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }
}
