package com.smart_solution.outsource.calculate;

import com.smart_solution.outsource.dto.MenuDTO;
import com.smart_solution.outsource.kindergartenMenu.ageStandard.AgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealAgeStandard.MealAgeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.mealTimeStandard.MealTimeStandardSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSave;
import com.smart_solution.outsource.kindergartenMenu.menu.MenuSaveRepository;
import com.smart_solution.outsource.meal.Meal;
import com.smart_solution.outsource.meal.MealRepository;
import com.smart_solution.outsource.menu.ageGroup.AgeGroup;
import com.smart_solution.outsource.menu.ageGroup.AgeGroupRepository;
import com.smart_solution.outsource.multiMenu.MultiMenuService;
import com.smart_solution.outsource.product.Product;
import com.smart_solution.outsource.product.ProductRepository;
import com.smart_solution.outsource.productMeal.ProductMeal;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
@Service
public record CalculateService(
        MenuSaveRepository menuSaveRepository,
        MealRepository mealRepository,
        ProductRepository productRepository,
        AgeGroupRepository ageGroupRepository
) {


    public List<CalculateProduct> getwqwedrw(Integer menuId, Integer ageGroupId, Integer number){

        MenuSave menuSave = menuSaveRepository.findById(menuId).get();
        AgeGroup ageGroup = ageGroupRepository.findById(ageGroupId).get();
        return calculateProduct(menuSave,ageGroup,number);
    }

    public List<CalculateProduct> calculateProduct(MenuSave menuSave, AgeGroup ageGroup, Integer number) {

        List<CalculateProduct> list = new ArrayList<>();

        List<MenuDTO> oneMenu = getOneMenu(menuSave, ageGroup);


        for (MenuDTO menu : oneMenu) {
            Product product = productRepository.findByName(menu.getProductName()).get();
            addList(product, list, (menu.getWeightDto().multiply(BigDecimal.valueOf(number))).divide(BigDecimal.valueOf(1000),6,RoundingMode.HALF_UP), ageGroup);
        }
        return list;
    }

    public void addList(Product product, List<CalculateProduct> list, BigDecimal weight, AgeGroup ageGroup) {

        boolean res = true;

        for (CalculateProduct calculateProduct : list) {
            if (calculateProduct.getProductName().equals(product.getName())) {
                calculateProduct.setWeight(calculateProduct.getWeight().add(weight));
                res = false;
            }
        }

        if (res) {
            CalculateProduct calculateProduct = new CalculateProduct();
            calculateProduct.setWeight(weight);
            calculateProduct.setProductName(product.getName());
            calculateProduct.setAgeGroupName(ageGroup.getName());
            calculateProduct.setProduct(product);
            calculateProduct.setAgeGroup(ageGroup);
            list.add(calculateProduct);
        }
    }

    public List<MenuDTO> getOneMenu(MenuSave menuSave, AgeGroup ageGroup) {

        List<MenuDTO> list = new ArrayList<>();

        for (MealTimeStandardSave mealTimeStandardSave : menuSave.getMealTimeStandardSaves()) {
            for (MealAgeStandardSave mealAgeStandardSave : mealTimeStandardSave.getMealAgeStandardSaves()) {
                for (AgeStandardSave ageStandardSave : mealAgeStandardSave.getAgeStandardSaves()) {

                    if (ageStandardSave.getAgeGroupId().equals(ageGroup.getId())){

                        Meal meal = mealRepository.findById(mealAgeStandardSave.getMealId()).get();

                        for (ProductMeal productMeal : meal.getProductMeals()) {
                            BigDecimal weight = productMeal.getWeight();
                            BigDecimal divide = weight.divide(meal.getWeight(), 6, RoundingMode.HALF_UP);
                            BigDecimal multiply = divide.multiply(ageStandardSave.getWeight());

                            BigDecimal withoutExit = productMeal.getWithoutExit();
                            BigDecimal withoutExitDivide = withoutExit.divide(meal.getWeight(), 6, RoundingMode.HALF_UP);
                            withoutExit = withoutExitDivide.multiply(ageStandardSave.getWeight());

                            String productWeight = (multiply.divide(BigDecimal.valueOf(1), 6, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(1000)).toString();
                            BigDecimal decimal = (multiply.divide(BigDecimal.valueOf(1), 6, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(1000));
                            String productWithoutExit = (withoutExit.divide(BigDecimal.valueOf(1), 6, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(1000)).toString();
                            String ageWeight = (ageStandardSave.getWeight()).multiply(BigDecimal.valueOf(1000)).toString();

//                            String replaceWeight = productWeight.replace('.', ',');
//                            String replaceWithoutExit = productWithoutExit.replace('.', ',');
//                            String replaceAgeWeight = ageWeight.replace('.', ',');
//
//
//                            String name = ageStandard.getAgeGroup().getName();
//                            if(name.equals("3-4") || name.equals("4-7")){
//                                name = name + " yosh";
//                            }
                            MenuDTO menuDTO = new MenuDTO(
                                    menuSave.getName(),
                                    mealTimeStandardSave.getMealTimeName(),
                                    meal.getName(),
                                    ageStandardSave.getAgeGroupName(),
                                    productMeal.getProduct().getName(),
                                    productWeight,
                                    ageWeight,
                                    productWithoutExit);
                            menuDTO.setWeightDto(decimal);
                            list.add(menuDTO);
                        }

                    }
                }
            }
        }
        return list;
    }
}
